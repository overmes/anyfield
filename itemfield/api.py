from tastypie.authentication import SessionAuthentication
from tastypie.resources import ModelResource
from tastypie.validation import CleanedDataFormValidation

from fieldtype.api import DenyAuthorization
from itemfield.forms import FieldCreateForm
from itemfield.models import ItemFieldModel, ItemModel


class ItemFieldAuthorization(DenyAuthorization):
    def read_detail(self, object_list, bundle):
        return True

    def create_detail(self, object_list, bundle):
        return True

    def delete_detail(self, object_list, bundle):
        return bundle.request.user == bundle.obj.user


class ItemFieldResource(ModelResource):
    def hydrate(self, bundle):
        bundle.data['user'] = bundle.request.user.id
        return bundle

    def dehydrate(self, bundle):
        bundle.data.update(bundle.obj.get_data(title=True))
        bundle.data['type'] = bundle.obj.type.id
        bundle.data['item'] = bundle.obj.item.id
        return bundle

    class Meta:
        queryset = ItemFieldModel.objects.all()
        resource_name = 'itemField'
        allowed_methods = ['post', 'delete']
        validation = CleanedDataFormValidation(form_class=FieldCreateForm)
        authentication = SessionAuthentication()
        authorization = ItemFieldAuthorization()
        always_return_data = True


class ItemResource(ModelResource):
    class Meta:
        queryset = ItemModel.objects.all()
        resource_name = 'item'
        allowed_methods = []
        authentication = SessionAuthentication()
        authorization = DenyAuthorization()