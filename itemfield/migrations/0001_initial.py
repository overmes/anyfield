# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import itemfield.mixin
import itemfield.models


class Migration(migrations.Migration):

    dependencies = [
        ('fieldtype', '__first__'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ItemFieldModel',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('raw_field', models.TextField(blank=True)),
                ('is_temp', models.BooleanField(default=True, db_index=True)),
            ],
            options={
            },
            bases=(itemfield.mixin.CheckBoundaryMixin, models.Model),
        ),
        migrations.CreateModel(
            name='ItemModel',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Settings',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('name', models.TextField(unique=True, validators=[itemfield.models.validate_setting])),
                ('value', models.TextField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='itemfieldmodel',
            name='item',
            field=models.ForeignKey(to='itemfield.ItemModel'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='itemfieldmodel',
            name='type',
            field=models.ForeignKey(to='fieldtype.FieldTypeModel'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='itemfieldmodel',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]
