from optparse import make_option
from django.conf import settings
import pymongo
from django.core.management.base import BaseCommand
from user.models import User
from itemfield.models import ItemModel, ItemFieldModel
from fieldtype.models import FieldModel, SelectValueModel


class Command(BaseCommand):
    help = 'Load mal data fields'
    option_list = BaseCommand.option_list + (
        make_option('--db',
                    action='store',
                    dest='db',
                    help='Source database name'),
    )

    copy_fields = {
        'title': (2, 'a'),
        'status': (11, 'c'),
        'english': (11, 'd'),
        'japanese': (11, 'e'),
        'image': (11, 'f'),
        'aired_from': (12, 'a'),
        'aired_to': (12, 'b'),
        'chapters': (12, 'c'),
        'volumes': (12, 'd'),
        'episodes': (12, 'e'),
        'length': (12, 'f'),
        'rating': (12, 'g'),
        'duration': (12, 'h'),
        'synopsis': (12, 'i'),
        'members_score': (15, 'a'),
        'favorites': (15, 'b'),
        'members': (15, 'c'),
        'scores': (15, 'd'),
        'avg': (16, 'a'),
        'weight': (16, 'b'),
        'completed_percent': (16, 'c'),
        'completed_sum': (16, 'd'),
        'drop_percent': (16, 'e'),
        'drop_sum': (16, 'f'),
        'hold_percent': (16, 'g'),
        'hold_sum': (16, 'h'),
        'plan_percent': (16, 'i'),
        'plan_sum': (16, 'j'),
        'watching_percent': (16, 'k'),
        'watching_sum': (16, 'l'),
    }

    multi_fields = {14, 17}

    def __init__(self):

        super().__init__()
        self.cache = {}
        self.select_dict = {}
        self.user = User.objects.get(username='admin')
        self.id_index_dict = {}

    def set_db(self, source_db_name):
        self.con = pymongo.MongoClient(host=settings.MONGODB.get('HOST'), port=settings.MONGODB.get('PORT'))
        self.source_db = self.con[source_db_name]
        self.source_items_col = self.source_db.anime

        self.dest_db = self.con.anyfield_mal
        self.dest_items_col = self.dest_db.items
        self.dest_text_col = self.dest_db.text
        self.dest_users_col = self.dest_db.users

    def field_model_cache(self, tid, fid):
        cache_value = self.cache.get((tid, fid))
        if cache_value:
            return cache_value
        else:
            return self.cache.setdefault((tid, fid), FieldModel.objects.get(parent=tid, fid=fid))

    def pre_load_all_selects(self):
        for select_value in SelectValueModel.objects.all():
            current_field = self.select_dict.setdefault((select_value.parent.parent_id, select_value.parent.fid), {})
            current_field.setdefault(select_value.name, select_value.number)

    def get_select_index(self, num, name, value):
        current_field = self.select_dict.setdefault((num, name), {})
        index = current_field.setdefault(value, len(current_field) + 1)
        return index

    def add_select_values(self):
        for path, data in self.select_dict.items():
            field_type = FieldModel.objects.get(parent=path[0], fid=path[1])
            select_values = [SelectValueModel(parent=field_type, number=v, name=n, user=self.user, is_temp=False) for
                             n, v in data.items() if not SelectValueModel.objects.filter(parent=field_type, name=n)]
            SelectValueModel.objects.bulk_create(select_values)

    def replace_relations(self, dest_items):
        for insert_item in dest_items:
            rel_field = insert_item.get('17', [])
            for rel in rel_field:
                if rel:
                    replace_index = self.id_index_dict.get(rel['b'])
                    if replace_index:
                        rel['b'] = replace_index.id
                    else:
                        del rel['a'], rel['b']
            insert_item['17'] = [r for r in rel_field if r] or []

    def create_indexes(self):
        self.dest_items_col.create_index([('i', pymongo.ASCENDING)])
        self.dest_text_col.create_index([('text', 'text'), ('f', pymongo.ASCENDING), ('s', pymongo.ASCENDING)])
        self.dest_text_col.create_index([('i', pymongo.ASCENDING), ('f', pymongo.ASCENDING), ('s', pymongo.ASCENDING)])
        self.dest_text_col.create_index([('id', pymongo.ASCENDING)])
        self.dest_users_col.create_index([('i', pymongo.ASCENDING)])
        self.dest_users_col.create_index([('u', pymongo.ASCENDING)])

    def update_or_create_field(self, type_id, item, raw_field):
        try:
            existed_field = ItemFieldModel.objects.get(user=self.user, type_id=type_id, item=item)
            existed_field.update(raw_field)
        except ItemFieldModel.DoesNotExist:
            ItemFieldModel.objects.create(user=self.user, type_id=type_id, item=item,
                                          raw_field=raw_field, is_temp=False)

    def update_or_create_multi_fields(self, type_id, item, fields):
        existed_fields = ItemFieldModel.objects.filter(user=self.user, type_id=type_id, item=item)

    def create_fields(self, dest_items):
        for types_data in dest_items:
            current_item = types_data.pop('i')
            for str_type_id, fields in types_data.items():
                type_id = int(str_type_id)
                if type_id in self.multi_fields:
                    self.update_or_create_multi_fields(type_id=type_id, item=current_item,
                                                       fields=fields)
                else:
                    self.update_or_create_field(type_id=type_id, item=current_item,
                                                raw_field=fields[0])

    def get_item(self, mal_id, type):
        item_id = ItemModel.find_id_by_mal_id(mal_id, type)
        return ItemModel.objects.get(id=item_id) if item_id else ItemModel.objects.create()

    def handle(self, *args, **options):
        self.set_db(options['db'])
        dest_items = []
        anime_url = 'http://myanimelist.net/anime/{}/'
        manga_url = 'http://myanimelist.net/manga/{}/'
        self.create_indexes()
        self.pre_load_all_selects()
        for source_item in self.source_items_col.find():
            dest_fields = {}
            dest_items.append(dest_fields)

            # 10 & 11 field
            field_11_first = dest_fields.setdefault('11', [{}])[0]
            mal_id_type = source_item.get('_id')
            mal_id = mal_id_type['i']
            field_11_first['a'] = mal_id
            if mal_id_type['t']:
                field_11_first['b'] = self.get_select_index(11, 'b', mal_id_type['t'])

            field_10_first = dest_fields.setdefault('10', [{}])[0]
            my_type = 1 if mal_id_type['t'] in ["TV", "Movie", "OVA", "Special", "ONA", "Music", ""] else 2
            field_10_first['a'] = my_type

            # i field
            item = self.get_item(mal_id, my_type)
            dest_fields['i'] = item

            self.id_index_dict[(field_11_first['a'], my_type)] = item

            # 19 a
            url = anime_url.format(mal_id) if my_type == 1 else manga_url.format(mal_id)
            dest_fields['19'] = [{'a': url}]

            # 14 field
            field_14 = []
            source_genres = source_item.get('genres')
            for genre in source_genres:
                genre_index = self.get_select_index(14, 'a', genre)
                field_14.append({'a': genre_index})
            if field_14:
                dest_fields['14'] = field_14

            # 17 field
            field_17 = []
            source_related = source_item.get('related')
            for rel_type, rel_list in source_related.items():
                rel_index = self.get_select_index(17, 'a', rel_type)
                for rel_link in rel_list:
                    rel_type_index = 1 if rel_link['t'] == 'anime' else 2
                    field_17.append({'a': rel_index, 'b': (rel_link['i'], rel_type_index)})
            if field_17:
                dest_fields['17'] = field_17

            for fname, path in self.copy_fields.items():
                field_first = dest_fields.get(str(path[0]), [{}])[0]
                field_object = self.field_model_cache(path[0], path[1])
                value_type = field_object.type
                value = source_item.get(fname)
                if value_type == 'se':
                    if value and value != 'None':
                        value_index = self.get_select_index(path[0], path[1], value)
                        field_first[path[1]] = value_index
                else:
                    if value:
                        field_first[path[1]] = value

                if field_first:
                    dest_fields[str(path[0])] = [field_first]

        self.replace_relations(dest_items)
        self.add_select_values()
        self.create_fields(dest_items)
