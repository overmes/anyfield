import ujson

from django import forms
from django.core.exceptions import ValidationError

from fieldtype.forms import ParentCleanMixin, UserCleanMixin
from fieldtype.models import FieldModel
from helpers.helpers import is_item_type
from itemfield.models import ItemFieldModel


class StepValidator():
    def __init__(self, step):
        self.step = step

    def __call__(self, value):
        normalizer = 10000
        remainder = (value*normalizer) % (self.step*normalizer)
        if remainder:
            raise ValidationError(u'%s is not a valid step' % (remainder/normalizer))


def get_formfield_for_field(field):
    field_type = field.type
    res = None
    if field_type == FieldModel.TEXT:
        res = forms.CharField()
    elif field_type == FieldModel.URL:
        res = forms.URLField()
    elif field_type == FieldModel.SELECT:
        choices = field.get_choices()
        res = forms.TypedChoiceField(choices=choices, coerce=int)
    elif field_type == FieldModel.VALUE:
        range = field.get_last_range_or_default()
        res = forms.FloatField(min_value=range.min, max_value=range.max, validators=[StepValidator(range.step)])
    elif field_type == FieldModel.IMAGE:
        res = forms.URLField()
    elif field_type == FieldModel.DATE:
        res = forms.IntegerField()
    elif field_type == FieldModel.LINK:
        res = forms.IntegerField()
    elif field_type == FieldModel.ID:
        res = forms.IntegerField()
    return res


class FieldCreateForm(forms.ModelForm, UserCleanMixin):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fids = []
        for fid, value in self.data.items():
            try:
                field = FieldModel.objects.get(fid=fid, parent=self.data.get('type'))
                self.fields[fid] = get_formfield_for_field(field)
                self.fids.append(fid)
            except FieldModel.DoesNotExist:
                pass

    def clean(self):
        cleaned_data = super().clean()
        self.check_fields_count()
        cleaned_data['raw_field'] = ujson.dumps({fid: cleaned_data.get(fid) for fid in self.fids})
        cleaned_data['is_temp'] = is_item_type(cleaned_data['type'].id)
        return cleaned_data

    def check_fields_count(self):
        if not len(set(self.fids) - {'pr'}):
            raise forms.ValidationError("You are not enter fields")

    class Meta:
        model = ItemFieldModel
        fields = ['user', 'item', 'raw_field', 'type', 'is_temp']