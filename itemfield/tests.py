from copy import deepcopy
import ujson

from django.contrib.auth.models import AnonymousUser
from django.core.management import call_command
from django.test import TestCase, override_settings
import pymongo
from django.conf import settings

from itemfield.models import ItemFieldModel, ItemModel
from user.models import User
from fieldtype.models import FieldTypeModel, SelectValueModel
from itemfield.forms import FieldCreateForm
from itemfield.test_data import fields_data
from itemfield.management.commands.load_mal_data import Command


@override_settings(MONGODB={'NAME': 'anyfield_test'})
class AddFieldTestCase(TestCase):
    def setUp(self):
        self.con = pymongo.MongoClient()
        self.con.drop_database('anyfield_test')
        self.db = self.con[settings.MONGODB['NAME']]
        self.all_fields = fields_data
        self.items_col = self.db.items
        self.users_col = self.db.users
        self.text_col = self.db.text

        self.user1 = User.objects.create_user(username='jacob', email='jacob@google.com', password='top_secret')
        self.user2 = User.objects.create_user(username='adsf', email='adfdsf@mail.ru', password='asdfasdf')
        self.user3 = User.objects.create_user(username='fdads', email='jfdsffd@…', password='fdsfdsaf')

        self.items_col.create_index([('i', pymongo.ASCENDING)])
        self.users_col.create_index([('i', pymongo.ASCENDING)])
        self.users_col.create_index([('u', pymongo.ASCENDING)])
        for one_field_data in fields_data:
            FieldTypeModel.load_fields_from_json(one_field_data, self.user1)
            for field in one_field_data['fields']:
                full_field_name = '{0}.{1}'.format(one_field_data['id'], field['fid'])
                if type == 'item':
                    self.items_col.create_index([(full_field_name, pymongo.ASCENDING)])
                else:
                    self.users_col.create_index([(full_field_name, pymongo.ASCENDING)])
        self.text_col.create_index(
            [('text', 'text'), ('f', pymongo.ASCENDING), ('s', pymongo.ASCENDING), ('u', pymongo.ASCENDING)])
        self.text_col.create_index([('id', pymongo.ASCENDING)])

    def test_get_field_from_mongodb(self):
        field, item, parent = {'e': 'hello'}, ItemModel.objects.create(), FieldTypeModel.objects.get(id=2)
        field_obj = ItemFieldModel.objects.create(raw_field=ujson.dumps(field), user=self.user1, item=item,
                                                  type=parent, is_temp=False)

        db_field = self.users_col.find_one({'u': self.user1.id,
                                            'i': item.id, str(parent.id): {'$elemMatch': {'id': field_obj.id}}},
                                           {str(parent.id) + '.$': 1, 'i': 1, 'u': 1, 'pr': 1})[str(parent.id)][0]
        db_field['e'] = field['e']
        self.assertDictEqual(db_field, field_obj.get_field_from_mongodb())

        field, item, parent = {'c': 'hello'}, ItemModel.objects.create(), FieldTypeModel.objects.get(id=1)
        field_obj = ItemFieldModel.objects.create(raw_field=ujson.dumps(field), user=self.user1, item=item,
                                                  type=parent, is_temp=False)

        db_field = self.items_col.find_one({'i': item.id, str(parent.id): {'$elemMatch': {'id': field_obj.id}}},
                                           {str(parent.id) + '.$': 1, 'i': 1, 'u': 1, 'pr': 1})[str(parent.id)][0]
        db_field['c'] = field['c']
        self.assertDictEqual(db_field, field_obj.get_field_from_mongodb())

    def test_user_text_field(self):
        field, item, parent = {'e': 'hello'}, ItemModel.objects.create(), FieldTypeModel.objects.get(id=2)
        field_obj = ItemFieldModel.objects.create(raw_field=ujson.dumps(field), user=self.user1, item=item,
                                                  type=parent, is_temp=False)

        db_field = self.text_col.find_one({'id': field_obj.id})
        self.assertEqual(db_field['text'], field['e'])

        field_obj.delete()
        db_field = self.text_col.find_one({'id': field_obj.id})
        self.assertFalse(db_field)

    def test_item_text_field(self):
        field, item, parent = {'c': 'hello'}, ItemModel.objects.create(), FieldTypeModel.objects.get(id=1)
        field_obj = ItemFieldModel.objects.create(raw_field=ujson.dumps(field), user=self.user1, item=item,
                                                  type=parent, is_temp=False)

        db_field = self.text_col.find_one({'id': field_obj.id})
        self.assertTrue(db_field['text'], field['c'])

        field_obj.vote(self.user1, -1)
        db_field = self.text_col.find_one({'id': field_obj.id})
        self.assertFalse(db_field)

    def test_user_require(self):
        field = {'a': 1}
        item = 1
        type = 1
        field.update({'type': type, 'item': item, 'user': AnonymousUser().id})

        field_form = FieldCreateForm(field)
        self.assertFalse(field_form.is_valid())

    def test_create_delete_user_field(self):
        field, item, parent = {'a': 1}, ItemModel.objects.create(), FieldTypeModel.objects.get(id=2)
        field_obj = ItemFieldModel.objects.create(raw_field=ujson.dumps(field), user=self.user1, item=item,
                                                  type=parent, is_temp=False)

        db_field = self.users_col.find_one({'u': self.user1.id,
                                            'i': item.id, str(parent.id): {'$elemMatch': {'id': field_obj.id}}},
                                           {str(parent.id) + '.$': 1, 'i': 1, 'u': 1, 'pr': 1})
        self.assertTrue(db_field)
        db_field.pop('_id')
        self.assertDictEqual(db_field,
                             {'i': 1, 'u': 1, 'pr': 0, str(parent.id): [{'a': 1, 'pr': 0, 'id': field_obj.id}]})

        field_obj.delete()
        db_field = self.users_col.find_one({'u': self.user1.id, 'i': item.id,
                                            str(parent.id): {'$elemMatch': {'id': field_obj.id}}},
                                           {str(parent.id) + '.$': 1, 'i': 1, 'u': 1, 'pr': 1})
        self.assertFalse(db_field)

    def test_item_create_form(self):
        item = ItemModel.objects.create()
        field, item, type = {'a': 1, 'user': self.user1.id}, item.id, 1
        field.update({'type': type, 'item': item})

        field_form = FieldCreateForm(field)
        field_form.is_valid()
        field_obj = field_form.save()
        for n, v in {'item': 1, 'a': 1, 'type': 1, 'is_temp': True, 'user': self.user1.id}.items():
            self.assertEqual(field_obj.to_dict()[n], v)

        db_field = ItemFieldModel.objects.get(id=field_obj.id)
        self.assertEqual(db_field.item_id, 1)

    def test_temp_vote(self):
        field, item, parent = {'c': 'hello'}, ItemModel.objects.create(), FieldTypeModel.objects.get(id=1)
        field_obj = ItemFieldModel.objects.create(raw_field=ujson.dumps(field), user=self.user1, item=item,
                                                  type=parent, is_temp=True)

        field_obj.vote(self.user1, 1)
        self.assertEqual(field_obj.vote_sum, 1)

        field_obj.vote(self.user1, 1)
        self.assertEqual(field_obj.vote_sum, 1)

        field_obj.vote(self.user2, 1)
        self.assertEqual(field_obj.vote_sum, 2)

        field_obj.vote(self.user1, -1)
        self.assertEqual(field_obj.vote_sum, 0)

    def test_border_vote(self):
        field, item, parent = {'c': 'hello'}, ItemModel.objects.create(), FieldTypeModel.objects.get(id=1)
        field_obj = ItemFieldModel.objects.create(raw_field=ujson.dumps(field), user=self.user1, item=item,
                                                  type=parent, is_temp=True)

        field_obj.vote(self.user1, 1)
        field_obj.vote(self.user2, 1)
        self.assertFalse(ItemFieldModel.objects.get(id=field_obj.id).is_temp)

        db_field = self.items_col.find_one({'i': item.id, str(parent.id): {'$elemMatch': {'id': field_obj.id}}},
                                           {str(parent.id) + '.$': 1, 'i': 1})
        self.assertTrue(db_field)
        db_field.pop('_id')
        self.assertDictEqual(db_field, {'i': 1, str(parent.id): [{'c': 1, 'id': field_obj.id}]})

        field_obj.vote(self.user1, -1)
        db_field = self.items_col.find_one({'i': item.id, str(parent.id): {'$elemMatch': {'id': field_obj.id}}},
                                           {str(parent.id) + '.$': 1, 'i': 1})
        self.assertFalse(db_field)
        self.assertTrue(ItemFieldModel.objects.get(id=field_obj.id).is_temp)

    def test_update_field(self):
        # ITEM FIELDS
        # UPDATE KEY
        field, item, parent = {'c': 'hello', 'b': 123}, ItemModel.objects.create(), FieldTypeModel.objects.get(id=1)
        field_obj = ItemFieldModel.objects.create(raw_field=ujson.dumps(field), user=self.user1, item=item,
                                                  type=parent, is_temp=True)
        new_value = {'c': '123', 'b': 321, 'id': field_obj.id}
        field_obj.update(deepcopy(new_value))
        self.assertDictEqual(field_obj.get_data(), new_value)

        field_obj = ItemFieldModel.objects.create(raw_field=ujson.dumps(field), user=self.user1, item=item,
                                                  type=parent, is_temp=False)
        new_value = {'c': '12345', 'b': 4321, 'id': field_obj.id}
        field_obj.update(deepcopy(new_value))
        self.assertDictEqual(field_obj.get_data(), new_value)

        # DELETE KEY
        field, item, parent = {'c': 'hello'}, ItemModel.objects.create(), FieldTypeModel.objects.get(id=1)
        field_obj = ItemFieldModel.objects.create(raw_field=ujson.dumps(field), user=self.user1, item=item,
                                                  type=parent, is_temp=True)
        new_value = {'b': 321, 'id': field_obj.id}
        field_obj.update(deepcopy(new_value))
        self.assertDictEqual(field_obj.get_data(), new_value)

        field_obj = ItemFieldModel.objects.create(raw_field=ujson.dumps(field), user=self.user1, item=item,
                                                  type=parent, is_temp=False)
        new_value = {'b': 4321, 'id': field_obj.id}
        field_obj.update(deepcopy(new_value))
        self.assertDictEqual(field_obj.get_data(), new_value)

        # USER FIELDS
        # UPDATE KEY
        field, item, parent = {'e': 'asdf', 'a': 123}, ItemModel.objects.create(), FieldTypeModel.objects.get(id=2)
        field_obj = ItemFieldModel.objects.create(raw_field=ujson.dumps(field), user=self.user1, item=item,
                                                  type=parent, is_temp=True)
        new_value = {'e': 'fdas', 'a': 321, 'id': field_obj.id}
        field_obj.update(deepcopy(new_value))
        self.assertDictEqual(field_obj.get_data(), new_value)

        field_obj = ItemFieldModel.objects.create(raw_field=ujson.dumps(field), user=self.user1, item=item,
                                                  type=parent, is_temp=False)
        new_value = {'e': 'gfds', 'a': 4321, 'id': field_obj.id, 'pr': 0}
        field_obj.update(deepcopy(new_value))
        self.assertDictEqual(field_obj.get_data(), new_value)

        # DELETE KEY
        field, item, parent = {'a': 123}, ItemModel.objects.create(), FieldTypeModel.objects.get(id=2)
        field_obj = ItemFieldModel.objects.create(raw_field=ujson.dumps(field), user=self.user1, item=item,
                                                  type=parent, is_temp=True)
        new_value = {'e': 'fdas', 'id': field_obj.id}
        field_obj.update(deepcopy(new_value))
        self.assertDictEqual(field_obj.get_data(), new_value)

        field_obj = ItemFieldModel.objects.create(raw_field=ujson.dumps(field), user=self.user1, item=item,
                                                  type=parent, is_temp=False)
        new_value = {'e': 'gfds', 'id': field_obj.id, 'pr': 0}
        field_obj.update(deepcopy(new_value))
        self.assertDictEqual(field_obj.get_data(), new_value)


@override_settings(MONGODB={'NAME': 'anyfield_test'})
class AddFieldDataImportTestCase(TestCase):
    def setUp(self):
        self.con = pymongo.MongoClient()
        self.con.drop_database(settings.MONGODB['NAME'])
        self.db = self.con[settings.MONGODB['NAME']]
        self.items_col = self.db.items
        self.users_col = self.db.users
        self.text_col = self.db.text
        self.anime_col = self.db.anime

        self.user1 = User.objects.create_user(username='admin', email='jacob@google.com', password='top_secret')

        self.data = [
            {"_id": {"i": 1, "t": "Manga"}, "members": 5, "related": {"side story": [{"i": 2, "t": "manga"}]},
             "genres": ["Mystery", "Drama", ]},
            {"_id": {"i": 2, "t": "Manga"}, "members": 6, "related": {"side story": [{"i": 3, "t": "manga"}]},
             "genres": ["Mystery", "Drama", ]},
            {"_id": {"i": 3, "t": "Manga"}, "members": 7, "related": {"side story": [{"i": 1, "t": "manga"}]},
             "genres": ["Mystery", "Drama", ]},
        ]
        self.anime_col.insert(self.data)

        call_command('load_mal_fields', db=settings.MONGODB['NAME'])

    def test_second_update(self):
        # load data
        call_command('load_mal_data', db=settings.MONGODB['NAME'])
        item = ItemModel.find_id_by_mal_id(1, 2)
        self.assertEqual(ItemModel.objects.all().count(), len(self.data), "should create new items")
        self.assertEqual(ItemFieldModel.objects.filter(item=item, type=14).count(), 2, "should create 2 genres ")
        select_values_len = SelectValueModel.objects.all().count()
        item_fields_len = ItemFieldModel.objects.all().count()

        call_command('load_mal_data', db=settings.MONGODB['NAME'])
        self.assertEqual(ItemModel.objects.all().count(), len(self.data), "shouldn't create new items")
        self.assertEqual(SelectValueModel.objects.all().count(), select_values_len, "shouldn't create new selects")
        self.assertEqual(ItemFieldModel.objects.all().count(), item_fields_len, "shouldn't create new item fields")

        self.anime_col.insert(
            {"_id": {"i": 4, "t": "Manga"}, "members": 8, "related": {"side story": [{"i": 1, "t": "manga"}]},
             "genres": ["Mystery", "Drama", "School"]},
        )
        self.anime_col.update(
            {"_id": {"i": 1, "t": "Manga"}}, {'$set': {"members": 10}}
        )
        item = ItemModel.find_id_by_mal_id(1, 2)
        item_members_field = ItemFieldModel.objects.get(item=item, type=15)

        call_command('load_mal_data', db=settings.MONGODB['NAME'])
        self.assertEqual(ItemModel.objects.all().count(), len(self.data) + 1, "should create new items")
        self.assertEqual(SelectValueModel.objects.all().count(), select_values_len + 1, "should create new selects")
        self.assertEqual(ItemFieldModel.objects.all().count(), item_fields_len + 6, "should create new item fields")

        item_members_field = ItemFieldModel.objects.get(pk=item_members_field.id).get_data()
        self.assertEqual(item_members_field['c'], 10)