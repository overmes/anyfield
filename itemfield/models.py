import ujson
from django.contrib.contenttypes.fields import GenericRelation

from django.core.exceptions import ValidationError
from django.db import models
from django.conf import settings
from helpers.helpers import get_li_fids, get_text_fids, is_item_type
from itemfield.mixin import CheckBoundaryMixin

from query.driver import QueryDriver
from query.query import SingleQuerySolver
from user.models import User
from vote.models import UserVote


class ItemModel(models.Model):
    def save(self, *args, **kwargs):
        is_new = not bool(self.id)
        super().save(*args, **kwargs)
        if is_new:
            self.driver.create_item(self.id)

    @staticmethod
    def find_id_by_mal_id(item_id, type_id):
        return QueryDriver().get_id_by_mal_id(item_id, type_id)

    @property
    def driver(self):
        return QueryDriver()

    @staticmethod
    def get_item_data(pk, user, fields=None):
        from fieldtype.models import FieldTypeModel
        query_solver = SingleQuerySolver(all_fields=FieldTypeModel.get_all_groups(), user=user, item=pk,
                                         fields=fields)
        result = query_solver.solve()
        ItemModel.add_temp_field(result, pk) if result else None
        return result

    @staticmethod
    def add_temp_field(item_data, item_id, fields=None):
        temp_fields = ItemFieldModel.objects.filter(item=item_id, is_temp=True)
        if fields:
            temp_fields = temp_fields.filter(type__in=fields)
        group_field = {}
        for f in temp_fields:
            data = f.to_dict(title=True)
            group_field.setdefault(str(f.type_id), []).append(data)

        for fieldType, fields in group_field.items():
            data_fields = item_data['i'].setdefault(fieldType, [])
            data_fields.extend(fields)


class ItemFieldModel(CheckBoundaryMixin, models.Model):
    raw_field = models.TextField(blank=True)
    user = models.ForeignKey(User)
    item = models.ForeignKey('ItemModel')
    type = models.ForeignKey('fieldtype.FieldTypeModel')
    is_temp = models.BooleanField(default=True, db_index=True)
    votes = GenericRelation(UserVote, related_query_name='votes')

    @property
    def prefix(self):
        return 'ITEM_FIELD'

    @property
    def driver(self):
        return QueryDriver(w=getattr(self, 'w', None))

    def save(self, *args, **kwargs):
        if not self.is_temp and self.raw_field:
            data = self.load_data()
            if data:
                self.raw_field = ''
                super().save(*args, **kwargs)
                text_fid = get_text_fids(self.type_id)
                if is_item_type(self.type_id):
                    self.driver.insert_item_field(self.id, self.item_id, self.type_id, data, text_fid)
                else:
                    self.driver.insert_user_field(self.id, self.item_id, self.type_id, self.user_id, data, text_fid)
        else:
            super().save(*args, **kwargs)

    def load_data(self):
        return ujson.loads(self.raw_field) if type(self.raw_field) is str else self.raw_field

    def set_raw_data(self, data):
        self.raw_field = ujson.dumps(data)

    def get_data(self, title=False):
        if self.is_temp:
            data = self.load_data()
            data['id'] = self.id
        else:
            data = self.get_field_from_mongodb()
        if title:
            li_fids = get_li_fids(self.type_id)
            for fid in li_fids:
                if fid in data:
                    data[fid] = [data[fid], self.driver.get_title(data[fid])]
        return data

    def clear_fid(self, fid):
        data = self.get_data()
        data.pop('id')
        data.pop(fid, None)
        self.set_raw_data(data)
        if not data:
            self.delete()
        else:
            self.save()

    def on_temp(self):
        self.load_field_from_mongodb()

    def load_field_from_mongodb(self):
        if is_item_type(self.type_id):
            self.set_raw_data(self.driver.delete_item_field(self.id, self.item_id, self.type_id))

    def get_field_from_mongodb(self):
        if is_item_type(self.type_id):
            return self.driver.get_item_field(self.id, self.item_id, self.type_id)
        else:
            return self.driver.get_user_field(self.id, self.item_id, self.type_id, self.user_id)

    def delete(self, *args, **kwargs):
        if not self.is_temp:
            if is_item_type(self.type_id):
                self.driver.delete_item_field(self.id, self.item_id, self.type_id)
            else:
                self.driver.delete_user_field(self.id, self.item_id, self.type_id, self.user_id)
        super().delete()

    def update(self, new_value):
        if self.is_temp:
            self.set_raw_data(new_value)
        else:
            text_fid = get_text_fids(self.type_id)
            if is_item_type(self.type_id):
                return self.driver.update_item_field(self.id, self.item_id, self.type_id, new_value, text_fid)
            else:
                return self.driver.update_user_field(self.id, self.item_id, self.type_id, self.user_id, new_value,
                                                     text_fid)
        self.save()

    def to_dict(self, title=False):
        res = super().to_dict(exclude='raw_field')
        field_data = self.get_data(title)
        res.update(field_data)
        return res

    @staticmethod
    def clear_field(type_id, fid):
        temp_fields = ItemFieldModel.objects.filter(type=type_id, is_temp=True)
        for field in temp_fields:
            field.clear_fid(fid)

    def vote(self, user, value):
        UserVote.objects.create(user=user, value=value, content_type=self.content_type, object_id=self.id)


def validate_setting(value):
    if not hasattr(settings, value):
        raise ValidationError('Setting {0} not exist'.format(value))
