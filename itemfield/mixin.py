from constance import config
from django.contrib.contenttypes.models import ContentType
from django.forms import model_to_dict
from vote.models import UserVote


class CheckBoundaryMixin:
    @property
    def content_type(self):
        return ContentType.objects.get_for_model(self.__class__)

    @property
    def vote_sum(self):
        return UserVote.vote_sum(self.content_type, self.id)

    @property
    def prefix(self):
        raise NotImplemented()

    def on_temp(self):
        pass

    def check_boundary(self, vote):
        vote_count = self.vote_sum + vote.value
        if vote_count >= getattr(config, '{}_VOTE_UP_BORDER'.format(self.prefix)):
            if self.is_temp:
                self.add_karma(vote, 1)
                self.set_temp(False)
        elif vote_count <= getattr(config, '{}_VOTE_DELETE_BORDER'.format(self.prefix)):
                self.delete()
        elif vote_count <= getattr(config, '{}_VOTE_DOWN_BORDER'.format(self.prefix)):
            if not self.is_temp:
                self.on_temp()
                self.add_karma(vote, -1)
                self.set_temp(True)

    def to_dict(self, *args, **kwargs):
        res = model_to_dict(self, *args, **kwargs)
        return res

    def set_temp(self, value):
        self.is_temp = value
        self.save()

    def add_karma(self, last_vote, coef):
        self.user.add_karma(coef*5, self.prefix)

        all_votes = UserVote.type_filter(self.content_type, self.id)
        for vote in all_votes:
            karma = coef*1 if vote.value > 0 else -1*coef
            vote.user.add_karma(karma, self.prefix)