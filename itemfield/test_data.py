fields_data = [
    {
        'id': 1, 'name': 'Genres', 'description': 'List of genres field',
        'fields': [
            {
                'fid': 'a',
                'name': 'Genre', 'type': 'se', 'values': [{'number': 1, 'name': 'Comedy', 'is_temp': False}, {'number': 2, 'name': 'Action', 'is_temp': False}],
                'is_temp': False
            },
            {
                'fid': 'b',
                'name': 'Genre Value', 'type': 'va', 'values': [0, 1, 0.1],
                'is_temp': False
            },
            {
                'fid': 'c',
                'name': 'Some text', 'type': 'te',
                'is_temp': False
            }
        ],
        'type': 'it',
        'read': 'pu',
        'is_temp': False
    },
    {
        'id': 2, 'name': 'User Score', 'description': 'User score',
        'fields': [
            {
                'fid': 'a',
                'name': 'Digit score', 'type': 'va', 'values': [0, 10, 0.1],
                'is_temp': False
            },
            {
                'fid': 'b', 'name': 'Boolean score', 'type': 'va', 'values': [0, 1, 1],
                'is_temp': False
            },
            {
                'fid': 'c', 'name': 'Date', 'type': 'da',
                'is_temp': False
            },
            {
                'fid': 'd', 'name': 'Status', 'type': 'se',
                'values': [{'number': 1, 'name': 'Complete'}, {'number': 2, 'name': 'Plan to watch'}],
                'is_temp': False
            },
            {
                'fid': 'e', 'name': 'Comment', 'type': 'te',
                'is_temp': False
            }
        ],
        'type': 'us',
        'read': 'pu',
        'is_temp': False
    }
]