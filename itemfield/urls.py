from django.conf.urls import patterns, include, url
from itemfield.api import ItemFieldResource

item_field_resource = ItemFieldResource()

api_patterns = patterns(
    '',
    url(r'^api/', include(item_field_resource.urls)),
)

urlpatterns = patterns(
    '',
    url(r'', include(api_patterns)),
)

