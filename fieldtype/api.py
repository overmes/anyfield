from tastypie.authentication import SessionAuthentication, MultiAuthentication
from tastypie.authorization import Authorization
from tastypie.cache import SimpleCache
from tastypie import fields
from tastypie.resources import ModelResource
from tastypie.validation import CleanedDataFormValidation
from fieldtype.forms import FieldTypeCreateForm, FieldCreateForm, SelectCreateForm, RangeCreateForm

from fieldtype.models import FieldTypeModel, FieldModel, SelectValueModel, RangeValueModel


class DenyAuthorization(Authorization):
    def read_list(self, object_list, bundle):
        return []

    def read_detail(self, object_list, bundle):
        return False

    def create_list(self, object_list, bundle):
        return []

    def create_detail(self, object_list, bundle):
        return False

    def update_list(self, object_list, bundle):
        return []

    def update_detail(self, object_list, bundle):
        return False

    def delete_list(self, object_list, bundle):
        return []

    def delete_detail(self, object_list, bundle):
        return False


class CustomAuthorization(DenyAuthorization):
    def read_list(self, object_list, bundle):
        return object_list

    def read_detail(self, object_list, bundle):
        return True

    def create_detail(self, object_list, bundle):
        return True


class AnyGetAuthentication(SessionAuthentication):
    def is_authenticated(self, request, **kwargs):
        if not request.user.is_authenticated() and request.method == 'GET':
            return True

    def get_identifier(self, request):
        if not request.user.is_authenticated() and request.method == 'GET':
            return 'anon'


class FieldTypeResource(ModelResource):
    fields = fields.ToManyField('fieldtype.api.FieldResource', 'fieldmodel_set', full=True, readonly=True)

    def hydrate(self, bundle):
        bundle.data['user'] = bundle.request.user.id
        return bundle

    class Meta:
        queryset = FieldTypeModel.prefetch.filter()
        resource_name = 'fieldType'
        cache = SimpleCache(timeout=60)
        allowed_methods = ['get', 'post']
        validation = CleanedDataFormValidation(form_class=FieldTypeCreateForm)
        authentication = MultiAuthentication(SessionAuthentication(), AnyGetAuthentication())
        authorization = CustomAuthorization()
        always_return_data = True


class FieldResource(ModelResource):
    values = fields.ToManyField('fieldtype.api.SelectValueResource', 'selectvaluemodel_set', full=True, readonly=True)
    range = fields.ToManyField('fieldtype.api.RangeValueResource', 'rangevaluemodel_set', full=True, readonly=True)

    def hydrate(self, bundle):
        bundle.data['user'] = bundle.request.user.id
        return bundle

    class Meta:
        queryset = FieldModel.objects.all()
        resource_name = 'field'
        cache = SimpleCache(timeout=60)
        allowed_methods = ['get', 'post']
        authentication = MultiAuthentication(SessionAuthentication(), AnyGetAuthentication())
        authorization = CustomAuthorization()
        validation = CleanedDataFormValidation(form_class=FieldCreateForm)
        always_return_data = True
        fields = ['fid', 'name', 'description', 'is_temp', 'type', 'id']


class SelectValueResource(ModelResource):
    def hydrate(self, bundle):
        bundle.data['user'] = bundle.request.user.id
        return bundle

    class Meta:
        queryset = SelectValueModel.objects.all()
        resource_name = 'select'
        cache = SimpleCache(timeout=60)
        allowed_methods = ['get', 'post']
        authentication = MultiAuthentication(SessionAuthentication(), AnyGetAuthentication())
        authorization = CustomAuthorization()
        validation = CleanedDataFormValidation(form_class=SelectCreateForm)
        always_return_data = True


class RangeValueResource(ModelResource):
    def hydrate(self, bundle):
        bundle.data['user'] = bundle.request.user.id
        return bundle

    class Meta:
        queryset = RangeValueModel.objects.all()
        resource_name = 'range'
        cache = SimpleCache(timeout=60)
        allowed_methods = ['get', 'post']
        authentication = MultiAuthentication(SessionAuthentication(), AnyGetAuthentication())
        authorization = CustomAuthorization()
        validation = CleanedDataFormValidation(form_class=RangeCreateForm)
        always_return_data = True