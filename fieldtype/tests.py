from django.test import TestCase, override_settings
import pymongo
from django.conf import settings
import ujson
from fieldtype.models import FieldTypeModel, FieldModel
from itemfield.models import ItemFieldModel, ItemModel
from itemfield.test_data import fields_data
from user.models import User


@override_settings(MONGODB={'NAME': 'anyfield_test'}, AUTH_USER_MODEL='auth.User')
class FieldTestCase(TestCase):
    def setUp(self):
        self.con = pymongo.MongoClient()
        self.con.drop_database('anyfield_test')
        self.db = self.con[settings.MONGODB['NAME']]
        self.all_fields = fields_data
        self.items_col = self.db.items
        self.users_col = self.db.users
        self.text_col = self.db.text

        self.items_col.create_index([('i', pymongo.ASCENDING)])
        self.users_col.create_index([('i', pymongo.ASCENDING)])
        self.users_col.create_index([('u', pymongo.ASCENDING)])

        self.user1 = User.objects.create_user(username='user', email='user@user.com', password='some')
        self.user2 = User.objects.create_user(username='user2', email='user2@user2.com', password='some2')
        for one_field_data in fields_data:
            FieldTypeModel.load_fields_from_json(one_field_data, self.user1)
            for field in one_field_data['fields']:
                full_field_name = '{0}.{1}'.format(one_field_data['id'], field['fid'])
                if one_field_data['type'] == 'it':
                    self.items_col.create_index([(full_field_name, pymongo.ASCENDING)])
                else:
                    self.users_col.create_index([(full_field_name, pymongo.ASCENDING)])
        self.text_col.create_index(
            [('text', 'text'), ('f', pymongo.ASCENDING), ('s', pymongo.ASCENDING), ('u', pymongo.ASCENDING)])
        self.text_col.create_index([('id', pymongo.ASCENDING)])



    def test_delete_user_type(self):
        field, item, parent = {'e': 'hello'}, ItemModel.objects.create(), FieldTypeModel.objects.get(id=2)
        ItemFieldModel.objects.create(raw_field=ujson.dumps(field), user=self.user1, item=item,
                                      type=parent, is_temp=False)
        ItemFieldModel.objects.create(raw_field=ujson.dumps(field), user=self.user1, item=item,
                                      type=parent, is_temp=False)

        parent_id = parent.id
        self.assertTrue(self.users_col.find_one({'i': item.id, str(parent_id): {'$exists': True}}))
        self.assertTrue(self.text_col.find_one({'f': str(parent_id)}))
        parent.delete()
        self.assertFalse(self.users_col.find_one({'i': item.id, str(parent_id): {'$exists': True}}))
        self.assertFalse(self.text_col.find_one({'f': str(parent_id)}))
        self.assertFalse(ItemFieldModel.objects.filter(type=parent).count())

    def test_delete_item_type(self):
        field, item, parent = {'c': 'hello'}, ItemModel.objects.create(), FieldTypeModel.objects.get(id=1)
        ItemFieldModel.objects.create(raw_field=ujson.dumps(field), user=self.user1, item=item,
                                      type=parent, is_temp=False)
        ItemFieldModel.objects.create(raw_field=ujson.dumps(field), user=self.user1, item=item,
                                      type=parent, is_temp=False)
        parent_id = parent.id
        self.assertTrue(self.items_col.find_one({'i': item.id, str(parent_id): {'$exists': True}}))
        self.assertTrue(self.text_col.find_one({'f': str(parent_id)}))
        parent.delete()
        self.assertFalse(self.text_col.find_one({'f': str(parent_id)}))
        self.assertFalse(self.items_col.find_one({'i': item.id, str(parent_id): {'$exists': True}}))
        self.assertFalse(ItemFieldModel.objects.filter(type=parent).count())

    def test_delete_user_fid(self):
        field, item, parent = {'e': 'hello', 'a': 1}, ItemModel.objects.create(), FieldTypeModel.objects.get(id=2)
        field_obj1 = ItemFieldModel.objects.create(raw_field=ujson.dumps(field), user=self.user1, item=item,
                                                   type=parent, is_temp=False)

        FieldModel.objects.get(fid='e', parent=parent).delete()
        self.assertFalse(field_obj1.get_data().get('e'))

    def test_delete_item_fid(self):
        field, item, parent = {'c': 'hello', 'a': 1}, ItemModel.objects.create(), FieldTypeModel.objects.get(id=1)
        field_obj1 = ItemFieldModel.objects.create(raw_field=ujson.dumps(field), user=self.user1, item=item,
                                                   type=parent, is_temp=False)
        field_obj2 = ItemFieldModel.objects.create(raw_field=ujson.dumps(field), user=self.user1, item=item,
                                                   type=parent, is_temp=True)
        field_obj3 = ItemFieldModel.objects.create(raw_field=ujson.dumps({'c': 'hi'}), user=self.user1, item=item,
                                                   type=parent, is_temp=True)

        FieldModel.objects.get(parent=parent, fid='c').delete()
        self.assertFalse(field_obj1.get_data().get('c'))
        self.assertFalse(ItemFieldModel.objects.get(id=field_obj2.id).get_data().get('c'))
        self.assertFalse(ItemFieldModel.objects.filter(id=field_obj3.id).count())