from django.contrib import admin

from fieldtype.models import FieldTypeModel, FieldModel, SelectValueModel, RangeValueModel


class Fieldline(admin.TabularInline):
    model = FieldModel


class FieldTypeModelAdmin(admin.ModelAdmin):
    inlines = [
        Fieldline,
    ]


class SelectValueModelAdmin(admin.ModelAdmin):
    list_display = ('parent_name', 'name')


class RangeValueModelAdmin(admin.ModelAdmin):
    pass

admin.site.register(RangeValueModel, RangeValueModelAdmin)
admin.site.register(FieldTypeModel, FieldTypeModelAdmin)
admin.site.register(FieldModel)
admin.site.register(SelectValueModel, SelectValueModelAdmin)