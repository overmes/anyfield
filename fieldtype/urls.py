from django.conf.urls import patterns, include, url

from fieldtype.api import FieldTypeResource, FieldResource, SelectValueResource, RangeValueResource


field_type_resource = FieldTypeResource()
field_resource = FieldResource()
select_value_resource = SelectValueResource()
range_value_resource = RangeValueResource()

api_patterns = patterns(
    '',
    url(r'^api/', include(field_type_resource.urls)),
    url(r'^api/', include(field_resource.urls)),
    url(r'^api/', include(select_value_resource.urls)),
    url(r'^api/', include(range_value_resource.urls)),
)

urlpatterns = patterns(
    '',
    url(r'', include(api_patterns)),
)

