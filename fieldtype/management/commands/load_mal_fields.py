from django.core.management.base import BaseCommand
from user.models import User
from fieldtype.models import FieldTypeModel
from ._default_fields import fields_data


class Command(BaseCommand):
    help = 'Load mal types and fields'

    def handle(self, *args, **options):
        user = User.objects.get(username='admin')
        for field_type in fields_data:
            FieldTypeModel.load_fields_from_json(field_type, user)