import copy

from django.db import models
from django.db.models import Prefetch
from django.forms.models import model_to_dict

from helpers.helpers import is_item_type
from itemfield.mixin import CheckBoundaryMixin
from itemfield.models import ItemFieldModel
from query.driver import QueryDriver
from user.models import User


class FieldTypePrefetchManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().prefetch_related(
            Prefetch('fieldmodel_set__selectvaluemodel_set', queryset=SelectValueModel.objects.select_related('parent')),
            Prefetch('fieldmodel_set__rangevaluemodel_set', queryset=RangeValueModel.objects.select_related('parent'))
        )


class FieldTypeModel(CheckBoundaryMixin, models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200, unique=True)
    description = models.CharField(max_length=600)
    user = models.ForeignKey(User)
    is_temp = models.BooleanField(default=True)

    ITEM = 'it'
    USER = 'us'
    SPECIAL = 'sp'
    TYPES = (
        (ITEM, 'Item'),
        (USER, 'User'),
        (SPECIAL, 'Special')
    )
    type = models.CharField(choices=TYPES, max_length=2)

    READ = (
        ('pu', 'Public'),
        ('sy', 'System'),
        ('pe', 'Personal')
    )
    read = models.CharField(choices=READ, max_length=2, default='pu')

    EDIT = (
        ('no', 'None'),
        ('pu', 'Public'),
        ('us', 'User')
    )
    edit = models.CharField(choices=EDIT, max_length=2, default='pu')

    objects = models.Manager()
    prefetch = FieldTypePrefetchManager()

    @staticmethod
    def title_type_index():
        return 2

    @staticmethod
    def score_type_index():
        return 18

    @staticmethod
    def recommendation_type_index():
        return 20

    @staticmethod
    def id_type_index():
        return 1

    @staticmethod
    def selected_type_index():
        return 3

    @property
    def prefix(self):
        return 'TYPE'

    @property
    def fields(self):
        return self.fieldmodel_set.all()

    @property
    def fids(self):
        return self.fields.values_list('fid', flat=True)

    @staticmethod
    def get_all_groups():
        result = []
        all_field_types = list(FieldTypeModel.objects.prefetch_related(
            Prefetch('fieldmodel_set',
            queryset=FieldModel.objects.select_related('parent'),
            to_attr='fields_set'
        )))
        for field_type in all_field_types:
            current_item = model_to_dict(field_type)
            all_fields = [model_to_dict(f) for f in field_type.fields_set]
            current_item['fields'] = all_fields
            result.append(current_item)
        return result

    def get_field_by_fid(self, fid):
        try:
            res = self.fieldmodel_set.get(fid=fid)
        except FieldModel.DoesNotExist:
            res = None
        return res

    def __str__(self):
        return self.name

    @staticmethod
    def load_fields_from_json(data, user):
        data = copy.deepcopy(data)
        fields = data.pop('fields')
        data['user'] = user
        current_field_type, created = FieldTypeModel.objects.get_or_create(**data)
        for field in fields:
            FieldModel.load_field_from_json(current_field_type, field, user)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        self._create_view_control_field()

    def delete(self, *args, **kwargs):
        driver = QueryDriver()
        driver.delete_type(self.id)
        ItemFieldModel.objects.filter(type=self).delete()
        for field in self.fields:
            driver.delete_item_index(self.id, field.fid) if self.is_item_type() else driver.delete_user_index(self.id, field.fid)
        super().delete(*args, **kwargs)

    def _create_view_control_field(self):
        if self.type == 'us' and 'pr' not in self.fids:
            field = FieldModel.objects.create(parent=self, fid='pr', name='Private', is_temp=False,
                                              description='Public/Private control', type='se', user=self.user)
            SelectValueModel.objects.create(parent=field, number=0, name='Public', description='Public', user=self.user, is_temp=False)
            SelectValueModel.objects.create(parent=field, number=1, name='Private', description='Private', user=self.user, is_temp=False)

    def is_editable(self):
        return self.edit != 'no'

    def is_item_type(self):
        return self.type == 'it'


class FieldModel(models.Model, CheckBoundaryMixin):
    parent = models.ForeignKey('FieldTypeModel')
    fid = models.CharField(max_length=2, blank=True)
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=600)
    user = models.ForeignKey(User)
    is_temp = models.BooleanField(default=True)

    SELECT = 'se'
    VALUE = 'va'
    TEXT = 'te'
    IMAGE = 'im'
    DATE = 'da'
    LINK = 'li'
    URL = 'ur'
    ID = 'id'
    COUNT = 'co'
    SELECTED = 'si'
    TYPES = (
        (SELECT, 'Select'),
        (VALUE, 'Value'),
        (TEXT, 'Text'),
        (IMAGE, 'Image'),
        (DATE, 'Date'),
        (LINK, 'Item Link'),
        (URL, 'Url'),
        (ID, 'Id'),
        (SELECTED, 'Selected Ids'),
        (COUNT, 'Count')
    )
    type = models.CharField(choices=TYPES, max_length=2)

    class Meta:
        unique_together = (("parent", "fid"), ("parent", "name"))

    @property
    def prefix(self):
        return 'FIELD'

    @property
    def values(self):
        return self.selectvaluemodel_set.all()

    @property
    def range(self):
        return self.rangevaluemodel_set.all()

    def get_last_range_or_default(self):
        last_db_range = self.range.filter(is_temp=False, is_deleted=False).order_by('-id').first()
        default_range = RangeValueModel(min=0, max=0, step=1, parent=self, user=self.user)
        return last_db_range or default_range

    def __str__(self):
        return '{} {}'.format(self.parent.name, self.name)

    def get_choices(self):
        values = SelectValueModel.objects.filter(parent=self)
        return [(v.number, v.name) for v in values]

    @staticmethod
    def load_field_from_json(parent, field, user):
        values = field.pop('values', [])
        field['parent'] = parent
        field['user'] = user
        current_field, created = FieldModel.objects.get_or_create(**field)

        if field['type'] == FieldModel.SELECT:
            for v in values:
                SelectValueModel.load_select_from_json(current_field, v, user)
        if field['type'] in [FieldModel.VALUE, FieldModel.ID] and values:
                RangeValueModel.load_range_from_json(current_field, values, user)

    def save(self, *args, **kwargs):
        if not self.fid:
            self.fid = self.get_next_fid()
        super().save(*args, **kwargs)
        self.create_index()

    def create_index(self):
        if self.type not in [FieldModel.IMAGE, FieldModel.URL]:
            driver = QueryDriver()
            if is_item_type(self.parent_id):
                driver.create_item_index(self.parent_id, self.fid)
            else:
                driver.create_user_index(self.parent_id, self.fid)

    def get_next_fid(self):
        last_fid = FieldModel.objects.filter(parent=self.parent).exclude(fid='pr').order_by('-fid').first()
        if last_fid:
            next_fid = chr(ord(last_fid.fid) + 1)
        else:
            next_fid = 'a'
        return next_fid

    def delete(self, *args, **kwargs):
        driver = QueryDriver()
        if is_item_type(self.parent_id):
            driver.delete_item_fid(self.parent_id, self.fid)
            driver.delete_item_index(self.parent_id, self.fid)
        else:
            driver.delete_user_fid(self.parent_id, self.fid)
            driver.delete_user_index(self.parent_id, self.fid)
        ItemFieldModel.clear_field(self.parent_id, self.fid)
        super().delete(*args, **kwargs)


class SelectValueModel(models.Model, CheckBoundaryMixin):
    parent = models.ForeignKey('FieldModel')
    number = models.IntegerField(blank=True)
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=600, null=True, blank=True)
    user = models.ForeignKey(User)
    is_temp = models.BooleanField(default=True)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        unique_together = (("parent", "number"), ("parent", "name"))

    @property
    def prefix(self):
        return 'SELECT'

    def __str__(self):
        return '{} {}'.format(self.parent.name, self.name)

    def parent_name(self):
        return self.parent.name

    def save(self, *args, **kwargs):
        if self.number is None:
            self.number = self.get_next_number()
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        self.is_deleted = True
        self.save()

    def get_next_number(self):
        last_number_obj = SelectValueModel.objects.filter(parent=self.parent).order_by('-number').first()
        if last_number_obj:
            next_number = last_number_obj.number + 1
        else:
            next_number = 1
        return next_number

    @staticmethod
    def load_select_from_json(parent, value, user):
        value['parent'] = parent
        value['user'] = user
        current_value, created = SelectValueModel.objects.get_or_create(**value)


class RangeValueModel(models.Model, CheckBoundaryMixin):
    parent = models.ForeignKey('FieldModel')
    min = models.FloatField()
    max = models.FloatField()
    step = models.FloatField()
    is_temp = models.BooleanField(default=True)
    user = models.ForeignKey(User)
    is_deleted = models.BooleanField(default=False)

    def __str__(self):
        return '{} \t min: {} max: {} step: {}'.format(self.parent.name, self.min, self.max, self.step)

    def parent_name(self):
        return self.parent.name

    def delete(self, *args, **kwargs):
        self.is_deleted = True
        self.save()

    @staticmethod
    def load_range_from_json(parent, values, user):
        RangeValueModel.objects.get_or_create(min=values[0], max=values[1], step=values[2], parent=parent,
                                              is_temp=False, user=user)

    @property
    def prefix(self):
        return 'RANGE'