from django import forms

from fieldtype.models import FieldModel, SelectValueModel, RangeValueModel, FieldTypeModel
from helpers.helpers import is_editable


class UserCleanMixin:
    def clean_user(self):
        user = self.cleaned_data['user']
        if not user.is_authenticated():
            raise forms.ValidationError("You are not login")
        return user


class ParentCleanMixin:
    def get_type(self):
        return self.cleaned_data['parent']

    def clean_parent(self):
        if not is_editable(self.get_type().id):
            raise forms.ValidationError("This field type not editable")
        return self.cleaned_data['parent']


class FieldCreateForm(forms.ModelForm, UserCleanMixin, ParentCleanMixin):
    class Meta:
        model = FieldModel
        fields = ['parent', 'name', 'description', 'user', 'type']


class SelectCreateForm(forms.ModelForm, UserCleanMixin, ParentCleanMixin):

    def get_type(self):
        return self.cleaned_data['parent'].parent

    class Meta:
        model = SelectValueModel
        fields = ['parent', 'name', 'description', 'user']


class RangeCreateForm(forms.ModelForm, UserCleanMixin, ParentCleanMixin):

    def get_type(self):
        return self.cleaned_data['parent'].parent

    class Meta:
        model = RangeValueModel
        fields = ['parent', 'min', 'max', 'step', 'user']


class FieldTypeCreateForm(forms.ModelForm, UserCleanMixin):
    class Meta:
        model = FieldTypeModel
        fields = ['name', 'description', 'user', 'type']