from tastypie.cache import SimpleCache
from tastypie.resources import ModelResource
from pages.models import StaticPage


class StaticPageResource(ModelResource):
    class Meta:
        queryset = StaticPage.objects.all()
        resource_name = 'staticPage'
        allowed_methods = ['get']
        cache = SimpleCache(timeout=360)