from django.db import models
from tinymce.widgets import TinyMCE


class StaticPage(models.Model):
    id = models.CharField(primary_key=True, max_length=100)
    html = models.TextField()

    def __str__(self):
        return self.id
