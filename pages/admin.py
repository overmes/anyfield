from django.contrib import admin
from django.db import models
from tinymce.widgets import TinyMCE
from .models import StaticPage


class WideTinyMCE(TinyMCE):
    def __init__(self, *attrs):
        super().__init__(attrs={'cols': 200, 'rows': 50}, *attrs)


@admin.register(StaticPage)
class StaticPageAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': WideTinyMCE},
    }