from django.conf.urls import patterns, include, url
from pages.api import StaticPageResource

static_page_resource = StaticPageResource()

api_patterns = patterns(
    '',
    url(r'^api/', include(static_page_resource.urls)),
)

urlpatterns = patterns(
    '',
    url(r'', include(api_patterns)),
)

