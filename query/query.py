from query.driver import QueryDriver
from query.query_lib import QueryLogger, ItemQuerySolver, UserQuerySolver


class QuerySolver:
    MAX_LIMIT = 100

    def __init__(self, all_fields, user=0, query=None, sort=None, limit=MAX_LIMIT, skip=0, items_set=None):
        self._driver = self._dr = QueryDriver(all_fields)
        self._log = QueryLogger()
        self._sort = sort
        self._limit = limit if limit and limit <= self.MAX_LIMIT else self.MAX_LIMIT
        self._skip = skip if skip else 0
        self._user = user or 0
        self._query = query or {}
        self._items_set = items_set

        self._queries = {}
        for query_index, query_dict in self._query.items():
            self._queries[query_index] = self.query_fabric(query_index, query_dict)

        if sort and sort[0] not in self._query:
            index = sort[0]
            self._queries[index] = self.query_fabric(index, {})

        if not self._queries:
            self._queries['i'] = self.query_fabric('i', {})

    def __str__(self):
        return str('''QuerySolver log:
            time = {0}
            query = {1}
            sort = {2}
            limit = {3}
            skip = {4}
            user = {5}'''.format(self._log.timers, self._query, self._sort, self._limit, self._skip, self._user))

    def _get_excluded_items(self, matched_items):
        if self._items_set and matched_items is not None and len(self._items_set) - len(matched_items) < len(matched_items):
            return None, list(self._items_set.difference(matched_items))
        else:
            return matched_items, None

    def query_fabric(self, query_index, query_dict):
        if query_index == 'i':
            return ItemQuerySolver(query_index, self._dr, self._log, self._user, query_dict, self._sort, self._limit,
                                   self._skip)
        else:
            return UserQuerySolver(query_index, self._dr, self._log, self._user, query_dict, self._sort,
                                   self._limit, self._skip)

    def solve(self):
        last_query_index = self.get_last_query_index()
        last_query = self._queries.pop(last_query_index)

        matched_items = None
        excluded_items = None
        if last_query_index == 'i' and self._queries and last_query.has_query():
            matched_items = last_query.get_matched_items()
            matched_items, excluded_items = self._get_excluded_items(matched_items)
            self._log.add_timer('i')

        for current_index, current_query in self._queries.items():
            continue_flag = matched_items is None or len(matched_items) > 0
            if current_query.has_query() and continue_flag:
                matched_items = current_query.get_matched_items(matched_items, excluded_items)
                matched_items, excluded_items = self._get_excluded_items(matched_items)
                self._log.add_timer(current_index)

        last_query_result = last_query.make_last_query(matched_items, excluded_items)
        self._log.add_timer('last_query ' + last_query_index)

        union_result = self._get_union_result(last_query_index, last_query_result)
        self._log.add_timer('add_fields')

        self._change_ids_to_text(union_result)
        self._log.add_timer('replace_text')

        self._change_links_to_text(union_result)
        full_query_time = self._log.add_timer('replace_links', last=True)

        if full_query_time > 1:
            self._log.log('warning', str(self))
        return union_result

    def get_last_query_index(self):
        i_sort = not self._sort or self._sort[0] == 'i'
        empty_or_i_query = not self._queries or 'i' in self._queries
        if i_sort and empty_or_i_query:
            return 'i'
        else:
            return self._sort[0] if self._sort else list(self._queries.keys())[0]

    def _get_union_result(self, last_query_index, last_query_result, project=True):
        matched_items = [i['i'] for i in last_query_result]

        union_result = []
        for result in last_query_result:
            union_result.append({last_query_index: result})

        for current_index, current_query in self._queries.items():
            current_items_fields = current_query.make_fields_query(matched_items, project=project)
            self._update_result(union_result, current_index, matched_items, current_items_fields)

        return union_result

    def _update_result(self, union_result, current_index, matched_items, current_items_fields):
        for item_fields in current_items_fields:
            current_item = item_fields['i']
            current_position = matched_items.index(current_item)
            union_result[current_position].update({current_index: item_fields})

    def _change_ids_to_text(self, results_list):
        ids = self._find_type_ids('te', results_list)
        idtext = self._find_text(ids)
        self._replace_ids_to_text('te', results_list, idtext)

    def _change_links_to_text(self, results_list):
        ids = self._find_type_ids('li', results_list)
        idtext = self._find_link_text(ids)
        self._replace_ids_to_text('li', results_list, idtext)

    def _replace_ids_to_text(self, field_type, results_list, idtext):
        for i in results_list:
            for query_index, result in i.items():
                for field_num, sub_fields in result.items():
                    if isinstance(sub_fields, list):
                        for field in sub_fields:
                            for field_name, value in field.items():
                                # TODO: 'te' can be error
                                if isinstance(value, int) and self._dr.all_fields[field_num]['fields'].get(
                                        field_name, {}).get('type') == field_type:
                                    field[field_name] = idtext[value]

    def _find_type_ids(self, field_type, results_list):
        text_ids = []
        for i in results_list:
            for query_index, result in i.items():
                for field_num, sub_fields in result.items():
                    if isinstance(sub_fields, list):
                        for field in sub_fields:
                            for field_name, value in field.items():
                                # TODO: 'te' can be error
                                if self._dr.all_fields[field_num]['fields'].get(field_name, {}).get('type') == field_type:
                                    text_ids.append(value)
        return text_ids

    def _find_text(self, text_ids):
        text_query = {'spec': {'_id': {'$in': text_ids}}, 'fields': {'text': 1}}
        self._log.log('debug', text_query)
        text_query_result = self._dr.text_col.find(**text_query)
        res = {r['_id']: r['text'] for r in text_query_result}
        return res

    def _find_link_text(self, items_ids):
        text_query = {'spec': {'i': {'$in': items_ids}, 'f': '2', 's': 'a'}, 'fields': {'text': 1, 'i': 1}}
        self._log.log('debug', text_query)
        text_query_result = self._dr.text_col.find(**text_query)
        res = {r['i']: [r['i'], r['text']] for r in text_query_result}
        return res


class SingleQuerySolver(QuerySolver):
    def __init__(self, all_fields, item, user=0, fields=None):
        query = {'i': {'query': {'i': item}}}

        self.fields = fields
        query['i'].update({'fields': self.fields}) if self.fields else None
        query.update({str(user): {'i': item}}) if user else None
        self.item = item
        super().__init__(all_fields, user=user, query=query)

    def solve(self):
        last_query_index = 'i'
        last_query = self._queries.pop(last_query_index)

        projection = self.fields is not None
        last_query_result = last_query.make_last_query(project=projection)
        self._log.add_timer('last_query ' + last_query_index)

        union_result = self._get_union_result(last_query_index, last_query_result, project=projection)
        self._log.add_timer('add_fields')

        self._change_ids_to_text(union_result)
        self._log.add_timer('replace_text')

        self._change_links_to_text(union_result)
        full_query_time = self._log.add_timer('replace_links', last=True)

        if full_query_time > 1:
            self._log.log('warning', str(self))
        result = self.get_first(union_result)
        if not projection and result:
            self.delete_id(result)
        return result

    def get_first(self, result):
        if result:
            return result[0]
        else:
            return None

    def delete_id(self, result):
        for index, data in result.items():
            if '_id' in data:
                del data['_id']