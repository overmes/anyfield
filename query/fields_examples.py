import copy

field_types = ['va', 'url', 'text', 'se', 'da', 'link', 'vote', 'image']

fields_data = [
    {'id': 0, 'name': 'Id', 'fields': [{'fid': 'a', 'type': 'va'}], 'type': 'it', 'read': 'pu', 'edit': None},
    {
        'id': 1, 'name': 'Type', 'description': 'Type',
        'fields': [
            {
                'fid': 'a',
                'type': 'se',
                'values': [{'n': 1, 'v': 'Anime'}, {'n': 2, 'v': 'Manga'}, {'n': 3, 'v': 'Authors'}],
                'description': 'Content Type'
            },
            {
                'fid': 'b',
                'type': 'se',
                'values': [{'n': 1, 'v': 'TV'}, {'n': 2, 'v': 'Manga'}, {'n': 3, 'v': 'Movie'}],
                'description': 'Type'
            }
        ],
        'type': 'it',
        'read': 'pu'
    },
    {
        'id': 2, 'name': 'Title', 'description': 'Main title field',
        'fields': [
            {
                'fid': 'a',
                'name': 'Title', 'type': 'te', 'description': 'Title'},
        ],
        'type': 'it',
        'read': 'pu'
    },
    {
        'id': 3, 'name': 'Genres', 'description': 'List of genres field',
        'fields': [
            {
                'fid': 'a',
                'name': 'Genre', 'type': 'se', 'values': [{'n': 1, 'v': 'Comedy'}, {'n': 2, 'v': 'Action'}]
            },
            {
                'fid': 'b',
                'name': 'Genre Value', 'type': 'va', 'values': [0, 1, 0.1]
            }
        ],
        'type': 'it',
        'read': 'pu'
    },
    {
        'id': 4, 'name': 'User Score', 'description': 'User score',
        'fields': [
            {
                'fid': 'a',
                'name': 'Digit score', 'type': 'va', 'values': [0, 10, 0.1]},
            {
                'fid': 'b', 'name': 'Boolean score', 'type': 'va', 'values': [0, 1, 1]},
            {
                'fid': 'c', 'name': 'Date', 'type': 'da'},
            {
                'fid': 'd', 'name': 'Status', 'type': 'se',
                'values': [{'n': 1, 'v': 'Complete'}, {'n': 2, 'v': 'Plan to watch'}]},
            {
                'fid': 'e', 'name': 'Comment', 'type': 'te'}
        ],
        'type': 'us',
        'read': 'pu'
    },
    {
        'id': 5, 'name': 'related', 'description': 'Related items',
        'fields': [
            {
                'fid': 'a', 'name': 'Type', 'type': 'se',
                'values': [{'n': 1, 'v': 'Sequel'}, {'n': 2, 'v': 'Prequel'}]},
            {
                'fid': 'b', 'name': 'Item', 'type': 'link'}
        ],
        'type': 'it',
        'read': 'pu'
    },
    {
        'id': 6, 'name': 'System recommendation', 'description': 'Automatic system recommendation score',
        'fields': [
            {
                'fid': 'a',
                'type': 'va',
                'values': [0, 10, 0.1]
            }
        ],
        'type': 'us',
        'read': 'pe'
    },
    {
        'id': 7, 'name': 'Average', 'description': 'Average score',
        'fields': [
            {
                'fid': 'a',
                'type': 'va',
                'values': [0, 10, 0.1],
                'description': 'Average score value'
            },
        ],
        'type': 'it',
        'read': 'pu'
    },
]


fields_copy = copy.deepcopy(fields_data)
for field_type in fields_copy:
    field_type['fields'] = {f['fid']: f for f in field_type['fields']}
fields_dict = {str(f['id']): f for f in fields_copy}

text_data = [
    {'_id': 1, 'text': 'Some name', 'f': '2', 's': 'a'},
    {'_id': 2, 'text': 'Good', 'f': '4', 's': 'e','s': 'e', 'u': 1},
    {'_id': 3, 'text': 'Bad', 'f': '4', 's': 'e', 'u': 1},
    {'_id': 4, 'text': 'Some manga name', 'f': '2', 's': 'a'},
    {'_id': 5, 'text': 'Some movie name', 'f': '2', 's': 'a'},
]

idtext = {t['_id']: t['text'] for t in text_data}


def get_id(state=[0]):
    state[0] += 1
    return state[0]


items_data = [
    {
        'i': 1,
        '1': [{'a': 1, 'b': 1, 'id': get_id(), 'li': 1}],
        '2': [{'a': 1, 'id': 2, 'li': 1}],
        '3': [
            {'a': 1, 'b': 0.5, 'id': get_id(), 'li': 1},
            {'a': 2, 'b': 0.6, 'id': get_id(), 'li': 1}
        ],
        '7': [{'a': 5, 'id': get_id(), 'li': 1}]

    },
    {
        'i': 2,
        '1': [{'a': 2, 'b': 2, 'id': get_id(), 'li': 1}],
        '2': [{'a': 4, 'id': get_id(), 'li': 1}],
        '3': [
            {'a': 2, 'b': 0.3, 'id': get_id(), 'li': 1}
        ],
        '7': [{'a': 4, 'id': get_id(), 'li': 1}]

    },
    {
        'i': 3,
        '1': [{'a': 1, 'b': 3, 'id': get_id(), 'li': 1}],
        '2': [{'a': 5, 'id': get_id(), 'li': 1}],
        '3': [
            {'a': 1, 'b': 0.6, 'id': get_id(), 'li': 1}
        ],

    },
    {
        'i': 4,
        '1': [{'a': 2, 'b': 2, 'id': get_id(), 'li': 1}],
        '3': [
            {'a': 1, 'b': 0.8, 'id': get_id(), 'li': 1},
            {'a': 2, 'b': 0.2, 'id': get_id(), 'li': 1}
        ],
        '7': [{'id': get_id(), 'li': 1}]

    }

]

user_scores = [
    {
        'u': -1,
        'i': 1,
        'pr': 0
    },
    {
        'u': -1,
        'i': 2,
        'pr': 0
    },
    {
        'u': -1,
        'i': 3,
        'pr': 0
    },
    {
        'u': -1,
        'i': 4,
        'pr': 0
    },
    {
        'u': 1,
        'i': 1,
        '4': [
            {'a': 5, 'd': 1, 'id': get_id(), 'pr': 1},
            {'a': 6, 'd': 1, 'e': 2, 'id': get_id(), 'pr': 0}
        ],
        'pr': 0
    },
    {
        'u': 1,
        'i': 2,
        '4': [
            {'d': 2, 'e': 3, 'id': get_id(), 'pr': 0},
        ],
        'pr': 0
    },
    {
        'u': 1,
        'i': 3,
        '4': [
            {'a': 2, 'd': 1, 'id': get_id(), 'pr': 1}
        ],
        'pr': 0
    },
    {
        'u': 2,
        'i': 1,
        '4': [
            {'a': 7, 'd': 1, 'id': get_id(), 'pr': 0}
        ],
        'pr': 0
    },
    {
        'u': 2,
        'i': 2,
        '4': [
            {'a': 2, 'd': 1, 'id': get_id(), 'pr': 0},
            {'a': 6, 'd': 1, 'id': get_id(), 'pr': 0}
        ],
        'pr': 0
    },
    {
        'u': 2,
        'i': 3,
        '4': [
            {'a': 7, 'd': 1, 'id': get_id(), 'pr': 0},
            {'a': 6, 'd': 1, 'id': get_id(), 'pr': 0}
        ],
        'pr': 0
    },
    {
        'u': 3,
        'i': 1,
        '4': [
            {'a': 7, 'd': 1, 'id': get_id(), 'pr': 3}
        ],
        'pr': 0
    },
    {
        'u': 3,
        'i': 2,
        '4': [
            {'a': 2, 'd': 1, 'id': get_id(), 'pr': 0}
        ],
        'pr': 3
    }
]

# собирательный филды для групп
# филды для сортировки
