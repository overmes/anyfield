import copy
from django.conf import settings
import pymongo
from pymongo.errors import OperationFailure


con = pymongo.MongoClient(host=settings.MONGODB.get('HOST'), port=settings.MONGODB.get('PORT'))


class TextQueryDriver:
    def __init__(self, all_fields=None, w=None):
        self._con = con
        self._db = self._con[settings.MONGODB['NAME']]
        self.text_col = self._db.text
        self.stats_col = self._db.stats
        self.w = w


    def get_next_text_id(self):
        count_obj = self.stats_col.find_and_modify({'_id': 'text'}, {'$inc': {'count': 1}}, new=True, upsert=True)
        return count_obj['count']

    def insert_text(self, field_id, text, item_id, fid, parent_id):
        next_id = self.get_next_text_id()
        self.text_col.insert({'id': field_id, '_id': next_id, 'text': text,
                              'i': item_id, 's': fid, 'f': str(parent_id)}, w=self.w)
        return next_id

    def replace_text(self, field_id, item_id, parent_id, data, text_fid):
        for fid in filter(lambda fid: fid in text_fid, data):
            current_id = self.insert_text(field_id, data[fid], item_id, fid, parent_id)
            data[fid] = current_id

    def update_text(self, field_id, text, item_id, fid, parent_id):
        doc = self.text_col.find_and_modify({'id': field_id, 'i': item_id, 's': fid, 'f': str(parent_id)},
                                            {'$set': {'text': text}}, w=self.w)
        return doc['_id'] if doc else self.insert_text(field_id, text, item_id, fid, parent_id)

    def update_replace_text(self, field_id, item_id, parent_id, data, text_fid):
        for fid in filter(lambda fid: fid in text_fid, data):
            current_id = self.update_text(field_id, data[fid], item_id, fid, parent_id)
            data[fid] = current_id

    def replace_text_ids(self, field_id, data):
        text = self.text_col.find({'id': field_id})
        for t in text:
            if t['s'] in data:
                data[t['s']] = t['text']


class UtilQueryDriver:
    def __init__(self, all_fields=None, w=None):
        self._con = con
        self._db = self._con[settings.MONGODB['NAME']]
        self.items_col = self._db.items
        self.users_col = self._db.users
        self.fields_col = self._db.fields
        self.text_col = self._db.text

    def delete_type(self, parent_id):
        self.users_col.update({}, {'$unset': {str(parent_id): ''}}, multi=True)
        self.items_col.update({}, {'$unset': {str(parent_id): ''}}, multi=True)
        self.text_col.remove({'f': str(parent_id)}, multi=True)

    def delete_item_index(self, parent_id, fid):
        try:
            self.items_col.drop_index([('{}.{}'.format(parent_id, fid), pymongo.ASCENDING)])
        except OperationFailure:
            pass

    def delete_user_index(self, parent_id, fid):
        try:
            self.users_col.drop_index([('{}.{}'.format(parent_id, fid), pymongo.ASCENDING)])
        except OperationFailure:
            pass

    def create_item(self, item_id):
        self.items_col.insert({'i': item_id})

        item_null_field = {'u': -1, 'i': item_id, 'pr': 0}
        self.users_col.insert(item_null_field)

    def create_item_index(self, parent_id, fid):
        self.items_col.ensure_index([('{}.{}'.format(parent_id, fid), pymongo.ASCENDING)])

    def create_user_index(self, parent_id, fid):
        self.users_col.ensure_index([('{}.{}'.format(parent_id, fid), pymongo.ASCENDING)])

    def _delete_fid(self, col, parent_id, fid):
        str_parent = str(parent_id)
        query_result = col.aggregate([{'$match': {str_parent: {'$exists': True}}},
                                      {'$project': {'s': {'$size': '$' + str_parent}}}, {'$sort': {'s': -1}}])
        max_size = query_result['result'][0]['s'] if query_result['result'] else 0
        if max_size:
            for num in range(max_size):
                full_path = '{0}.{1}.{2}'.format(parent_id, num, fid)
                col.update({}, {'$unset': {full_path: ''}}, multi=True)
        self.text_col.remove({'f': str_parent, 's': fid})

    def delete_item_fid(self, parent_id, fid):
        self._delete_fid(self.items_col, parent_id, fid)

    def delete_user_fid(self, parent_id, fid):
        self._delete_fid(self.users_col, parent_id, fid)

    def get_title(self, item_id):
        return (self.text_col.find_one({'f': '2', 's': 'a', 'i': item_id}) or {}).get('text')

    def get_id_by_mal_id(self, item_id, item_type):
        query = {'11': {'$elemMatch': {'a': item_id}}, '10': {'$elemMatch': {'a': item_type}}}
        result = self.items_col.find_one(query)
        return result['i'] if result else None


class QueryDriver(TextQueryDriver, UtilQueryDriver):
    def __init__(self, all_fields=None, w=None):
        super().__init__(all_fields=all_fields, w=w)
        self._con = con
        self._db = self._con[settings.MONGODB['NAME']]
        self.items_col = self._db.items
        self.users_col = self._db.users
        self.fields_col = self._db.fields
        self.text_col = self._db.text
        self.stats_col = self._db.stats
        self.w = w
        if all_fields:
            cloned_fields = copy.deepcopy(all_fields)
            for field_type in cloned_fields:
                field_type['fields'] = {f['fid']: f for f in field_type['fields']}
            self.all_fields = {str(f['id']): f for f in cloned_fields}

    def close(self):
        self._con.close()

    def insert_user_field(self, field_id, item_id, parent_id, user_id, data, text_fid):
        data['id'] = field_id
        data['pr'] = self.get_pr(data.get('pr', 0), user_id)

        self.replace_text(field_id, item_id, parent_id, data, text_fid)

        doc = {'$push': {str(parent_id): data}}
        res = self.users_col.update({'i': item_id, 'u': user_id}, doc, upsert=True, w=self.w)
        if not res['updatedExisting']:
            self.users_col.update({'i': item_id, 'u': user_id}, {'$set': {'pr': 0}}, w=self.w)

    def insert_item_field(self, field_id, item_id, parent_id, data, text_fid):
        data['id'] = field_id
        self.replace_text(field_id, item_id, parent_id, data, text_fid)
        doc = {'$push': {str(parent_id): data}}
        self.items_col.update({'i': item_id}, doc, w=self.w)

    def get_item_field(self, field_id, item_id, parent_id):
        res = self.items_col.find_one({'i': item_id, str(parent_id): {'$elemMatch': {'id': field_id}}},
                                      {str(parent_id) + '.$': 1, 'i': 1})
        res = res[str(parent_id)][0] if res else None
        self.replace_text_ids(field_id, res) if res else None
        return res

    def get_user_field(self, field_id, item_id, parent_id, user_id):
        res = self.users_col.find_one({'i': item_id, 'u': user_id, str(parent_id): {'$elemMatch': {'id': field_id}}},
                                      {str(parent_id) + '.$': 1, 'i': 1})
        res = res[str(parent_id)][0] if res else None
        self.replace_text_ids(field_id, res) if res else None
        return res

    def get_pr(self, pr, user_id):
        return user_id if pr > 0 else 0

    def update_item_field(self, field_id, item_id, parent_id, data, text_fid):
        data['id'] = field_id
        self.update_replace_text(field_id, item_id, parent_id, data, text_fid)
        doc = {"$set": {str(parent_id) + '.$': data}}
        self.items_col.update({'i': item_id, str(parent_id): {'$elemMatch': {'id': field_id}}}, doc, w=self.w)

    def update_user_field(self, field_id, item_id, parent_id, user_id, data, text_fid):
        data['id'] = field_id
        data['pr'] = self.get_pr(data.get('pr', 0), user_id)

        self.update_replace_text(field_id, item_id, parent_id, data, text_fid)

        doc = {"$set": {str(parent_id) + '.$': data}}
        self.users_col.update({'i': item_id, 'u': user_id, str(parent_id): {'$elemMatch': {'id': field_id}}}, doc,
                              w=self.w)

    def delete_item_field(self, field_id, item_id, parent_id):
        field = self.get_item_field(field_id, item_id, parent_id)
        self.items_col.update({'i': item_id}, {'$pull': {str(parent_id): {'id': field_id}}})
        self.text_col.remove({'id': field_id}, multi=True)
        return field

    def delete_user_field(self, field_id, item_id, parent_id, user_id):
        self.users_col.update({'i': item_id, 'u': user_id}, {'$pull': {str(parent_id): {'id': field_id}}})
        self.text_col.remove({'id': field_id}, multi=True)