import copy
import pprint
import logging
import reprlib
import time
import inspect

from bson import SON


class QueryLogger:
    def __init__(self):
        self._pp = pprint.PrettyPrinter(width=160)
        self._repr = reprlib.Repr()
        self._repr.maxlevel = 10
        self.timers = []
        self.start_time = time.time()
        self.full_time = None
        self.logger = logging.getLogger('django')

    def add_timer(self, message, last=False):
        now = time.time()
        before_operations_time = self.timers_sum()
        last_op_time = round(now - self.start_time - before_operations_time, 3)
        self.timers.append({message: last_op_time})

        if last and not self.full_time:
            self.full_time = self.timers_sum()
            self.timers.append({'full': self.full_time})
        return self.full_time

    def get_full_time(self):
        if not self.full_time:
            self.full_time = self.timers_sum()
            self.timers.append({'full': self.full_time})
        return self.full_time

    def timers_sum(self):
        return round(sum([sum(t.values()) for t in self.timers]), 3)

    def log(self, level, message, format=True):
        stack = inspect.stack()
        log_path = '{}:\t\t{}:\t\t'.format(stack[1][1], stack[1][3])
        if format:
            getattr(self.logger, level)(log_path + self.format_message(message))
        else:
            getattr(self.logger, level)(log_path + message)

    def format_message(self, message):
        return self._pp.pformat(self._repr.repr(message))


class AbstractItemQuerySolver:
    MAX_LIMIT = 1000

    def __init__(self, index, driver, logger, user, query, sort=None, limit=100, skip=0):
        self._driver = self._dr = driver
        self._index = index
        self._log = logger
        self._user = user
        self._query = copy.deepcopy(query.copy())
        self._sort = sort
        self._limit = limit if limit <= self.MAX_LIMIT else self.MAX_LIMIT
        self._skip = skip

    def get_matched_items(self, matched_items=None, excluded_items=None):
        raise NotImplementedError()

    def has_query(self):
        return 'query' in self._query

    def make_last_query(self, matched_items=None, excluded_items=None):
        raise NotImplementedError()

    def make_fields_query(self, matched_items):
        raise NotImplementedError()

    def get_all_items(self):
        agg_query = [
            {'$group': {'_id': None, 'items': {'$addToSet': '$i'}}}
        ]

        self._log.log('debug', agg_query)
        agg_result = self._dr.items_col.aggregate(agg_query)
        self._log.add_timer('get all items'.format(self._index))
        return agg_result['result'][0]['items'] if agg_result['result'] else []

    def _replace_text_fields(self, current_query, field_num=None):
        if isinstance(current_query, list):
            for next_query in current_query:
                self._replace_text_fields(next_query, field_num)
        elif isinstance(current_query, dict):
            for n, next_query in current_query.items():
                if n.isdigit():
                    self._replace_text_fields(next_query, n)
                # TODO: can be error
                elif len(n) < 2:
                    # TODO: can be error
                    if field_num and self._dr.all_fields[field_num]['fields'][n]['type'] == 'te':
                        if '$not' in next_query:
                            current_query[n] = {
                                '$nin': self._get_text_search_items(next_query['$not']['$eq'], field_num, n)}
                        else:
                            current_query[n] = {'$in': self._get_text_search_items(next_query, field_num, n)}
                else:
                    self._replace_text_fields(next_query, field_num)

    def _get_text_search_items(self, query, field_num, field_name):
        agg_query = [
            {'$match': {'$text': {'$search': query}, 'f': field_num, 's': field_name}},
            {'$group': {'_id': None, 'ids': {'$addToSet': '$_id'}}}
        ]
        self._log.log('debug', agg_query)
        agg_result = self._dr.text_col.aggregate(agg_query)
        res = agg_result['result'][0]['ids'] if agg_result['result'] else []
        return res

    def _get_projection_for_fields(self):
        result = {'i': '$i', '_id': 0}
        for field in self._query.get('fields', []):
            result[field] = {"$ifNull": ['$' + field, []]}
        self._add_sort_projection(result)
        return result

    def _add_sort_projection(self, project):
        if self._sort and self._sort[0] == self._index:
            sort_str_key = self._sort[1][0][0].split('.')[0]
            project[sort_str_key] = {"$ifNull": ['$' + sort_str_key, []]}

    def _remove_empty_list_projection(self):
        result = {'i': '$i', '_id': 0}
        for field in self._query.get('fields', []):
            result[field] = self._remove_field(field)
        if self._sort and self._sort[0] == self._index:
            sort_str_key = self._sort[1][0][0].split('.')[0]
            result[sort_str_key] = self._remove_field(sort_str_key)
        return result

    def _remove_field(self, field):
        return {
            '$cond':
                {
                    'if': {'$eq': ['$' + field, []]}, 'then': None, 'else': '$' + field
                }
        }


class ItemQuerySolver(AbstractItemQuerySolver):
    def __init__(self, query_index, driver, logger, user, query, sort=None, limit=None, skip=0):
        super().__init__(query_index, driver, logger, user, query, sort, limit, skip)

    def get_matched_items(self, matched_items=None, excluded_items=None):
        current_query = copy.deepcopy(self._query['query'])
        self._replace_text_fields(current_query)
        self._log.add_timer('{} replace_text'.format(self._index))

        current_query.update({'i': {'$in': matched_items}}) if matched_items is not None else None
        current_query.update({'i': {'$nin': excluded_items}}) if excluded_items else None

        agg_query = [
            {'$match': current_query},
            {'$group': {'_id': None, 'items': {'$addToSet': '$i'}}}
        ]

        self._log.log('debug', agg_query)
        agg_result = self._dr.items_col.aggregate(agg_query)
        return agg_result['result'][0]['items'] if agg_result['result'] else []

    def make_last_query(self, matched_items=None, excluded_items=None, project=True):
        agg_query = []

        if self.has_query() or matched_items is not None or excluded_items:
            current_query = copy.deepcopy(self._query.get('query', {}))
            self._replace_text_fields(current_query)
            current_query.update({'i': {'$in': matched_items}}) if matched_items is not None else None
            current_query.update({'i': {'$nin': excluded_items}}) if excluded_items else None
            agg_query.append({'$match': current_query})

        if self._sort:
            agg_query.append({'$sort': SON(self._sort[1])})

        agg_query.extend([
            {'$skip': self._skip},
            {'$limit': self._limit},
        ])
        if project:
            project = self._get_projection_for_fields()
            agg_query.append({'$project': project})

        self._log.log('debug', agg_query)
        result = self._dr.items_col.aggregate(agg_query)['result']
        return result

    def make_fields_query(self, matched_items, project=True):
        current_query = {'i': {'$in': matched_items}}

        agg_query = [
            {'$match': current_query},
        ]
        if project:
            project = self._get_projection_for_fields()
            agg_query.append({'$project': project})

        self._log.log('debug', agg_query)
        result = self._dr.items_col.aggregate(agg_query)['result']
        return result


class UserQuerySolver(AbstractItemQuerySolver):
    def __init__(self, query_index, driver, logger, user, query, sort=None, limit=None, skip=0):
        super().__init__(query_index, driver, logger, user, query, sort, limit, skip)
        self._query_user_index = int(self._index)

    def get_matched_items(self, matched_items=None, excluded_items=None):
        current_query = copy.deepcopy(self._query['query'])
        self._replace_text_fields(current_query)
        self._log.add_timer('{} replace_text'.format(self._index))

        agg_query = self._get_user_query_agg(current_query, matched_items, excluded_items)

        agg_query.append({'$group': {'_id': None, 'items': {'$addToSet': '$i'}}})

        self._log.log('debug', agg_query)
        agg_result = self._dr.users_col.aggregate(agg_query)
        return agg_result['result'][0]['items'] if agg_result['result'] else []

    def make_last_query(self, matched_items=None, excluded_items=None, project=True):

        current_query = copy.deepcopy(self._query.get('query', {}))
        self._replace_text_fields(current_query)
        agg_query = self._get_user_query_agg(current_query, matched_items, excluded_items)

        if self._sort:
            agg_query.append({'$sort': SON(self._sort[1])})

        agg_query += [
            {'$skip': self._skip},
            {'$limit': self._limit},
        ]
        if project:
            project = self._get_projection_for_fields()
            agg_query.append({'$project': project})

        self._log.log('debug', agg_query)

        result = self._dr.users_col.aggregate(agg_query)['result']
        return result

    def make_fields_query(self, matched_items, project=True):
        query = None if project else {'u': {'$ne': -1}}
        agg_query = self._get_user_query_agg(query, matched_items)

        if project:
            project = self._get_projection_for_fields()
            agg_query += [{'$project': project}]

        self._log.log('debug', agg_query)
        result = self._dr.users_col.aggregate(agg_query)['result']
        return result

    def _get_user_query_agg(self, query, matched_items=None, excluded_items=None):
        user_items = self._get_user_items_ids()
        self._log.add_timer('{} get_user_items'.format(self._index))

        user_match = self._get_user_filter_with_nulls(matched_items, excluded_items, user_items=user_items)
        agg_query = [{'$match': user_match}]
        if self._user == self._query_user_index:
            if query:
                agg_query = [{'$match': {'$and': [user_match, query]}}]
        else:
            agg_query.append({'$sort': {'i': 1}})  # shuffle without sorting
            agg_query.append({'$redact': self._get_redact_filter()})

            remove_empty_list_projection = self._remove_empty_list_projection()
            agg_query.append({'$project': remove_empty_list_projection})

            agg_query.append({'$match': query}) if query else None
        return agg_query

    def _get_user_filter_with_nulls(self, matched_items=None, excluded_items=None, user_items=None):
        user_match = {'$or': [
            {'u': self._query_user_index},
            {'u': -1, 'i': {'$nin': user_items}}
        ]}
        user_match.update({'i': {'$in': matched_items}}) if matched_items is not None else None
        user_match.update({'i': {'$nin': excluded_items}}) if excluded_items else None
        return user_match

    def _get_user_items_ids(self):
        user_match = {'u': self._query_user_index, 'pr': {'$in': [0, self._user]}}
        agg_query = [
            {'$match': user_match},
            {'$group': {'_id': None, 'items': {'$addToSet': '$i'}}}
        ]
        self._log.log('debug', agg_query)
        agg_result = self._dr.users_col.aggregate(agg_query)
        return agg_result['result'][0]['items'] if agg_result['result'] else []

    def _get_redact_filter(self):
        user_reduct = {'$cond':
                           {
                               'if': {'$or': [{'$eq': ["$pr", self._user]}, {'$eq': ["$pr", 0]}]},
                               'then': "$$DESCEND",
                               'else': "$$PRUNE"
                           }}
        return user_reduct