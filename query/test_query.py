from copy import deepcopy
from django.conf import settings
from django.test import TestCase, override_settings
import pprint
import pymongo
from .fields_examples import *
from query.query import QuerySolver, SingleQuerySolver


@override_settings(MONGODB={'NAME': 'anyfield_test'})
class DefaultTestCase(TestCase):
    maxDiff = None

    def setUp(self):
        self.con = pymongo.MongoClient()
        self.con.drop_database('anyfield_test')
        self.db = self.con[settings.MONGODB['NAME']]
        self.all_fields = fields_data
        items_col = self.db.items
        users_col = self.db.users
        text_col = self.db.text

        items_col.create_index([('i', pymongo.ASCENDING)])
        users_col.create_index([('i', pymongo.ASCENDING)])
        users_col.create_index([('u', pymongo.ASCENDING)])
        for one_field_data in fields_data:
            for field in one_field_data['fields']:
                full_field_name = '{0}.{1}'.format(one_field_data['id'], field['fid'])
                if type == 'item':
                    items_col.create_index([(full_field_name, pymongo.ASCENDING)])
                else:
                    users_col.create_index([(full_field_name, pymongo.ASCENDING)])
        text_col.create_index([('text', 'text'), ('f', pymongo.ASCENDING), ('s', pymongo.ASCENDING), ('u', pymongo.ASCENDING)])

        items_col.insert(items_data, manipulate=False)
        users_col.insert(user_scores, manipulate=False)
        text_col.insert(text_data, manipulate=False)

    def tearDown(self):
        self.con.drop_database('anyfield_test')


class DriverTestCase(DefaultTestCase):
    def replace_ids_to_text(self, result):
        result = deepcopy(result)
        for i in result:
            for index, r in i.items():
                for field_num, sub_fields in r.items():
                        if isinstance(sub_fields, list):
                            for field in sub_fields:
                                for field_name, value in field.items():
                                    if isinstance(value, int) and fields_dict[field_num]['fields'].get(field_name, {}).get('type') == 'te':
                                        field[field_name] = idtext[value]
        return result

    def test_field_query(self):
        #query, fields
        query_solver = QuerySolver(self.all_fields, user=1, query={})
        result = query_solver.solve()
        answer = [{'i': {'i': i['i']}} for i in items_data]
        pprint.pprint(answer)
        self.assertListEqual(sorted(answer, key=lambda x: x['i']['i']), sorted(result, key=lambda x: x['i']['i']))

        query_solver = QuerySolver(self.all_fields)
        result = query_solver.solve()
        answer = [{'i': {'i': i['i']}} for i in items_data]
        pprint.pprint(answer)
        self.assertListEqual(sorted(answer, key=lambda x: x['i']['i']), sorted(result, key=lambda x: x['i']['i']))

        query_solver = QuerySolver(self.all_fields, user=1, query={'i': {'fields': ['1']}})
        result = query_solver.solve()
        answer = [{'i': {'1': i.get('1'), 'i': i['i']}} for i in items_data]
        pprint.pprint(answer)
        self.assertListEqual(sorted(answer, key=lambda x: x['i']['i']), sorted(result, key=lambda x: x['i']['i']))

        query_solver = QuerySolver(self.all_fields, user=1, query={
            'i': {'query': {'$and': [{'$and': [{'1': {'$elemMatch': {'a': 1}}}]}]}}
        })
        result = query_solver.solve()
        answer = [{'i': {'i': i['i']}} for i in items_data if any([True for f in i.get('1') if f.get('a') == 1])]
        pprint.pprint(answer)
        self.assertListEqual(sorted(answer, key=lambda x: x['i']['i']), sorted(result, key=lambda x: x['i']['i']))

        query_solver = QuerySolver(self.all_fields, user=1, query={
            'i': {'query': {'$and': [{'$and': [{'7': {'$elemMatch': {'a': 4}}}]}]}}
        })
        result = query_solver.solve()
        answer = [{'i': {'i': i['i']}} for i in items_data if any([True for f in i.get('7', []) if f.get('a') == 4])]
        pprint.pprint(answer)
        self.assertListEqual(sorted(answer, key=lambda x: x['i']['i']), sorted(result, key=lambda x: x['i']['i']))

        query_solver = QuerySolver(self.all_fields, user=1, query={
            'i': {'query': {'$and': [{'$and': [{'1': {'$elemMatch': {'a': 1}}}]}]}, 'fields': ['1']},
            '1': {'query': {'$and': [{'$and': [{'4': {'$elemMatch': {'d': 1}}}]}]}, 'fields': ['4']}
        })
        result = query_solver.solve()
        user_answer = {
            i['i']: {'4': i['4'], 'i': i['i']} for i in user_scores if any(
                [True for f in i.get('4', []) if f.get('d') == 1]) and i['u'] == 1
        }
        answer = [
            {'i': {'i': i['i'], '1': i['1']}, '1': user_answer[i['i']]} for i in items_data if any(
                [True for f in i.get('1') if f.get('a') == 1]) and i['i'] in user_answer
        ]
        answer = self.replace_ids_to_text(answer)
        pprint.pprint(answer)
        self.assertListEqual(sorted(answer, key=lambda x: x['i']['i']), sorted(result, key=lambda x: x['i']['i']))

        query_solver = QuerySolver(self.all_fields, user=1, query={
            'i': {'query': {'$and': [{'$and': [{'1': {'$elemMatch': {'a': 1}}}]}]}, 'fields': ['1']},
            '1': {'fields': ['4']}
        })
        result = query_solver.solve()
        user_answer = {
            i['i']: {'4': i['4'], 'i': i['i']} for i in user_scores if i['u'] == 1
        }
        answer = [
            {'i': {'i': i['i'], '1': i['1']}, '1': user_answer[i['i']]} for i in items_data if any(
                [True for f in i.get('1') if f.get('a') == 1])
        ]
        answer = self.replace_ids_to_text(answer)
        pprint.pprint(answer)
        self.assertListEqual(sorted(answer, key=lambda x: x['i']['i']), sorted(result, key=lambda x: x['i']['i']))

        query_solver = QuerySolver(self.all_fields, user=1, query={
            'i': {'fields': ['1']},
            '1': {'fields': ['4']}
        }, sort=('1', (('4.a', 1), )))
        result = query_solver.solve()
        user_answer = {
            i['i']: {'4': i['4'], 'i': i['i']} for i in user_scores if i['u'] == 1
        }
        answer = [
            {'i': {'i': i['i'], '1': i['1']}, '1': user_answer.get(i['i'], {'4': [], 'i': i['i']})} for i in items_data
        ]
        answer.sort(key=lambda a: min(a['1']['4'] or [{'a': -0.1}], key=lambda x: x.get('a', 0)).get('a', 0))
        pprint.pprint(answer)
        answer = self.replace_ids_to_text(answer)
        self.assertListEqual(answer, result)

        query_solver = QuerySolver(self.all_fields, user=1, query={
            'i': {'query': {'$and': [{'$and': [{'7': {'$elemMatch': {'$or': [{'a': 4}, {'a': 5}]}}}]}]}}
        })
        result = query_solver.solve()
        answer = [{'i': {'i': i['i']}} for i in items_data if any([True for f in i.get('7') or [{}] if f.get('a') in [4, 5]])]
        pprint.pprint(answer)
        self.assertListEqual(sorted(answer, key=lambda x: x['i']['i']), sorted(result, key=lambda x: x['i']['i']))

    def test_sort_query(self):
        query_solver = QuerySolver(self.all_fields, user=1, query={'i': {'fields': ['7']}}, sort=('i', (('7.a', 1), )))
        result = query_solver.solve()
        answer = [
            {'i': {'i': i['i'], '7': i.get('7', [])}} for i in items_data
        ]
        answer.sort(key=lambda a: min(a['i'].get('7') or [{}], key=lambda x: x.get('a')).get('a', 0))
        pprint.pprint(answer)
        self.assertListEqual(answer, result)

        query_solver = QuerySolver(self.all_fields, user=1, query={'i': {'fields': ['7']}}, sort=('i', (('7.a', 1), )), skip=1, limit=1)
        result = query_solver.solve()
        answer = [
            {'i': {'i': i['i'], '7': i.get('7', [])}} for i in items_data
        ]
        answer.sort(key=lambda a: min(a['i'].get('7') or [{}], key=lambda x: x.get('a')).get('a', 0))
        answer = answer[1:2]
        pprint.pprint(answer)
        self.assertListEqual(answer, result)

        query_solver = QuerySolver(self.all_fields, user=1, query={}, sort=('11', (('4.a', 1), )))
        result = query_solver.solve()

        query_solver = QuerySolver(self.all_fields, user=1, query={'1': {'fields': ['4']}}, sort=('1', (('4.a', 1), )))
        result = query_solver.solve()
        user_answer = {
            i['i']: {'4': i['4'], 'i': i['i']} for i in user_scores if i['u'] == 1
        }
        answer = [
            {'1': user_answer.get(i['i'], {'4': [], 'i': i['i']})} for i in items_data]
        answer.sort(key=lambda a: min(a['1']['4'] or [{'a': -0.1}], key=lambda x: x.get('a', 0)).get('a', 0))
        answer = self.replace_ids_to_text(answer)
        pprint.pprint(answer)
        self.assertListEqual(answer, result)

        query_solver = QuerySolver(self.all_fields, user=1, query={}, sort=('1', (('4.a', 1), )))
        result = query_solver.solve()

        query_solver = QuerySolver(self.all_fields, user=1, query={'2': {'fields': ['4']}}, sort=('1', (('4.a', 1), )))
        result = query_solver.solve()

    def test_text_query(self):
        query_solver = QuerySolver(self.all_fields, user=1, query={
            'i': {'query': {'$and': [{'$and': [{'2': {'$elemMatch': {'a': 'manga'}}}]}]}, 'fields': ['2']}
        })
        result = query_solver.solve()
        text_ids = {t['_id']: t['text'] for t in text_data if 'manga' in t['text']}
        answer = [{'i': {'i': i['i'], '2': i.get('2')}} for i in items_data if any(f.get('a') in text_ids for f in i.get('2', []))]
        answer = self.replace_ids_to_text(answer)
        pprint.pprint(answer)
        self.assertListEqual(sorted(answer, key=lambda x: x['i']), sorted(result, key=lambda x: x['i']))

    def test_null_query(self):
        query_solver = QuerySolver(self.all_fields, user=1, query={
            '1': {'query': {'$and': [{'$and': [{'4.a': None}]}]}}
        })
        result = query_solver.solve()
        not_null_user_items = [u['i'] for u in user_scores if u['u'] == 1 and any([True for f in u['4'] if f.get('a')])]
        answer = [{'1': {'i': i['i']}} for i in items_data if i['i'] not in not_null_user_items]
        pprint.pprint(answer)
        self.assertListEqual(sorted(answer, key=lambda x: x['1']['i']), sorted(result, key=lambda x: x['1']['i']))

        query_solver = QuerySolver(self.all_fields, user=1, query={
            'i': {'query': {'$and': [{'$and': [{'7.a': None}]}]}}
        })
        result = query_solver.solve()
        answer = [{'i': {'i': i['i']}} for i in items_data if any([True for f in i.get('7', [{}]) if f.get('a') is None])]
        pprint.pprint(answer)
        self.assertListEqual(sorted(answer, key=lambda x: x['i']['i']), sorted(result, key=lambda x: x['i']['i']))

        query_solver = QuerySolver(self.all_fields, user=1, query={
            'i': {'query': {'$and': [{'$and': [{'$or': [{'7': {'$elemMatch': {'a': 4}}}, {'7.a': None}]}]}]}}
        })
        result = query_solver.solve()
        answer = [{'i': {'i': i['i']}} for i in items_data if any([True for f in i.get('7') or [{}] if f.get('a') is None or f.get('a') == 4])]
        pprint.pprint(answer)
        self.assertListEqual(sorted(answer, key=lambda x: x['i']['i']), sorted(result, key=lambda x: x['i']['i']))

        query_solver = QuerySolver(self.all_fields, user=2, query={
            'i': {'fields': ['7']},
            '1': {'query': {'$and': [{'$and': [{'$or': [{'4.a': None}]}]}]}, 'fields': ['4']}
        }, sort=('i', (('7.a', -1), )))
        result = query_solver.solve()
        user_answer = {}
        for u in filter(lambda u: u['u'] == 1, user_scores):
            field = list(filter(lambda f: f.get('pr') == 0, u.get('4')))
            res = {'4': field, 'i': u['i']}
            user_answer[u['i']] = res
        answer = [{'i': {'i': i['i'], '7': i.get('7', [])}, '1': user_answer.get(i['i'], {'i': i['i'], '4': []})} for i in items_data if any(
            [True for f in user_answer.get(i['i'], {}).get('4') or [{}] if not f.get('a')])]
        answer.sort(key=lambda a: min(a['i'].get('7') or [{'a': -1}], key=lambda x: x.get('a', 0)).get('a', 0), reverse=True)
        answer = self.replace_ids_to_text(answer)
        pprint.pprint(answer)
        self.assertListEqual(answer, result)

        query_solver = QuerySolver(self.all_fields, user=1, query={
            'i': {'query': {'$or': [{'7.a': None}]}}
        })
        result = query_solver.solve()
        answer = [{'i': {'i': i['i']}} for i in items_data if any([True for f in i.get('7') or [{}] if f.get('a') is None])]
        pprint.pprint(answer)
        self.assertListEqual(sorted(answer, key=lambda x: x['i']['i']), sorted(result, key=lambda x: x['i']['i']))

    def test_private_scores(self):
        query_solver = QuerySolver(self.all_fields, user=2, query={
            '1': {'fields': ['4']}
        })
        result = query_solver.solve()
        user_answer = {
            i['i']: {'4': [f for f in i['4'] if f.get('pr') == 0], 'i': i['i']} for i in user_scores if i['u'] == 1 and any(True for f in i['4'] if f.get('pr') == 0)
        }
        answer = [
            {'1': user_answer.get(i['i'], {'4': [], 'i': i['i']})} for i in items_data
        ]
        answer = self.replace_ids_to_text(answer)
        pprint.pprint(answer)
        self.assertListEqual(sorted(answer, key=lambda x: x['1']['i']), sorted(result, key=lambda x: x['1']['i']))

        query_solver = QuerySolver(self.all_fields, user=2, query={
            '3': {'fields': ['4']}
        })
        result = query_solver.solve()
        user_answer = {
            i['i']: {'4': [f for f in i['4'] if f.get('pr') == 0], 'i': i['i']} for i in user_scores if i['u'] == 3 and i['pr'] == 0 and any(True for f in i['4'] if f.get('pr') == 0)
        }
        answer = [
            {'3': user_answer.get(i['i'], {'4': [], 'i': i['i']})} for i in items_data
        ]
        answer = self.replace_ids_to_text(answer)
        pprint.pprint(answer)
        self.assertListEqual(sorted(answer, key=lambda x: x['3']['i']), sorted(result, key=lambda x: x['3']['i']))

        query_solver = QuerySolver(self.all_fields, user=2, query={
            '1': {'fields': ['4']},
            'i': {'fields': ['7']}
        }, sort=('i', (('7.a', 1), )))
        result = query_solver.solve()
        user_answer = {
            i['i']: {'4': [f for f in i['4'] if f.get('pr') == 0], 'i': i['i']} for i in user_scores if i['u'] == 1 and any(True for f in i['4'] if f.get('pr') == 0)
        }
        answer = [
            {'i': {'7': i.get('7', []), 'i': i['i']}, '1': user_answer.get(i['i'], {'4': [], 'i': i['i']})} for i in items_data
        ]
        answer.sort(key=lambda a: min(a['i'].get('7') or [{}], key=lambda x: x.get('a')).get('a', 0))
        answer = self.replace_ids_to_text(answer)
        pprint.pprint(answer)
        self.assertListEqual(answer, result)

        query_solver = QuerySolver(self.all_fields, user=2, query={
            '1': {'query': {'$and': [{'$and': [{'4': {'$elemMatch': {'d': 1}}}]}]}, 'fields': ['4']},
            'i': {'fields': ['7']}
        }, sort=('i', (('7.a', 1), )))
        result = query_solver.solve()
        user_answer = {
            i['i']: {'4': [f for f in i['4'] if f.get('pr') == 0], 'i': i['i']} for i in user_scores if i['u'] == 1 and any(True for f in i['4'] if f.get('pr') == 0 and f.get('d') == 1)
        }
        answer = [
            {'i': {'7': i.get('7', []), 'i': i['i']}, '1': user_answer.get(i['i'], {'4': [], 'i': i['i']})} for i in items_data if i['i'] in user_answer
        ]
        answer.sort(key=lambda a: min(a['i'].get('7') or [{}], key=lambda x: x.get('a')).get('a', 0))
        answer = self.replace_ids_to_text(answer)
        pprint.pprint(answer)
        self.assertListEqual(answer, result)

    def test_not_query(self):
        query_solver = QuerySolver(self.all_fields, user=1, query={
            'i': {'query': {'$and': [{'$and': [{'7': {'$not': {'$elemMatch': {'a': 4}}}}]}]}}
        })
        result = query_solver.solve()
        answer = [{'i': {'i': i['i']}} for i in items_data if any([True for f in i.get('7') or [{}] if f.get('a') != 4])]
        pprint.pprint(answer)
        self.assertListEqual(sorted(answer, key=lambda x: x['i']['i']), sorted(result, key=lambda x: x['i']['i']))

        query_solver = QuerySolver(self.all_fields, user=1, query={
            'i': {'query': {'$and': [{'$and': [{'3': {'$not': {'$elemMatch': {'a': 2}}}}, {'3': {'$elemMatch': {'a': 1}}}]}]}}
        })
        result = query_solver.solve()
        answer = [{'i': {'i': i['i']}} for i in items_data if all([f.get('a') != 2 or f.get('a') == 1 for f in i.get('3') or [{}]])]
        pprint.pprint(answer)
        self.assertListEqual(sorted(answer, key=lambda x: x['i']['i']), sorted(result, key=lambda x: x['i']['i']))

        query_solver = QuerySolver(self.all_fields, user=1, query={
            'i': {'query': {'$and': [{'7': {'$not': {'$elemMatch': {'a': None}}}}, {'7.a': {'$not': {'$eq': None}}}]}}
        })
        result = query_solver.solve()
        answer = [{'i': {'i': i['i']}} for i in items_data if any([True for f in i.get('7') or [{}] if f.get('a') is not None])]
        pprint.pprint(answer)
        self.assertListEqual(sorted(answer, key=lambda x: x['i']['i']), sorted(result, key=lambda x: x['i']['i']))

        query_solver = QuerySolver(self.all_fields, user=1, query={
            'i': {'query': {'$and': [{'$and': [{'$or': [{'7': {'$elemMatch': {'a': {'$not': {'$eq': 4}}}}}, {'7.a': None}]}]}]}}
        })
        result = query_solver.solve()
        answer = [{'i': {'i': i['i']}} for i in items_data if any([True for f in i.get('7') or [{}] if f.get('a') != 4])]
        pprint.pprint(answer)
        self.assertListEqual(sorted(answer, key=lambda x: x['i']['i']), sorted(result, key=lambda x: x['i']['i']))

        query_solver = QuerySolver(self.all_fields, user=1, query={
            'i': {'query': {'$and': [{'$and': [{'$or': [{'7': {'$elemMatch': {'a': 4}}}, {'7.a': None}]}]}]}}
        })
        result = query_solver.solve()
        answer = [{'i': {'i': i['i']}} for i in items_data if any([True for f in i.get('7') or [{}] if f.get('a') is None or f.get('a') == 4])]
        pprint.pprint(answer)
        self.assertListEqual(sorted(answer, key=lambda x: x['i']['i']), sorted(result, key=lambda x: x['i']['i']))

        query_solver = QuerySolver(self.all_fields, user=1, query={
            'i': {'query': {'$and': [{'$and': [{'$or': [{'2': {'$elemMatch': {'a': {'$not': {'$eq': 'manga'}}}}}, {'2.a': None}]}]}]}, 'fields': ['2']}
        })
        result = query_solver.solve()
        text_ids = {t['_id']: t['text'] for t in text_data if 'manga' not in t['text']}
        answer = [{'i': {'i': i['i'], '2': i.get('2', [])}} for i in items_data if any(f.get('a') in text_ids or not f.get('a') for f in i.get('2', [{}]))]
        answer = self.replace_ids_to_text(answer)
        pprint.pprint(answer)
        self.assertListEqual(sorted(answer, key=lambda x: x['i']['i']), sorted(result, key=lambda x: x['i']['i']))

        query_solver = QuerySolver(self.all_fields, user=2, query={
            '1': {'query': {'$and': [{'4.a': {'$not': {'$eq': None}}}]}, 'fields': ['4']}
        })
        result = query_solver.solve()
        user_answer = {
            i['i']: {'4': [f for f in i['4'] if f.get('pr') == 0], 'i': i['i']} for i in user_scores if i['u'] == 1 and any(True for f in i['4'] if f.get('pr') == 0 and f.get('a') is not None)
        }
        answer = [{'1': user_answer[i['i']]} for i in items_data if i['i'] in user_answer]
        answer = self.replace_ids_to_text(answer)
        pprint.pprint(answer)
        self.assertListEqual(sorted(answer, key=lambda x: x['1']['i']), sorted(result, key=lambda x: x['1']['i']))

    def test_multi_user(self):
        query_solver = QuerySolver(self.all_fields, user=1, query={
            'i': {'fields': ['1']},
            '1': {'fields': ['4']},
            '2': {'fields': ['4']}
        }, sort=('i', (('1.a', 1), )))
        result = query_solver.solve()
        first_user_answer = {
            i['i']: i.get('4', []) for i in user_scores if i['u'] == 1
        }
        second_user_answer = {
            i['i']: i.get('4', []) for i in user_scores if i['u'] == 2
        }
        answer = [
            {'i': {'1': i['1'], 'i':i['i']},
             '1': {'4': first_user_answer.get(i['i'], []), 'i': i['i']},
             '2': {'4': second_user_answer.get(i['i'], []), 'i': i['i']},
            } for i in items_data
        ]
        answer = self.replace_ids_to_text(answer)
        answer.sort(key=lambda a: min(a['i']['1'] or [{'a': -0.1}], key=lambda x: x.get('a', 0)).get('a', 0))
        pprint.pprint(answer)
        self.assertListEqual(answer, result)

    def test_item_query(self):
        query_solver = SingleQuerySolver(self.all_fields, user=1, item=1)
        result = query_solver.solve()
        user_answer = {
            i['i']: {'4': i['4'], 'i': i['i'], 'pr': i['pr'], 'u': i['u']} for i in user_scores if i['u'] == 1
        }
        answer = [
            {'i': {'i': i['i'], '1': i['1'], '2': i['2'], '3': i['3'], '7': i['7']}, '1': user_answer[i['i']]} for i in items_data if i.get('i') == 1
        ]
        answer = self.replace_ids_to_text(answer)
        answer = answer[0]
        pprint.pprint(answer)
        self.assertDictEqual(answer, result)