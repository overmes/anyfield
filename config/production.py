from config.settings import *

DEBUG = False
TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['list.animeadvice.me']

SECRET_KEY = '123'

SESSION_COOKIE_DOMAIN = '.animeadvice.me'

RAVEN_CONFIG = {
    'dsn': '123123',
}

INSTALLED_APPS += ('raven.contrib.django.raven_compat',)

STATIC_URL = '/static/'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'anyfield',
        'USER': 'anyfield',
        'PASSWORD': '123123'
    }
}

MONGODB = {
    'NAME': 'anyfield_mal',
    'HOST': '127.0.0.1',
    'PORT': 27017
}

BROKER = 'amqp://anyfield:123123@localhost:5672/myvhost'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
        'KEY_PREFIX': 'anyfield'
    }
}

ITEM_FIELD_VOTE_UP_BORDER = 2
ITEM_FIELD_VOTE_DOWN_BORDER = 0
ITEM_FIELD_VOTE_DELETE_BORDER = -10

TYPE_VOTE_UP_BORDER = 2
TYPE_VOTE_DOWN_BORDER = 0
TYPE_VOTE_DELETE_BORDER = -10

FIELD_VOTE_UP_BORDER = 2
FIELD_VOTE_DOWN_BORDER = 0
FIELD_VOTE_DELETE_BORDER = -10

SELECT_VOTE_UP_BORDER = 2
SELECT_VOTE_DOWN_BORDER = 0
SELECT_VOTE_DELETE_BORDER = -10

RANGE_VOTE_UP_BORDER = 2
RANGE_VOTE_DOWN_BORDER = 0
RANGE_VOTE_DELETE_BORDER = -10