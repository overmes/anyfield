from celery import Celery
import requests
from requests.exceptions import RequestException
import ujson

app = Celery('tasks')
app.config_from_object('django.conf:settings')


@app.task
def load_user_scores(user_request_model):
    try:
        request = requests.get('http://localhost:2040/username/' + user_request_model.username)
        data = request.json()
        user_request_model.process_answer(data)
    except (RequestException, BaseException) as ex:
        user_request_model.set_error(ex)


@app.task
def load_recommendation(user_request_model):
    try:
        request = requests.post('http://localhost:2050/getAdvice',
                                data=ujson.dumps({'scores': user_request_model.get_mal_scores()}))
        data = request.json()
        user_request_model.process_answer(data)
    except (RequestException, BaseException) as ex:
        user_request_model.set_error(ex)


@app.task
def load_neighbours(user_request_model):
    try:
        data = ujson.dumps({'scores': user_request_model.get_mal_scores(),
                            'cross_count': user_request_model.cross_count})
        request = requests.post('http://localhost:2060/getNeighbours',
                                data=data)
        data = request.json()
        user_request_model.process_answer(data)
    except (RequestException, BaseException) as ex:
        user_request_model.set_error(ex)
