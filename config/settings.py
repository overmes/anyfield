"""
Django settings for anyfield project.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import pickle
import sys
from celery import Celery

BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/dev/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '(&@oh2xxa%(3j3ot*vnp0hou@la&i^io1)8pzumdd^%0!h==jf'

SESSION_COOKIE_DOMAIN = '.localhost.me'

pickle.HIGHEST_PROTOCOL = 2

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.PickleSerializer'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'tinymce',
    'bootstrapform',
    'adminsortable',
    'search',
    'user',
    'state',
    'fieldtype',
    'itemfield',
    'vote',
    'pages',
    'constance',
    'constance.backends.database'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    # 'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'config.urls'

WSGI_APPLICATION = 'config.wsgi.application'


# Database
# https://docs.djangoproject.com/en/dev/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'anyfield',
        'USER': 'anyfield',
        'PASSWORD': 'anyfield'
    }
}
if 'test' in sys.argv or 'test_coverage' in sys.argv:
    DATABASES['default']['ENGINE'] = 'django.db.backends.sqlite3'

MONGODB = {
    'NAME': 'anyfield_mal',
    'HOST': '127.0.0.1',
    'PORT': 27017
}



BROKER_URL = 'redis://localhost:6379/0'
CELERY_ACCEPT_CONTENT = ['pickle', 'json', 'msgpack', 'yaml']
app = Celery('tasks')

# TASTYPIE

TASTYPIE_DEFAULT_FORMATS = ['json']
TASTYPIE_ALLOW_MISSING_SLASH = True
API_LIMIT_PER_PAGE = 100

# Internationalization
# https://docs.djangoproject.com/en/dev/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/dev/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static_root')

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR,  'templates'),
)

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'cache',
        'TIMEOUT': 600,
    },
    'resources': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'TIMEOUT': 600
    }
}

LOGGING = {
    'version': 1,
    'root': {'level': 'DEBUG'},
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': BASE_DIR + '/log.log',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django': {
            'level': 'DEBUG',
            'handlers': ['console'],
        },
    },
}

TINYMCE_DEFAULT_CONFIG = {
    "theme": "advanced",
}

CONSTANCE_CONFIG = {
    'ITEM_FIELD_VOTE_UP_BORDER': (2, ''),
    'ITEM_FIELD_VOTE_DOWN_BORDER': (1, ''),
    'ITEM_FIELD_VOTE_DELETE_BORDER': (-2, ''),
    'TYPE_VOTE_UP_BORDER': (2, ''),
    'TYPE_VOTE_DOWN_BORDER': (1, ''),
    'TYPE_VOTE_DELETE_BORDER': (-2, ''),
    'FIELD_VOTE_UP_BORDER': (2, ''),
    'FIELD_VOTE_DOWN_BORDER': (1, ''),
    'FIELD_VOTE_DELETE_BORDER': (-2, ''),
    'SELECT_VOTE_UP_BORDER': (2, ''),
    'SELECT_VOTE_DOWN_BORDER': (1, ''),
    'SELECT_VOTE_DELETE_BORDER': (-2, ''),
    'RANGE_VOTE_UP_BORDER': (2, ''),
    'RANGE_VOTE_DOWN_BORDER': (1, ''),
    'RANGE_VOTE_DELETE_BORDER': (-2, ''),
    'DEFAULT_STATE': (1, ''),
}

CONSTANCE_BACKEND = 'constance.backends.database.DatabaseBackend'