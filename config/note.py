from config.settings import *

DEBUG = True
ALLOWED_HOSTS = ['localhost.me']
STATIC_URL = '/static/'