from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns(
    '',
    url(r'^admin/', include(admin.site.urls)),
    (r'^tinymce/', include('tinymce.urls')),
    url(r'', include('user.urls')),
    url(r'', include('state.urls')),
    url(r'', include('fieldtype.urls')),
    url(r'', include('itemfield.urls')),
    url(r'', include('vote.urls')),
    url(r'', include('pages.urls')),


    url(r'', include('search.urls')), #always last
)

