create virtualenv
pip install -r config/requirements.txt
set settings

migrate

sudo apt-get install rabbitmq-server
sudo rabbitmqctl add_user anyfield asdfrewqzxcv
sudo rabbitmqctl add_vhost myvhost
sudo rabbitmqctl set_permissions -p myvhost anyfield ".*" ".*" ".*"

bower install
compress all static files

start celery
start scores import daemon