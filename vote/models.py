from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models import Sum
from django.db.models.signals import pre_save
from django.dispatch import receiver

from user.models import User


class AllFieldsManager(models.Manager):
    def get_queryset(self):
        from fieldtype.models import FieldTypeModel, FieldModel, SelectValueModel
        field_type = ContentType.objects.get_for_model(FieldTypeModel)
        field = ContentType.objects.get_for_model(FieldModel)
        select = ContentType.objects.get_for_model(SelectValueModel)
        return super().get_queryset().filter(content_type_id__in=[field_type.id, field.id, select.id])


class ItemFieldsManager(models.Manager):
    def get_queryset(self):
        from itemfield.models import ItemFieldModel
        item_type = ContentType.objects.get_for_model(ItemFieldModel)
        return super().get_queryset().filter(content_type=item_type)


class UserVote(models.Model):
    user = models.ForeignKey(User)
    value = models.IntegerField()
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    objects = models.Manager()
    all_fields = AllFieldsManager()
    item_fields = ItemFieldsManager()

    def type_vote_sum(self):
        return UserVote.vote_sum(self.content_type, self.object_id)

    def user_vote_sum(self):
        return UserVote.vote_sum(self.content_type, self.object_id, self.user)

    def save(self, *args, **kwargs):
        UserVote.type_filter(content_type=self.content_type, object_id=self.object_id, user=self.user).delete()
        super().save(*args, **kwargs)

    @staticmethod
    def vote_sum(content_type, object_id, user=None):
        if user and not user.is_authenticated():
            return 0
        try:
            # check parent exists
            content_type.get_object_for_this_type(id=object_id)
            res = UserVote.type_filter(content_type, object_id, user).aggregate(Sum('value'))['value__sum'] or 0
        except ObjectDoesNotExist:
            res = None
        return res

    @staticmethod
    def type_filter(content_type, object_id, user=None):
        query = UserVote.objects.filter(content_type=content_type, object_id=object_id)
        if user:
            query = query.filter(user=user)
        return query


@receiver(pre_save, sender=UserVote)
def my_handler(sender, instance, **kwargs):
    if instance.content_object:
        instance.content_object.check_boundary(instance)