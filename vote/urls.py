from django.conf.urls import patterns, include, url
from vote.api import UserVoteResource
from vote.views import TypeUserVoteView, ItemUserVoteView


user_vote_resource = UserVoteResource()

api_patterns = patterns(
    '',
    url(r'^api/', include(user_vote_resource.urls)),
)

urlpatterns = patterns(
    '',
    url(r'^api/voteStats/', TypeUserVoteView.as_view()),
    url(r'^api/voteItemStats/(?P<id>\d+)/', ItemUserVoteView.as_view()),
    url(r'', include(api_patterns)),
)

