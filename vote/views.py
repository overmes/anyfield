from braces.views import JsonRequestResponseMixin
from django.contrib.contenttypes.models import ContentType
from django.views.generic import View
import ujson
from fieldtype.models import FieldTypeModel, FieldModel, SelectValueModel, RangeValueModel
from itemfield.models import ItemFieldModel

from vote.models import UserVote


class TypeUserVoteView(JsonRequestResponseMixin, View):
    def get(self, request):
        types_votes = UserVote.all_fields.all()
        response = {'all': {}, 'user': {}}

        types_ids = {
            ContentType.objects.get_for_model(FieldTypeModel).id: 'fieldType',
            ContentType.objects.get_for_model(FieldModel).id: 'field',
            ContentType.objects.get_for_model(SelectValueModel).id: 'select',
            ContentType.objects.get_for_model(RangeValueModel).id: 'range',
        }
        for vote in types_votes:
            key = '/api/{}/{}'.format(types_ids[vote.content_type_id], vote.object_id)
            response['all'][key] = response['all'].setdefault(key, 0) + vote.value

            if request.user.id == vote.user_id:
                response['user'][key] = response['user'].setdefault(key, 0) + vote.value

        return self.render_json_response(response, status=200)


class ItemUserVoteView(JsonRequestResponseMixin, View):
    def get(self, request, id):
        groups = ujson.loads(request.GET.get('groups')) if 'groups' in request.GET else None
        query = ItemFieldModel.objects.filter(item=id)
        if groups:
            query = query.filter(type__in=groups)
        item_fields = query.values_list('id', flat=True)
        item_votes = UserVote.item_fields.filter(object_id__in=item_fields)
        response = {'all': {}, 'user': {}}
        for vote in item_votes:
            key = '/api/itemField/{}'.format(vote.object_id)
            response['all'][key] = response['all'].setdefault(key, 0) + vote.value

            if request.user.id == vote.user_id:
                response['user'][key] = response['user'].setdefault(key, 0) + vote.value

        return self.render_json_response(response, status=200)