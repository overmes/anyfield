from django import forms
from fieldtype.forms import UserCleanMixin
from vote.models import UserVote


class VoteForm(forms.ModelForm, UserCleanMixin):
    class Meta:
        model = UserVote
        fields = ['user', 'value', 'object_id', 'content_type']