from django.contrib.contenttypes.models import ContentType
from tastypie import fields
from tastypie.authentication import SessionAuthentication
from tastypie.authorization import DjangoAuthorization, Authorization
from tastypie.contrib.contenttypes.fields import GenericForeignKeyField
from tastypie.resources import ModelResource
from tastypie.validation import CleanedDataFormValidation
from fieldtype.api import DenyAuthorization, FieldTypeResource, FieldResource, SelectValueResource, RangeValueResource
from fieldtype.models import FieldTypeModel, FieldModel, SelectValueModel, RangeValueModel
from itemfield.api import ItemFieldResource
from itemfield.models import ItemFieldModel

from vote.forms import VoteForm

from vote.models import UserVote


class ContentTypeResource(ModelResource):
    class Meta:
        queryset = ContentType.objects.all()
        allowed_methods = []
        authentication = SessionAuthentication()
        authorization = DjangoAuthorization()
        include_resource_uri = False


class UserVoteAuthorization(DenyAuthorization):
    def read_detail(self, object_list, bundle):
        return True

    def create_detail(self, object_list, bundle):
        return True


class UserVoteResource(ModelResource):
    content_type = fields.IntegerField()
    user = fields.IntegerField()
    user_vote_sum = fields.IntegerField()
    type_vote_sum = fields.IntegerField()
    is_temp = fields.BooleanField()
    is_deleted = fields.BooleanField()

    content_object = GenericForeignKeyField({
        FieldTypeModel: FieldTypeResource,
        FieldModel: FieldResource,
        SelectValueModel: SelectValueResource,
        RangeValueModel: RangeValueResource,
        ItemFieldModel: ItemFieldResource
    }, 'content_object')

    class Meta:
        queryset = UserVote.objects.all()
        resource_name = 'vote'
        allowed_methods = ['post', 'put']
        authentication = SessionAuthentication()
        authorization = UserVoteAuthorization()
        include_resource_uri = False
        always_return_data = True
        validation = CleanedDataFormValidation(form_class=VoteForm)

    def hydrate(self, bundle):
        bundle.data['user'] = bundle.request.user.id
        return bundle

    def dehydrate_content_type(self, bundle):
        return bundle.obj.content_type.id

    def dehydrate_user(self, bundle):
        return bundle.obj.user.id

    def dehydrate_user_vote_sum(self, bundle):
        return bundle.obj.user_vote_sum()

    def dehydrate_type_vote_sum(self, bundle):
        return bundle.obj.type_vote_sum()

    def dehydrate_is_temp(self, bundle):
        return bundle.obj.content_object.is_temp if bundle.obj.content_object else None

    def dehydrate_is_deleted(self, bundle):
        return bundle.obj.content_object.is_deleted if \
            bundle.obj.content_object and hasattr(bundle.obj.content_object, 'is_deleted') else None
