from django.contrib.auth.models import User
from django.test import override_settings, TestCase
from fieldtype.models import FieldTypeModel
from search.filters import QueryFilter


@override_settings(MONGODB={"NAME": "anyfield_test"})
class AddFieldTestCase(TestCase):
    fixtures = ["test_fields.json"]

    def setUp(self):
        self.admin = User.objects.create_user(username="admin", email="admin@admin.com", password="admin")
        self.all_fields = FieldTypeModel.get_all_groups()

    def test_empty(self):
        filters = []
        query_filter = QueryFilter(self.all_fields)
        query = query_filter.get_query(index_groups=filters)
        self.assertDictEqual(query, {})

    def test_include_exclude(self):
        filters = [
            {"parent": 14, "fields": [{"fid": "a", "or": True, "filter": [1, 2]}], "include": False}
        ]
        query_filter = QueryFilter(self.all_fields)
        query = query_filter.get_query(index_groups=filters)
        self.assertDictEqual(query,
                             {"$and": [
                                 {"$and": [
                                     {"14": {"$not": {"$elemMatch": {"a": {"$in": [1, 2]}}}}}
                                 ]}
                             ]})

    def test_include_exclude_select_and(self):
        filters = [
            {"parent": 14, "fields": [{"fid": "a", "or": False, "filter": [1, 2]}], "include": False}
        ]
        query_filter = QueryFilter(self.all_fields)
        query = query_filter.get_query(index_groups=filters)
        self.assertDictEqual(query,
                             {"$and": [
                                 {"$and": [
                                     {"14": {"$not": {"$elemMatch": {"a": 1}}}},
                                     {"14": {"$not": {"$elemMatch": {"a": 2}}}}
                                 ]}
                             ]})

    def test_simple_select_or(self):
        filters = [
            {"parent": 14, "fields": [{"fid": "a", "or": True, "filter": [1, 2]}], "include": True}
        ]
        query_filter = QueryFilter(self.all_fields)
        query = query_filter.get_query(index_groups=filters)
        self.assertDictEqual(query,
                             {"$and": [
                                 {"$and": [
                                     {"14": {"$elemMatch": {"a": {"$in": [1, 2]}}}}
                                 ]}
                             ]})

    def test_simple_select_and(self):
        filters = [
            {"parent": 14, "fields": [{"fid": "a", "or": False, "filter": [1, 2]}], "include": True}
        ]
        query_filter = QueryFilter(self.all_fields)
        query = query_filter.get_query(index_groups=filters)
        self.assertDictEqual(query,
                             {"$and": [
                                 {"$and": [
                                     {"14": {"$elemMatch": {"a": 1}}},
                                     {"14": {"$elemMatch": {"a": 2}}}
                                 ]}
                             ]})

    def test_simple_text(self):
        filters = [
            {"parent": 2, "fields": [{"fid": "a", "filter": "Hello"}], "include": True}
        ]
        query_filter = QueryFilter(self.all_fields)
        query = query_filter.get_query(index_groups=filters)
        self.assertDictEqual(query,
                             {"$and": [
                                 {"$and": [
                                     {"2": {"$elemMatch": {"a": "Hello"}}}
                                 ]}
                             ]})

    def test_simple_select_or_and_text(self):
        filters = [
            {"parent": 11, "include": True, "fields": [
                {"fid": "c", "or": True, "filter": [1, 2]},
                {"fid": "d", "filter": "Hello"}
            ]}
        ]
        query_filter = QueryFilter(self.all_fields)
        query = query_filter.get_query(index_groups=filters)
        self.assertDictEqual(query,
                             {"$and": [
                                 {"$and": [
                                     {"11": {"$elemMatch": {"c": {"$in": [1, 2]}, "d": "Hello"}}}
                                 ]}
                             ]})

    def test_simple_select_and_and_text(self):
        filters = [
            {"parent": 11, "include": True, "fields": [
                {"fid": "c", "or": False, "filter": [1, 2]},
                {"fid": "d", "filter": "Hello"}
            ]}
        ]
        query_filter = QueryFilter(self.all_fields)
        query = query_filter.get_query(index_groups=filters)
        self.assertDictEqual(query,
                             {"$and": [
                                 {"$and": [
                                     {"11": {"$elemMatch": {"c": 1, "d": "Hello"}}},
                                     {"11": {"$elemMatch": {"c": 2, "d": "Hello"}}}
                                 ]}
                             ]})

    def test_double_select_and_and_text(self):
        filters = [
            {"parent": 11, "include": True, "fields": [
                {"fid": "b", "or": False, "filter": [1, 2]},
                {"fid": "c", "or": False, "filter": [3, 4]},
                {"fid": "d", "filter": "Hello"}
            ]}
        ]
        query_filter = QueryFilter(self.all_fields)
        query = query_filter.get_query(index_groups=filters)
        self.assertDictEqual(query,
                             {"$and": [
                                 {"$and": [
                                     {"11": {"$elemMatch": {"b": 1, "c": 3, "d": "Hello"}}},
                                     {"11": {"$elemMatch": {"b": 2, "c": 3, "d": "Hello"}}},
                                     {"11": {"$elemMatch": {"b": 1, "c": 4, "d": "Hello"}}},
                                     {"11": {"$elemMatch": {"b": 2, "c": 4, "d": "Hello"}}}
                                 ]}
                             ]})

    def test_select_and_select_or_and_text(self):
        filters = [
            {"parent": 11, "include": True, "fields": [
                {"fid": "b", "or": True, "filter": [1, 2]},
                {"fid": "c", "or": False, "filter": [1, 2]},
                {"fid": "d", "filter": "Hello"}
            ]}
        ]
        query_filter = QueryFilter(self.all_fields)
        query = query_filter.get_query(index_groups=filters)
        self.assertDictEqual(query,
                             {"$and": [
                                 {"$and": [
                                     {"11": {"$elemMatch": {"b": {"$in": [1, 2]}, "c": 1, "d": "Hello"}}},
                                     {"11": {"$elemMatch": {"b": {"$in": [1, 2]}, "c": 2, "d": "Hello"}}}
                                 ]}
                             ]})

    def test_none_simple(self):
        filters = [
            {"parent": 14, "fields": [{"fid": "a", "or": True, "filter": [None]}], "include": True}
        ]
        query_filter = QueryFilter(self.all_fields)
        query = query_filter.get_query(index_groups=filters)
        self.assertDictEqual(query,
                             {"$and": [
                                 {"$and": [
                                     {"14.a": None}
                                 ]}
                             ]})

    def test_none_select_or(self):
        filters = [
            {"parent": 14, "fields": [{"fid": "a", "or": True, "filter": [1, None]}], "include": True}
        ]
        query_filter = QueryFilter(self.all_fields)
        query = query_filter.get_query(index_groups=filters)
        self.assertDictEqual(query,
                             {"$and": [
                                 {"$and": [
                                     {"$or": [
                                         {"14.a": None},
                                         {"14": {"$elemMatch": {"a": {"$in": [1, None]}}}}
                                     ]}
                                 ]}
                             ]})

    def test_none_select_or_and_select_or(self):
        filters = [
            {"parent": 11, "include": True, "fields": [
                {"fid": "b", "or": True, "filter": [1, None]},
                {"fid": "c", "or": True, "filter": [3]}
            ]}
        ]
        query_filter = QueryFilter(self.all_fields)
        query = query_filter.get_query(index_groups=filters)
        self.assertDictEqual(query,
                             {"$and": [
                                 {"$and": [
                                     {"11": {"$elemMatch": {"b": {"$in": [1, None]}, "c": {"$in": [3]}}}}
                                 ]}
                             ]})

    def test_none_select_or_and_select_and(self):
        filters = [
            {"parent": 11, "include": True, "fields": [
                {"fid": "b", "or": True, "filter": [1, None]},
                {"fid": "c", "or": False, "filter": [2, 3]}
            ]}
        ]
        query_filter = QueryFilter(self.all_fields)
        query = query_filter.get_query(index_groups=filters)
        self.assertDictEqual(query,
                             {"$and": [
                                 {"$and": [
                                     {"11": {"$elemMatch": {"b": {"$in": [1, None]}, "c": 2}}},
                                     {"11": {"$elemMatch": {"b": {"$in": [1, None]}, "c": 3}}}
                                 ]}
                             ]})

    def test_none_select_or_and_none_select_or(self):
        filters = [
            {"parent": 11, "include": True, "fields": [
                {"fid": "b", "or": True, "filter": [1, None]},
                {"fid": "c", "or": True, "filter": [2, None]}
            ]}
        ]
        query_filter = QueryFilter(self.all_fields)
        query = query_filter.get_query(index_groups=filters)
        self.assertDictEqual(query,
                             {"$and": [
                                 {"$and": [
                                     {"$or": [{"11.b": None}, {"11": {"$elemMatch": {"b": {"$in": [1, None]}}}}]},
                                     {"$or": [{"11.c": None}, {"11": {"$elemMatch": {"c": {"$in": [2, None]}}}}]}
                                 ]}
                             ]})

    def test_none_select_and(self):
        filters = [
            {"parent": 11, "include": True, "fields": [
                {"fid": "b", "or": False, "filter": [1, None]}
            ]}
        ]
        query_filter = QueryFilter(self.all_fields)
        query = query_filter.get_query(index_groups=filters)
        self.assertDictEqual(query,
                             {"$and": [
                                 {"$and": [
                                     {"11": {"$elemMatch": {"b": 1}}},
                                     {"11": {"$elemMatch": {"b": None}}}
                                 ]}
                             ]})

    def none_select_and_and_none_select_or(self):
        filters = [
            {"parent": 11, "include": True, "fields": [
                {"fid": "b", "or": False, "filter": [1, None]},
                {"fid": "c", "or": True, "filter": [2, None]}
            ]}
        ]
        query_filter = QueryFilter(self.all_fields)
        query = query_filter.get_query(index_groups=filters)
        self.assertDictEqual(query,
                             {"$and": [
                                 {"$and": [
                                     {"11": {"$elemMatch": {"b": 1,"c": {"$in": [2, None]}}}},
                                     {"11": {"$elemMatch": {"b": None,"c": {"$in": [2, None]}}}}
                                 ]}
                             ]})