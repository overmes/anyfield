from django.conf import settings
from django.views.generic import TemplateView


class MainPageView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['DEBUG'] = settings.DEBUG
        return context


class JasminView(TemplateView):
    template_name = 'jasmin.html'