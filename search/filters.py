from copy import deepcopy
from fieldtype.models import FieldTypeModel, FieldModel


class QueryFilter:
    def __init__(self, all_groups):
        self.all_groups_dict = {g['id']: deepcopy(g) for g in all_groups}
        for group_id, data in self.all_groups_dict.items():
            data['fields'] = {f['fid']: f for f in data['fields']}
        self.field_filter = FieldFilter()

    def get_query(self, index_groups):
        filters = list(map(self.get_group_filter, index_groups))
        return {'$and': filters} if filters else {}

    def get_group_filter(self, group):
        boxes = self.get_group_boxes(group)

        extended_filters = []
        for current_boxes in boxes:
            full_filter = {}

            for box in current_boxes:
                if box['None']:
                    extended_filters.append(self.get_none_filter(group, box))
                else:
                    full_filter.update(box['val'])

            if full_filter:
                if group['parent'] in [FieldTypeModel.selected_type_index(), FieldTypeModel.id_type_index()]:
                    extended_filters.append(self.get_id_filter(group, full_filter))
                else:
                    extended_filters.append(self.get_elem_match_filter(group, full_filter))

        return {'$and': extended_filters} if extended_filters else {}

    def get_id_filter(self, group, full_filter):
        return full_filter if group['include'] else {'i': {'$not': full_filter['i']}}

    def get_elem_match_filter(self, group, full_filter):
        str_group_id = str(group['parent'])
        return {str_group_id: {'$elemMatch': full_filter}} if group['include'] else {
            str_group_id: {'$not': {'$elemMatch': full_filter}}}

    def get_none_filter(self, group, box):
        group_none_filter = {}
        group_id = group['parent']
        if group['include']:
            group_none_filter['{0}.{1}'.format(group_id, box['fid'])] = None
        else:
            group_none_filter['{0}.{1}'.format(group_id, box['fid'])] = {'$not': {'$eq': None}}

        if len(box['val'][box['fid']].get('$in', 0)) > 1:
            group_filter = {}
            if group['include']:
                group_filter[str(group_id)] = {'$elemMatch': box['val']}
                if box['or']:
                    result = {'$or': [group_none_filter, group_filter]}
                else:
                    result = {'$and': [group_none_filter, group_filter]}
            else:
                group_filter[str(group_id)] = {'$not': {'$elemMatch': box['val']}}
                if box['or']:
                    result = {'$and': [group_none_filter, group_filter]}
                else:
                    result = {'$or': [group_none_filter, group_filter]}

        else:
            result = group_none_filter
        return result

    def get_group_boxes(self, group):
        boxes = []

        group_boxes = []
        have_not_none_field = False
        for field in group['fields']:
            field_scheme = self.all_groups_dict[group['parent']]['fields'][field['fid']]
            current_filter = self.field_filter.get_box(field_scheme, field)
            if current_filter and current_filter['val']:
                group_boxes.append(current_filter)
                have_not_none_field = have_not_none_field or not current_filter['None']

        for box in group_boxes:
            box['None'] = False if have_not_none_field else box['None']
            if box['type'] == 'multise':
                boxes = self.get_multiply_boxes(boxes, box)
            else:
                self.append_box(boxes, box)
        return boxes

    def get_multiply_boxes(self, boxes, new_box):
        new_boxes = []
        for value in new_box['val']:
            if boxes:
                for box in boxes:
                    box_clone = deepcopy(box)
                    box_clone.append({'type': 'se', 'val': value, 'None': False})
                    new_boxes.append(box_clone)
            else:
                new_boxes.append([{'type': 'se', 'val': value, 'None': False}])
        return new_boxes

    def append_box(self, boxes, new_box):
        if boxes:
            for box in boxes:
                box.append(new_box)
        else:
            boxes.append([new_box])

    def get_index(self, group):
        return 'i' if self.all_groups_dict[group['parent']]['type'] == FieldTypeModel.ITEM else group['user']


class FieldFilter:
    def get_box(self, field_scheme, field):
        fid = field['fid']
        filter = field.get('filter')
        filter_function = FieldFilter.fields_filters[field_scheme['type']].__func__
        return filter_function(field, fid, filter) if filter else None

    @staticmethod
    def _select_filter(field, field_fid, filter):
        if field['or']:
            return {'type': 'se', 'val': {field_fid: {'$in': filter}}, 'None': None in filter, 'or': field.get('or'),
                    'fid': field_fid}
        else:
            return {
                'type': 'multise', 'val': map(lambda f: {field_fid: f}, filter), 'None': False,
                'or': field.get('or'), 'fid': field_fid
            }

    @staticmethod
    def _text_filter(field, field_fid, filter):
        return {'type': 'te', 'val': {field_fid: filter}, 'None': False, 'fid': field_fid}

    @staticmethod
    def _get_range_query(filter):
        query_filter = {}
        query_filter.update({'$gte': filter[0]}) if filter[0] else None
        query_filter.update({'$lte': filter[1]}) if filter[1] else None
        return query_filter

    @staticmethod
    def _value_filter(field, field_fid, filter):
        query_filter = FieldFilter._get_range_query(filter)
        return {'type': 'va', 'val': {field_fid: query_filter}, 'None': False,
                'fid': field_fid}

    @staticmethod
    def _id_filter(field, field_fid, filter):
        query_filter = FieldFilter._get_range_query(filter)
        return {'type': 'id', 'val': {'i': query_filter}, 'None': False, 'fid': field_fid}

    @staticmethod
    def _link_filter(field, field_fid, filter):
        return {'type': 'li', 'val': {field_fid: {'$in': [f[0] for f in filter]}}, 'None': any(f[0] is None for f in filter),
                'fid': field_fid, 'or': True}

    @staticmethod
    def _selected_filter(field, field_fid, filter):
        return {'type': 'id', 'val': {'i': {'$in': filter}}, 'None': False, 'fid': field_fid}

    @staticmethod
    def _date_filter(field, field_fid, filter):
        query_filter = FieldFilter._get_range_query(filter)
        return {'type': 'da', 'val': {field_fid: query_filter}, 'None': False, 'fid': field_fid}

    fields_filters = {
        FieldModel.SELECT: _select_filter,
        FieldModel.TEXT: _text_filter,
        FieldModel.VALUE: _value_filter,
        FieldModel.LINK: _link_filter,
        FieldModel.ID: _id_filter,
        FieldModel.SELECTED: _selected_filter,
        FieldModel.DATE: _date_filter
    }
