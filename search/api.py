import ujson
from django.core.exceptions import ObjectDoesNotExist

from tastypie.authorization import Authorization
from tastypie.bundle import Bundle
from tastypie.cache import SimpleCache
from tastypie.resources import Resource
import time
from itemfield.models import ItemFieldModel, ItemModel

from query.query import QuerySolver, SingleQuerySolver
from fieldtype.models import FieldTypeModel
from search.filters import QueryFilter


class ItemsCache:
    def __init__(self):
        self.last_request = 0
        self.items = set()
        self.cache_timeout = 300

    def get_items(self):
        current_time = time.time()
        if self.last_request + 300 < current_time:
            self.items = self.load_items()
            self.last_request = current_time
        return self.items

    def load_items(self):
        return set(ItemModel.objects.all().values_list('id', flat=True))

items_cache = ItemsCache()


class QueryResource(Resource):
    class Meta:
        resource_name = 'query'
        authorization = Authorization()
        cache = SimpleCache(timeout=15)

    def dehydrate(self, bundle):
        bundle.data.update(bundle.obj)
        return bundle

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}
        obj = bundle_or_obj.obj if isinstance(bundle_or_obj, Bundle) else bundle_or_obj
        kwargs['pk'] = (obj.get(list(obj.keys())[0])).get('i') if bundle_or_obj else None
        return kwargs

    def get_object_list(self, request):
        get = request.GET
        all_groups = FieldTypeModel.get_all_groups()
        query_data = ujson.loads(get.get('query') or "{}")
        self.replace_index_filters(all_groups, query_data)
        params = {
            'user': request.user.id or 0,
            'query': query_data
        }

        sort = ujson.loads(get.get('sort') or "[]")
        params.update({'sort': sort}) if sort else None

        limit = get.get('limit')
        params.update({'limit': int(limit)}) if limit and limit.isdigit() else None

        skip = get.get('skip')
        params.update({'skip': int(skip)}) if skip and skip.isdigit() else None

        params.update(all_fields=all_groups)
        params['items_set'] = items_cache.get_items()
        query_solver = QuerySolver(**params)
        results = query_solver.solve()

        if get.get('tempFields') == 'on':
            QueryResource.add_temp_fields(results, params['query'].get('fields'))

        return results

    def obj_get_list(self, bundle, **kwargs):
        return self.get_object_list(bundle.request)

    def obj_get(self, bundle, **kwargs):
        user = bundle.request.user.id or 0
        pk = int(kwargs.get('pk'))
        result = ItemModel.get_item_data(pk, user)
        QueryResource.add_resource_uri(result) if result else None
        if not result:
            raise ObjectDoesNotExist('Sorry, no results on that page.')
        return result

    def replace_index_filters(self, all_groups, query):
        query_filter = QueryFilter(all_groups)
        for index, index_query in query.items():
            if index_query.get('query'):
                index_query['query'] = query_filter.get_query(index_groups=index_query.get('query'))

    @staticmethod
    def add_temp_fields(results, fields=None):
        for r in results:
            ItemModel.add_temp_field(r, r.get('i', {}).get('i'), fields)
            QueryResource.add_resource_uri(r)

    @staticmethod
    def add_resource_uri(result):
        for index, data in result.items():
            for group_id, group in data.items():
                if group_id.isdigit():
                    for field in group:
                        field['resource_uri'] = '/api/itemField/{}'.format(field['id'])


class TempFieldsResource(QueryResource):
    class Meta:
        resource_name = 'tempFields'
        authorization = Authorization()

    def get_object_list(self, request):
        query = ItemFieldModel.objects.filter(is_temp=True)
        if request.user.is_authenticated():
            query = query.exclude(votes__user=request.user)
        temp_item_fields = query.order_by('?')[:10]
        results = []
        appended_set = set()
        for temp_field in temp_item_fields:
            field = temp_field.type_id
            item, fields = temp_field.item_id, [str(field), str(FieldTypeModel.title_type_index())]
            results.append(self.get_item(item, fields)) if (item, field) not in appended_set else None
            appended_set.add((item, field))
        return results

    def obj_get_list(self, bundle, **kwargs):
        return self.get_object_list(bundle.request)

    def get_item(self, item, fields):
        result = ItemModel.get_item_data(item, user=None, fields=fields)
        TempFieldsResource.add_resource_uri(result) if result else None
        if not result:
            raise ObjectDoesNotExist('Sorry, no results on that page.')
        return result