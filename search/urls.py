from django.conf.urls import patterns, include, url
from search.api import QueryResource, TempFieldsResource
from search.views import MainPageView, JasminView

query_resource = QueryResource()
temp_fields_resource = TempFieldsResource()

api_patterns = patterns(
    '',
    url(r'^api/', include(query_resource.urls)),
    url(r'^api/', include(temp_fields_resource.urls)),
)

urlpatterns = patterns(
    '',
    url(r'', include(api_patterns)),
    url(r'^jasmine/', JasminView.as_view(), name='jasmine'),
    url(r'^.*', MainPageView.as_view(), name='main_page'),
)

