require.config({
    baseUrl: '/static/js/',
    paths: {
        underscore: 'bower_components/underscore/underscore',
        backbone: 'bower_components/backbone/backbone',
        marionette: 'bower_components/marionette/lib/backbone.marionette',
        jquery: 'bower_components/jquery/dist/jquery',
        text: 'bower_components/requirejs-text/text',
        select: 'bower_components/bootstrap-select/dist/js/bootstrap-select',
        'ajax-select': 'bower_components/ajax-bootstrap-select/dist/js/ajax-bootstrap-select',
        chosen: 'bower_components/chosen_v1.1.0/chosen.jquery',
        bootstrap: 'bower_components/bootstrap/dist/js/bootstrap',
        masonry: 'bower_components/masonry/dist/masonry.pkgd',
        nested: 'bower_components/backbone-nested-model/backbone-nested',
        packery: 'bower_components/packery/dist/packery.pkgd',
        jasmine: 'bower_components/jasmine/lib/jasmine-core/jasmine',
        'jasmine-html': 'bower_components/jasmine/lib/jasmine-core/jasmine-html',
        'boot': 'bower_components/jasmine/lib/jasmine-core/boot',
        modelbinder: 'bower_components/backbone.modelbinder/Backbone.ModelBinder',
        moment: 'bower_components/moment/moment',
        ionslider: 'bower_components/ionrangeslider/js/ion.rangeSlider',
        datepicker: 'bower_components/bootstrap-datepicker/js/bootstrap-datepicker',
        storage: 'bower_components/simpleStorage/simpleStorage',
        backform: 'bower_components/backform/src/backform',
        ravenjs: 'bower_components/raven-js/dist/raven',
        'jquery-ui': 'bower_components/jquery-ui/ui',
        handlebars: 'bower_components/handlebars/handlebars.amd',
        nprogress: 'bower_components/nprogress/nprogress'
    },
    shim: {
        ravenjs: {
            deps: ['jquery']
        },
        backbone: {
            exports: 'Backbone'
        },
        backform: {
            deps: ['underscore', 'bootstrap', 'backbone']
        },
        ionslider: {
            deps: ['jquery']
        },
        'ajax-select': {
            deps: ['select']
        },
        'jasmine': {
            exports: 'window.jasmineRequire'
        },
        'jasmine-html': {
            deps: ['jasmine'],
            exports: 'window.jasmineRequire'
        },
        'boot': {
            deps: ['jasmine', 'jasmine-html'],
            exports: 'window.jasmineRequire'
        },
        underscore: {
            exports: '_'
        },
        select: {
            deps: ['jquery', 'bootstrap']
        },
        bootstrap: {
            deps: ['jquery']
        },
        chosen: {
            deps: ['jquery']
        },
        masonry: {
            deps: ['jquery']
        }
    }
});

requirejs.onError = function (err) {
    var errorMsg = "<h1>requirejs error: </h1>";
    errorMsg += err.requireType;
    if (err.requireType === 'timeout') {
        errorMsg += ('<p>modules: ' + err.requireModules + '</p>');
    }
    window.document.write(errorMsg);
    throw err;
};