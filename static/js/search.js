require([
    'jquery',
    'search/App',
    'search/helpers',
    'ravenjs'
], function ($, app, helpers) {
    if (DEBUG){
         app.start();
    } else {
        Raven.config('http://725d285745ec491680fbe7239e0560d5@sentry.animeadvice.me/2', {
            whitelistUrls: [/.*animeadvice\\.me/],
            ignoreErrors: []
        }).install();
        try {
            app.start();
        } catch(err) {
            Raven.captureException(err);
        }

        $(document).ajaxError( function(e, jqXHR, options, exception){
            Raven.captureMessage(jqXHR.statusCode, {tags: { url: options.url, text: jqXHR.statusText}});
            return false;
        });

        var previous = window.onerror;
        window.onerror = function (message, file, line, column, errorObj) {
            previous(message, file, line, column, errorObj);
            Raven.captureMessage(message, {tags: { file: file, line: line}});
        }
    }
});
