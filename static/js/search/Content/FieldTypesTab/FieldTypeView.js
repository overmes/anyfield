define([
    'backbone',
    'marionette',
    'text!search/Content/FieldTypesTab/FieldTypeTemplate.html',
    'search/Content/FieldTypesTab/FieldView',
    'search/Content/FieldTypesTab/AddFieldView',
    'search/Content/Common/GroupsCollection/FieldModel',
    'search/Content/Common/Vote/VoteView'
], function (Backbone, Marionette, FieldTypeTemplate, FieldView, AddFieldView, FieldModel, VoteView) {
    'use strict';
    return Marionette.CompositeView.extend({
        initialize: function (options) {
            this.app = options.app;
            this.collection = this.model.get('fields');
        },
        className: 'list-group-item types-list',
        tagName: 'div',
        childViewContainer: ".list-group",
        getChildView: function (item) {
            if (item.get('id')) {
                return FieldView;
            } else {
                return AddFieldView;
            }
        },
        ui: {
            'newButton': '.field-btn-new',
            'vote': '.type-vote'
        },
        events: {
            'click @ui.newButton': 'addNewField'
        },
        modelEvents: {
            "change": "render"
        },
        addNewField: function () {
            this.collection.add(new FieldModel(), {at: 0});
        },
        template: _.template(FieldTypeTemplate),

        childViewOptions: function (model, index) {
            return {
                app: this.app,
                parent: this.model
            };
        },

        templateHelpers: function () {
            var self = this;
            return {
                labelType: self.model.get('type') === 'it' ? 'success' : 'warning',
                typeName:  {
                        'us': 'User',
                        'it': 'Item'
                }[this.model.get('type')],
                cid: this.cid,
                user: this.app.request('user')
            };
        },
        onRender: function () {
            if (this.model.get('edit') !== 'no') {
                this.voteView = new VoteView({target: this.model, app: this.app});
                this.ui.vote.html(this.voteView.render().el);
            }
        },

        onBeforeDestroy: function () {
            if (this.voteView) this.voteView.destroy();
        }
    });
});