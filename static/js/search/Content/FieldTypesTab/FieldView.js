define([
    'backbone',
    'marionette',
    'text!search/Content/FieldTypesTab/FieldTemplate.html',
    'search/Content/FieldTypesTab/Select/SelectCollectionView',
    'search/Content/Common/Vote/VoteView'
], function (Backbone, Marionette, FieldTemplate, SelectCollectionView, VoteView) {
    'use strict';
    return Marionette.ItemView.extend({
        initialize: function (options) {
            this.app = options.app;
            this.parent = options.parent;
        },
        className: 'list-group-item field-view',
        template: _.template(FieldTemplate),

        templateHelpers: function () {
            return {
                typeName: this.model.getTypeName()
            };
        },

        ui: {
            vote: '.field-vote'
        },
        modelEvents: {
            "change": "render"
        },

        onRender: function () {
            if (['se', 'va', 'si'].indexOf(this.model.get('type')) > -1) {
                if (this.model.get('type') === 'se') {
                    var valuesCollection = new Backbone.Collection(this.model.get('values') || []);
                } else {
                    var valuesCollection = new Backbone.Collection(this.model.get('range') || []);
                }
                this.selectView = new SelectCollectionView({app: this.app, collection: valuesCollection,
                    parent: this.model, type: this.parent});
                this.$('.selects').html(this.selectView.render().el);
            }
            if (this.parent.get('edit') !== 'no' && this.model.get('fid') !== 'pr') {
                this.voteView = new VoteView({target: this.model, app: this.app});
                this.ui.vote.html(this.voteView.render().el);
            }
        },

        onBeforeDestroy: function () {
            if (this.selectView) this.selectView.destroy();
            if (this.voteView) this.voteView.destroy();
        }
    });
});