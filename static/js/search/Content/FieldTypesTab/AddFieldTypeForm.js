define([
    'backform'
], function (Backform) {
    'use strict';
    return Backform.Form.extend({
        events: {
            "submit": 'onSubmit',
        },
        onSubmit: function (e) {
            e.preventDefault();
            var self = this;
            this.model.save()
                .done(function (result) {
                    var collection = self.model.collection;
                    collection.remove(self.model);
                    collection.add(self.model, {at: 0});
                })
                .fail(function (error) {
                    if (error.responseJSON && 'fieldType' in error.responseJSON) {
                        self.model.errorModel.clear();
                        self.model.errorModel.set(error.responseJSON.fieldType);
                    }
                });
            return false;
        },
        onInit: function () {
            this.changeType();
            this.model.on('change:type', this.changeType, this);
        },
        onRender: function () {
            this.changeType();
        },
        changeType: function () {
            if (this.model.get('type') === 'va') {
                this.$('.min,.max,.step').removeClass('hidden');
            } else {
                this.$('.min,.max,.step').addClass('hidden');
            }
        },
        fields: [
            {
                name: "name",
                label: "Name",
                control: "input"
            },
            {
                name: "description",
                label: "Description",
                control: "input"
            },
            {
                name: "type",
                label: "Type",
                control: "select",
                options: [
                    {label: 'None', value: null},
                    {label: 'User', value: 'us'},
                    {label: 'Item', value: 'it'}
                ]
            },
            {
                control: "button",
                label: "Save"
            },
            {
                name: "__all__",
                control: "spacer"
            }
        ]
    });
});