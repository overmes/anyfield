define([
    'backbone',
    'marionette',
    'search/Content/FieldTypesTab/AddFieldForm'
], function (Backbone, Marionette, AddFieldForm) {
    'use strict';
    return Marionette.ItemView.extend({
        initialize: function (options) {
            this.app = options.app;
            this.parent = options.parent;
            this.model.set('parent', this.parent.get('id'));
        },
        template: _.template(''),
        className: 'list-group-item field-view',
        onRender: function () {
            var form = new AddFieldForm({model: this.model});
            this.$el.append(form.render().el);
        }
    });
});