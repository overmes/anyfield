define([
    'backbone',
    'marionette',
    'search/Content/FieldTypesTab/AddFieldTypeForm'
], function (Backbone, Marionette, AddFieldTypeForm) {
    'use strict';
    return Marionette.ItemView.extend({
        initialize: function (options) {
            this.app = options.app;
        },
        className: 'list-group-item types-list',
        template: _.template(''),
        onRender: function () {
            var form = new AddFieldTypeForm({model: this.model});
            this.$el.append(form.render().el);
        }
    });
});