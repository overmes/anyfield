define([
    'backform'
], function (Backform) {
    'use strict';
    return Backform.Form.extend({
        events: {
            "submit": 'onSubmit'
        },
        onSubmit: function (e) {
            e.preventDefault();
            var self = this;
            this.model.save()
                .done(function (result) {
                    var collection = self.model.collection;
                    collection.remove(self.model);
                    collection.add(self.model, {at: 0});
                })
                .fail(function (error) {
                    if (error.responseJSON && 'field' in error.responseJSON) {
                        self.model.errorModel.clear();
                        self.model.errorModel.set(error.responseJSON.field);
                    }
                });
            return false;
        },
        fields: [
            {
                name: "name",
                label: "Name",
                control: "input"
            },
            {
                name: "description",
                label: "Description",
                control: "input"
            },
            {
                name: "type",
                label: "Type",
                control: "select",
                options: [
                    {label: 'None', value: null},
                    {label: 'Select', value: 'se'},
                    {label: 'Item Link', value: 'li'},
                    {label: 'Url', value: 'ur'},
                    {label: 'Text', value: 'te'},
                    {label: 'Value', value: 'va'},
                    {label: 'Date', value: 'da'},
                    {label: 'Image', value: 'im'}
                ]
            },
            {
                control: "button",
                label: "Save"
            },
            {
                name: "parent",
                control: "spacer"
            },
            {
                name: "__all__",
                control: "spacer"
            }
        ]
    });
});