define([
    'backbone',
    'marionette',
    'text!search/Content/FieldTypesTab/FieldTypesCollectionTemplate.html',
    'search/Content/FieldTypesTab/FieldTypeView',
    'search/Content/FieldTypesTab/AddFieldTypeView',
    'search/Content/Common/GroupsCollection/GroupModel'
], function (Backbone, Marionette, FieldTypesCollectionTemplate, FieldTypeView, AddFieldTypeView, GroupModel) {
    'use strict';
    return Marionette.CompositeView.extend({
        initialize: function (options) {
            this.app = options.app;
            this.collection = this.app.request('groupsCollection');
            this.user = this.app.request('user');
            this.listenTo(this.user, 'change:id', this.render);
        },
        className: 'container',
        childViewContainer: ".list-group",
        id: 'FieldsTab',
        getChildView: function (item) {
            if (item.get('id')) {
                return FieldTypeView;
            } else {
                return AddFieldTypeView;
            }
        },
        template: _.template(FieldTypesCollectionTemplate),
        ui: {
            'newButton': '.type-btn-new'
        },
        events: {
            'click @ui.newButton': 'addNewType'
        },
        addNewType: function () {
            this.collection.add(new GroupModel(), {at: 0});
        },

        childViewOptions: function (model, index) {
            return {
                app: this.app
            };
        },
        templateHelpers: function () {
            return {
                user: this.user
            };
        }
    });
});