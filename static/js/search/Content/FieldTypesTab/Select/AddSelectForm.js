define([
    'backform'
], function (Backform) {
    'use strict';
    return Backform.Form.extend({
        events: {
            "submit": 'onSubmit',
        },
        onSubmit: function (e) {
            e.preventDefault();
            var self = this;
            this.model.save()
                .done(function (result) {
                    var collection = self.model.collection;
                    collection.remove(self.model);
                    collection.add(self.model, {at: 0});
                })
                .fail(function (error) {
                    if (error.responseJSON && 'select' in error.responseJSON) {
                        self.model.errorModel.clear();
                        self.model.errorModel.set(error.responseJSON.select);
                    }
                });
            return false;
        },
        fields: [
            {
                name: "name",
                label: "Name",
                control: "input"
            },
            {
                name: "description",
                label: "Description",
                control: "input"
            },
            {
                control: "button",
                label: "Save"
            },
            {
                name: "__all__",
                control: "spacer"
            }
        ]
    });
});