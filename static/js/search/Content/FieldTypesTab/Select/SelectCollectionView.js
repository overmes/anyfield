define([
    'backbone',
    'marionette',
    'text!search/Content/FieldTypesTab/Select/SelectCollectionTemplate.html',
    'search/Content/FieldTypesTab/Select/SelectView',
    'search/Content/FieldTypesTab/Select/RangeView',
    'search/Content/FieldTypesTab/Select/AddSelectView',
    'search/Content/FieldTypesTab/Select/AddRangeView',
    'search/Content/Common/GroupsCollection/SelectModel',
    'search/Content/Common/GroupsCollection/RangeModel',
    'bootstrap'
], function (Backbone, Marionette, SelectCollectionTemplate, SelectView, RangeView, AddSelectView, AddRangeView, SelectModel, RangeModel) {
    'use strict';
    return Marionette.CompositeView.extend({
        initialize: function (options) {
            this.app = options.app;
            this.parent = options.parent;
            this.type = options.type;
        },
        ui: {
            addSelect: '.select-btn-add',
            collapse: '.collapse'
        },
        events: {
            'click @ui.addSelect': 'addSelect'
        },
        childViewContainer: ".list-group",
        getChildView: function (item) {
            if (item.get('is_deleted')) {
                return Marionette.ItemView.extend({template: _.template('')});
            } else if (item.get('id')) {
                if (this.parent.get('type') === 'se') {
                    return SelectView;
                } else {
                    return RangeView;
                }
            } else {
                if (this.parent.get('type') === 'se') {
                    return AddSelectView;
                } else {
                    return AddRangeView;
                }
            }
        },
        template: _.template(SelectCollectionTemplate),
        addSelect: function () {
            if (this.parent.get('type') === 'se') {
                this.collection.add(new SelectModel(), {at: 0});
            } else {
                this.collection.add(new RangeModel(), {at: 0});
            }
            this.ui.collapse.collapse('show');
        },

        childViewOptions: function (model, index) {
            return {
                app: this.app,
                parent: this.parent,
                type: this.type
            };
        },

        templateHelpers: function () {
            return {
                cid: this.cid,
                fid: this.parent.get('fid'),
                type: this.type,
                parent: this.parent,
                user: this.app.request('user')
            };
        }
    });
});