define([
    'backbone',
    'marionette',
    'text!search/Content/FieldTypesTab/Select/SelectTemplate.html',
    'search/Content/Common/Vote/VoteView'
], function (Backbone, Marionette, SelectTemplate, VoteView) {
    'use strict';
    return Marionette.ItemView.extend({
        initialize: function (options) {
            this.app = options.app;
            this.type = options.type;
            this.parent = options.parent;
        },
        className: "list-group-item",
        template: _.template(SelectTemplate),
        ui: {
            vote: '.select-vote'
        },
        modelEvents: {
            "change": "render"
        },
        onRender: function () {
            if (this.type.get('edit') !== 'no' && this.parent.get('fid') !== 'pr') {
                this.voteView = new VoteView({target: this.model, app: this.app});
                this.ui.vote.html(this.voteView.render().el);
            }
        },

        onBeforeDestroy: function () {
            if (this.voteView) this.voteView.destroy();
        },

        templateHelpers: function () {
            return {
                parent: this.parent
            }
        }
    });
});