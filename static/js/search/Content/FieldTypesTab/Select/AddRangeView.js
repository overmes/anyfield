define([
    'backbone',
    'marionette',
    'search/Content/FieldTypesTab/Select/AddRangeForm'
], function (Backbone, Marionette, AddRangeForm) {
    'use strict';
    return Marionette.ItemView.extend({
        initialize: function (options) {
            this.app = options.app;
            this.parent = options.parent;
            this.model.set('parent', this.parent.get('id'));
        },
        className: "list-group-item",
        template: _.template(''),
        onRender: function () {
            var form = new AddRangeForm({model: this.model});
            this.$el.append(form.render().el);
        }
    });
});