define([
    'backbone',
    'marionette',
    'search/Content/Neighbours/NeiRequestView'
], function (Backbone, Marionette, NeiRequestView) {
    'use strict';
    return Marionette.CompositeView.extend({
        initialize: function (options) {
            this.app = options.app;
        },
        id: 'NeighboursRequestsTable',
        tagName: 'table',
        className: 'table table-bordered table-striped',
        template: _.template("<thead><tr>" +
                "<th>Date</th><th>Status</th><th>Message</th><th>Shared items</th>" +
            "</tr></thead><tbody></tbody>"),
        childViewContainer: "tbody",
        childView: NeiRequestView
    });
});