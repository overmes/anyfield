define([
    'backbone',
    'marionette',
    'text!search/Content/Neighbours/NeighboursTemplate.html',
    'search/Content/Neighbours/NeiRequestsCollection',
    'search/Content/Neighbours/NeiRequestModel',
    'search/Content/Neighbours/AddNeiRequestForm',
    'search/Content/Neighbours/NeiRequestsCollectionView',
    'search/Content/Neighbours/ResultTable/ResultView'
], function (Backbone, Marionette, NeighboursTemplate, NeiRequestsCollection, NeiRequestModel,
             AddNeiRequestForm, NeiRequestsCollectionView, ResultView) {
    'use strict';
    return Marionette.LayoutView.extend({
        template: _.template(NeighboursTemplate),

        regions: {
            addRequestRegion: '#AddNeiRequest',
            requestsRegion: '#NeiRequests',
            activeRegion: '#ActiveResult'
        },

        initialize: function (options) {
            this.app = options.app;
            this.requests = new NeiRequestsCollection();
            this.user = this.app.request('user');
            this.loadRequests();

            this.listenTo(this.user, 'change:id', this.loadRequests);
            this.listenTo(this.requests, 'sync', this.onFetch);
            this.listenTo(this.requests, 'change:active', this.showResult);
        },

        showResult: function (model, options) {
            this.activeRegion.show(new ResultView({app: this.app, model: model}));
        },

        loadRequests: function () {
            if (this.user.isAuthenticated()) {
                this.requests.fetchLast(this.user, 10);
            }
            this.render();
        },

        onFetch: function (object) {
            if (object instanceof Backbone.Collection) {
                this.requestsRegion.show(new NeiRequestsCollectionView({
                    collection: this.requests
                }));
                if (this.requests.length === 0) {
                    this.showAddRequestView();
                } else {
                    var firstModel = this.requests.at(0);
                    if (firstModel.get('status') !== 'Loading') {
                        this.showAddRequestView();
                    } else {
                        this.checkModelLoading(firstModel);
                        firstModel.fetch();
                    }
                }
            }

        },

        showAddRequestView: function () {
            var newRequestModel = new NeiRequestModel();
            this.addRequestRegion.show(new AddNeiRequestForm({model: newRequestModel}));
            this.requests.add(newRequestModel);
            this.checkModelLoading(newRequestModel);
        },

        checkModelLoading: function (model) {
            var self = this;
            this.listenTo(model, 'sync', function () {
                self.checkLoading(model);
            });
        },

        checkLoading: function (newRequestModel) {
            if (newRequestModel.get('status') === 'Loading') {
                this.addRequestRegion.reset();
                setTimeout(function () {newRequestModel.fetch(); }, 2000);
            } else {
                this.showAddRequestView();
            }
        },

        templateHelpers: function () {
            return {
                user: this.user
            }
        }

    });
});