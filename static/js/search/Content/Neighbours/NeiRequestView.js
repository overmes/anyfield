define([
    'backbone',
    'marionette',
    'text!search/Content/Neighbours/NeiRequestTemplate.html',
    'moment'
], function (Backbone, Marionette, NeiRequestTemplate, moment) {
    'use strict';
    return Marionette.ItemView.extend({
        template: _.template(NeiRequestTemplate),
        tagName: 'tr',
        getClassName: function () {
            var active = this.model.get('active');
            if (active) return 'active';

            var status = this.model.get('status');
            if (status === 'Success') {
                return 'success';
            }
            if (status === 'Loading') {
                return 'warning';
            }
            if (status === 'Error') {
                return 'danger';
            }
        },
        className: function () {return this.getClassName();},
        initialize: function (options) {
            this.app = options.app;
        },

        templateHelpers: function () {
            var self = this;
            return {
                getDate: function () {
                    return moment(self.model.get('date')).format('LLL');
                }
            };
        },

        onChange: function () {
            this.$el.removeClass();
            this.$el.addClass(this.getClassName());
            this.render();
        },

        setActive: function () {
            var collection = this.model.collection;
            if (collection.activeRequest) collection.activeRequest.set('active', false);
            collection.activeRequest = this.model;
            this.model.set('active', true);
        },

        events: {
            'click': 'setActive'
        },

        modelEvents: {
            'change': 'onChange'
        }
    });
});