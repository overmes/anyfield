define([
    'search/helpers',
    'backform'
], function (helpers, Backform) {
    'use strict';
    return Backform.Form.extend({
        events: {
            "submit": helpers.onFormSubmit
        },
        fields: [
            {
                name: "cross_count",
                label: "Shared items",
                control: "input",
                placeholder: "Minimum shared items"
            },
            {
                name: "help",
                label: "Algorithm get Score Group(you have to fill it or make import) and count neighbours.",
                control: "help"
            },
            {
                control: "button",
                label: "Count"
            },
            {
                name: "__all__",
                control: "spacer"
            }
        ]
    });
});