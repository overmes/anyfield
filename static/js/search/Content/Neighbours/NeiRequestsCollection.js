define(['backbone', 'search/Content/Neighbours/NeiRequestModel'], function (Backbone, NeiRequestModel) {
    'use strict';
    return Backbone.Collection.extend({
        model: NeiRequestModel,
        url: '/api/userNeighbours/',
        fetchLast: function (user, limit) {
            this.fetch({data: {'order_by': '-date', 'limit': limit, 'user': user.get('id')}});
        },
        parse: function (request) {
            return request.objects;
        }
    });
});