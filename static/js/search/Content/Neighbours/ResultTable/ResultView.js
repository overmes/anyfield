define([
    'backbone',
    'marionette',
    'text!search/Content/Neighbours/ResultTable/ResultTemplate.html',
], function (Backbone, Marionette, ResultTemplate) {
    'use strict';
    return Marionette.ItemView.extend({
        template: _.template(ResultTemplate),
        initialize: function (options) {
            this.app = options.app;
        },

        modelEvents: {
            'change': 'render'
        },

        templateHelpers: function () {
            return {
                compatibility: function (value) {
                    return Math.round(value*100);
                }
            }
        }
    });
});