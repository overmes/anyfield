define([
    'backbone',
    'marionette',
    'text!search/Content/Recommendation/RecommendationTemplate.html',
    'search/Content/Recommendation/RecRequestsCollection',
    'search/Content/Recommendation/RecRequestModel',
    'search/Content/Recommendation/AddRecRequestForm',
    'search/Content/Recommendation/RecRequestsCollectionView'
], function (Backbone, Marionette, RecommendationTemplate, RecRequestsCollection, RecRequestModel,
             AddRecRequestForm, RecRequestsCollectionView) {
    'use strict';
    return Marionette.LayoutView.extend({
        template: _.template(RecommendationTemplate),

        regions: {
            addRequestRegion: '#AddRecRequest',
            requestStatusRegion: '#RecRequestStatus'
        },

        initialize: function (options) {
            this.app = options.app;
            this.requests = new RecRequestsCollection();
            this.requests.fetchLast(this.app.request('user'));

            this.listenToOnce(this.requests, 'sync', this.onFetch);
        },

        onFetch: function () {
            this.requestStatusRegion.show(new RecRequestsCollectionView({collection: this.requests}));
            if (this.requests.length === 0) {
                this.showAddRequestView();
            } else {
                var firstModel = this.requests.at(0);
                if (firstModel.get('status') !== 'Loading') {
                    this.showAddRequestView();
                } else {
                    this.checkModelLoading(firstModel);
                    firstModel.fetch();
                }
            }
        },

        showAddRequestView: function () {
            var newRequestModel = new RecRequestModel();
            this.addRequestRegion.show(new AddRecRequestForm({model: newRequestModel}));
            this.requests.add(newRequestModel);
            this.checkModelLoading(newRequestModel);
        },

        checkModelLoading: function (model) {
            var self = this;
            this.listenTo(model, 'sync', function () {
                self.checkLoading(model);
            });
        },

        checkLoading: function (newRequestModel) {
            if (newRequestModel.get('status') === 'Loading') {
                this.addRequestRegion.reset();
                setTimeout(function () {newRequestModel.fetch(); }, 2000);
            } else {
                this.showAddRequestView();
            }
        }

    });
});