define([
    'search/helpers',
    'backform'
], function (helpers, Backform) {
    'use strict';
    return Backform.Form.extend({
        events: {
            "submit": helpers.onFormSubmit
        },
        fields: [
            {
                name: "private",
                label: "Private recommendation",
                control: "boolean"
            },
            {
                name: "help",
                label: "Algorithm get Score Group(you have to fill it or make import) and count recommendations. " +
                    "After this results will be storing in Recommendation Group.",
                control: "help"
            },
            {
                control: "button",
                label: "Count"
            },
            {
                name: "__all__",
                control: "spacer"
            }
        ]
    });
});