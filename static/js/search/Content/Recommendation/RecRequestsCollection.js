define(['backbone', 'search/Content/Recommendation/RecRequestModel'], function (Backbone, RecRequestModel) {
    'use strict';
    return Backbone.Collection.extend({
        model: RecRequestModel,
        url: '/api/userRec/',
        fetchLast: function (user) {
            this.fetch({data: {'order_by': '-date', 'limit': 1, 'user': user.get('id')}});
        },
        parse: function (request) {
            return request.objects;
        }
    });
});