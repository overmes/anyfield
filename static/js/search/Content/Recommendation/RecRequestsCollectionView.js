define([
    'backbone',
    'marionette',
    'search/Content/Recommendation/RecRequestView'
], function (Backbone, Marionette, RecRequestView) {
    'use strict';
    return Marionette.CollectionView.extend({
        initialize: function (options) {
            this.app = options.app;
        },
        childView: RecRequestView
    });
});