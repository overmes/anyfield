define([
    'search/helpers',
    'backform'
], function (helpers, Backform) {
    'use strict';
    return Backform.Form.extend({
        events: {
            "submit": helpers.onFormSubmit
        },
        fields: [
            {
                name: "username",
                label: "Username",
                control: "input",
                placeholder: "MAL user name"
            },
            {
                name: "private",
                label: "Private",
                control: "boolean"
            },
            {
                name: "previous_delete",
                label: "Delete all User Scores fields",
                control: "boolean"
            },
            {
                name: "help",
                label: "Without delete will be added only scores for items " +
                    "which haven't got any User Scores fields",
                control: "help"
            },
            {
                control: "button",
                label: "Load"
            },
            {
                name: "__all__",
                control: "spacer"
            }
        ]
    });
});