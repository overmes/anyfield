define([
    'backbone',
    'marionette',
    'text!search/Content/ScoresImport/ScoresRequestTemplate.html',
    'moment'
], function (Backbone, Marionette, ScoresRequestTemplate, moment) {
    'use strict';
    return Marionette.ItemView.extend({
        template: _.template(ScoresRequestTemplate),

        initialize: function (options) {
            this.app = options.app;
        },

        templateHelpers: function () {
            var self = this;
            return {
                getAlertClass: function () {
                    var status = self.model.get('status');
                    if (status === 'Success') {
                        return 'alert-success';
                    }
                    if (status === 'Loading') {
                        return 'alert-warning';
                    }
                    if (status === 'Error') {
                        return 'alert-danger';
                    }
                },
                getDate: function () {
                    return moment(self.model.get('date')).format('LLL');
                }
            };
        },

        modelEvents: {
            'change': 'render'
        }
    });
});