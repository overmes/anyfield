define([
    'backbone',
    'marionette',
    'search/Content/ScoresImport/ScoresRequestView'
], function (Backbone, Marionette, ScoresRequestView) {
    'use strict';
    return Marionette.CollectionView.extend({
        initialize: function (options) {
            this.app = options.app;
        },
        childView: ScoresRequestView
    });
});