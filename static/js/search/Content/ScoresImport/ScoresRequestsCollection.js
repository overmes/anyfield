define(['backbone', 'search/Content/ScoresImport/ScoresRequestModel'], function (Backbone, ScoresRequestModel) {
    'use strict';
    return Backbone.Collection.extend({
        model: ScoresRequestModel,
        url: '/api/userRequest/',
        fetchLast: function (user) {
            this.fetch({data: {'order_by': '-date', 'limit': 1, 'user': user.get('id')}});
        },
        parse: function (request) {
            return request.objects;
        }
    });
});