define([
    'backbone',
    'marionette',
    'text!search/Content/ScoresImport/ScoresImportTemplate.html',
    'search/Content/ScoresImport/ScoresRequestsCollection',
    'search/Content/ScoresImport/ScoresRequestModel',
    'search/Content/ScoresImport/AddScoreRequestForm',
    'search/Content/ScoresImport/ScoresRequestsCollectionView'
], function (Backbone, Marionette, ScoresImportTemplate, ScoresRequestsCollection, ScoresRequestModel,
             AddScoreRequestForm, ScoresRequestsCollectionView) {
    'use strict';
    return Marionette.LayoutView.extend({
        template: _.template(ScoresImportTemplate),

        regions: {
            addRequestRegion: '#AddScoreRequest',
            requestStatusRegion: '#ScoreRequestStatus'
        },

        initialize: function (options) {
            this.app = options.app;
            this.requests = new ScoresRequestsCollection();
            this.requests.fetchLast(this.app.request('user'));

            this.listenToOnce(this.requests, 'sync', this.onFetch);
        },

        onFetch: function () {
            this.requestStatusRegion.show(new ScoresRequestsCollectionView({collection: this.requests}));
            if (this.requests.length === 0) {
                this.showAddRequestView();
            } else {
                var firstModel = this.requests.at(0);
                if (firstModel.get('status') !== 'Loading') {
                    this.showAddRequestView();
                } else {
                    this.checkModelLoading(firstModel);
                    firstModel.fetch();
                }
            }
        },

        showAddRequestView: function () {
            var newRequestModel = new ScoresRequestModel();
            this.addRequestRegion.show(new AddScoreRequestForm({model: newRequestModel}));
            this.requests.add(newRequestModel);
            this.checkModelLoading(newRequestModel);
        },

        checkModelLoading: function (model) {
            var self = this;
            this.listenTo(model, 'sync', function () {
                self.checkLoading(model);
            });
        },

        checkLoading: function (newRequestModel) {
            if (newRequestModel.get('status') === 'Loading') {
                this.addRequestRegion.reset();
                setTimeout(function () {newRequestModel.fetch(); }, 2000);
            } else {
                this.showAddRequestView();
            }
        }

    });
});