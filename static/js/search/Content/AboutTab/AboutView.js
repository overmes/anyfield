define([
    'backbone',
    'marionette',
    'search/Content/AboutTab/StaticPageModel'
], function (Backbone, Marionette, StaticPageModel) {
    'use strict';
    return Marionette.ItemView.extend({
        template: _.template(''),
        initialize: function (options) {
            this.model = new StaticPageModel({id: options.controller});
            this.model.fetch();
        },

        modelEvents: {
            'sync': 'onSync'
        },

        onSync: function () {
            this.template = _.template(this.model.get('html'));
            this.render();
        }
    });
});