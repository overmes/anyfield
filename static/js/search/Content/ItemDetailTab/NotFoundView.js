define([
    'backbone',
    'marionette',
], function (Backbone, Marionette) {
    'use strict';
    return Marionette.ItemView.extend({
        initialize: function (options) {
            this.app = options.app;
        },

        template: _.template(
        '<div class="alert alert-danger" role="alert">' +
            '<h1>Item with id #<%- obj.id %> not found</h1>' +
        '</div>')
    });
});