define([
    'backbone',
    'marionette',
    'text!search/Content/ItemDetailTab/ItemDetailTemplate.html',
    'search/Content/ItemDetailTab/GroupView'
], function (Backbone, Marionette, ItemDetailTemplate, GroupView) {
    'use strict';
    return Marionette.CompositeView.extend({
        initialize: function (options) {
            this.app = options.app;
            this.user = this.app.request('user');
            this.appController = this.app.appController;

            this.collection = options.selectedGroupsCollection;
            if (!this.collection) {
                this.collection = this.app.request('groupsCollection').getFilteredCollection(function (field) {
                    return !field.get('is_temp') && field.has('id') && field.get('type') !== 'si';
                });
            }
            this.itemGroupsCollection = this.model.getGroupsCollection(this.user);
            this.app.request('voteStats').loadItemStats(this.model.get('id'), this.collection.pluck('id'));
        },
        id: 'ItemDetailView',
        childViewContainer: '.item-child',
        template: _.template(ItemDetailTemplate),
        childView: GroupView,
        ui: {
            link: 'h1 > a'
        },

        events: {
            'click @ui.link': 'linkClick'
        },
        linkClick: function (event) {
            event.preventDefault();
            event.stopPropagation();
            var target = event.currentTarget;
            var itemId = target.href.split("/")[4];
            var title = target.text;
            this.appController.showItemTab(itemId, title);
        },
        childViewOptions: function (model, index) {
            return {
                app: this.app,
                itemGroup: this.itemGroupsCollection.get(model.get('id')),
                itemId: this.model.get('id')
            };
        },
        templateHelpers: function () {
            return {
                itemUrl: this.model.getItemUrl(false)
            };
        }
    });
});