define([
    'backbone',
    'marionette',
    'text!search/Content/ItemDetailTab/AddFieldTemplate.html',
    'search/Content/Common/ItemSelect/SingleItemSelectView',
    'datepicker',
    'moment',
    'modelbinder',
    'select'
], function (Backbone, Marionette, FieldTemplate, SingleItemSelectView, DatePicker, moment, ModelBinder) {
    'use strict';
    return Marionette.ItemView.extend({
        tagName: 'tr',
        initialize: function (options) {
            this.app = options.app;
            this.user = this.app.request('user');
            this.currentGroup = options.currentGroup;
            this.controls = options.controls;

            this.listenTo(this.model, 'sync', this.onSave);
            this.listenTo(this.model, 'error', this.onError);
        },

        onSave: function (model, resp, options) {
            this.model.unset('errors');
            var collection = this.model.collection;
            collection.remove(this.model);
            collection.add(this.model);
        },
        onError: function (model, xhr, options) {
            this.model.set('errors', xhr.responseJSON.itemField);
            this.render();
        },
        template: _.template(FieldTemplate),

        ui: {
            select: 'select',
            date: 'input[data-type=da]',
            save: '.js-save',
            input: ':input:not(:button)',
            itemSelect: 'div[data-type=li]',
            delete: '.js-del'
        },

        events: {
            'click @ui.save': 'saveField',
            'click @ui.delete': 'deleteField',
            'change @ui.input': 'setFieldData'
        },

        deleteField: function () {
            this.model.collection.remove(this.model);
        },

        setFieldData: function () {
            var allInputs = this.ui.input;
            var self = this;
            allInputs.each(function () {
                var value = this.value;
                if (value && value !== 'null') {
                    if (this.dataset['type'] === 'da') value = moment(this.value, 'L').unix();
                    self.model.set(this.name, value);
                } else {
                    self.model.unset(this.name);
                }
            });
        },

        saveField: function () {
            this.model.save();
        },

        onRender: function () {
            this.ui.select.selectpicker();
            this.ui.date.datepicker({
                autoclose: true,
                todayHighlight: true,
                startView: 0,
                minViewMode: 0
            });
            var self = this;

            this.ui.itemSelect.each( function (index, element) {
                var fid = element.dataset['fid'];
                var itemSelectView = new SingleItemSelectView({app: self.app, attribute: fid, model: self.model});
                self.$("div[data-fid={0}]".format(fid)).html(itemSelectView.render().el);
            });
        },

        templateHelpers: function () {
            var self = this;
            return {
                currentGroup: this.currentGroup,
                isSelected: function (fid, value) {
                    return parseInt(self.model.get(fid)) === value.number;
                },
                formatDate: function (value) {
                    return value ? moment.unix(value).format('L') : '';
                },
                user: this.user,
                controls: this.controls
            };
        }
    });
});