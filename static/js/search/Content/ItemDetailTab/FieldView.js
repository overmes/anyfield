define([
    'backbone',
    'marionette',
    'text!search/Content/ItemDetailTab/FieldTemplate.html',
    'search/Content/SearchTab/Request/SerializeController',
    'search/Content/Common/Vote/VoteView'
], function (Backbone, Marionette, FieldTemplate, SerializeController, VoteView) {
    'use strict';
    return Marionette.ItemView.extend({
        tagName: 'tr',
        className: function () {
            return this.model.get('is_temp') ? 'success' : null;
        },
        initialize: function (options) {
            this.app = options.app;

            this.options = this.app.request('user').get('settings');
            this.appController = this.app.appController;
            this.currentGroup = options.currentGroup;
            this.controls = options.controls;
        },
        modelEvents: {
            "change": "render"
        },

        onRender: function () {
            this.$el.attr('class', this.className());
            if (this.currentGroup.get('edit') !== 'no' && this.currentGroup.get('type') !== 'us') {
                this.voteView = new VoteView({target: this.model, app: this.app, is_item: true});
                this.ui.vote.html(this.voteView.render().el);
            }
        },
        onBeforeDestroy: function () {
            if (this.voteView) this.voteView.destroy();
        },
        template: _.template(FieldTemplate),

        ui: {
            link: '.field-li a',
            delete: '.js-delete',
            vote: '.view-vote-place',
            textShow: '.table-text-show',
            textHide: '.table-text-hide'
        },

        events: {
            'click @ui.link': 'linkClick',
            'click @ui.delete': 'deleteClick',
            'click @ui.textShow': 'onTextShow',
            'click @ui.textHide': 'onTextHide'
        },

        onTextShow: function (event) {
            event.stopPropagation();
            var $target = $(event.currentTarget.parentElement);
            $target.find('.table-text-show').hide();
            $target.find('.table-text-hidden').removeClass('hidden');
        },

        onTextHide: function (event) {
            event.stopPropagation();
            var $target = $(event.currentTarget.parentElement.parentElement);
            $target.find('.table-text-show').show();
            $target.find('.table-text-hidden').addClass('hidden');
        },

        linkClick: function (event) {
            event.preventDefault();
            var target = event.currentTarget;
            var itemId = target.href.split("/")[4];
            var title = target.href.split("/")[5];
            this.appController.showItemTab(itemId, title);
        },

        deleteClick: function (event) {
            this.model.destroy({wait: true});
        },

        templateHelpers: function () {
            var self = this;
            return {
                currentGroup: this.currentGroup,
                getSerializeFields: function () {
                    return SerializeController.serializeFields(self.model.toJSON(), self.currentGroup, self.options);
                },
                getField: function (index) {
                    return self.currentGroup.getByIndex(index);
                },
                user: this.app.request('user'),
                controls: this.controls
            };
        }
    });
});