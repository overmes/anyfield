define([
    'backbone',
    'marionette',
    'text!search/Content/ItemDetailTab/ItemTemplate.html',
    'search/Content/SearchTab/Request/ItemModel',
    'search/Content/ItemDetailTab/ItemDetailView',
    'search/Content/ItemDetailTab/NotFoundView'
], function (Backbone, Marionette, ItemTemplate, ItemModel, ItemDetailView, NotFoundView) {
    'use strict';
    return Marionette.LayoutView.extend({
        regions: {
            "detailRegion": "#ItemDetailView"
        },
        className: 'container',
        initialize: function (options) {
            this.app = options.app;
            this.model = new ItemModel({id: options.itemId});
            this.user = this.app.request('user');
            this.model.fetch();
            this.listenTo(this.model, 'sync', this.onFetch);
            this.listenTo(this.model, 'error', this.onError);

            this.listenTo(this.user, 'change:id', function () {
                this.model.fetch();
            });
        },

        onError: function () {
            this.detailRegion.show(new NotFoundView({app: this.app, model: this.model}));
        },

        onFetch: function () {
            this.app.vent.trigger('item:detail:loaded', this.model.getId(), this.model.getTitle());
            this.detailRegion.show(new ItemDetailView({app: this.app, model: this.model}));
        },

        template: _.template(ItemTemplate)
    });
});