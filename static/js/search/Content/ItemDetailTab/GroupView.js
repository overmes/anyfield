define([
    'backbone',
    'marionette',
    'text!search/Content/ItemDetailTab/GroupTemplate.html',
    'search/Content/ItemDetailTab/FieldView',
    'search/Content/ItemDetailTab/AddFieldView',
    'search/Content/SearchTab/Request/ItemGroupFieldModel',
    'search/Content/SearchTab/Request/ItemGroupFieldsCollection'
], function (Backbone, Marionette, GroupTemplate, FieldView, AddFieldView, ItemGroupFieldModel, ItemGroupFieldsCollection) {
    'use strict';
    return Marionette.CompositeView.extend({
        initialize: function (options) {
            _.defaults(options, {controls: this.model.get('edit') !== 'no'});

            this.app = options.app;
            this.user = this.app.request('user');
            this.itemId = options.itemId;
            this.controls = options.controls;
            this.itemGroup = options.itemGroup;
            if (this.itemGroup) {
                this.collection = this.itemGroup.get('fields');
            } else {
                this.collection = new ItemGroupFieldsCollection();
            }
        },
        ui: {
            addField: '.js-add-field'
        },
        events: {
            'click @ui.addField': 'addField'
        },
        addField: function (event) {
            return this.collection.add(new ItemGroupFieldModel({item: this.itemId, type: this.model.get('id')}));
        },
        getChildView: function (item) {
            if (item.has("id")) {
                return FieldView;
            } else {
                return AddFieldView;
            }
        },
        template: _.template(GroupTemplate),
        childViewContainer: "tbody",

        childViewOptions: function () {
            return {
                currentGroup: this.model,
                app: this.app,
                controls: this.controls
            };
        },

        templateHelpers: function () {
            return {
                user: this.user,
                controls: this.controls
            };
        }
    });
});