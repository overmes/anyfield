define([
    'backbone',
    'search/Content/Common/GroupsCollection/GroupModel',
    'search/Content/Common/GroupsCollection/FieldsCollection'
], function (Backbone, GroupModel, FieldsCollection) {
    'use strict';
    var current = Backbone.Collection.extend({
        model: GroupModel,
        url: '/api/fieldType/',

        parse: function (response) {
            return response.objects;
        },

        getFilteredCollection: function (filter) {
            var filteredGroups = this.filter(function (group) {
                var filteredFields = group.get('fields').filter(filter);
                if (filteredFields.length === 1 && filteredFields[0].get('fid') === 'pr') {
                    return false;
                } else {
                    return _.any(filteredFields);
                }
            });
            var newGroups = _.map(filteredGroups, function (group) {
                var newGroup = new GroupModel(group.toJSON());
                newGroup.set('fields', group.get('fields').getFilteredCollection(filter));
                return newGroup;
            });
            return new current(newGroups);
        },

        clone: function () {
            var newGroups = this.map(function (group) {
                var newGroup = new GroupModel(group.toJSON());
                newGroup.set('fields', new FieldsCollection(group.get('fields').toJSON()));
                return newGroup;
            });
            return new current(newGroups);
        },

        getField: function (group, field) {
            var currentGroup = this.get(group);
            return currentGroup ? currentGroup.get('fields').get(field) : null;
        },

        getTitleField: function () {
            return this.get(this.getTitleFieldId()).getByFid('a');
        },

        getContentTypeField: function () {
            return this.get(10).getByFid('a');
        },

        getTitleFieldId: function () {
            return 2;
        },

        getSelectedItemsField: function () {
            return this.get(3).getByFid('a');
        }
    });
    return current;
});