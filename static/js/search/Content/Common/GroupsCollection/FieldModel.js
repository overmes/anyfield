define(['backbone'], function (Backbone) {
    'use strict';
    return Backbone.Model.extend({
        urlRoot: '/api/field/',
        getTypeName: function () {
            return this.types[this.get('type')];
        },
        types: {
            'se': 'Select',
            'li': 'Link',
            'te': 'Text',
            'va': 'Value',
            'da': 'Date',
            'im': 'Image',
            'id': 'Id',
            'co': 'Count'
        }
    });
});