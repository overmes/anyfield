define(['backbone', 'search/Content/Common/GroupsCollection/FieldModel'], function (Backbone, FieldModel) {
    'use strict';
    var current = Backbone.Collection.extend({
        model: FieldModel,
        getFilteredCollection: function (filter) {
            return new current(this.filter(filter));
        }
    });
    return current;
});