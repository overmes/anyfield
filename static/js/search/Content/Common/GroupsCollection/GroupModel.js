define(['backbone', 'search/Content/Common/GroupsCollection/FieldsCollection'], function (Backbone, FieldsCollection) {
    'use strict';
    return Backbone.Model.extend({
        urlRoot: '/api/fieldType/',
        idAttribute: 'id',

        initialize: function (options) {
            if (options && !options.fields) this.set('fields', new FieldsCollection());
        },

        parse: function (response) {
            _.each(response.fields, function (field) {
                field.parent = this;
            }, this);
            response.fields = new FieldsCollection(response.fields);
            response.fields.parent = this;
            return response
        },

        defaults: {
            index: 'i'
        },

        getByIndex: function (index) {
            return this.get('fields').at(index);
        },

        getByFid: function (fid) {
            return this.get('fields').filter(function (field) {
                return field.get('fid') === fid;
            })[0];
        }
    });
});