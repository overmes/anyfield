define([
    'backbone',
    'marionette',
    'text!search/Content/Common/FieldsSelect/GroupSelectTemplate.html',
    'search/Content/Common/FieldsSelect/FieldsSelectView',
    'masonry',
    'search/Content/Common/FieldsSelect/OptionsStateModel',
    'modelbinder'
], function (Backbone, Marionette, GroupSelectTemplate, FieldsSelectView, Masonry, OptionsStateModel, ModelBinder) {
    'use strict';
    return Marionette.CompositeView.extend({
        childView: FieldsSelectView,
        className: 'panel panel-info fields-select',
        childViewContainer: '.panel-body',
        childViewEventPrefix: 'field',

        template: _.template(GroupSelectTemplate),

        initialize: function (options) {
            this.app = options.app;
            this.collection = options.collection;
            this.selectedGroups = options.selectedGroups;
            this.multi = 'multi' in options ? options.multi: true;
            this.state = new OptionsStateModel({multiuser: false, multiitem: false});
            this.modelBinder = new ModelBinder();
        },

        ui: {
            grid: '.fields-grid',
            multiuser: '.multi-user',
            multiitem: '.multi-item'
        },

        childViewOptions: function (model, index) {
            return {
                state: this.state,
                selectedGroups: this.selectedGroups,
                app: this.app
            };
        },

        templateHelpers: function () {
            return {
                multi: this.multi
            }
        },

        onRender: function () {
            var msnry = new Masonry(this.ui.grid.get(0), {
                columnWidth: 200,
                itemSelector: '.field-type',
                gutter: 5,
                isOriginLeft: true,
                isOriginTop: true
            });

            if (this.multi) {
                var bindings = {'multiuser': {selector: this.ui.multiuser}, 'multiitem': {selector: this.ui.multiitem}};
                this.modelBinder.bind(this.state, this.el, bindings);
            }
        }

    });
});