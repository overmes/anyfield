define([
    'jquery',
    'backbone',
    'marionette',
    'text!search/Content/Common/FieldsSelect/ItemSelectTemplate.html'
], function ($, Backbone, Marionette, ItemSelectTemplate) {
    'use strict';
    return Marionette.ItemView.extend({
        template: _.template(ItemSelectTemplate),
        className: 'list-group-item field-select',
        tagName: 'a',

        initialize: function (options) {
            this.app = options.app;
            this.selectedGroups = options.selectedGroups;
            this.group = options.group;
            this.state = options.state;

            this.listenTo(this.selectedGroups, 'group:field:disabled', this.onDisabled);
        },

        ui: {
            div: 'div'
        },

        events: {
            'click @ui.div': 'onSelect'
        },

        onSelect: function (event) {
            var is_added = this.selectedGroups.selectField(this.group, this.model, this.getMulti());
            if (is_added !== undefined) this.setDisabled(is_added);
            if (is_added) {
                this.app.vent.trigger('group:field:selected', this.group, this.model, this.getMulti(),
                    this.selectedGroups);
            }
        },

        onDisabled: function (group, model, disabled) {
            if (this.group === group && this.model === model) {
                this.setDisabled(disabled);
            }
        },

        getMulti: function () {
            if (this.state)
                return this.group.get('type') === 'us' ? this.state.get('multiuser') : this.state.get('multiitem');
        },

        setDisabled: function (isDisabled) {
            if (isDisabled) {
                this.$el.addClass('disabled');
            } else {
                this.$el.removeClass('disabled');
            }
        },

        onRender: function () {
            var selectedGroups = this.selectedGroups.getGroupsByGroup(this.group);
            if (selectedGroups.length && selectedGroups[0].isFieldSelected(this.model)) {
                this.setDisabled(true);
            }
        }
    });
});