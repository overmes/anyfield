define([
    'jquery',
    'backbone',
    'marionette',
    'text!search/Content/Common/FieldsSelect/FieldsSelectTemplate.html',
    'search/Content/Common/FieldsSelect/ItemSelectView'
], function ($, Backbone, Marionette, FieldsSelectTemplate, ItemSelectView) {
    'use strict';
    return Marionette.CompositeView.extend({
        childView: ItemSelectView,
        childViewContainer: '.child-fields',
        template: _.template(FieldsSelectTemplate),
        className: 'field-type list-group',
        tagName: 'ul',

        initialize: function (options) {
            this.app = options.app;
            this.selectedGroups = options.selectedGroups;
            this.collection = this.model.get('fields');
            this.state = options.state;
        },

        childViewOptions: function (model, index) {
            return {
                state: this.state,
                group: this.model,
                selectedGroups: this.selectedGroups,
                app: this.app
            };
        }

    });
});