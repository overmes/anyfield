define([
    'backbone',
    'marionette',
    'search/Content/SearchTab/Request/SerializeController',
    'search/Content/SearchTab/FieldsPanel/SelectedGroupsList/SelectedGroupsCollection',
    'search/Content/SearchTab/Request/RequestModel',
    'ajax-select'
], function (Backbone, Marionette, SerializeController, SelectedGroupsCollection, RequestModel) {
    'use strict';
    return Marionette.ItemView.extend({
        initialize: function (options) {
            this.app = options.app;
            this.attribute = options.attribute;
            this.width = options.width || '300px';
            this.contentTypeGroup = this.app.request('groupsCollection').get('10');
            this.groupsCollection = this.app.request('groupsCollection');
        },

        ui: {
            select: 'select'
        },

        events: {
            'change @ui.select': 'onSelectChange'
        },
        getTypeLabel: function (type) {
            return {
                'Anime': 'label-success',
                'Manga': 'label-primary'
            }[type];
        },
        getContent: function (type, text) {
            var template = _.template("<span class='item-select-option label <%- class_name %>'><%- type %></span> <%- title %>");
            return template({class_name: this.getTypeLabel(type), type: type, title: text});
        },
        queryFactory: function () {
            var groupsCollection = this.groupsCollection;
            var app = this.app;
            return function () {
                var fields = new SelectedGroupsCollection();
                var titleGroup = fields.addField(groupsCollection.getTitleField());
                titleGroup.set({'on': true});
                var contentTypeGroup = fields.addField(groupsCollection.getContentTypeField());
                contentTypeGroup.set({'on': true});

                var filters = new SelectedGroupsCollection();
                var selectedFilterGroup = filters.addField(groupsCollection.getTitleField(),
                    {'filter': this.plugin.query, 'on': true});
                selectedFilterGroup.set({'on': true, 'include': true});

                var requestModel = new RequestModel({}, {
                    app: app,
                    selectedGroups: fields,
                    filtersGroups: filters
                });
                return requestModel.getQuery();
            };

        },

        onRender: function () {
            var self = this;
            var selectPicker = this.ui.select.selectpicker({liveSearch: true, width: this.width});
            //TODO: preserve selected https://github.com/truckingsim/Ajax-Bootstrap-Select/issues/41
            selectPicker.ajaxSelectPicker({
                ajax: {
                    type: 'GET',
                    url: '/api/query/',
                    data: self.queryFactory(),
                    dataType: 'json'
                },
                preprocessData: function (data) {
                    var result =  _.map(data.objects, function (raw) {
                        var type = SerializeController.serializeFields(raw.i['10'][0], self.contentTypeGroup)[0];
                        var text = raw.i['2'][0].a;
                        return {
                            'value': raw.i.i,
                            'text': "{0} {1}".format(type, text),
                            'data': {
                                'content': self.getContent(type, text)
                            }
                        };
                    });
                    result.unshift({value: null, text: 'None'});
                    return result;
                },
                locale: {
                    emptyTitle: 'Select item'
                },
                preserveSelected: true
            });
        },

        templateHelpers: function () {
            var self = this;
            return {
                getInit: function () {
                    return self.model.get(self.attribute);
                },
                multi: this.multi
            };
        }

    });
});