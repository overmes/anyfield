define([
    'backbone',
    'marionette',
    'text!search/Content/Common/ItemSelect/MultiItemSelectTemplate.html',
    'search/Content/Common/ItemSelect/AbstractItemSelectView'
], function (Backbone, Marionette, MultiItemSelectTemplate, AbstractItemSelectView) {
    'use strict';
    return AbstractItemSelectView.extend({
        template: _.template(MultiItemSelectTemplate),

        onSelectChange: function (event) {
            if (this.model) {
                var data = [];
                this.ui.select.find('option:selected').each(function (index, elem) {
                    var name = elem.text;
                    var value = parseInt(elem.value);
                    data.push([value, name]);
                });
                this.$('.filter-option').text(_.map(data, function (d) {return d[1]; }).join(', '));
                this.model.set(this.attribute, data);
            }
        }
    });
});