define([
    'backbone',
    'marionette',
    'text!search/Content/Common/ItemSelect/SingleItemSelectTemplate.html',
    'search/Content/Common/ItemSelect/AbstractItemSelectView'
], function (Backbone, Marionette, SingleItemSelectTemplate, AbstractItemSelectView) {
    'use strict';
    return AbstractItemSelectView.extend({
        template: _.template(SingleItemSelectTemplate),

        onSelectChange: function (event) {
            if (this.model) {
                var value = parseInt(this.ui.select.selectpicker('val')) || null;
                var name = this.ui.select.find('option:selected').text();
                this.$('.filter-option').text(name);
                var data = {title: name};
                data[this.attribute] = value;
                this.model.set(data);
            }
        }
    });
});