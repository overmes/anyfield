define(['backbone'], function (Backbone) {
    'use strict';
    return Backbone.Model.extend({
        urlRoot: '/api/vote/',

        initialize: function (data, options) {
            this.target = options.target;
            this.stats = options.stats;
            this.set('content_object', this.target.get('resource_uri'));
            this.listenTo(this, 'sync', this.onSync);
        },

        onSync: function () {
            if (this.get('type_vote_sum') !== null && this.get('is_deleted') !== true) {
                this.target.set('is_temp', this.get('is_temp'));
                this.stats.setVote(this);
            } else {
                this.target.collection.remove(this.target);
            }
        },

        vote: function (value) {
            this.set('value', value);
            this.set('id', null);
            this.save();
        }

    });
});