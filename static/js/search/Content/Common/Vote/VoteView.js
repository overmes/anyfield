define([
    'backbone',
    'marionette',
    'text!search/Content/Common/Vote/VoteTemplate.html',
    'search/Content/Common/Vote/VoteModel'
], function (Backbone, Marionette, VoteTemplate, VoteModel) {
    'use strict';
    return Marionette.ItemView.extend({
        initialize: function (options) {
            this.app = options.app;
            this.voteStats = this.app.request('voteStats');
            this.target = options.target;

            this.model = new VoteModel({}, {target: this.target, stats: this.voteStats});
            this.is_item = options.is_item || false;

            this.user = this.app.request('user');
            this.listenTo(this.user, 'change:id', this.userChange);
            this.listenTo(this.voteStats, 'change', this.render);
            this.userChange();
        },

        template: _.template(VoteTemplate),

        className: 'view-vote panel',

        ui: {
            vote: '.js-vote'
        },

        events: {
            'click @ui.link': 'linkClick',
            'click @ui.delete': 'deleteClick',
            'click @ui.vote': 'voteClick'
        },

        tagName: 'span',

        userChange: function () {
            if (this.user.isAuthenticated()) {
                this.$el.removeClass('hidden');
            } else {
                this.$el.addClass('hidden');
            }
        },

        voteClick: function (event) {
            event.preventDefault();
            event.stopPropagation();
            var target = event.currentTarget;
            var value = parseInt(target.dataset.value);
            this.model.vote(value);
        },

        templateHelpers: function () {
            return {
                user: this.user,
                target: this.target,
                stats: this.voteStats
            };
        }
    });
});