define(['backbone', 'search/Content/Common/Vote/VoteItemStatsModel'], function (Backbone, VoteItemStatsModel) {
    'use strict';
    return Backbone.Model.extend({
        urlRoot: '/api/voteStats/',

        defaults: {
            user: {},
            all: {}
        },

        initialize: function (data, options) {
            this.app = options.app;
            this.user = this.app.request('user');

            this.listenTo(this.user, 'change:id', function () { return this.fetch()});
        },

        getUserVote: function (target) {
            return this.getVote('user', target);
        },

        getVoteSum: function (target) {
            return this.getVote('all', target);
        },

        setVote: function (voteModel) {
            var target = voteModel.target;
            this.get('user')[target.get('resource_uri')] = voteModel.get('user_vote_sum');
            this.get('all')[target.get('resource_uri')] = voteModel.get('type_vote_sum');
            this.trigger('change');
        },

        getVote: function (type, target) {
            return this.get(type)[target.get('resource_uri')];
        },

        addItemStats: function (itemStats) {
            _.extend(this.get('all'), itemStats.get('all'));
            _.extend(this.get('user'), itemStats.get('user'));
            this.trigger('change');
        },

        loadItemStats: function (itemId, groupIds) {
            var self = this;
            var itemStats = new VoteItemStatsModel({id: itemId});
            itemStats.fetch({data: {groups: JSON.stringify(groupIds)}}).done(function () {
                self.addItemStats(itemStats);
            });
        }
    });
});