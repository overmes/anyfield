define(['backbone'], function (Backbone) {
    'use strict';
    return Backbone.Model.extend({
        urlRoot: '/api/voteItemStats/',
        url: function () {
            return this.get('id') ? this.urlRoot + this.get('id') + '/' : this.urlRoot;
        }
    });
});