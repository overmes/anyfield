define([
    'backbone',
    'marionette',
    'text!search/Content/Common/UserSelect/UserSelectTemplate.html',
    'modelbinder',
    'ajax-select'
], function (Backbone, Marionette, UserSelectTemplate, ModelBinder) {
    'use strict';
    return Marionette.ItemView.extend({
        template: _.template(UserSelectTemplate),

        initialize: function (options) {
            this.app = options.app;
            this.userModel = this.app.request('user');
            if (this.model && !this.model.has('user') && this.userModel.isAuthenticated()) {
                this.model.set({'user': this.userModel.get('id'), 'username': this.userModel.get('username')}, {silent: true});
            }
        },

        ui: {
            select: 'select'
        },

        events: {
            'change @ui.select': 'onSelectChange'
        },

        onShow: function () {
            var self = this;
            var selectPicker = this.ui.select.selectpicker({liveSearch: true, width: '100%'});
            selectPicker.ajaxSelectPicker({
                ajax: {
                    type: 'GET',
                    url: '/api/user/',
                    data: {
                        username__icontains: '{{{q}}}'
                    }
                },
                preprocessData: function (data) {
                    var users =  _.map(data.objects, function (raw) {
                        return {
                            'value': raw.id,
                            'text': raw.username
                        };
                    });
                    if (self.userModel.isAuthenticated()) {
                        users.unshift({value: self.userModel.get('id'), text: self.userModel.get('username')});
                    }
                    users.unshift({value: null, text: 'Select user'});
                    return users;
                },
                locale: {
                    emptyTitle: 'Select user'
                }
            });

//            this.$('ul.dropdown-menu.selectpicker').on('mousedown', $.proxy(this.onSelectChange, this));
        },

        onSelectChange: function (event) {
            if (this.model) {
                var value = parseInt(this.ui.select.selectpicker('val')) || null;
                var username = this.ui.select.find('option:selected').text();
                this.$('.filter-option').text(username);
                var old_user = this.model.get('user');
                this.model.set({'user': value, 'username': username, 'old_user': old_user});
            }
        },

        templateHelpers: function () {
            return {
                userModel: this.userModel
            };
        }

    });
});