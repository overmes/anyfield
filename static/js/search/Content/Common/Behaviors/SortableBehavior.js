define([
    'backbone',
    'marionette',
    'jquery-ui/sortable'
], function (Backbone, Marionette) {
    'use strict';
    return  Marionette.Behavior.extend({
        defaults: {
            parent: 'ul',
            child: 'li'
        },
        initialize: function () {
            this.currentSort = [];
        },

        onSortChange: function (event, ui) {
            this.currentSort = [];
            this.view.children.each(function (view) {
                var uniqueId = _.uniqueId();
                view.model.uniqueId = uniqueId;
                view.$el.attr('id', uniqueId);
            });
            this.currentSort = this.view.$(this.options.child).map(function (index, elem) {
                return elem.id;
            }).toArray();
            this.view.collection.sort();
        },

        onRender: function () {
            var self = this;
            this.view.collection.comparator = function(model) {
                var index = self.currentSort.indexOf(model.uniqueId);
                return  index >= 0 ? index : 9999;
            };
            this.view.$(this.options.parent).sortable({
                update: $.proxy(this.onSortChange, this),
                delay: 200
            });

        }

    });
});