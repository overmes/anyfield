define([
    'backbone',
    'marionette',
    'nprogress'
], function (Backbone, Marionette, NProgress) {
    'use strict';
    NProgress.configure({ trickleRate: 0.1, trickleSpeed: 200 ,showSpinner: false});
    return  Marionette.Behavior.extend({
        modelEvents: {
            "request": 'start',
            "sync": 'done'
        },

        collectionEvents: {
            "request": 'start',
            "sync": 'done'
        },

        start: function () {
            NProgress.start();
        },

        done: function () {
            NProgress.done()
        }
    });
});