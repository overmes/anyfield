define([
    'backbone',
    'marionette',
    'modelbinder',
    'bootstrap'
], function (Backbone, Marionette, ModelBinder) {
    'use strict';
    return  Marionette.Behavior.extend({
        initialize: function() {
            this.modelBinder = new ModelBinder();
            if (!this.view.options.model.has('include')) {
                this.view.options.model.set('include', true, {silent: true});
            }
        },

        onShow: function () {
            var inSelect = this.getSelector('.js-in');
            var outSelect = this.getSelector('.js-out');
            var bindings = {
                include: [
                    {selector: inSelect + ' input', converter: this.radioConverter},
                    {selector: outSelect + ' input', converter: this.revertRadioConverter}
                ]
            };
            this.modelBinder.bind(this.view.options.model, this.view.el, bindings);
            if (this.view.options.model.get('include')) {
                this.$el.find(inSelect).addClass('active');
            } else {
                this.$el.find(outSelect).addClass('active');
            }
            this.$el.find('.inout-select .btn').button();
        },


        radioConverter: function (direction, value) {
            return !!value;
        },

        revertRadioConverter: function (direction, value) {
            return !value;
        },

        getSelector: function (selector) {
            var res;
            if (this.view.$(selector).length > 1) {
                res = selector + ':not(.child-fields ' + selector + ')';
            } else {
                res = selector;
            }
            return res;
        }

    });
});