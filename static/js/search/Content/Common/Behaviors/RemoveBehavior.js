define([
    'backbone',
    'marionette'
], function (Backbone, Marionette) {
    'use strict';
    return  Marionette.Behavior.extend({
        events: {
            'click .js-remove': 'removeModel'
        },

        defaults: {
            removeEmptyCollection: true
        },

        removeModel: function (event) {
            event.stopPropagation();
            var currentModel = this.view.model;
            var currentCollection = currentModel.collection;
            currentCollection.remove(currentModel);
            if (this.options.removeEmptyCollection && !currentCollection.length && currentCollection.parent) {
                var groupCollection = currentCollection.parent.collection;
                groupCollection.remove(currentCollection.parent);
            }
        }

    });
});