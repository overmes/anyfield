define([
    'backbone',
    'marionette',
    'modelbinder'
], function (Backbone, Marionette, ModelBinder) {
    'use strict';
    return  Marionette.Behavior.extend({
        initialize: function () {
            this.modelBinder = new ModelBinder();
            if (!this.view.options.model.has('on')) {
                this.view.options.model.set('on', true, {silent: true});
            }
        },

        onShow: function () {
            var currentSelector;
            var count = this.view.$('.onoff').length;
            if (count) {
                currentSelector = count > 1 ? '.onoff:not(.child-fields .onoff)' : '.onoff';
                var bindings = {'on': currentSelector};
                this.modelBinder.bind(this.view.options.model, this.view.el, bindings);
            }
        }

    });
});