define([
    'backbone',
    'marionette',
    'text!search/Content/ContentTemplate.html',
    'search/Content/SearchTab/SearchView',
    'search/Content/AboutTab/AboutView',
    'search/Content/ItemDetailTab/ItemTab',
    'search/Content/FieldTypesTab/FieldTypesCollectionView',
    'search/Content/ScoresImport/ScoresImportView',
    'search/Content/Recommendation/RecommendationView',
    'search/Content/SearchTab/AddFieldsTab/AddFieldsTabView',
    'search/Content/TempItemsFieldsTab/TempItemsFieldsTab',
    'search/Content/Neighbours/NeighboursView'
], function (Backbone, Marionette, ContentTemplate, SearchView, AboutView, ItemTab, FieldTypesCollectionView,
             ScoresImportView, RecommendationView, AddFieldsTabView, TempItemsFieldsTab, NeighboursView) {
    'use strict';
    return Marionette.LayoutView.extend({
        template: _.template(ContentTemplate),

        regions: {
            container: '.container-fluid'
        },

        initialize: function (options) {
            this.app = options.app;
            this.tabs = {
                'showSearchTab': SearchView,
                'showAboutTab': AboutView,
                'showHowTo': AboutView,
                'showFieldsTab': FieldTypesCollectionView,
                'showScoresImport': ScoresImportView,
                'showRecommendation': RecommendationView,
                'showTempFieldsTab': TempItemsFieldsTab,
                'showNeighbours': NeighboursView
            };

            this.listenTo(this.app.vent, 'tab:show', this.showTab);
            this.listenTo(this.app.vent, 'item:detail', this.showItemDetail);
            this.listenTo(this.app.vent, 'view:field:add', this.showAddFields);
        },

        showAddFields: function () {
            this.container.show(new AddFieldsTabView({app: this.app}));
        },

        showTab: function (tabName, options) {
            var tab = this.tabs[tabName];
            if (tab) {
                this.container.show(new tab({app: this.app, controller: options}));
            }
        },

        showItemDetail: function (itemId) {
            var itemView = new ItemTab({app: this.app, itemId: itemId});
            this.container.show(itemView);
        }
    });
});