define([
    'backbone',
    'marionette',
    'text!search/Content/TempItemsFieldsTab/TempItemsFieldsTemplate.html',
    'search/Content/SearchTab/Request/ItemsCollection',
    'search/Content/ItemDetailTab/ItemDetailView',
    'search/Content/Common/GroupsCollection/GroupsCollection'
], function (Backbone, Marionette, TempItemsFieldsTemplate, ItemsCollection, ItemDetailView, GroupsCollection) {
    'use strict';
    var collection;
    return Marionette.CompositeView.extend({
        template: _.template(TempItemsFieldsTemplate),
        childViewContainer: '#TempItems',
        childView: ItemDetailView,
        className: 'container',
        emptyView: Marionette.ItemView.extend({template: _.template('<h2>Temp fields not found</h2>')}),

        initialize: function (options) {
            this.app = options.app;
            this.user = this.app.request('user');
            this.groupsCollection = this.app.request('groupsCollection');

            if (collection) {
                this.collection = collection;
            } else {
                this.collection = new ItemsCollection();
                this.collection.url = '/api/tempFields/';
                this.fetchCollection();
                collection = this.collection;
            }

            this.listenTo(this.user, 'change:id', function () {
                this.fetchCollection();
            });
        },

        ui: {
            reload: '.js-reload'
        },

        events: {
            'click @ui.reload': 'fetchCollection'
        },

        fetchCollection: function () {
            this.collection.fetch({reset: true});
        },

        childViewOptions: function (model, index) {
            var groupsIds = _.filter(_.keys(model.get('i')), function (groupId) {
                return groupId !== 'i' && parseInt(groupId, 10) !== this.groupsCollection.getTitleFieldId();
            }, this);
            var groups = new GroupsCollection(_.map(groupsIds, function (groupId) {
                return this.groupsCollection.get(parseInt(groupId, 10));
            }, this));
            return {
                app: this.app,
                selectedGroupsCollection: groups,
                className: ''
            };
        }
    });
});