define([
    'backbone',
    'marionette',
    'text!search/Content/SearchTab/Pagination/PaginationTemplate.html'
], function (Backbone, Marionette, PaginationTemplate) {
    'use strict';
    return Marionette.ItemView.extend({
        className: 'list-pagination',
        template: _.template(PaginationTemplate),

        templateHelpers: function () {
            var self = this;
            return {
                getCurrentPage: function () {
                    return Math.ceil(self.requestModel.get('skip')/self.requestModel.get('limit') + 1);
                },
                isLastPage: function () {
                    return self.itemsCollection.lastFetchItemsCount < self.requestModel.get('limit');
                }
            };
        },

        ui: {
            'page': 'li'
        },

        events: {
            'click @ui.page': 'changePage'
        },

        changePage: function (event) {
            var dataset = event.currentTarget.dataset;
            var nextPage = +dataset.page;
            this.requestModel.setPage(nextPage);
        },

        initialize: function (options) {
            this.app = options.app;
            this.requestModel = this.app.request('requestModel');
            this.itemsCollection = this.app.request('itemsCollection');

            this.listenTo(this.itemsCollection, 'sync', this.render);
        }

    });
});