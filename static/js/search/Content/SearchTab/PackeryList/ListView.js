define([
    'backbone',
    'marionette',
    'search/Content/SearchTab/PackeryList/PackeryChildView'
], function (Backbone, Marionette, PackeryChildView) {
    'use strict';
    return Marionette.CollectionView.extend({
        tagName: 'ul',
        className: 'list-group',
        childView: PackeryChildView,

        collectionEvents: {
            "sync": "render"
        },

        modelEvents: {
            "change:fields:shown": "checkFieldLoaded"
        },

        childViewOptions: function (model, index) {
            return {app: this.app};
        },

        initialize: function (options) {
            this.app = options.app;

            this.collection = this.app.request('itemsCollection');
            this.listenTo(this.collection, 'sync', this.render);
            this.model = this.app.request('fieldsCollection');
        },

        checkFieldLoaded: function (model, index, name, value) {
            if (this.collection.isLoaded(model.get('id'))) {
                this.render();
            }
        }

    });
});