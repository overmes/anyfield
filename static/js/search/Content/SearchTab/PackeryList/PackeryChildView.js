define([
    'backbone',
    'marionette',
    'text!search/Content/SearchTab/PackeryList/PackeryChildTemplate.html',
    'packery'
], function (Backbone, Marionette, PackeryChildTemplate, Packery) {
    'use strict';
    return Marionette.ItemView.extend({
        tagName: 'li',
        className: 'list-group-item packery-child',
        template: _.template(PackeryChildTemplate),

        ui: {
            grid: '.packery-fields-grid'
        },

        templateHelpers: function () {
            var fields = this.fields;
            return {
                getFields: function () {
                    return fields.toJSON();
                },

                serialize: function (id, fid, values) {
                    var typeField = fields.get(id);
                    return typeField.serialize(fid, values[id]);
                }
            };
        },

        initialize: function (options) {
            this.app = options.app;
            this.fields = this.app.request('fieldsCollection');
        },

        onShow: function () {
            var packery = new Packery(this.ui.grid.get(0), {
                itemSelector: '.packery-field',
                gutter: 5
            });
        }

    });
});