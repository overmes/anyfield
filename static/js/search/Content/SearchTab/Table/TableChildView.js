define([
    'backbone',
    'marionette',
    'text!search/Content/SearchTab/Table/TableChildTemplate.html'
], function (Backbone, Marionette, TableChildViewTemplate) {
    'use strict';
    return Marionette.ItemView.extend({
        tagName: 'tr',
        template: _.template(TableChildViewTemplate),

        initialize: function (options) {
            this.app = options.app;
            this.appController = this.app.appController;
            this.selectedGroups = this.app.request('selectedGroups');
            this.selectedItemsCollection = this.app.request('selectedItemsCollection');
            this.listenTo(this.selectedItemsCollection, 'reset', this.setSelectIndicator);
            this.itemId = this.model.get('id');
            this.user = this.app.request('user');
            this.requestModel = options.requestModel;
            this.childIndex = options.childIndex;
            this.itemsCount = options.itemsCount;
            this.setSelectIndicator();
        },

        ui: {
            link: '.field-li a, .controls a',
            textShow: '.table-text-show',
            textHide: '.table-text-hide',
            td: 'td'
        },

        events: {
            'click @ui.link': 'linkClick',
            'click': 'onSelect',
            'click @ui.td': 'onTdClick',
            'click @ui.textShow': 'onTextShow',
            'click @ui.textHide': 'onTextHide'

        },

        onTextShow: function (event) {
            event.stopPropagation();
            var $target = $(event.currentTarget.parentElement);
            $target.find('.table-text-show').hide();
            $target.find('.table-text-hidden').removeClass('hidden');
        },

        onTextHide: function (event) {
            event.stopPropagation();
            var $target = $(event.currentTarget.parentElement.parentElement);
            $target.find('.table-text-show').show();
            $target.find('.table-text-hidden').addClass('hidden');
        },

        linkClick: function (event) {
            event.preventDefault();
            event.stopPropagation();
            var target = event.currentTarget;
            var itemId = target.href.split("/")[4];
            var title = target.text;
            this.appController.showItemTab(itemId, title);
        },

        onRender: function () {
            this.$().after('123');
        },

        onSelect: function (event) {
            var tableMode = this.user.get('settings').get('tableMode');
            if (tableMode === 'detail') {
                this.appController.showItemTab(this.itemId);
            } else if (tableMode === 'select') {
                this.app.vent.trigger('view:item:select', this.itemId);
                this.setSelectIndicator();
            }
        },

        onTdClick: function (event) {
            var tableMode = this.user.get('settings').get('tableMode');
            var target = event.currentTarget;
            var gid = target.dataset.gid;
            if (gid && tableMode === 'edit') {
                var group = this.app.request('groupsCollection').get(gid);
                this.app.vent.trigger('item:field:modal', this.model, group);
            }
        },

        setSelectIndicator: function () {
            if (this.isSelected()) {
                this.$el.addClass('success');
            } else {
                this.$el.removeClass('success');
            }
        },

        isSelected: function () {
            return !!this.selectedItemsCollection.get(this.itemId);
        },

        templateHelpers: function () {
            var self = this;
            return {
                selectedGroups: this.selectedGroups,
                model: this.model,
                itemPath: this.model.getItemPath(),
                itemId: this.itemId,
                isSelected: function () {
                    return self.isSelected();
                },
                options: this.user.get('settings'),
                request: this.requestModel,
                itemCount: function () {
                    return self.requestModel.get('skip') + self.childIndex + 1 + (self.requestModel.get('limit') - self.itemsCount);
                }
            };
        }

    });
});