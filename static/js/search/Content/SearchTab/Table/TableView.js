define([
    'backbone',
    'marionette',
    'text!search/Content/SearchTab/Table/TableTemplate.html',
    'search/Content/SearchTab/Table/TableChildView',
    'search/Content/SearchTab/Table/ItemFieldsModalView',
    'search/Content/Common/Behaviors/LoadingBehavior'
], function (Backbone, Marionette, TableViewTemplate, TableChildView, ItemFieldsModalView, LoadingBehavior) {
    'use strict';
    return Marionette.CompositeView.extend({
        template: _.template(TableViewTemplate),
        childView: TableChildView,
        emptyView: Marionette.ItemView.extend({
            template: _.template('<h3 style="text-align: center">Nothing Found</h3>')
        }),
        childViewContainer: ".child",

        collectionEvents: {
            "sync": "render"
        },

        modelEvents: {
            "fields:change": "render",
            "fields:sort": "render",
            "fields:add": "render",
            "fields:remove": "render",
            "change:on": "render",
            "remove": "render",
            "sort": "render"
        },

        events: {
            'click tr .sortable': 'changeSort'
        },

        ui: {
            modal: '.modal-view'
        },

        childViewOptions: function (model, index) {
            return {
                app: this.app,
                requestModel: this.requestModel,
                childIndex: index,
                itemsCount: this.collection.length
            };
        },

        initialize: function (options) {
            this.app = options.app;

            this.collection = this.app.request('itemsCollection');
            this.requestModel = this.app.request('requestModel');
            this.model = this.app.request('selectedGroups');
            this.user = this.app.request('user');

            this.listenTo(this.app.vent, 'item:field:modal', this.onModalView);
        },

        onModalView: function (item, group) {
            this.itemModel = new ItemFieldsModalView({app: this.app, item: item, group: group});
            this.modalRegion.show(this.itemModel);
        },

        onBeforeDestroy: function () {
            if (this.itemModel) this.itemModel.destroy();
        },

        onRender: function () {
            this.$("img").error(function () {
                $(this).hide();
            });
            this.modalRegion = new Marionette.Region({el: this.ui.modal});
        },

        changeSort: function (event) {
            var dataset = event.currentTarget.dataset;
            var index = dataset['index'];
            var gid = dataset['gid'];
            var fid = dataset['fid'];
            var direction = dataset['direction'];
            this.requestModel.setSort(index, gid, fid, this.getNextDirection(direction));
        },

        getNextDirection: function (direction) {
            return {
                '0': -1,
                '1': 0,
                '-1': 1
            }[direction];
        },

        templateHelpers: function () {
            var self = this;
            return {
                selectedGroups: this.model,
                getSortGlyphicon: function (groupModel, fieldModel) {
                    var currentSort = '' + self.requestModel.getSort(groupModel, fieldModel);
                    var classes = {
                        '1': 'glyphicon-sort-by-attributes',
                        '-1': 'glyphicon-sort-by-attributes-alt',
                        '0': 'glyphicon-sort'
                    };
                    return classes[currentSort];
                },
                getSortDirection: function (groupModel, fieldModel) {
                    return self.requestModel.getSort(groupModel, fieldModel);
                },
                itemsCount: function () {
                    return self.collection.length;
                }
            };
        },

        behaviors: {
            LoadingBehavior: {
                behaviorClass: LoadingBehavior
            }
        }

    });
});