define([
    'backbone',
    'marionette',
    'text!search/Content/SearchTab/Table/ItemFieldsModalTemplate.html',
    'search/Content/SearchTab/AddFieldsTab/ItemsGroupsView',
    'bootstrap'
], function (Backbone, Marionette, ItemFieldsModalTemplate, ItemsGroupsView) {
    'use strict';
    return Marionette.LayoutView.extend({
        template: _.template(ItemFieldsModalTemplate),

        initialize: function (options) {
            this.app = options.app;
            this.item = options.item;
            this.group = options.group;
            this.groupsCollection = this.app.request('groupsCollection');
        },

        regions: {
            bodyRegion: '.modal-body'
        },

        ui: {
            modal: '#FieldsModal'
        },
        onRender: function () {
            this.ui.modal.modal();
            var itemsGroupsView = new ItemsGroupsView({
                app: this.app,
                selectedItems: new Backbone.Collection([this.item]),
                selectedGroupType: this.group
            });
            this.bodyRegion.show(itemsGroupsView);
        },

        onBeforeDestroy: function () {
            this.ui.modal.modal('hide');
            $('body').removeClass('modal-open');
            $('body').removeAttr('style');
            $('.modal-backdrop').remove();
        }
    });
});