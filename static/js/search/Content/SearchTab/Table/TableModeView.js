define([
    'backbone',
    'marionette',
    'modelbinder',
    'select'
], function (Backbone, Marionette, ModelBinder) {
    'use strict';
    return Marionette.ItemView.extend({
        template: _.template('<select data-width="165px">' +
                    '<option value="select">Select Mode</option>' +
                    '<option value="detail">Open Detail Mode</option>' +
                    '<option value="edit">View/Edit/Add Fields</option>' +
                    '<option value="idle">Idle Mode</option>' +
                  '</select>'),

        initialize: function (options) {
            this.app = options.app;
            this.modelBinder = new ModelBinder();
            this.user = this.app.request('user');
        },
        ui: {
            select: 'select'
        },
        onRender: function () {
            this.ui.select.selectpicker();
            this.ui.select.selectpicker('val', this.user.get('settings').get('tableMode'));
            this.modelBinder.bind(this.user.get('settings'), this.el, {tableMode: this.ui.select});
            this.ui.select.selectpicker('render');
        }
    });
});