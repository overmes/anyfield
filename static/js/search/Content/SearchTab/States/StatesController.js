define([
    'backbone',
    'marionette',
    'search/Content/SearchTab/States/LocalStates/LocalStatesCollection',
    'search/Content/SearchTab/States/SharedStates/SharedStatesCollection',
    'search/Content/SearchTab/States/SharedStates/SharedStateModel'
], function (Backbone, Marionette, LocalStatesCollection, SharedStatesCollection, SharedStateModel) {
    'use strict';
    return Marionette.Controller.extend({
        initialize: function (options) {
            this.app = options.app;
            this.requestModel = this.app.request('requestModel');
            this.timeout = 500;
            this.change = {};
            this.localStates = new LocalStatesCollection([], {app: this.app});
            this.sharedStates = new SharedStatesCollection([], {app: this.app});

            if (this.isSupported()) {
                this.storage = window.localStorage;
                this.addListeners();
                this.localStates.loadStates();
            }
            this.user = this.app.request('user');

            this.listenTo(this.user, 'change:id', this.fetchSharedStates);
            this.listenTo(this.sharedStates, 'add', this.triggerChangeEvent);

            this.debounceChange = _.debounce(this.onChange, this.timeout);
        },
        fetchSharedStates: function () {
            if (this.user.isAuthenticated()) {
                this.sharedStates.loadUserStates(this.user);
            } else {
                this.sharedStates.reset();
            }
        },
        debounceChangeType: function (type) {
            return function (model) {
                var key = type;
                if (type === 'page') key = '{0} {1}'.format(type, this.requestModel.getPage());
                this.change[key] = true;
                this.debounceChange(model);
            };
        },
        addListeners: function () {
            this.listenTo(this.requestModel.selectedGroups, 'add remove change fields:change fields:remove fields:add', this.debounceChangeType('fields'));
            this.listenTo(this.requestModel.filtersGroups, 'add remove change fields:add fields:change fields:remove', this.debounceChangeType('filters'));
            this.listenTo(this.requestModel, 'change:sort', this.debounceChangeType('sort'));
            this.listenTo(this.requestModel, 'change:skip', this.debounceChangeType('page'));
            this.listenTo(this.app.vent, 'items:load:more', this.debounceChangeType('page'));
            this.listenTo(this.requestModel, 'change:limit', this.debounceChangeType('limit'));
            this.listenTo(this.requestModel.selectedItemsCollection, 'add remove reset', this.debounceChangeType('select'));
        },

        getLastLocalState: function () {
            return this.localStates && this.localStates.at(0);
        },

        getLocalState: function (id) {
            return this.localStates && this.localStates.get(id);
        },

        loadLastLocalState: function () {
            this.loadLocalState(this.getLastLocalState());
        },

        loadSharedState: function (sharedState) {
                this.requestModel.deserialize(sharedState.toJSON(), {silent: true});
                this.localStates.saveState(sharedState.get('name') || 'shared', sharedState);
                this.triggerChangeEvent(sharedState);
        },

        loadLocalState: function (localState) {
            this.requestModel.deserialize(localState.toJSON(), {silent: true});
            this.triggerChangeEvent(localState);
        },

        fetchSharedState: function (id) {
            var sharedState = new SharedStateModel({id: id}, {app: this.app});
            var self = this;
            sharedState.fetch()
                .done(function () {
                    self.loadSharedState(sharedState);
                })
        },

        hasLastLocalState: function () {
            return this.localStates && this.localStates.length > 0;
        },

        isSupported: function () {
            return typeof(window.localStorage) !== "undefined";
        },

        onChange: function () {
            this.localStates.saveState(this.getChanges(this.change));
            this.triggerChangeEvent(this.getLastLocalState());
            this.change = {};
        },
        getChanges: function (changesDict) {
            return _.keys(changesDict);
        },
        triggerChangeEvent: function (state) {
            this.app.vent.trigger('storage:state:change', state.get('type'), state.get('id'), state.get('name'));
        }
    });
});