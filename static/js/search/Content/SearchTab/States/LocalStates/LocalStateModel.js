define([
    'backbone',
    'moment'
], function (Backbone, moment) {
    'use strict';
    return Backbone.Model.extend({
        initialize: function (data, options) {
            this.app = options.app;
            this.requestModel = this.app.request('requestModel');
            this.storage = window.localStorage;

            if (this.get('id')) {
                this.load(this.get('id'));
            } else {
                this.save(this.getNextId());
            }
        },

        defaults: {
            type: 'local'
        },

        load: function (id) {
            var data = {};
            try {
                data = JSON.parse(this.storage[id]);
            } catch (SyntaxError) {
                console.log('Local state parse error');
            }
            data['date'] = moment(data['date']);
            this.set(data);
        },

        save: function (id) {
            this.set(this.requestModel.serialize());
            this.set('date', moment());
            this.set('id', id);
            this.storage[id] = JSON.stringify(this.toJSON());
        },

        getNextId: function () {
            var currentKey = this.getInt('end') + 1;
            this.storage['end'] = currentKey;
            return currentKey;
        },

        getInt: function (key) {
            return parseInt(this.storage[key]);
        }
    });
});