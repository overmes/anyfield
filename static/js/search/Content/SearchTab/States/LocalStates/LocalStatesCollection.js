define([
    'backbone',
    'search/Content/SearchTab/States/LocalStates/LocalStateModel'
], function (Backbone, LocalStateModel) {
    'use strict';
    return Backbone.Collection.extend({
        model: LocalStateModel,

        initialize: function (data, options) {
            this.app = options.app;
            this.storage = window.localStorage;
            this.MAX_LENGTH = 500;
            this.storage['start'] = this.getInt('start') || 0;
            this.storage['end'] = this.getInt('end') || 0;

            this.listenTo(this, 'add', this.onAdd);
        },

        loadStates: function () {
            for (var i = this.getInt('start') || 1; i <= this.getInt('end'); i++) {
                this.add({id: i}, {app: this.app, silent: true});
            }
            this.trigger('add')
        },

        onAdd: function () {
            if (this.storage.length >= this.MAX_LENGTH) {
                var currentStart = this.getInt('start');
                this.storage['start'] = currentStart + 1;
                this.storage.removeItem(currentStart);
                this.remove(this.get(currentStart));
            }
        },

        saveState: function (changes, sharedState) {
            this.add({changes: changes}, {app: this.app});
        },

        comparator: function (state) {
            return -state.get('date');
        },

        getInt: function (key) {
            return parseInt(this.storage[key]);
        }
    });
});