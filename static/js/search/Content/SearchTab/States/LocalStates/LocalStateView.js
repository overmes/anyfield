define([
    'backbone',
    'marionette',
    'text!search/Content/SearchTab/States/LocalStates/LocalStateTemplate.html'
], function (Backbone, Marionette, LocalStateTemplate) {
    'use strict';
    return Marionette.ItemView.extend({
        initialize: function (options) {
            this.app = options.app;
            this.storageController = this.app.request('statesController');
        },
        className: 'list-group-item local-state',
        tagName: 'li',
        template: _.template(LocalStateTemplate),
        events: {
            'click': 'loadState'
        },
        loadState: function () {
            this.storageController.loadLocalState(this.model);
        }
    });
});