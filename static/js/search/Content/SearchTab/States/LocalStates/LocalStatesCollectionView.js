define([
    'backbone',
    'marionette',
    'text!search/Content/SearchTab/States/LocalStates/LocalStatesCollectionTemplate.html',
    'search/Content/SearchTab/States/LocalStates/LocalStateView'
], function (Backbone, Marionette, LocalStatesCollectionTemplate, LocalStateView) {
    'use strict';
    return Marionette.CompositeView.extend({
        initialize: function (options) {
            this.app = options.app;
            this.collection = this.app.request('statesController').localStates;
        },
        childViewContainer: ".list-group",
        childView: LocalStateView,
        template: _.template(LocalStatesCollectionTemplate),
        childViewOptions: function (model, index) {
            return {
                app: this.app
            };
        }
    });
});