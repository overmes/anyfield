define([
    'backbone',
    'marionette',
    'text!search/Content/SearchTab/States/SharedStates/SharedStatesCollectionTemplate.html',
    'search/Content/SearchTab/States/SharedStates/SharedStateView',
    'search/Content/SearchTab/States/SharedStates/SharedStateModel'
], function (Backbone, Marionette, SharedStatesCollectionTemplate, SharedStateView, SharedStateModel) {
    'use strict';
    return Marionette.CompositeView.extend({
        initialize: function (options) {
            this.app = options.app;
            this.collection = this.app.request('statesController').sharedStates;
            this.user = this.app.request('user');
            this.listenTo(this.user, 'change:id', this.render);
        },
        collectionEvents: {
            'sync': 'render'
        },
        ui: {
            share: '#ShareButton',
            loadMore: '#LoadMore'
        },
        events: {
            'click @ui.share': 'share',
            'click @ui.loadMore': 'loadMore'
        },
        share: function () {
            var self = this;
            var state = new SharedStateModel();
            state.loadCurrentState(this.app.request('requestModel'));
            state.save().done(function (){
                self.collection.add(state);
            });
        },
        loadMore: function () {
            this.collection.loadMore();
        },
        childViewContainer: ".list-group",
        childView: SharedStateView,
        template: _.template(SharedStatesCollectionTemplate),
        childViewOptions: function (model, index) {
            return {
                app: this.app
            };
        },
        templateHelpers: function () {
            var self = this;
            return {
                user: this.user,
                hasMore: function () {return self.collection.totalCount > self.collection.length; }
            };
        }
    });
});