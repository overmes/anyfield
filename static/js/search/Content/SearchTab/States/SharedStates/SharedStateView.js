define([
    'backbone',
    'marionette',
    'text!search/Content/SearchTab/States/SharedStates/SharedStateTemplate.html',
    'text!search/Content/SearchTab/States/SharedStates/NameStateTemplate.html'
], function (Backbone, Marionette, SharedStateTemplate, NameStateTemplate) {
    'use strict';
    return Marionette.ItemView.extend({
        initialize: function (options) {
            this.app = options.app;
            this.statesController = this.app.request('statesController');
            this.appController = this.app.request('appController');
        },
        className: 'list-group-item',
        tagName: 'li',
        template: _.template(SharedStateTemplate),
        ui: {
            share: '#ShareButton',
            name: '.js-name',
            default: '.js-default',
            save: '.js-save',
            update: '.js-update',
            remove: '.js-remove',
            back: '.js-back',
            input: 'input'
        },
        events: {
            'click': 'loadState',
            'click @ui.name': 'setName',
            'click @ui.default': 'setDefault',
            'click @ui.save': 'onSave',
            'click @ui.update': 'onUpdate',
            'click @ui.remove': 'onRemove',
            'click @ui.back': 'onBack'
        },
        setName: function (event) {
            event.stopPropagation();
            this.template = _.template(NameStateTemplate);
            this.render();
        },
        onBack: function (event) {
            event.stopPropagation();
            this.template = _.template(SharedStateTemplate);
            this.render();
        },
        onSave: function (event) {
            event.stopPropagation();
            var name = this.ui.input.val();
            if (name) {
                this.model.save({'name': name}, {patch: true});
                this.template = _.template(SharedStateTemplate);
            }
        },
        onUpdate: function (event) {
            event.stopPropagation();
            this.model.loadCurrentState(this.app.request('requestModel'));
            this.model.save({full: true});
        },
        onRemove: function (event) {
            event.stopPropagation();
            this.model.destroy();
        },
        setDefault: function (event) {
            event.stopPropagation();
            this.model.save({'is_default': !this.model.get('is_default')}, {patch: true});
        },
        loadState: function () {
            this.statesController.fetchSharedState(this.model.get('id'));
        },
        modelEvents: {
            "sync": "render"
        }
    });
});