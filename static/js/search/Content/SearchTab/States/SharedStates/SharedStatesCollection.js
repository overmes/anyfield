define([
    'backbone',
    'search/Content/SearchTab/States/SharedStates/SharedStateModel'
], function (Backbone, SharedStateModel) {
    'use strict';
    return Backbone.Collection.extend({
        model: SharedStateModel,
        url: '/api/state/',
        initialize: function () {
        },
        loadMore: function () {
            this.fetch({data: {user: this.user.get('id'), offset: this.length, order_by: '-date'}, reset: false, remove: false});
        },

        parse: function (response) {
            this.totalCount = response.meta.total_count || 0;
            return response.objects;
        },

        loadUserStates: function (user) {
            this.user = user;
            this.fetch({data: {user: user.get('id'), offset: 0, order_by: '-date'}, reset: true});
        },

        comparator: function (state) {
            return -state.get('date');
        }
    });
});