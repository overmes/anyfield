define(['backbone', 'moment'], function (Backbone, moment) {
    'use strict';
    return Backbone.Model.extend({
        urlRoot: '/api/state/',
        defaults: {
            type: 'shared',
            is_default: false
        },
        initialize: function (data, options) {
        },

        loadCurrentState: function (requestModel) {
            var types = [];
            var data = requestModel.serialize();
            _.each(data.fields, function (value, key) {
                value['type'] = 'f';
                types.push(value);
            });
            delete data.fields;
            _.each(data.filters, function (value, key) {
                value['type'] = 'l';
                types.push(value);
            });
            delete data.filters;
            data.types = types;
            data.date = moment();
            this.set(data);
        },
        parse: function (response) {
            response.fields = _.filter(response['types'], function (type) {
                return type.type === 'field';
            });
            response.filters = _.filter(response['types'], function (type) {
                return type.type === 'filter';
            });
            response.date = moment(response.date);
            return response;
        }
    });
});