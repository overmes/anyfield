define([
    'backbone',
    'marionette',
    'text!search/Content/SearchTab/States/StatesLayoutTemplate.html',
    'search/Content/SearchTab/States/LocalStates/LocalStatesCollectionView',
    'search/Content/SearchTab/States/SharedStates/SharedStatesCollectionView',
    'bootstrap'
], function (Backbone, Marionette, StatesLayoutTemplate, LocalStatesCollectionView, SharedStatesCollectionView) {
    'use strict';
    return Marionette.LayoutView.extend({
        initialize: function (options) {
            this.app = options.app;
        },
        template: _.template(StatesLayoutTemplate),

        regions: {
            localRegion: "#LocalStates",
            sharedRegion: "#SharedStates"
        },
        className: 'panel panel-info',
        id: 'StatesPanel',

        onBeforeShow: function () {
            this.$('StatesTabs').tab();
            this.localRegion.show(new LocalStatesCollectionView({app: this.app}));
            this.sharedRegion.show(new SharedStatesCollectionView({app: this.app}));
        }
    });
});