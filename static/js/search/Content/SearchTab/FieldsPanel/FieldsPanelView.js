define([
    'backbone',
    'marionette',
    'text!search/Content/SearchTab/FieldsPanel/FieldsTemplate.html',
    'search/Content/Common/FieldsSelect/GroupSelectView',
    'search/Content/SearchTab/FieldsPanel/SelectedGroupsList/SelectedFieldsList/SelectedFieldsView',
    'search/Content/Common/Behaviors/SortableBehavior'
], function (Backbone, Marionette, FieldsTemplate, GroupSelectView, SelectedFieldsView, SortableBehavior) {
    'use strict';
    return Marionette.CompositeView.extend({
        className: 'panel panel-info',
        ui: {
            addButton: '#AddButton'
        },
        childView: SelectedFieldsView,
        childViewContainer: "#SelectedFields",
        sort: false,

        initialize: function (options) {
            this.app = options.app;
            this.collection = this.app.request('selectedGroups');
            this.listenTo(this.app.vent, 'request:loaded', this.render);
            this.listenTo(this.app.vent, 'group:field:selected', this.onSelectField);
            this.enableFields = this.getEnableFields();
        },
        onSelectField: function (group, field, multi, selectedGroups) {
            var fieldSelected = this.collection.isFieldSelected(group, field);
            var enableField = this.enableFields.getField(group, field);
            if (this.collection != selectedGroups && !fieldSelected && enableField) {
                var disabled = this.collection.selectField(group, field, multi);
                if (disabled !== undefined) this.collection.trigger('group:field:disabled', group, field, disabled);
            }
        },
        getEnableFields: function () {
            return this.app.request('groupsCollection').getFilteredCollection(function (field) {
                return !field.get('is_temp') && field.has('id') && field.get('type') !== 'si';
            });
        },
        onRender: function () {
            var groupsSelectView = new GroupSelectView({app: this.app,
                selectedGroups: this.collection, collection: this.enableFields});
            this.drop = new Drop({
                target: this.ui.addButton.get(0),
                content: groupsSelectView.el,
                position: 'right top',
                openOn: 'click',
                constrainToWindow: false,
                constrainToScrollParent: false
            });
            this.drop.on('open', groupsSelectView.render);
        },
        childViewOptions: function (model, index) {
            return {
                app: this.app
            };
        },
        template: _.template(FieldsTemplate),

        behaviors: {
            SortableBehavior: {
                behaviorClass: SortableBehavior,
                parent: '#SelectedFields',
                child: '#SelectedFields > .list-group'
            }
        }
    });
});