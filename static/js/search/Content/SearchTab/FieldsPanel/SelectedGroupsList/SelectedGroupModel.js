define([
    'backbone',
    'search/Content/SearchTab/FieldsPanel/SelectedGroupsList/SelectedFieldsList/SelectedFieldsCollection'
], function (Backbone, SelectedFieldsCollection) {
    'use strict';
    return Backbone.Model.extend({
        initialize: function (options) {
            var fieldsCollection = new SelectedFieldsCollection();
            this.set({fields: fieldsCollection, parent: options.parent});
            this.listenTo(fieldsCollection, 'all', this.onFieldsChange);
            fieldsCollection.parent = this;
        },

        onFieldsChange: function (eventName) {
            this.trigger('fields:' + eventName, this);
        },

        serialize: function () {
            var currentModel = this.toJSON();
            currentModel.parent = this.get('parent').get('id');
            currentModel.fields = this.get('fields').serialize();
            return currentModel;
        },

        serializeForFilter: function () {
            var fields = this.get('fields').serializeForFilter();
            if (fields && fields.length) {
                var currentModel = {};
                currentModel.parent = this.get('parent').get('id');
                currentModel.fields = fields;
                currentModel.include = this.get('include');
                return currentModel;
            }
        },

        deserialize: function (groupData, GroupsCollection, options) {
            var currentGroup = GroupsCollection.get(groupData.parent);
            if (currentGroup) {
                var currentFieldsCollection = this.get('fields');
                this.set(groupData, options);
                this.set({'parent': currentGroup, 'fields': currentFieldsCollection.deserialize(groupData.fields, currentGroup, options)}, options);
                return this;
            }
        },

        checkUser: function () {
            return this.get('parent').get('type') !== 'us' || this.get('user');
        },

        isActive: function () {
            return this.get('on') && this.checkUser();
        },

        isFieldsActive: function () {
            return _.any(this.get('fields').map(function (field) {
                return field.isActive();
            }));
        },

        getIndex: function () {
            return this.get('parent').get('type') === 'us' ? '' + this.get('user') : 'i';
        },

        isFieldSelected: function (fieldModel) {
            var selectedField = this.get('fields').get(fieldModel.get('id'));
            if (selectedField) {
                return true;
            }
        },

        addField: function (fieldModel, data) {
            var currentFields = this.get('fields');
            if (!currentFields.get(fieldModel.get('id'))) {
                return currentFields.add(_.extend({id: fieldModel.get('id'), parent: fieldModel}, data));
            }
        },

        removeField: function (fieldModel) {
            this.get('fields').remove(fieldModel.get('id'));
            if (this.get('fields').length === 0) {
                if (this.collection) this.collection.remove(this);
            }
        }

    });
});