define([
    'backbone',
    'search/Content/SearchTab/FieldsPanel/SelectedGroupsList/SelectedFieldsList/SelectedFieldModel'
], function (Backbone, SelectedFieldModel) {
    'use strict';
    return Backbone.Collection.extend({
        model: SelectedFieldModel,

        serialize: function () {
            return this.map(function (fieldModel) {
                return fieldModel.serialize();
            });
        },

        serializeForFilter: function () {
            return _.compact(this.map(function (fieldModel) {
                return fieldModel.serializeForFilter();
            }));
        },

        deserialize: function (fieldsArray, groupModel, options) {
            this.reset(_.filter(_.map(fieldsArray, function (field) {
                return new SelectedFieldModel({}).deserialize(field, groupModel, options);
            }), _.identity), options);
            return this;
        }
    });
});