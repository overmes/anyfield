define(['backbone'], function (Backbone) {
    'use strict';
    return Backbone.Model.extend({
        initialize: function (options) {
            this.set('parent', options.parent);
        },

        serialize: function () {
            var result = this.toJSON();
            result.parent = this.get('parent').get('id');
            delete result.id;
            return result;
        },

        isActive: function () {
            return this.get('on');
        },

        hasFilter: function () {
            return this.has('filter') && this.get('filter').length > 0;
        },

        getFilter: function () {
            var res = this.get('filter');
            if (res instanceof Backbone.Collection) res = res.pluck('id');
            return res;
        },

        serializeForFilter: function () {
            if (this.isActive() && this.hasFilter()) {
                var filter = this.getFilter();
                var result = {};
                result.fid = this.get('parent').get('fid');
                result.or = this.get('or');
                result.filter = filter;
                return result;
            }
        },

        deserialize: function (fieldData, groupModel, options) {
            var parentField = groupModel.get('fields').get(fieldData.parent);
            if (parentField) {
                this.set(fieldData, options);
                this.set('parent', parentField, options);
                this.set('id', parentField.get('id'));
                return this;
            }
        },

        isSortable: function () {
            var type = this.get('parent').get('type');
            return ['te', 'ur', 'im', 'co'].indexOf(type) === -1;
        }
    });
});