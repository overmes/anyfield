define([
    'backbone',
    'marionette',
    'text!search/Content/SearchTab/FieldsPanel/SelectedGroupsList/SelectedFieldsList/SelectedItemTemplate.html',
    'search/Content/Common/Behaviors/RemoveBehavior',
    'search/Content/Common/Behaviors/OnOffBehavior'
], function (Backbone, Marionette, SelectedItemTemplate, RemoveBehavior, OnOffBehavior) {
    'use strict';
    return Marionette.ItemView.extend({
        tagName: 'li',
        className: 'list-group-item',
        template: _.template(SelectedItemTemplate),
        behaviors: {
            RemoveBehavior : {
                behaviorClass: RemoveBehavior,
                removeEmptyCollection: true
            },
            OnOffBehavior: {
                behaviorClass: OnOffBehavior
            }
        }

    });
});