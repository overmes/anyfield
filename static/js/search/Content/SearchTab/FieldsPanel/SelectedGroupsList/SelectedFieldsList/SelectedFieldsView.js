define([
    'backbone',
    'marionette',
    'text!search/Content/SearchTab/FieldsPanel/SelectedGroupsList/SelectedFieldsList/SelectedFieldsTemplate.html',
    'search/Content/SearchTab/FieldsPanel/SelectedGroupsList/SelectedFieldsList/SelectedItemView',
    'search/Content/Common/UserSelect/UserSelectView',
    'search/Content/Common/Behaviors/RemoveBehavior',
    'search/Content/Common/Behaviors/OnOffBehavior',
    'search/Content/Common/Behaviors/SortableBehavior'
], function (Backbone, Marionette, SelectedFieldsTemplate, SelectedItemView, UserSelectView, RemoveBehavior, OnOffBehavior,
    SortableBehavior) {
    'use strict';
    return Marionette.CompositeView.extend({
        tagName: 'div',
        className: 'list-group',
        childView: SelectedItemView,
        childViewContainer: '.child-fields',
        template: _.template(SelectedFieldsTemplate),
        sort: false,

        initialize: function (options) {
            this.app = options.app;
            this.collection = this.model.get('fields');
        },

        ui: {
            list: 'ul'
        },

        onShow: function () {
            this.userSelectRegion = new Backbone.Marionette.Region({
                el: this.$el.find(".user-select")
            });
            if (this.model.get('parent').get('type') === 'us') {
                this.userSelectRegion.show(new UserSelectView({app: this.app, model: this.model}));
            }
        },

        behaviors: {
            RemoveBehavior : {
                behaviorClass: RemoveBehavior
            },
            OnOffBehavior: {
                behaviorClass: OnOffBehavior
            },
            SortableBehavior: {
                behaviorClass: SortableBehavior,
                parent: 'ul',
                child: 'ul > li'
            }
        }
    });
});