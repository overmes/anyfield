define([
    'backbone',
    'search/Content/SearchTab/FieldsPanel/SelectedGroupsList/SelectedGroupModel'
], function (Backbone, SelectedGroupModel) {
    'use strict';
    return Backbone.Collection.extend({
        model: SelectedGroupModel,

        serialize: function () {
            return this.map(function (groupModel) {
                return groupModel.serialize();
            });
        },

        deserialize: function (groupsArray, GroupsCollection, options) {
            this.reset(_.filter(_.map(groupsArray, function (group) {
                return new SelectedGroupModel({}).deserialize(group, GroupsCollection, options);
            }), _.identity), options);
            return this;
        },

        loadState: function (groupsArray, GroupsCollection) {
            this.deserialize(groupsArray, GroupsCollection);
        },

        selectField: function (group, field, isMulti) {
            var selectedGroup = this.getOrAddGroup(group, field, isMulti);
            if (selectedGroup.isFieldSelected(field)) {
                selectedGroup.removeField(field);
                var selectedGroups = this.getGroupsByGroup(group);
                if (_.all(selectedGroups, function (currentGroup) {return !currentGroup.isFieldSelected(field); }, this)) {
                    return false;
                }
            } else {
                selectedGroup.addField(field);
                return true;
            }
        },

        isFieldSelected: function (group, field) {
            var currentGroup = this.getGroupsByGroup(group);
            return currentGroup ? _.any(currentGroup, function(selectedGroup){
                    return selectedGroup.isFieldSelected(field)}):false;
        },

        getSelectedItemsGroup: function () {
            return  this.filter(function (groupModel) {
                return groupModel.get('parent').get('id') === 3;
            })[0];
        },

        getGroupsByGroup: function (group) {
            return this.filter(function (groupModel) {
                return groupModel.get('parent').get('type') === group.get('type') && groupModel.get('parent').get('id') === group.get('id');
            });
        },

        getOrAddGroup: function (groupModel, fieldModel, isMulti) {
            var usersSelectedGroups = this.getGroupsByGroup(groupModel);
            if (isMulti || usersSelectedGroups.length === 0) {
                //special login for fields select,
                //return first group with selected field
                for (var index in usersSelectedGroups) {
                    if (!usersSelectedGroups[index].isFieldSelected(fieldModel)) {
                        return usersSelectedGroups[index];
                    }
                }
                return this.addGroup(groupModel);
            } else {
                return usersSelectedGroups[0];
            }
        },

        addGroup: function (parent) {
            return this.add({parent: parent});
        },

        addField: function (field, data) {
            var newGroup = this.addGroup(field.get('parent'));
            var newField = newGroup.addField(field, data);
            return newGroup;
        }
    });
});