define([
    'backbone',
    'marionette',
    'text!search/Content/SearchTab/LoadMore/LoadMoreTemplate.html'
], function (Backbone, Marionette, LoadMoreTemplate) {
    'use strict';
    return Marionette.ItemView.extend({
        className: 'list-pagination',
        template: _.template(LoadMoreTemplate),

        templateHelpers: function () {
            var self = this;
            return {
                isLastPage: function () {
                    return self.itemsCollection.lastFetchItemsCount < self.requestModel.get('limit');
                }
            };
        },

        ui: {
            'load': '.js-load'
        },

        events: {
            'click @ui.load': 'loadMore'
        },

        loadMore: function (event) {
            this.itemsCollection.loadMore();
            this.app.vent.trigger('items:load:more');
        },

        initialize: function (options) {
            this.app = options.app;
            this.requestModel = this.app.request('requestModel');
            this.itemsCollection = this.app.request('itemsCollection');

            this.listenTo(this.itemsCollection, 'sync', this.render);
        }

    });
});