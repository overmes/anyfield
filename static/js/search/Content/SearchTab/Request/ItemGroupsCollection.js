define([
    'backbone',
    'search/Content/SearchTab/Request/ItemGroupsModel'
], function (Backbone, ItemGroupsModel) {
    'use strict';
    return Backbone.Collection.extend({
        model: ItemGroupsModel
    });
});