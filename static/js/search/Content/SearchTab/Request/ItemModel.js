define([
    'backbone',
    'search/Content/SearchTab/Request/SerializeController',
    'search/Content/SearchTab/Request/ItemGroupsCollection'
], function (Backbone, SerializeController, ItemGroupsCollection) {
    'use strict';
    return Backbone.Model.extend({
        urlRoot: '/api/query/',

        initialize: function (data, options) {
            this.serializer = SerializeController;

            if (this.has('resource_uri')) {
                this.set('id', this.getId());
            }
        },

        getTitle: function () {
            var i = this.get('i');
            return i && i['2'] && i['2'][0] && i['2'][0]['a'] ?
                i['2'][0]['a'] : null;
        },

        getItemPath: function () {
            return '/item/{0}/'.format(this.get('id'));
        },

        getItemUrl: function (blank) {
            return '<a href="{0}" {2}>{1}</a>'.format(this.getItemPath(),
                    _.escape(this.getTitle()) || this.get('id'), blank ? 'target="_blank"' : '');
        },

        getId: function () {
            return parseInt(this.get('resource_uri').split('/').slice(-1)[0]);
        },

        url: function () {
            return this.urlRoot + this.id + '/';
        },

        serialize: function (groupModel, options) {
            return this.serializer.serialize(this, groupModel, options);
        },

        getGroupsCollection: function (user) {
            var i = this.get('id');

            var groupsArray = _.map(this.get('i'), function (groupArray, groupId) {
                //special for id field
                if (groupId === 'i') {
                    return {fields: [{'a': i, id: 0, vote_count: null}], id: 1, i: i}
                } else {
                    groupId = parseInt(groupId);
                    return {fields: groupArray, id: groupId, i: i};
                }
            });
            if (this.has(user.get('id'))) {
                _.each(this.get(user.get('id')), function (groupArray, groupId) {
                    if ($.isNumeric(groupId)) {
                        groupId = parseInt(groupId);
                        groupsArray.push({fields: groupArray, id: groupId, i: i});
                    }
                });
            }

            return new ItemGroupsCollection(groupsArray);
        }
    });
});