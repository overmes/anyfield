define(['backbone', 'marionette', 'moment'], function (Backbone, Marionette, Moment) {
    'use strict';
    var controller = Marionette.Controller.extend({
        getGroupElemMatchFilter: function (groupModel) {
            var boxes = this.getGroupFilters(groupModel);
            var groupId = groupModel.get('parent').get('id').toString();

            var extendedFilters = [];
            _.each(boxes, function (currentBox) {
                var fullFilter = {};
                _.each(currentBox, function (filter) {
                    if (filter.none) {
                        extendedFilters.push(this.getNullFilter(groupId, filter, groupModel));
                    } else {
                        _.extend(fullFilter, filter.val);
                    }
                }, this);

                if (Object.keys(fullFilter).length > 0) {
                    //special cases for item id filters
                    if (groupId !== '1' && groupId !== '3') {
                        extendedFilters.push(this.getElemMatchFilter(groupModel, groupId, fullFilter));
                    } else {
                        extendedFilters.push(this.getIdFilter(groupModel, groupId, fullFilter));
                    }
                }
            }, this);

            var result = {};
            if (extendedFilters.length > 0) {
                result = {'$and': extendedFilters};
            }

            return result;
        },

        getIdFilter: function (groupModel, groupId, fullFilter) {
            if (groupModel.get('include')) {
                return fullFilter;
            } else {
                return {i: {$not: fullFilter['i']}};
            }
        },

        getElemMatchFilter: function (groupModel, groupId, fullFilter) {
            var groupFilter = {};
            if (groupModel.get('include')) {
                groupFilter[groupId] = {$elemMatch: fullFilter};
            } else {
                groupFilter[groupId] = {$not: {$elemMatch: fullFilter}};
            }
            return groupFilter;
        },

        getNullFilter: function (groupId, filter, groupModel) {
            var res;
            var groupNullFilter = {};
            if (groupModel.get('include')) {
                groupNullFilter[groupId + '.' + Object.keys(filter.val)[0]] = null;
            } else {
                groupNullFilter[groupId + '.' + Object.keys(filter.val)[0]] = {$not: {$eq: null}};
            }
            if (filter.val[Object.keys(filter.val)[0]].$in.length > 1) {
                var groupFilter = {};
                if (groupModel.get('include')) {
                    groupFilter[groupId] = {$elemMatch: filter.val};
                    if (filter.or) {
                        res = {$or: [groupNullFilter, groupFilter]};
                    } else {
                        res = {$and: [groupNullFilter, groupFilter]};
                    }
                } else {
                    groupFilter[groupId] = {$not: {$elemMatch: filter.val}};
                    if (filter.or) {
                        res = {$and: [groupNullFilter, groupFilter]};
                    } else {
                        res = {$or: [groupNullFilter, groupFilter]};
                    }
                }
            } else {
                res = groupNullFilter;
            }
            return res;
        },

        getGroupFilters: function (groupModel) {
            var boxes = [];

            var currentFiltersArray = [];
            var haveNotNullField = false;
            groupModel.get('fields').each(function (field) {
                var currentFilters = this.getFieldFilter(field);
                if (currentFilters && currentFilters.val) {
                    currentFiltersArray.push(currentFilters);
                    haveNotNullField = haveNotNullField || !currentFilters.none;
                }
            }, this);

            _.each(currentFiltersArray, function (currentFilters) {
                if (haveNotNullField) {
                    currentFilters.none = false;
                }
                if (currentFilters.type === 'multise') {
                    boxes = this.getMultiplyFieldsFilters(boxes, currentFilters);
                } else {
                    this.appendFieldFilters(boxes, currentFilters);
                }
            }, this);
            return boxes;
        },

        getMultiplyFieldsFilters: function (boxes, filters) {
            var newBoxes = [];
            _.each(filters.val, function (filter) {
                if (boxes.length > 0) {
                    _.each(boxes, function (previousBox) {
                        var cloneBox = previousBox.slice(0);
                        cloneBox.push({type: 'se', val: filter, none: false});
                        newBoxes.push(cloneBox);
                    });
                } else {
                    newBoxes.push([{type: 'se', val: filter, none: false}]);
                }

            });
            return newBoxes;
        },

        appendFieldFilters: function (boxes, filter) {
            if (boxes.length > 0) {
                _.each(boxes, function (currentBox) {
                    currentBox.push(filter);
                });
            } else {
                boxes.push([filter]);
            }
        },

        getFieldFilter: function (field) {
            var fieldId = field.get('parent').get('fid').toString();
            var filter = field.get('filter');

            if (!field.get('on')) {
                return null;
            }
            return this[field.get('parent').get('type') + 'Filter'](field, fieldId, filter);
        },

        checkFilter: function (filter) {
            if (filter && filter.length) {
                return true;
            }
        },

        seFilter: function (field, fieldId, filter) {
            if (!this.checkFilter(filter)) return null;

            if (field.get('or')) {
                var fieldIdFilter = {};
                fieldIdFilter[fieldId] = {'$in': filter};
                return {type: 'se', val: fieldIdFilter, none: filter.indexOf(null) >= 0, or: field.get('or')};
            } else {
                var result = [];
                _.each(filter, function (subFilter) {
                    var subFilterDict = {};
                    subFilterDict[fieldId] = subFilter;
                    result.push(subFilterDict);
                });
                return {type: 'multise', val: result, none: false, or: field.get('or')};
            }
        },

        teFilter: function (field, fieldId, filter) {
            if (!this.checkFilter(filter)) return null;
            var fieldIdFilter = {};
            fieldIdFilter[fieldId] = filter;
            return {type: 'te', val: fieldIdFilter, none: false};
        },

        vaFilter: function (field, fieldId, filter) {
            if (!this.checkFilter(filter)) return null;
            var fieldIdFilter = {};
            fieldIdFilter[fieldId] = {$gte: filter[0], $lte: filter[1]};
            return {type: 'va', val: fieldIdFilter, none: false};
        },
        liFilter: function (field, fieldId, filter) {
            if (filter >= 0){
                var fieldIdFilter = {};
                fieldIdFilter[fieldId] = filter;
                return {type: 'li', val: fieldIdFilter, none: false};
            }
        },

        idFilter: function (field, fieldId, filter) {
            if (!this.checkFilter(filter)) return null;
            var fieldIdFilter = {'i': {$gte: filter[0], $lte: filter[1]}};
            return {type: 'id', val: fieldIdFilter, none: false};
        },

        siFilter: function (field, fieldId, filter) {
            if (!filter) return null;
            var fieldIdFilter = {i: {$in: filter.pluck('id')}};
            return {type: 'id', val: fieldIdFilter, none: false};
        },

        daFilter: function (field, fieldId, filter) {
            if (!this.checkFilter(filter) || !(filter[0] || filter[1])) return null;
            var fieldIdFilter = {};
            fieldIdFilter[fieldId] = {$gte: filter[0] || undefined, $lte: filter[1] || undefined};
            return {type: 'da', val: fieldIdFilter, none: false};
        }
    });
    return new controller();
});