define([
    'backbone',
    'search/Content/SearchTab/Request/FiltersController',
    'storage'
], function (Backbone, FiltersController, Storage) {
    'use strict';
    return Backbone.Model.extend({
        defaults: {
            query: {},
            timeout: 500,
            limit: 30,
            skip: 0,
            sort: [] //('i', (('15.a', -1), ))
        },
        initialize: function (data, options) {
            this.app = options.app;
            this.selectedGroups = options.selectedGroups;
            this.filtersGroups = options.filtersGroups;
            this.selectedItemsCollection = this.app.request('selectedItemsCollection');
            this.tempFields = options.tempFields;

            this.debounceChange = _.debounce(this.onChange, this.get('timeout'));
            this.fastDebounceChange = _.debounce(this.onChange, this.get('timeout') / 100);

            this.setListeners();
        },
        debounceChangeType: function (type) {
            return function (model) {
                this.change = type;
                this.debounce = true;
                if (type === 'page' || type === 'sort' || type === 'limit') {
                    this.fastDebounceChange(model);
                } else {
                    this.debounceChange(model);
                }
            };
        },
        setListeners: function () {
            this.listenTo(this.selectedGroups, 'add remove change', this.debounceChangeType('fields'));
            this.listenTo(this.filtersGroups, 'remove change fields:change fields:remove', this.debounceChangeType('filters'));
            this.listenTo(this, 'change:sort', this.debounceChangeType('sort'));
            this.listenTo(this, 'change:skip', this.debounceChangeType('page'));
            this.listenTo(this, 'change:limit', this.debounceChangeType('limit'));
        },

        onChange: function (model) {
            if (this.debounce) {
                this.checkUserSortChange(model);
                this.triggerChange();
            }
            this.debounce = false;
        },

        triggerChange: function () {
            this.trigger('request:change');
        },

        getQuery: function () {
            var query = {};
            var selectedGroups = this.getSelectedGroups();
            $.extend(true, query, selectedGroups);
            var filtersGroups = this.getFilters();
            $.extend(true, query, filtersGroups);
            this.set('query', query);
            return {
                query: JSON.stringify(this.get('query')),
                sort: JSON.stringify(this.get('sort')),
                limit: this.get('limit'),
                skip: this.get('skip'),
                tempFields: this.tempFields ? 'on' : undefined
            };
        },

// SERIALIZATION
        serialize: function () {
            var query = {};
            query.fields = this.selectedGroups.serialize();
            query.filters = this.filtersGroups.serialize();
            query.skip = this.get('skip');
            query.sort = this.get('sort');
            query.change = this.change;
            query.limit = this.get('limit');
            query.selected_items = this.selectedItemsCollection.pluck('id');
            return query;
        },

        deserialize: function (query, options) {
            this.selectedItemsCollection.reset(_.map(query.selected_items, function (id) {return {id: id}}), options);
            this.selectedGroups.deserialize(query.fields, this.app.request('groupsCollection'), options);
            this.filtersGroups.deserialize(query.filters, this.app.request('groupsCollection'), options);
            this.set({'skip': query.skip, 'sort': query.sort, 'limit': query.limit || 30}, options);
            this.setSelectedItemsFilter(options);

            this.debounce = true;
            this.onChange();
            this.app.vent.trigger('request:loaded');
        },

        setSelectedItemsFilter: function (options) {
            var group = this.filtersGroups.getSelectedItemsGroup();
            if (group) group.get('fields').at(0).set('filter', this.selectedItemsCollection, options)
        },

// PAGING
        setPage: function (nextPage, options) {
            this.set('skip', this.get('limit') * (nextPage - 1), options);
        },

        getPage: function () {
            return (this.get('skip') / this.get('limit')) + 1;
        },

// GET SORT
        getSort: function (groupModel, fieldModel) {
            if (this.getSortIndex() === groupModel.getIndex()) {
                if (this.getSortGroupId() === groupModel.get('parent').get('id') &&
                    this.getSortFieldId() === fieldModel.get('parent').get('fid')) {
                    return this.getSortDirection();
                }
            }
            return 0;
        },

        getSortIndex: function () {
            return this.get('sort').length ? this.get('sort')[0] : null;
        },

        getSortGroupId: function () {
            return this.get('sort').length ? +this.get('sort')[1][0][0].split('.')[0] : null;
        },

        getSortFieldId: function () {
            return this.get('sort').length ? this.get('sort')[1][0][0].split('.')[1] : null;
        },

        getSortDirection: function () {
            return this.get('sort').length ? this.get('sort')[1][0][1] : null;
        },

        setSort: function (index, gid, fid, direction, options) {
            if (direction) {
                this.set('sort', ['' + index, [[gid + '.' + fid, direction]]], options);
            } else {
                this.set('sort', [], options);
            }
        },

        checkUserSortChange: function (fieldModel) {
            if (fieldModel) {
                var user = fieldModel.get('user');
                var user_change = user && user != fieldModel.get('old_user');
                var index = this.getSortIndex();
                var is_user_sort = index !== 'i' && parseInt(this.getSortIndex()) === fieldModel.get('old_user');
                if (user_change && is_user_sort) {
                    this.setSort(user, this.getSortGroupId(), this.getSortFieldId(),
                        this.getSortDirection(), {silent: true});
                }
            }
        },

        setDefaultSort: function () {
            this.setSort('i', '16', 'b', -1);
        },
// GET SELECTED FIELDS
        getSelectedGroups: function () {
            var onFieldsGroups = this.selectedGroups.filter(function (groupModel) {
                return groupModel.isActive();
            });

            var splitUsersGroups = _.groupBy(onFieldsGroups, function (groupModel) {
                return groupModel.getIndex();
            });

            var eachUsersSelectedGroups = {};
            _.each(splitUsersGroups, function (groupsList, user) {
                eachUsersSelectedGroups[user] = {fields: _.map(groupsList, function (groupModel) {
                    return '' + groupModel.get('parent').get('id');
                })};
            });

            return eachUsersSelectedGroups;
        },

// GET FILTERS
        getFilters: function () {
            var onFiltersGroups = this.filtersGroups.filter(function (groupModel) {
                return groupModel.isActive();
            });

            var splitUsersGroups = _.groupBy(onFiltersGroups, function (groupModel) {
                return groupModel.getIndex();
            });

            var eachUsersSelectedGroups = {};
            _.each(splitUsersGroups, function (groupsList, id) {
                var filters = _.compact(_.map(groupsList, function (groupModel) {
                    return groupModel.serializeForFilter();
                }));
                if (filters.length > 0) {
                    eachUsersSelectedGroups[id] = {query: filters};
                }
            }, this);
            return eachUsersSelectedGroups;
        }
    });
});