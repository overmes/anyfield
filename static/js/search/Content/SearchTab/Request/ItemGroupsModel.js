define([
    'backbone',
    'search/Content/SearchTab/Request/ItemGroupFieldsCollection'
], function (Backbone, ItemGroupFieldsCollection) {
    'use strict';
    return Backbone.Model.extend({
        initialize: function (data, options) {
            this.set('fields', new ItemGroupFieldsCollection(data.fields));
        }
    });
});