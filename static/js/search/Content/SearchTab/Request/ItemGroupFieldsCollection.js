define([
    'backbone',
    'search/Content/SearchTab/Request/ItemGroupFieldModel'
], function (Backbone, ItemGroupFieldModel) {
    'use strict';
    return Backbone.Collection.extend({
        model: ItemGroupFieldModel
    });
});