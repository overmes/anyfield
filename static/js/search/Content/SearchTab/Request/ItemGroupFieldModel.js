define([
    'backbone'
], function (Backbone) {
    'use strict';
    return Backbone.Model.extend({
        urlRoot: '/api/itemField/',
        url: function () {
            return this.get('id') ? this.urlRoot + this.get('id') + '/' : this.urlRoot;
        }
    });
});