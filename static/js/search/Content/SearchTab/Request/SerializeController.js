define([
    'backbone',
    'marionette',
    'moment',
    'search/helpers',
    'search/Content/Common/GroupsCollection/FieldModel'
], function (Backbone, Marionette, Moment, helpers, FieldModelClass) {
    'use strict';
    var controller = Marionette.Controller.extend({
        serialize: function (itemModel, groupModel, options) {
            options = options || new Backbone.Model();
            var index = groupModel.getIndex();
            var groupId = (groupModel.get('parent') || groupModel).get('id');
            if (itemModel.has(index)) {
                var groupValues = itemModel.get(index)[groupId];
                return this.serializeGroup(itemModel, groupValues, groupId, groupModel, options);
            } else {
                return [];
            }
        },

        serializeGroup: function (itemModel, groupValuesArray, groupId, groupModel, options) {
            var result = [];
            // hack for main id field
            if (groupId !== 1) {
                for (var i in groupValuesArray) {
                    result.push(this.serializeFields(groupValuesArray[i], groupModel, options));
                }
            } else {
                var fieldsValues = [];
                groupModel.get('fields').each(function (fieldModel) {
                    var fid = fieldModel.get('parent').get('fid');
                    if (fid === 'a') {
                        fieldsValues.push(itemModel.get('i')['i'])
                    }
                    if (fid === 'b') {
                        fieldsValues.push(null);
                    }
                }, this);
                result.push(fieldsValues);
            }
            return result;
        },

        serializeFields: function (fieldsValuesDict, groupModel, options) {
            options = options || new Backbone.Model();
            var result = [];
            groupModel.get('fields').each(function (fieldModel) {
                if (!(fieldModel instanceof FieldModelClass)) {
                    fieldModel = fieldModel.get('parent') || fieldModel;
                }
                var fid = fieldModel.get('fid');
                result.push(this.serializeField(fieldsValuesDict[fid], fieldModel, options));
            }, this);

            return result;
        },

        serializeField: function (fieldValue, fieldModel, options) {
            var fieldType = fieldModel.get('type');
            if (fieldValue !== undefined && fieldValue !== null) {
                if (_.isString(fieldValue)) fieldValue = _.escape(fieldValue);
                if (fieldModel.get('fid') != 'pr') {
                    return this[fieldType + 'Serialize'](fieldValue, fieldModel, options);
                } else {
                    return this.prSerialize(fieldValue, fieldModel, options);
                }
            }
        },
        prSerialize: function (fieldValue, fieldModel, options) {
            return this.seSerialize(fieldValue > 0 ? 1 : 0, fieldModel);
        },
        seSerialize: function (fieldValue, fieldModel) {
            var valueName = _.filter(fieldModel.get('values'), function (valueObj) {
                return valueObj.number === fieldValue;
            });
            return valueName.length ? valueName[0].name : '';
        },
        idSerialize: function (fieldValue, fieldModel, options) {
            return fieldValue;
        },
        vaSerialize: function (fieldValue, fieldModel, options) {
            return Math.round(fieldValue * 100) / 100;
        },
        teSerialize: function (fieldValue, fieldModel, options) {
            var tableTextLength = options.get('tableTextLength');
            if (fieldValue.length > tableTextLength) {
                return ('{0}<span><span class="table-text-show">...<a>Show more</a></span>' +
                    '<span class="table-text-hidden hidden">{1} <a class="table-text-hide">Hide</a></span></span>')
                    .format(fieldValue.slice(0, tableTextLength), fieldValue.slice(tableTextLength));
            }
            return fieldValue;
        },
        imSerialize: function (fieldValue, fieldModel, options) {
            return "<img src='{0}'></img>".format(fieldValue);
        },
        daSerialize: function (fieldValue, fieldModel, options) {
            var day = Moment.unix(fieldValue);
            return day.isValid() ? day.format('LL') : '';
        },
        liSerialize: function (fieldValue, fieldModel, options) {
            if (fieldValue && fieldValue.length) {
                var id = fieldValue[0];
                var title = _.escape(fieldValue[1]);
                return "<a href='/item/{0}/{2}/'>{1}</a>".format(id, title, helpers.convertToSlug(title));
            }
        },
        urSerialize: function (fieldValue, fieldModel, options) {
            return "<a target='_blank' href='{0}'>{1}</a>".format(fieldValue, helpers.getLocation(fieldValue).hostname);
        }
    });
    return new controller();
});