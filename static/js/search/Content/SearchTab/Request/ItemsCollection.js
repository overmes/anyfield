define([
    'backbone',
    'search/Content/SearchTab/Request/ItemModel'
], function (Backbone, ItemModel) {
    'use strict';
    return Backbone.Collection.extend({
        url: '/api/query/',
        model: ItemModel,

        parse: function (response) {
            return response.objects;
        },

        initialize: function (models, options) {
            this.lastFetchItemsCount = 0;
            this.listenTo(this, 'sync', this.onSync);
        },

        setRequest: function (request) {
            this.requestModel = request;
            this.listenTo(this.requestModel, 'request:change', this.onRequestChange);
        },

        setUser: function (user) {
            this.user = user;
            this.listenTo(this.user, 'change:id', this.onRequestChange);
        },

        onRequestChange: function () {
            this.load();
        },

        load: function (options) {
            options = _.defaults(options || {}, {reset: true, remove: true});
            var currentQuery = this.requestModel.getQuery();
            console.log(JSON.stringify(currentQuery));
            console.log(this.requestModel.serialize());

            var self = this;
            this.fetch(_.extend({data: currentQuery}, options));
        },

        onSync: function (model, resp, options) {
            this.lastFetchItemsCount = resp.objects.length;
        },

        loadMore: function () {
            this.requestModel.setPage(this.requestModel.getPage() + 1, {silent: true});
            this.load({reset: false, remove: false});
        }

    });
});