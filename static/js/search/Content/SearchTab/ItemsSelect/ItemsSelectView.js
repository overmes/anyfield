define([
    'backbone',
    'marionette',
    'text!search/Content/SearchTab/ItemsSelect/ItemsSelectTemplate.html',
    'search/Content/SearchTab/FieldsPanel/SelectedGroupsList/SelectedGroupModel',
], function (Backbone, Marionette, ItemsSelectTemplate, SelectedGroupModel) {
    'use strict';
    return Marionette.ItemView.extend({
        template: _.template(ItemsSelectTemplate),
        className: 'item-select-view',

        ui: {
            select: '.js-select',
            clear: '.js-clear',
            addField: '.js-add-field'
        },

        events: {
            'click @ui.select': 'showSelectedItems',
            'click @ui.clear': 'clearSelectedItem',
            'click @ui.addField': 'addField'
        },

        addField: function (event) {
            this.app.vent.trigger('view:field:add');
        },
        initialize: function (options) {
            this.app = options.app;
            this.user = this.app.request('user');

            this.selectedItemsCollection = this.app.request('selectedItemsCollection');

            this.listenTo(this.app.vent, 'request:loaded', this.render);
            this.listenTo(this.user, 'change:id', this.render);
            this.listenTo(this.app.vent, 'view:item:select', this.onItemSelect);
        },
        clearSelectedItem: function () {
            this.selectedItemsCollection.reset();
            this.render();
        },
        onItemSelect: function (id) {
            if (this.selectedItemsCollection.get(id)) {
                this.selectedItemsCollection.remove(id);
            } else {
                this.selectedItemsCollection.add({'id': id});
            }
            this.render();
        },

        templateHelpers: function () {
            var self = this;
            return {
                getSelectedCount: function () {
                    return self.selectedItemsCollection.length;
                },
                user: this.user
            }
        },

        showSelectedItems: function () {
            this.app.vent.trigger('view:select:show');
        }

    });
});