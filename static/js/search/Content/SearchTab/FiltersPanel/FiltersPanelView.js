define([
    'backbone',
    'marionette',
    'text!search/Content/SearchTab/FiltersPanel/FiltersTemplate.html',
    'search/Content/Common/FieldsSelect/GroupSelectView',
    'search/Content/SearchTab/FiltersPanel/GroupFilterView'
], function (Backbone, Marionette, FiltersTemplate, GroupSelectView, GroupFilterView) {
    'use strict';
    return Marionette.CompositeView.extend({
        className: 'panel panel-info',

        ui: {
            addButton: '#GroupAddButton'
        },

        childView: GroupFilterView,
        childViewContainer: "#SubFilters",

        initialize: function (options) {
            this.app = options.app;
            this.collection = this.app.request('filtersGroups');
            this.listenTo(this.app.vent, 'request:loaded', this.render);
            this.listenTo(this.app.vent, 'view:select:show', this.addSelectedItemsFilter);
            this.listenTo(this.app.vent, 'group:field:selected', this.onSelectField);
            this.enableFilters = this.getEnableFilters();
        },
        onSelectField: function (group, field, multi, selectedGroups) {
            var fieldSelected = this.collection.isFieldSelected(group, field);
            var enableField = this.enableFilters.getField(group, field);
            if (this.collection != selectedGroups && !fieldSelected && enableField) {
                var disabled = this.collection.selectField(group, field, multi);
                if (disabled !== undefined) this.collection.trigger('group:field:disabled', group, field, disabled);
            }
        },

        addSelectedItemsFilter: function () {
            var field = this.app.request('groupsCollection').getSelectedItemsField();
            var selectedItemGroup = this.collection.getGroupsByGroup(field.get('parent'));
            if (selectedItemGroup.length === 0) {
                this.collection.addField(field, {'filter': this.app.request('selectedItemsCollection')});
                this.collection.trigger('fields:change');
            } else {
                this.collection.remove(selectedItemGroup);
            }
        },
        getEnableFilters: function () {
            return this.app.request('groupsCollection').getFilteredCollection(function (field) {
                return ['im', 'ur', 'si', 'co'].indexOf(field.get('type')) < 0 && !field.get('is_temp') && field.has('id');
            });
        },

        onRender: function () {
            var groupsSelectView = new GroupSelectView({app: this.app, selectedGroups: this.collection,
                                                        collection: this.enableFilters});
            this.drop = new Drop({
                target: this.ui.addButton.get(0),
                content: groupsSelectView.el,
                position: 'right top',
                openOn: 'click',
                constrainToWindow: false,
                constrainToScrollParent: false
            });
            this.drop.on('open', groupsSelectView.render);
        },

        childViewOptions: function () {
            return {
                app: this.app
            };
        },

        template: _.template(FiltersTemplate)
    });
});