define([
    'backbone',
    'marionette',
    'text!search/Content/SearchTab/FiltersPanel/GroupTemplate.html',
    'search/Content/Common/FieldsSelect/FieldsSelectView',
    'search/Content/SearchTab/FiltersPanel/FiltersViews/FiltersController',
    'search/Content/Common/Behaviors/RemoveBehavior',
    'search/Content/Common/Behaviors/OnOffBehavior',
    'search/Content/Common/Behaviors/IncludeExcludeBehavior',
    'search/Content/Common/UserSelect/UserSelectView',
    'bootstrap'
], function (Backbone, Marionette, GroupTemplate, FieldsSelectView, FiltersController, RemoveBehavior, OnOffBehavior,
             IncludeExcludeBehavior, UserSelectView) {
    'use strict';
    return  Marionette.CompositeView.extend({
        childViewContainer: ".child-fields",
        className: 'panel panel-success',
        getChildView: function (item) {
            return FiltersController.getFieldFilterClass(item.get('parent'));
        },

        ui: {
            addButton: '.add-button'
        },

        events: {
            'click @ui.addButton': 'onFilterBoxAdd'
        },

        initialize: function (options) {
            this.app = options.app;
            this.collection = options.model.get('fields');
        },

        onShow: function () {
            this.userSelectRegion = new Backbone.Marionette.Region({
                el: this.$el.find(".user-select")
            });
            if (this.model.get('parent').get('type') === 'us') {
                this.userSelectRegion.show(new UserSelectView({app: this.app, model: this.model}));
            }
        },

        template: _.template(GroupTemplate),

        childViewOptions: function (item) {
            return {
                app: this.app
            };
        },
        behaviors: {
            RemoveBehavior : {
                behaviorClass: RemoveBehavior
            },
            OnOffBehavior: {
                behaviorClass: OnOffBehavior
            },
            IncludeExcludeBehavior: {
                behaviorClass: IncludeExcludeBehavior
            }
        }
    });
});