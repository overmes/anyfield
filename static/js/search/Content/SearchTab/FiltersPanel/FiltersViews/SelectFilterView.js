define([
    'backbone',
    'marionette',
    'search/Content/SearchTab/FiltersPanel/FiltersViews/AbstractFilterView',
    'text!search/Content/SearchTab/FiltersPanel/FiltersViews/SelectFilterTemplate.html',
    'search/Content/Common/Behaviors/LogicBehavior',
    'modelbinder',
    'select',
    'bootstrap'
], function (Backbone, Marionette, AbstractFilterView, SelectFilterTemplate, LogicBehavior, ModelBinder) {
    'use strict';
    return  AbstractFilterView.extend({

        //look abstract filter view getTemplate
        filterTemplate: SelectFilterTemplate,

        initialize: function () {
            this.modelBinder = new ModelBinder();
        },

        ui: {
            select: 'select'
        },

        converter: function (direction, value) {
            if (direction === 'ViewToModel') {
                return _.map(value, function (val) {return val !== 'null' ? parseInt(val) : null; });
            } else {
                return _.map(value, function (val) {return val !== 'null' ? "" + val : "null"; });
            }
        },

        onShow: function () {
            this.ui.select.selectpicker();
            this.ui.select.selectpicker('val', this.converter('ModelToView', this.model.get('filter')));
            var bindings = {filter: {selector: this.ui.select, converter: this.converter}};
            this.modelBinder.bind(this.model, this.el, bindings);
            this.ui.select.selectpicker('render');
        },

        behaviors: $.extend({
            LogicBehavior: {
                behaviorClass: LogicBehavior
            }
        }, AbstractFilterView.prototype.behaviors)

    });
});