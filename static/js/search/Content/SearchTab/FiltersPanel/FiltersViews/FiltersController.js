define([
    'backbone',
    'marionette',
    'search/Content/SearchTab/FiltersPanel/FiltersViews/SelectFilterView',
    'search/Content/SearchTab/FiltersPanel/FiltersViews/TextFilterView',
    'search/Content/SearchTab/FiltersPanel/FiltersViews/RangeFilterView',
    'search/Content/SearchTab/FiltersPanel/FiltersViews/DateRangeView',
    'search/Content/SearchTab/FiltersPanel/FiltersViews/LinkFilterView',
    'search/Content/SearchTab/FiltersPanel/FiltersViews/SelectedItemsFilterView'
], function (Backbone, Marionette, SelectFilterView, TextFilterView, RangeFilterView, DateRangeView, LinkFilterView,
             SelectedItemsFilterView) {
    'use strict';
    var Controller = Marionette.Controller.extend({
        getFieldFilterClass: function (field) {
            var res;
            var type = field.get('type');
            switch (type) {
            case 'se':
                res = SelectFilterView;
                break;
            case 'te':
                res = TextFilterView;
                break;
            case 'va':
                res = RangeFilterView;
                break;
            case 'id':
                res = RangeFilterView;
                break;
            case 'li':
                res = LinkFilterView;
                break;
            case 'da':
                res = DateRangeView;
                break;
            case 'si':
                res = SelectedItemsFilterView;
                break;
            }
            return res;
        }
    });

    return new Controller();
});