define([
    'backbone',
    'marionette',
    'text!search/Content/SearchTab/FiltersPanel/FiltersViews/AbstractFilterTemplateSimple.html',
    'search/Content/Common/Behaviors/RemoveBehavior',
    'search/Content/Common/Behaviors/OnOffBehavior'
], function (Backbone, Marionette, AbstractFilterTemplate, RemoveBehavior, OnOffBehavior) {
    'use strict';
    return  Marionette.ItemView.extend({
        className: 'view-filter-field',

        getTemplate: function () {
            var childTemplate = this.filterTemplate;
            return this.template ||
                _.template(AbstractFilterTemplate.replace('replacebyfieldfilter', childTemplate));
        },

        behaviors: {
            RemoveBehavior : {
                behaviorClass: RemoveBehavior
            },
            OnOffBehavior: {
                behaviorClass: OnOffBehavior
            }
        }
    });
});