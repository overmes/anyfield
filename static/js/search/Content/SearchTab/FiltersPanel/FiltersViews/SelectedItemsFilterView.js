define([
    'backbone',
    'marionette',
    'search/Content/SearchTab/FiltersPanel/FiltersViews/AbstractFilterView'
], function (Backbone, Marionette, AbstractFilterView) {
    'use strict';
    return  AbstractFilterView.extend({
        template: _.template('<span class="label label-primary center-block"><%- selectedCount() %></span>'),

        initialize: function (options) {
            this.app = options.app;
            this.filter = this.model.get('filter');
            if (this.filter) this.listenTo(this.filter, 'add remove reset', this.onChange);
        },

        onChange: function () {
            this.render();
            this.model.trigger('change');
        },

        templateHelpers: function () {
            var self = this;
            return {
                selectedCount: function () {return self.filter ? self.filter.length:'';}
            }
        }

    });
});