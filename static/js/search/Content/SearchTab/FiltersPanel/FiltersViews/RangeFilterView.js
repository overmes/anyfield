define([
    'backbone',
    'marionette',
    'search/Content/SearchTab/FiltersPanel/FiltersViews/AbstractFilterView',
    'text!search/Content/SearchTab/FiltersPanel/FiltersViews/RangeFilterTemplate.html',
    'modelbinder',
    'ionslider'
], function (Backbone, Marionette, AbstractFilterView, RangeFilterTemplate, ModelBinder, ionslider) {
    'use strict';
    return  AbstractFilterView.extend({

        //look abstract filter view getTemplate
        filterTemplate: _.template(RangeFilterTemplate),

        ui: {
            slider: ".range-slider",
            from: "[name=from]",
            to: "[name=to]",
            sliderView: ".slider-view",
            inputView: ".input-view",
            jsView: ".js-view",
            form: ".form-control"
        },

        events: {
            "click @ui.jsView": "changeView",
            "keyup @ui.form": "onFormChange"
        },
        initialize: function () {
            this.type = 'slider';
        },
        changeView: function () {
            this.type = this.type === 'slider' ? 'input' : 'slider';
            this.showCurrentType();
        },
        onFormChange: function () {
            var from = parseInt(this.ui.from.val(), 10);
            var to = parseInt(this.ui.to.val(), 10);
            this.model.set({filter: [from, to]});
            this.ui.slider.data("ionRangeSlider").update({
                from: from, to: to
            });
        },
        setForm: function (from, to) {
            this.ui.from.val(from);
            this.ui.to.val(to);
        },
        showCurrentType: function () {
            if (this.type === 'slider') {
                this.ui.sliderView.show();
                this.ui.inputView.hide();
            } else {
                this.ui.inputView.show();
                this.ui.sliderView.hide();
            }
        },

        onShow: function() {
            this.showCurrentType();
            var self = this;
            var range = this.model.get('parent').get('range')[0];
            var min = range ? range.min : 0;
            var max = range ? range.max : 10000;
            var step = range ? range.step : 1;

            var filter = this.model.get('filter') || [];
            this.ui.slider.ionRangeSlider({
                type: 'double',
                min: min,
                max: max,
                from: filter[0] || min,
                to: filter[1] || max,
                step: step,
                hide_min_max: false,
                onFinish: function (slider) {
                    self.model.set({filter: [slider.from, slider.to]});
                    self.setForm(slider.from, slider.to);
                }
            });
            this.slider = this.ui.slider.data("ionRangeSlider");
            this.setForm(filter[0], filter[1]);
        },

        onDestroy: function () {
            if (this.slider) this.slider.destroy();
        }
    });
});