define([
    'backbone',
    'marionette',
    'search/Content/SearchTab/FiltersPanel/FiltersViews/AbstractFilterView',
    'modelbinder'
], function (Backbone, Marionette, AbstractFilterView, ModelBinder) {
    'use strict';
    return  AbstractFilterView.extend({

        //look abstract filter view getTemplate
        filterTemplate: '<input type="text" class="form-control">',

        initialize: function () {
            this.modelBinder = new ModelBinder();
        },

        ui: {
            input: '.form-control'
        },

        onShow: function () {
            var bindings = {filter: {selector: this.ui.input}};
            this.modelBinder.bind(this.model, this.el, bindings, {changeTriggers: {'': 'change keyup'}});
        }

    });
});