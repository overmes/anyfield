define([
    'backbone',
    'marionette',
    'search/Content/SearchTab/FiltersPanel/FiltersViews/AbstractFilterView',
    'text!search/Content/SearchTab/FiltersPanel/FiltersViews/DateRangeTemplate.html',
    'datepicker',
    'modelbinder',
    'moment'
], function (Backbone, Marionette, AbstractFilterView, DateRangeTemplate, DatePicker, ModelBinder, moment) {
    'use strict';
    return  AbstractFilterView.extend({

        //look abstract filter view getTemplate
        filterTemplate: DateRangeTemplate,

        initialize: function () {
//            this.modelBinder = new ModelBinder();
//            var bindings = {
//                from: {selector: this.ui.from, converter: this.converter},
//                to: {selector: this.ui.to, converter: this.converter}
//            };
//            this.modelBinder.bind(this.model, this.el, bindings);
        },

        ui: {
            input: '.input-daterange',
            from: '[name=from]',
            to: '[name=to]'
        },

        converter: function (direction, value, attribute, model) {
            if (direction === 'ViewToModel') {
                return moment(value, "MM/DD/YYYY").unix();
            } else {
                return value ? moment.unix(value).format('L') : '';
            }
        },

        onShow: function () {
            var self = this;
            this.ui.from.datepicker('setDate', this.getFormattedDate(this.getFromDate()));
            this.ui.to.datepicker('setDate', this.getFormattedDate(this.getToDate()));
            this.ui.input.datepicker({
                multidate: true,
                autoclose: true,
                todayHighlight: true,
                startView: 0,
                minViewMode: 0
            }).on('changeDate', function (event) {
                var name = event.target.name;
                if (name === 'from') {
                    self.model.set('filter', [self.getUnixDate(event.date), self.getToDate()]);
                } else {
                    self.model.set('filter', [self.getFromDate(), self.getUnixDate(event.date)]);
                }
            });
        },
        getFromDate: function () {
            if (this.model.has('filter')) {
                return this.model.get('filter')[0];
            } else {
                return null;
            }
        },
        getToDate: function () {
            if (this.model.has('filter')) {
                return this.model.get('filter')[1];
            } else {
                return null;
            }
        },
        getFormattedDates: function () {
            return [this.getFormattedDate(this.getFromDate()), this.getFormattedDate(this.getToDate())];
        },
        getFormattedDate: function (value) {
            return value ? moment.unix(value).toDate() : '';
        },
        getUnixDate: function (value) {
            return moment(value).unix();
        }

    });
});