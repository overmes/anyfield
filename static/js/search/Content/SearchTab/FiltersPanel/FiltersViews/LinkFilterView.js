define([
    'backbone',
    'marionette',
    'search/Content/SearchTab/FiltersPanel/FiltersViews/AbstractFilterView',
    'search/Content/Common/ItemSelect/MultiItemSelectView'
], function (Backbone, Marionette, AbstractFilterView, MultiItemSelectView) {
    'use strict';
    return  AbstractFilterView.extend({

        //look abstract filter view getTemplate
        filterTemplate: '<div class="link"></div>',

        initialize: function (options) {
            this.app = options.app;
        },

        ui: {
        },

        onShow: function () {
            var itemSelectView = new MultiItemSelectView({app: this.app, attribute: 'filter', model: this.model, width: '100%'});
            this.$(".link").html(itemSelectView.render().el);
        }

    });
});