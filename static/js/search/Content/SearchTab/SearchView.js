define([
    'backbone',
    'marionette',
    'text!search/Content/SearchTab/SearchTemplate.html',
    'search/Content/SearchTab/FieldsPanel/FieldsPanelView',
    'search/Content/SearchTab/Table/TableView',
    'search/Content/SearchTab/PackeryList/ListView',
    'search/Content/SearchTab/FiltersPanel/FiltersPanelView',
    'search/Content/SearchTab/Pagination/PaginationView',
    'search/Content/SearchTab/States/StatesLayoutView',
    'search/Content/SearchTab/ItemsSelect/ItemsSelectView',
    'search/Content/SearchTab/Table/TableModeView',
    'search/Content/SearchTab/LoadMore/LoadMoreView'
], function (Backbone, Marionette, SearchTemplate, FieldsPanelView, TableView, ListView, FiltersPanelView,
             PaginationView, StatesLayoutView, ItemsSelectView, TableModeView, LoadMoreView) {
    'use strict';
    return Marionette.LayoutView.extend({
        initialize: function (options) {
            this.app = options.app;
            this.addResizeListner();
        },

        addResizeListner: function () {
            $(window).resize($.proxy(this.changeProportions, this));
        },

        changeProportions: function () {
            var currentWidth = $(window).width();
            if (currentWidth < 1500 && this.$('.input-controls .col-sm-2')) {
                this.$('.input-controls').removeClass('col-sm-2').addClass('col-sm-3');
                this.$('.list-controls').removeClass('col-sm-10').addClass('col-sm-9');
            }
            if (currentWidth >= 1500 && this.$('.input-controls .col-sm-3')) {
                this.$('.input-controls').removeClass('col-sm-3').addClass('col-sm-2');
                this.$('.list-controls').removeClass('col-sm-9').addClass('col-sm-10');
            }
        },

        template: _.template(SearchTemplate),

        regions: {
            fieldsRegion: '#Fields',
            tableRegion: '#SearchTable',
            filterRegion: '#Filters',
            upperPaginationRegion: '#UpperPagination',
            downPaginationRegion: '#DownPagination',
            statesRegion: '#States',
            selectedItemsRegion: '#SelectedItems',
            tableModeRegion: '#TableMode',
            loadMoreRegtion: '#LoadMore'
        },

        onShow: function () {
            this.changeProportions();
            this.fieldsRegion.show(new FieldsPanelView({app: this.app}));
            this.tableView = new TableView({app: this.app});
            this.tableRegion.show(this.tableView);
            this.filterRegion.show(new FiltersPanelView({app: this.app}));
            this.upperPaginationRegion.show(new PaginationView({app: this.app}));
            this.downPaginationRegion.show(new PaginationView({app: this.app}));
            this.selectedItemsRegion.show(new ItemsSelectView({app: this.app}));
            this.tableModeRegion.show(new TableModeView({app: this.app}));
            this.loadMoreRegtion.show(new LoadMoreView({app: this.app}));
            if (this.app.request('statesController').isSupported()) {
                this.statesRegion.show(new StatesLayoutView({app: this.app}));
            }
        },

        events: {
            'click .js-print': 'onPrint',
            'click .js-refresh': 'onRefresh'
        },

        onPrint: function () {
            this.$el.html('Page #{0} <div id="SearchTable"></div>'.format(this.app.request('requestModel').getPage()));
            this.$('#SearchTable').html(this.tableView.el);
        },

        onRefresh: function () {
            this.app.request('requestModel').triggerChange();
        }
    });
});