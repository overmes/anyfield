define([
    'backbone',
    'marionette',
    'text!search/Content/SearchTab/AddFieldsTab/AddFieldsTabTemplate.html',
    'search/Content/SearchTab/Request/ItemsCollection',
    'search/Content/SearchTab/AddFieldsTab/AddGroupsView',
    'search/Content/SearchTab/AddFieldsTab/ItemsGroupsView',
    'search/Content/Common/GroupsCollection/GroupsCollection',
    'select'
], function (Backbone, Marionette, AddFieldsTabTemplate, ItemsCollection, AddGroupsView, ItemsGroupsView, GroupsCollection) {
    'use strict';
    return Marionette.LayoutView.extend({
        template: _.template(AddFieldsTabTemplate),
        id: 'AddFieldsView',
        className: 'container',
        initialize: function (options) {
            this.app = options.app;
            this.groupsCollection = this.app.request('groupsCollection');
            this.selectedGroupType = null;
        },
        regions: {
            itemGroupsRegions: '.view-item-groups',
            newGroupRegions: '.view-group-new'
        },
        ui: {
            selectGroup: '.js-select-group'
        },

        events: {
            'change @ui.selectGroup': 'onGroupSelect'
        },

        onGroupSelect: function (event) {
            var target = event.currentTarget;
            var value = target.value;
            if (value !== 'null') {
                this.selectedGroupType = this.groupsCollection.get(parseInt(value));
            } else {
                this.selectedGroupType = null;
            }
            this.render();
        },

        onRender: function () {
            this.ui.selectGroup.selectpicker();
            if (this.selectedGroupType) {
                this.ui.selectGroup.selectpicker('val', this.selectedGroupType.get('id'));
                var itemsGroupsView = new ItemsGroupsView({app: this.app, selectedItems: this.app.request('selectedItemsCollection'),
                    selectedGroupType: this.selectedGroupType});
                this.itemGroupsRegions.show(itemsGroupsView);
                this.newGroupRegions.show(new AddGroupsView({app: this.app, selectedGroupType: this.selectedGroupType,
                    itemsGroupsView: itemsGroupsView}));
            }
        },

        getEditableGroups: function () {
            return new GroupsCollection(this.app.request('groupsCollection').filter(function (group) {
                return !group.get('is_temp') && group.get('edit') !== 'no';
            }));
        },

        templateHelpers: function () {
            return {
                editableGroups: this.getEditableGroups()
            };
        }
    });
});