define([
    'backbone',
    'marionette',
    'text!search/Content/SearchTab/AddFieldsTab/AddGroupsTemplate.html',
    'search/Content/ItemDetailTab/GroupView',
    'search/Content/SearchTab/Request/ItemGroupFieldModel'
], function (Backbone, Marionette, AddGroupsTemplate, GroupView, ItemGroupFieldModel) {
    'use strict';
    return Marionette.LayoutView.extend({
        template: _.template(AddGroupsTemplate),
        regions: {
            addGroupRegion: '.group-field-new'
        },
        ui: {
            add: '.js-add-field',
            save: '.js-save-field',
            remove: '.js-remove-field'
        },
        events: {
            'click @ui.add': 'addGroup',
            'click @ui.save': 'saveAll',
            'click @ui.remove': 'removeAll'
        },
        initialize: function (options) {
            this.app = options.app;
            this.selectedGroupType = options.selectedGroupType;
            this.itemsGroupsView = options.itemsGroupsView;
        },

        eachGroup: function (func) {
            this.itemsGroupsView.children.each(function (itemView) {
                var itemId = itemView.model.get('id');
                itemView.itemGroupsCollection.each(function (itemGroupModel) {
                    if (itemGroupModel.get('id') === this.selectedGroupType.get('id'))
                        func(itemGroupModel, itemId);
                }, this);
            }, this);
        },

        saveAll: function () {
            var fieldsToSave = [];
            this.eachGroup(function (itemGroupModel, itemId) {
                itemGroupModel.get('fields').each(function (field) {
                    if (!field.has('id')) fieldsToSave.push(field);
                })
            });
            this.saveFieldsOneByOne(fieldsToSave);
        },

        saveFieldsOneByOne: function (fieldsList) {
            var currentField = 0;
            function saveCurrentField() {
                if (currentField < fieldsList.length)
                    fieldsList[currentField].save().always(function () {
                        currentField += 1;
                        saveCurrentField();
                    });
            }
            saveCurrentField();
        },

        removeAll: function () {
            this.eachGroup(function (itemGroupModel, itemId) {
                itemGroupModel.get('fields').each(function (field) {
                    if (!field.get('id')) field.collection.remove(field);
                })
            });
        },

        addGroup: function () {
            var self = this;
            this.eachGroup(function (itemGroupModel, itemId) {
                var newField = self.newGroup.clone();
                newField.set('item', itemId);
                itemGroupModel.get('fields').add(newField);
            });
        },
        onShow: function () {
            this.groupView = new GroupView({app: this.app, controls: false, model: this.selectedGroupType});
            this.newGroup = this.groupView.addField();
            this.addGroupRegion.show(this.groupView);
        }
    });
});