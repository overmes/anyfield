define([
    'backbone',
    'marionette',
    'search/Content/SearchTab/Request/RequestModel',
    'search/Content/SearchTab/FieldsPanel/SelectedGroupsList/SelectedGroupsCollection',
    'search/Content/SearchTab/Request/ItemsCollection',
    'search/Content/ItemDetailTab/ItemDetailView',
    'search/Content/Common/GroupsCollection/GroupsCollection',
    'select'
], function (Backbone, Marionette, RequestModel, SelectedGroupsCollection, ItemsCollection,
    ItemDetailView, GroupsCollection) {
    'use strict';
    return Marionette.CollectionView.extend({
        childView: ItemDetailView,
        initialize: function (options) {
            this.app = options.app;
            this.user = this.app.request('user');
            this.collection = new ItemsCollection();

            this.groupsCollection = this.app.request('groupsCollection');
            this.selectedGroupType = options.selectedGroupType;
            this.selectedItems = options.selectedItems;

            this.setCollectionRequest();
            this.loadCollection();

            this.listenTo(this.user, 'change:id', function () {
                this.loadCollection();
            });
        },

        setCollectionRequest: function () {
            this.fields = new SelectedGroupsCollection();
            this.filters = new SelectedGroupsCollection();
            var titleGroup = this.fields.addField(this.groupsCollection.getTitleField());
            titleGroup.set({'on': true, 'user': this.app.request('user').get('id')});

            var selectedGroup = this.fields.addGroup(this.selectedGroupType);
            selectedGroup.set({'on': true, 'user': this.app.request('user').get('id')});

            var selectedFilterGroup = this.filters.addField(this.groupsCollection.getSelectedItemsField(),
                {'filter': this.selectedItems, 'on': true});
            selectedFilterGroup.set({'on': true, 'include': true});

            this.requestModel = new RequestModel({}, {
                app: this.app,
                selectedGroups: this.fields,
                filtersGroups: this.filters,
                tempFields: true
            });
            this.collection.setRequest(this.requestModel);
        },

        loadCollection: function () {
            var selectedItems = this.app.request('selectedItemsCollection');
            if (selectedItems.length > 0) {
                this.collection.reset();
                this.collection.load({reset: false, remove: false});
                var lastPage = 1 + selectedItems.length / this.requestModel.get('limit');
                for (var page = 2; page < lastPage; page += 1) {
                    this.requestModel.setPage(page, {silent: true});
                    this.collection.load({reset: false, remove: false});
                }
            }
        },

        getSelectedGroupsCollection: function () {
            return new GroupsCollection([this.selectedGroupType]);
        },

        childViewOptions: function () {
            return {
                app: this.app,
                selectedGroupsCollection: this.getSelectedGroupsCollection(),
                className: ''
            };
        }
    });
});