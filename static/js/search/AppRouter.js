define([
    'backbone',
    'marionette',
    'search/helpers'
], function (Backbone, Marionette, helpers) {
    'use strict';
    return Marionette.AppRouter.extend({
        initialize: function (options) {
            this.app = options.app;
            this.controller = this.app.appController;
            this.storageController = this.app.request('statesController');
            this.addListners();
            this.tabNameUrls = _.invert(this.appRoutes);
        },

        addListners: function () {
            this.listenTo(this.app.vent, 'tab:show', this.changeTabLink);
            this.listenTo(this.app.vent, 'item:detail:loaded', this.setItemLink);
            this.listenTo(this.app.vent, 'storage:state:change', this.setStateLink);
        },
        appRoutes: {
            '': 'showSearchTab',
            'about/': 'showAboutTab',
            'howto/': 'showHowTo',
            'fields/': 'showFieldsTab',
            'tempFields/': 'showTempFieldsTab',
            'scoresImport/': 'showScoresImport',
            'recommendation/': 'showRecommendation',
            'neighbours/': 'showNeighbours',
            'item/:id/(:title/)': 'showItemTab',
            'localState/:id/': 'loadLocalState',
            'sharedState/:id/(:name/)': 'loadSharedState',
            '*path':  'showSearchTab'
        },

        changeTabLink: function (tabName) {
            var url = this.tabNameUrls[tabName];
            if (url === '*path') {url = this.searchTabUrl ? this.searchTabUrl : ''; }
            Backbone.history.navigate(url);
        },

        setStateLink: function (type, stateId, name) {
            var template = (type === 'local') ? 'localState/{0}/'.format(stateId) : 'sharedState/{0}/'.format(stateId);
            this.searchTabUrl = template.format(stateId);
            if (name) this.searchTabUrl += '{0}/'.format(helpers.convertToSlug(name));
            Backbone.history.navigate(this.searchTabUrl);
        },

        setItemLink: function (itemId, title) {
            title =  title ? helpers.convertToSlug(title) + '/': '';
            var url = this.tabNameUrls.showItemTab.replace(':id', itemId).replace('(:title/)', title);
            Backbone.history.navigate(url);
        }

    });
});