define([
    'backbone',
    'marionette',
    'search/Content/SearchTab/States/SharedStates/SharedStateModel'
], function (Backbone, Marionette, SharedStateModel) {
    'use strict';
    return Marionette.Controller.extend({
        initialize: function (options) {
            this.app = options.app;
        },

        linkClick: function (event) {
            event.preventDefault();
            var target = event.currentTarget;
            var tabName = target.dataset.tab;
            this[tabName]();
        },

        showSearchTab: function () {
            this.stateLoadCheck();
            this.app.vent.trigger('tab:show', 'showSearchTab');
        },

        showTempFieldsTab: function () {
            this.app.vent.trigger('tab:show', 'showTempFieldsTab');
        },

        showNeighbours: function () {
            this.app.vent.trigger('tab:show', 'showNeighbours');
        },

        loadLocalState: function (id) {
            var localState = this.app.request('statesController').getLocalState(id);
            if (localState) {
                this.app.request('statesController').loadLocalState(localState);
                this.stateLoaded = true;
            }
            this.showSearchTab();
        },

        loadSharedState: function (id) {
            var sharedState = new SharedStateModel({id: id}, {app: this.app});
            var self = this;
            sharedState.fetch()
                .done(function () {
                    self.app.request("statesController").loadSharedState(sharedState);
                    self.stateLoaded = true;
                    self.showSearchTab();
                })
                .error(function () {
                    self.showSearchTab();
                });
        },

        showAboutTab: function () {
            this.app.vent.trigger('tab:show', 'showAboutTab', 'about');
        },

        showHowTo: function () {
            this.app.vent.trigger('tab:show', 'showHowTo', 'howto');
        },

        showScoresImport: function () {
            this.app.vent.trigger('tab:show', 'showScoresImport');
        },

        showRecommendation: function () {
            this.app.vent.trigger('tab:show', 'showRecommendation');
        },

        showFieldsTab: function () {
            this.app.vent.trigger('tab:show', 'showFieldsTab');
        },

        showItemTab: function (itemId, title) {
            this.app.vent.trigger('item:detail', itemId, title);
        },

        stateLoadCheck: function () {
            if (!this.stateLoaded) {
                this.loadDefaultState();
                this.stateLoaded = true;
            }
        },

        loadDefaultState: function () {
            var defaultState = new SharedStateModel({id: 'default'}, {app: this.app});
            var app = this.app;
            defaultState.fetch({success: function () {
                app.request("statesController").loadSharedState(defaultState);
            }});
        }
    });
});