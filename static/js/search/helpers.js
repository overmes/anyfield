define(['jquery', 'nprogress'], function ($, NProgress) {
    'use strict';
    if (!String.prototype.format) {
      String.prototype.format = function() {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function(match, number) {
          return typeof args[number] != 'undefined'
            ? args[number]
            : match
          ;
        });
      };
    }
    // using jQuery
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            var csrftoken = getCookie('csrftoken');
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
    var helpers = {
        convertToSlug: function (Text) {
            return Text
                .replace(/[^\w ]+/g, '')
                .replace(/ +/g, '-');
        },

        getUrlResourceName: function (url) {
            return url.match(/\/api\/(.*)\//)[1];
        },

        getLocation: function(href) {
            var l = document.createElement("a");
            l.href = href;
            return l;
        },

        onFormSubmit: function (e) {
            e.preventDefault();
            var self = this;
            this.model.save()
                .done(function (result) {
                    var collection = self.model.collection;
                    if (collection) {
                        collection.remove(self.model);
                        collection.add(self.model, {at: 0});
                    }
                })
                .fail(function (error) {
                    var name = helpers.getUrlResourceName(self.model.urlRoot);
                    if (error.responseJSON && name in error.responseJSON) {
                        self.model.errorModel.clear();
                        self.model.errorModel.set(error.responseJSON[name]);
                    }
                });
            return false;
        }
    };
    return helpers;
});