define([
    'backbone',
    'marionette',
    'search/Content/ContentView',
    'search/Navigation/NavigationView',
    'search/AppController',
    'search/AppRouter',
    'search/Content/SearchTab/Request/ItemsCollection',
    'search/Content/Common/GroupsCollection/GroupsCollection',
    'search/Content/SearchTab/FieldsPanel/SelectedGroupsList/SelectedGroupsCollection',
    'search/Navigation/User/UserModel',
    'search/Content/SearchTab/Request/RequestModel',
    'search/Content/Common/Vote/VoteStatsModel',
    'search/Content/SearchTab/States/StatesController',
    'search/helpers'
], function (Backbone, Marionette, ContentView, NavigationView, AppController, AppRouter, ItemsCollection,
             GroupsCollection, SelectedGroupsCollection, UserModel, RequestModel, VoteStatsModel, StatesController,
             helpers) {
    'use strict';
    var app = new Marionette.Application();
    window.app = app;

    app.addRegions({
        containerRegion: '#Container',
        navigationRegion: '#Navigation'
    });

    app.addInitializer(function () {
        this.helpers = helpers;
        this.user = new UserModel();
        this.user.checkLogIn();

        this.voteStats = new VoteStatsModel({}, {app: app});
    });

    app.reqres.setHandler("user", function () {
        return app.user;
    });

    app.reqres.setHandler("voteStats", function () {
        return app.voteStats;
    });

    app.addInitializer(function () {
        this.groupsCollection = new GroupsCollection();
        this.groupsCollection.fetch({success: function () {
            app.execute("renderContent");
        }});
        this.selectedGroups = new SelectedGroupsCollection();
        this.filtersGroups = new SelectedGroupsCollection();
        this.selectedItemsCollection = new Backbone.Collection();
    });

    app.reqres.setHandler("itemsCollection", function () {
        return app.itemsCollection;
    });

    app.reqres.setHandler("groupsCollection", function () {
        return app.groupsCollection;
    });

    app.reqres.setHandler("selectedGroups", function () {
        return app.selectedGroups;
    });

    app.reqres.setHandler("filtersGroups", function () {
        return app.filtersGroups;
    });

    app.reqres.setHandler("requestModel", function () {
        return app.requestModel;
    });

    app.reqres.setHandler("statesController", function () {
        return app.statesController;
    });

    app.reqres.setHandler("appController", function () {
        return app.appController;
    });

    app.reqres.setHandler("selectedItemsCollection", function () {
        return app.selectedItemsCollection;
    });

    app.addInitializer(function () {
        this.appController = new AppController({app: this});
        this.requestModel = new RequestModel({}, {
            app: this,
            selectedGroups: app.request('selectedGroups'),
            filtersGroups: app.request('filtersGroups')
        });

        this.itemsCollection = new ItemsCollection();
        this.itemsCollection.setRequest(this.requestModel);
        this.itemsCollection.setUser(this.user);

        this.statesController = new StatesController({app: this});
    });

    app.addInitializer(function () {
        this.tabs =  {
            'showSearchTab': '/',
            'showAboutTab': '/about/',
            'showScoresImport': '/scoresImport/',
            'showFieldsTab': '/fields/',
            'showTempFieldsTab': '/tempFields/',
            'showRecommendation': '/recommendation/',
            'showNeighbours': '/neighbours/',
            'showHowTo': '/howto/'
        };
        this.router = new AppRouter({app: app});
    });

    app.addInitializer( function () {
        _.extend(Backform, {
            controlLabelClassName: "control-label col-sm-1",
            controlsClassName: "col-sm-4"
        });
    });

    app.commands.setHandler("renderContent", function () {
        var navigationView = new NavigationView({app: app});
        var contentView = new ContentView({app: app});
        app.containerRegion.show(contentView);
        app.navigationRegion.show(navigationView);
        Backbone.history.start({pushState: true});
    });
    return app;
});