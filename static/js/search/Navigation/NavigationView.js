define([
    'backbone',
    'marionette',
    'text!search/Navigation/NavigationTemplate.html',
    'search/Navigation/User/UserView',
    'search/Navigation/User/AuthenticatedUserView'
], function (Backbone, Marionette, NavigationTemplate, UserView, AuthenticatedUserView) {
    'use strict';
    return Marionette.LayoutView.extend({
        template: _.template(NavigationTemplate),

        regions: {
            'userLogInRegion': '#UserLogIn',
            'userSingUpRegion': '#UserSignUp',
            'userPanelRegion': '#UserPanel'
        },

        templateHelpers: function () {
            return {
                tabs: this.app.tabs
            };
        },

        initialize: function (options) {
            this.app = options.app;
            this.appController = this.app.appController;

            this.listenTo(this.app.vent, 'tab:show', this.changeActiveTab);
            this.listenTo(this.app.vent, 'item:detail', this.changeActiveTab);
            this.currentTab = null;

            this.userLogInView = new UserView({type: 'login', app: this.app});
            this.userSignUpView = new UserView({type: 'signup', app: this.app});
            this.userPanelView = new AuthenticatedUserView({app: this.app});
        },

        onShow: function () {
            this.userPanelRegion.show(this.userPanelView);
            this.userLogInRegion.show(this.userLogInView);
            this.userSingUpRegion.show(this.userSignUpView);
        },

        ui: {
            'links': '.js-tabs li a',
            'brand': '.navbar-brand'
        },

        events: {
            'click @ui.links': 'linkClick',
            'click @ui.brand': 'linkClick'
        },

        linkClick: function (event) {
            this.appController.linkClick(event);
        },

        getMenuEntry: function (tabName) {
            return this.$el.find('.nav li a[data-tab="' + tabName + '"]');
        },

        changeActiveTab: function (tabName) {
            if (this.currentTab) {
                var currentMenuItem = this.getMenuEntry(this.currentTab);
                currentMenuItem.parent().removeClass('active');
            }
            var menuItem = this.getMenuEntry(tabName);
            menuItem.parent().addClass('active');
            this.currentTab = tabName;
        }

    });
});