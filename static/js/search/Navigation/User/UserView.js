define([
    'backbone',
    'marionette',
    'search/Navigation/User/UserModel',
    'text!search/Navigation/User/UserTemplate.html',
    'text!search/Navigation/User/AuthenticatedUserTemplate.html',
    'modelbinder'
], function (Backbone, Marionette, UserModel, UserTemplate, AuthenticatedUserTemplate, ModelBinder) {
    'use strict';
    return Marionette.ItemView.extend({
        tagName: 'ul',
        className: 'nav navbar-nav pull-right login hidden',
        template: _.template(UserTemplate),

        ui: {
            submit: '.submit-button',
            form: 'form',
            dropdown: '.dropdown',
            firstinput: 'input:first',
            input: 'input'
        },

        events: {
            'click @ui.submit': 'submit',
            'keyup @ui.input': 'keyPressHandler'
        },

        submit: function (event) {
            switch (this.type) {
            case 'login':
                this.model.logIn(this.getFormData());
                break;
            case 'signup':
                this.model.signUp(this.getFormData());
                break;
            }
        },

        keyPressHandler: function (event) {
            if (event.keyCode === 13) {
                this.submit();
            }
        },

        getFormData: function () {
            var res = {};
            var formArray = this.ui.form.serializeArray();
            _.each(formArray, function (input) {
                res[input.name] = input.value;
            });
            return res;
        },

        initialize: function (options) {
            this.type = options.type;
            this.app = options.app;

            this.AppUserModel = this.app.request('user');
            this.listenTo(this.AppUserModel, 'change:id', this.onAppModelChange);

            this.model = new UserModel();
            this.binder = new Backbone.ModelBinder();
        },

        modelEvents: {
            "sync": "onSync"
        },

        onShow: function () {
            if (this.type === 'login') {
                var bindings = {
                    error_msg: [
                        {'selector': '.alert'},
                        {'selector': '.alert', converter: this.errorClassConverter,  elAttribute: 'class'}
                    ]
                };
                this.binder.bind(this.model, this.el, bindings);

                var self = this;
                this.ui.dropdown.on('shown.bs.dropdown', function () {
                    self.ui.firstinput.focus();
                });
            }
            this.onAppModelChange();
        },

        errorClassConverter: function (direction, value) {
            if (value) {
                return 'alert alert-danger';
            } else {
                return 'alert alert-danger hidden';
            }
        },

        onSync: function () {
            if (this.model.isAuthenticated()) {
                this.AppUserModel.set(this.model.toJSON());
                this.model.clear();
                this.ui.input.val('');
            }
        },

        onAppModelChange: function () {
            if (this.AppUserModel.isAuthenticated()) {
                this.hide();
            } else {
                this.show();
            }
        },

        hide: function () {
            this.$el.addClass('hidden');
        },

        show: function () {
            this.$el.removeClass('hidden');
        },

        templateHelpers: function () {
            var forumUrl = 'http://animeadvice.me/askbot/account/signup/?login_provider=local'.format(window.location.host);
            return {
                type: this.type,
                forumUrl: forumUrl
            };
        }
    });
});