define(['jquery', 'backbone'], function ($, Backbone) {
    'use strict';
    return Backbone.Model.extend({
        urlRoot: '/user/',

        initLoginInterval: function () {
            var self = this;
            setInterval(function () {
                if (!self.isAuthenticated()) {
                    self.checkLogIn();
                }
            }, 2000);
        },

        logOut: function () {
            var self = this;
            $.get('/user/logout/', function (data) {
                self.clear();
                self.set(self.defaults);
            });
        },

        defaults: {
            error_msg: '',
            username: '',
            settings: new Backbone.Model({
                tableMode: 'select',
                tableTextLength: 300
            })
        },

        logIn: function (loginData) {
            this.fetch({data: loginData});
        },

        signUp: function (signUpData) {
            this.set(signUpData, {silent: true});
            this.save();
        },

        checkLogIn: function () {
            this.fetch({silent: true, success: function (model, response, options) {
                if (model.isAuthenticated()) {
                    model.trigger('change:id');
                } else {
                    model.set('id', null);
                }
            }});
        },

        isAuthenticated: function () {
            return this.get('id') !== null && this.get('id') !== undefined;
        }
    });
});