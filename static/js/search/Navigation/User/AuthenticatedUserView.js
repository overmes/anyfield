define([
    'backbone',
    'marionette',
    'text!search/Navigation/User/AuthenticatedUserTemplate.html',
    'modelbinder'
], function (Backbone, Marionette, AuthenticatedUserTemplate) {
    'use strict';
    return Marionette.ItemView.extend({
        tagName: 'ul',
        className: 'nav navbar-nav pull-right login hidden',
        template: _.template(AuthenticatedUserTemplate),

        ui: {
            submit: '.submit-button',
            form: 'form',
            scoresImport: '#ScoresImport',
            rec: '#Recommendation'
        },

        events: {
            'click @ui.submit': 'submit',
            'click @ui.scoresImport': 'scoresImportTab',
            'click @ui.rec': 'recTab'
        },

        scoresImportTab: function () {
            this.appController.showScoresImport();
        },

        recTab: function () {
            this.appController.showRecommendation();
        },

        submit: function () {
            this.model.logOut();
        },

        initialize: function (options) {
            this.app = options.app;
            this.appController = this.app.appController;
            this.model = this.app.request('user');
        },

        modelEvents: {
            "change:id": "onChange"
        },

        onChange: function () {
            if (this.model.isAuthenticated()) {
                this.render();
                this.show();
            } else {
                this.hide();
            }
        },

        onShow: function () {
            this.onChange();
        },

        hide: function () {
            this.$el.addClass('hidden');
        },

        show: function () {
            this.$el.removeClass('hidden');
        }
    });
});