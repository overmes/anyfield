require(['/static/js/config.js'], function () {
    "use strict";
    require(['boot'], function () {
        //http://stackoverflow.com/questions/19240302/does-jasmine-2-0-really-not-work-with-require-js
        var specs = [];
        specs.push('tests/Search/FiltersTests/FilterTest');
        specs.push('tests/Search/SelectFieldsTests/SelectFieldsTest');
        require(specs, function (specs) {
            window.onload();
        });
    });
});
