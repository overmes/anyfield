define([
    'backbone',
    'tests/Search/FiltersTests/TestGroupsCollection',
    'search/Content/SearchTab/FieldsPanel/SelectedGroupsList/SelectedGroupsCollection'
], function (Backbone, TestGroupsCollection, SelectedGroupsCollection) {
    'use strict';

    describe("Select fields test", function () {
        it("serialize & deserialize", function () {
            var startData = [{"id":"us0","parent":18,"fields":[{"parent":271,"on":true}],"on":true,"user":38,"username":"verylongusername"},{"id":"us1","parent":18,"fields":[{"parent":271,"on":true}],"on":true,"user":"34","username":"test4"},{"id":"it11","parent":11,"fields":[{"parent":240,"on":false},{"parent":241,"on":true},{"parent":242,"on":true}],"on":false},{"id":"it10","parent":10,"fields":[{"parent":239,"on":false}],"on":true}];
            var startCollection = new SelectedGroupsCollection().deserialize(startData, TestGroupsCollection);
            expect(startCollection.serialize()).toEqual(startData);
        });

        it("deserialize miss field", function () {
            var startData = [{"id":"us0","parent":18,"fields":[{"parent":271,"on":true}],"on":true,"user":38,"username":"verylongusername"},{"id":"us1","parent":18,"fields":[{"parent":271,"on":true}],"on":true,"user":"34","username":"test4"},{"id":"it11","parent":11,"fields":[{"parent":240,"on":false},{"parent":241,"on":true},{"parent":242,"on":true}],"on":false},{"id":"it10","parent":10,"fields":[{"parent":239,"on":false}],"on":true},{"id":"it16","parent":16,"fields":[{"parent":257,"on":true}],"on":true}];
            var res = [{"id":"us0","parent":18,"fields":[{"parent":271,"on":true}],"on":true,"user":38,"username":"verylongusername"},{"id":"us1","parent":18,"fields":[{"parent":271,"on":true}],"on":true,"user":"34","username":"test4"},{"id":"it11","parent":11,"fields":[{"parent":240,"on":false},{"parent":241,"on":true},{"parent":242,"on":true}],"on":false},{"id":"it10","parent":10,"fields":[{"parent":239,"on":false}],"on":true}];
            var startCollection = new SelectedGroupsCollection().deserialize(startData, TestGroupsCollection);
            expect(startCollection.serialize()).toEqual(res);
        });
    });
});