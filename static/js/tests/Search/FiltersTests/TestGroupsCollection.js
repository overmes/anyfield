/**
 * Created by overmes on 14.10.14.
 */
define([
    'search/Content/Common/GroupsCollection/GroupsCollection',
    'search/Content/Common/GroupsCollection/FieldsCollection'
], function (GroupsCollection, FieldsCollection) {
    "use strict";
    return new GroupsCollection([
        {
            description: "Item Content Types",
            edit: "pu",
            fields: new FieldsCollection([
                {
                    description: "Content Type",
                    fid: "a",
                    id: 239,
                    name: "Type",
                    range: [ ],
                    resource_uri: "/api/field/239/",
                    type: "se",
                    values: [
                        {
                            description: "",
                            id: 511,
                            name: "Anime",
                            number: 1,
                            resource_uri: "/api/selectValueModel/511/"
                        },
                        {
                            description: "",
                            id: 512,
                            name: "Manga",
                            number: 2,
                            resource_uri: "/api/selectValueModel/512/"
                        }
                    ]
                }
            ]),
            id: 10,
            name: "Content Type",
            read: "pu",
            resource_uri: "/api/fieldType/10/",
            type: "it"
        },
        {
            description: "MAL Main Item Data",
            edit: "no",
            fields: new FieldsCollection([
                {
                    description: "Id",
                    fid: "a",
                    id: 240,
                    name: "Id",
                    range: [ ],
                    resource_uri: "/api/field/240/",
                    type: "va",
                    values: [ ]
                },
                {
                    description: "Type",
                    fid: "b",
                    id: 241,
                    name: "Type",
                    range: [ ],
                    resource_uri: "/api/field/241/",
                    type: "se",
                    values: [
                        {
                            description: null,
                            id: 493,
                            name: "Special",
                            number: 7,
                            resource_uri: "/api/selectValueModel/493/"
                        },
                        {
                            description: null,
                            id: 494,
                            name: "Manhwa",
                            number: 6,
                            resource_uri: "/api/selectValueModel/494/"
                        },
                        {
                            description: null,
                            id: 495,
                            name: "Movie",
                            number: 2,
                            resource_uri: "/api/selectValueModel/495/"
                        },
                        {
                            description: null,
                            id: 496,
                            name: "Manga",
                            number: 0,
                            resource_uri: "/api/selectValueModel/496/"
                        },
                        {
                            description: null,
                            id: 497,
                            name: "One Shot",
                            number: 4,
                            resource_uri: "/api/selectValueModel/497/"
                        },
                        {
                            description: null,
                            id: 498,
                            name: "Doujin",
                            number: 11,
                            resource_uri: "/api/selectValueModel/498/"
                        },
                        {
                            description: null,
                            id: 499,
                            name: "Manhua",
                            number: 10,
                            resource_uri: "/api/selectValueModel/499/"
                        },
                        {
                            description: null,
                            id: 500,
                            name: "Novel",
                            number: 5,
                            resource_uri: "/api/selectValueModel/500/"
                        },
                        {
                            description: null,
                            id: 501,
                            name: "ONA",
                            number: 8,
                            resource_uri: "/api/selectValueModel/501/"
                        },
                        {
                            description: null,
                            id: 502,
                            name: "TV",
                            number: 1,
                            resource_uri: "/api/selectValueModel/502/"
                        },
                        {
                            description: null,
                            id: 503,
                            name: "Music",
                            number: 9,
                            resource_uri: "/api/selectValueModel/503/"
                        },
                        {
                            description: null,
                            id: 504,
                            name: "OVA",
                            number: 3,
                            resource_uri: "/api/selectValueModel/504/"
                        }
                    ]
                },
                {
                    description: "Status",
                    fid: "c",
                    id: 242,
                    name: "Status",
                    range: [ ],
                    resource_uri: "/api/field/242/",
                    type: "se",
                    values: [
                        {
                            description: null,
                            id: 505,
                            name: "Currently Airing",
                            number: 3,
                            resource_uri: "/api/selectValueModel/505/"
                        },
                        {
                            description: null,
                            id: 506,
                            name: "Finished Airing",
                            number: 2,
                            resource_uri: "/api/selectValueModel/506/"
                        },
                        {
                            description: null,
                            id: 507,
                            name: "Publishing",
                            number: 1,
                            resource_uri: "/api/selectValueModel/507/"
                        },
                        {
                            description: null,
                            id: 508,
                            name: "Finished",
                            number: 0,
                            resource_uri: "/api/selectValueModel/508/"
                        },
                        {
                            description: null,
                            id: 509,
                            name: "Not yet published",
                            number: 5,
                            resource_uri: "/api/selectValueModel/509/"
                        },
                        {
                            description: null,
                            id: 510,
                            name: "Not yet aired",
                            number: 4,
                            resource_uri: "/api/selectValueModel/510/"
                        }
                    ]
                },
                {
                    description: "Main Title",
                    fid: "d",
                    id: 243,
                    name: "Title",
                    range: [ ],
                    resource_uri: "/api/field/243/",
                    type: "te",
                    values: [ ]
                },
                {
                    description: "English Title",
                    fid: "e",
                    id: 244,
                    name: "English Title",
                    range: [ ],
                    resource_uri: "/api/field/244/",
                    type: "te",
                    values: [ ]
                },
                {
                    description: "Japanese Title",
                    fid: "f",
                    id: 245,
                    name: "Japanese Title",
                    range: [ ],
                    resource_uri: "/api/field/245/",
                    type: "te",
                    values: [ ]
                },
                {
                    description: "Image",
                    fid: "g",
                    id: 246,
                    name: "Image",
                    range: [ ],
                    resource_uri: "/api/field/246/",
                    type: "im",
                    values: [ ]
                }
            ]),
            id: 11,
            name: "MAL Main Data",
            read: "pu",
            resource_uri: "/api/fieldType/11/",
            type: "it"
        },
        {
            description: "User Score",
            edit: "us",
            fields: new FieldsCollection([
                {
                    description: "Score",
                    fid: "a",
                    id: 271,
                    name: "Score",
                    range: [ ],
                    resource_uri: "/api/field/271/",
                    type: "va",
                    values: [ ]
                },
                {
                    description: "Status",
                    fid: "b",
                    id: 272,
                    name: "Status",
                    range: [ ],
                    resource_uri: "/api/field/272/",
                    type: "se",
                    values: [
                        {
                            description: "",
                            id: 281,
                            name: "Watching",
                            number: 1,
                            resource_uri: "/api/selectValueModel/281/"
                        },
                        {
                            description: "",
                            id: 282,
                            name: "Complited",
                            number: 2,
                            resource_uri: "/api/selectValueModel/282/"
                        },
                        {
                            description: "",
                            id: 283,
                            name: "On-hold",
                            number: 3,
                            resource_uri: "/api/selectValueModel/283/"
                        },
                        {
                            description: "",
                            id: 284,
                            name: "Droped",
                            number: 4,
                            resource_uri: "/api/selectValueModel/284/"
                        },
                        {
                            description: "",
                            id: 285,
                            name: "Plan to watch",
                            number: 6,
                            resource_uri: "/api/selectValueModel/285/"
                        }
                    ]
                },
                {
                    description: "Date",
                    fid: "c",
                    id: 273,
                    name: "Date",
                    range: [ ],
                    resource_uri: "/api/field/273/",
                    type: "da",
                    values: [ ]
                }
            ]),
            id: 18,
            name: "User Score",
            read: "pu",
            resource_uri: "/api/fieldType/18/",
            type: "us"
        }
    ]);
});