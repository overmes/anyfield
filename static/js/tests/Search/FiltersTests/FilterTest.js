define([
    'backbone',
    'marionette',
    'tests/Search/FiltersTests/TestGroupsCollection',
    'search/Content/SearchTab/FieldsPanel/SelectedGroupsList/SelectedGroupsCollection',
    'search/Content/SearchTab/Request/ItemsCollection',
    'search/Content/SearchTab/Request/RequestModel'
], function (Backbone, Marionette, TestGroupsCollection, SelectedGroupsCollection, ItemsCollection, RequestModel) {
    'use strict';
    var app;
    beforeEach(function () {
        app = new Marionette.Application();
        app.addInitializer(function () {
            this.groupsCollection = TestGroupsCollection;
            this.selectedGroups = new SelectedGroupsCollection();
            this.filtersGroups = new SelectedGroupsCollection();
        });
        app.reqres.setHandler("itemsCollection", function () {
            return app.itemsCollection;
        });
        app.reqres.setHandler("requestModel", function () {
            return app.requestModel;
        });
        app.reqres.setHandler("groupsCollection", function () {
            return app.groupsCollection;
        });
        app.reqres.setHandler("selectedGroups", function () {
            return app.selectedGroups;
        });
        app.reqres.setHandler("filtersGroups", function () {
            return app.filtersGroups;
        });
        app.reqres.setHandler("user", function () {
            return app.user;
        });
        app.addInitializer(function () {
            this.user = new Backbone.Model();
            this.requestModel = new RequestModel({}, {
                app: this,
                selectedGroups: app.request('selectedGroups'),
                filtersGroups: app.request('filtersGroups')
            });
            this.itemsCollection = new ItemsCollection([], {app: this});
        });
        app.start();
    });

    describe("Include Exclude Behavior", function () {
        it("Group Include Exclude", function () {
            var request = app.request("requestModel");
            var filtersGroups = app.request('filtersGroups');
            filtersGroups.deserialize([{"parent":10,"fields":[{"id":239,"parent":239,"on":true,"or":true,"filter":[1,2]}],"on":true,"include":false}], TestGroupsCollection);
            expect(request.getFilters()).toEqual({i: {query: { $and: [
                { $and: [
                    { 10: {$not: { $elemMatch: { a: { $in: [ 1, 2 ] } } } } }
                ] }
            ] }}});
        });

        it("Group Include Exclude Select And", function () {
            var request = app.request("requestModel");
            var filtersGroups = app.request('filtersGroups');
            filtersGroups.deserialize([{"parent":10,"fields":[{"id":239,"parent":239,"on":true,"or":false,"filter":[1,2]}],"on":true,"include":false}], TestGroupsCollection);
            expect(request.getFilters()).toEqual({i: {query: { $and: [
                { $and: [
                    { 10: {$not: { $elemMatch: { a: 1 } } } },
                    { 10: {$not: { $elemMatch: { a: 2 } } } }
                ] }
            ] }}});
        });

    });

    describe("On Off Behavior", function () {
        it("Simple Select Or True", function () {
            var request = app.request("requestModel");
            var filtersGroups = app.request('filtersGroups');
            filtersGroups.deserialize([{"parent":10,"fields":[{"id":239,"parent":239,"on":true,"or":true,"filter":[1,2]}],"on":true,"include":true}], TestGroupsCollection);
            expect(request.getFilters()).toEqual({i: {query: { $and: [
                { $and: [
                    { 10: { $elemMatch: { a: { $in: [ 1, 2 ] } } } }
                ] }
            ] }}});
        });

        it("Simple Select Or False", function () {
            var request = app.request("requestModel");
            var filtersGroups = app.request('filtersGroups');
            filtersGroups.deserialize([{"parent":10,"fields":[{"id":239,"parent":239,"on":false,"or":true,"filter":[1,2]}],"on":true,"include":true}], TestGroupsCollection);
            expect(request.getFilters()).toEqual({});
        });

        it("Simple Select Group False", function () {
            var request = app.request("requestModel");
            var filtersGroups = app.request('filtersGroups');
            filtersGroups.deserialize([{"parent":10,"fields":[{"id":239,"parent":239,"on":true,"or":true,"filter":[1,2]}],"on":false,"include":true}], TestGroupsCollection);
            expect(request.getFilters()).toEqual({});
        });

    });

    describe("FilterCollection getFilters", function () {
        it("Empty", function () {
            var request = app.request("requestModel");
            var filtersGroups = app.request('filtersGroups');
            filtersGroups.deserialize([], TestGroupsCollection);
            expect(request.getFilters()).toEqual({});
        });

        it("Empty User", function () {
            var request = app.request("requestModel");
            var filtersGroups = app.request('filtersGroups');
            filtersGroups.deserialize([{"parent":18,"fields":[{"id":272,"parent":272,"or":true,"on":true,"filter":[2]}],"on":true,"include":true}] , TestGroupsCollection);
            expect(request.getFilters()).toEqual({});
        });

        it("Simple Select Or", function () {
            var request = app.request("requestModel");
            var filtersGroups = app.request('filtersGroups');
            filtersGroups.deserialize([{"parent":10,"fields":[{"id":239,"parent":239,"on":true,"or":true,"filter":[1,2]}],"on":true,"include":true}], TestGroupsCollection);
            expect(request.getFilters()).toEqual({ i: { query: { $and: [
                { $and: [
                    { 10: { $elemMatch: { a: { $in: [ 1, 2 ] } } } }
                ] }
            ] }}});
        });

        it("Simple User Select Or", function () {
            var request = app.request("requestModel");
            var filtersGroups = app.request('filtersGroups');
            filtersGroups.deserialize([{"parent":18,"fields":[{"id":272,"parent":272,"on":true,"or":true,"filter":[1,2]}],"on":true,"include":true,"user":3,"username":"admin"}], TestGroupsCollection);
            expect(request.getFilters()).toEqual({ 3: { query: { $and: [
                { $and: [
                    { 18: { $elemMatch: { b: { $in: [ 1, 2 ] } } } }
                ] }
            ] }}});
        });

        it("Simple Select And", function () {
            var request = app.request("requestModel");
            var filtersGroups = app.request('filtersGroups');
            filtersGroups.deserialize([{"parent":10,"fields":[{"id":239,"parent":239,"on":true,"or":false,"filter":[1,2]}],"on":true,"include":true}] , TestGroupsCollection);
            expect(request.getFilters()).toEqual({ i: { query: { $and: [
                { $and: [
                    { 10: { $elemMatch: { a: 1 } } },
                    { 10: { $elemMatch: { a: 2 } } }
                ] }
            ] }}});
        });

        it("Simple Text", function () {
            var request = app.request("requestModel");
            var filtersGroups = app.request('filtersGroups');
            filtersGroups.deserialize([{"parent":11,"fields":[{"id":243,"parent":243,"on":true,"or":true,"filter":"Hello"}],"on":true,"include":true}] , TestGroupsCollection);
            expect(request.getFilters()).toEqual({ i: { query: { $and: [
                { $and: [
                    { 11: { $elemMatch: { d: 'Hello' } } }
                ] }
            ] }}});
        });

        it("Simple Select Or and Text", function () {
            var request = app.request("requestModel");
            var filtersGroups = app.request('filtersGroups');
            filtersGroups.deserialize([{"parent":11,"fields":[{"id":243,"parent":243,"on":true,"or":true,"filter":"Hello"},{"id":241,"parent":241,"on":true,"or":true,"filter":[1,2]}],"on":true,"include":true}], TestGroupsCollection);
            var answer = { i: { query: { $and: [
                { $and: [
                    { 11: { $elemMatch: { b: { $in: [ 1, 2 ] }, d: 'Hello' } } }
                ] }
            ] }}};
            expect(request.getFilters()).toEqual(answer);
        });

        it("Simple Select And and Text", function () {
            var request = app.request("requestModel");
            var filtersGroups = app.request('filtersGroups');
            filtersGroups.deserialize([{"parent":11,"fields":[{"id":241,"parent":241,"on":true,"or":false,"filter":[1,2]},{"id":243,"parent":243,"on":true,"or":true,"filter":"Hello"}],"on":true,"include":true}], TestGroupsCollection);
            var answer = { i: { query: { $and: [
                { $and: [
                    { 11: { $elemMatch: { b: 1, d: 'Hello' } } },
                    { 11: { $elemMatch: { b: 2, d: 'Hello' } } }
                ] }
            ] }}};
            expect(request.getFilters()).toEqual(answer);
        });

        it("Double Select And and Text", function () {
            var request = app.request("requestModel");
            var filtersGroups = app.request('filtersGroups');
            filtersGroups.deserialize([{"parent":11,"fields":[{"id":241,"parent":241,"on":true,"or":false,"filter":[1,2]},{"id":242,"parent":242,"on":true,"or":false,"filter":[3,4]},{"id":243,"parent":243,"on":true,"or":true,"filter":"Hello"}],"on":true,"include":true}], TestGroupsCollection);

            expect(request.getFilters()).toEqual({ i: { query: { $and: [
                { $and: [
                    { 11: { $elemMatch: { b: 1, c: 3, d: 'Hello' } } },
                    { 11: { $elemMatch: { b: 2, c: 3, d: 'Hello' } } },
                    { 11: { $elemMatch: { b: 1, c: 4, d: 'Hello' } } },
                    { 11: { $elemMatch: { b: 2, c: 4, d: 'Hello' } } }
                ] }
            ] }}});
        });

        it("Select And and Select Or and Text", function () {
            var request = app.request("requestModel");
            var filtersGroups = app.request('filtersGroups');
            filtersGroups.deserialize([{"parent":11,"fields":[{"id":241,"parent":241,"on":true,"or":false,"filter":[1,2]},{"id":242,"parent":242,"on":true,"or":true,"filter":[3,4]},{"id":243,"parent":243,"on":true,"or":true,"filter":"Hello"}],"on":true,"include":true}], TestGroupsCollection);
            expect(request.getFilters()).toEqual({ i: { query: { $and: [
                { $and: [
                    { 11: { $elemMatch: { b: 1, c: { $in: [ 3, 4 ] }, d: 'Hello' } } },
                    { 11: { $elemMatch: { b: 2, c: { $in: [ 3, 4 ] }, d: 'Hello' } } }
                ] }
            ] }}});
        });
    });

    describe("Null fields", function () {
        it("Simple", function () {
            var request = app.request("requestModel");
            var filtersGroups = app.request('filtersGroups');
            filtersGroups.deserialize([{"fields":[{"id":241,"parent":241,"or":true,"on":true,"filter":[null]}],"parent":11,"on":true,"include":true}] , TestGroupsCollection);
            expect(request.getFilters()).toEqual({i: {query: { $and: [
                { $and: [
                    { '11.b': null }
                ] }
            ] }}});
        });

        it("Null select or", function () {
            var request = app.request("requestModel");
            var filtersGroups = app.request('filtersGroups');
            filtersGroups.deserialize([{"parent":11,"fields":[{"id":241,"parent":241,"or":true,"on":true,"filter":[3,null]}],"on":true,"include":true}] , TestGroupsCollection);
            expect(request.getFilters()).toEqual({i: {query: { $and: [
                { $and: [
                    {$or: [{ '11.b': null }, {'11': {$elemMatch: {b: {$in: [3, null]}}}}]}
                ] }
            ] }}});
        });

        it("Null select or and select or", function () {
            var request = app.request("requestModel");
            var filtersGroups = app.request('filtersGroups');
            filtersGroups.deserialize([{"parent":11,"fields":[{"id":241,"parent":241,"or":true,"on":true,"filter":[3,null]},{"id":242,"parent":242,"or":true,"on":true,"filter":[3]}],"on":true,"include":true}], TestGroupsCollection);
            expect(request.getFilters()).toEqual({i: {query: { $and: [
                { $and: [
                    {'11': {$elemMatch: {b: {$in: [3, null]}, c: {$in: [3]}}}}
                ] }
            ] }}});
        });

        it("Null select or and select and", function () {
            var request = app.request("requestModel");
            var filtersGroups = app.request('filtersGroups');
            filtersGroups.deserialize([{"parent":11,"fields":[{"id":241,"parent":241,"or":true,"on":true,"filter":[3,null]},{"id":242,"parent":242,"or":false,"on":true,"filter":[3,2]}],"on":true,"include":true}], TestGroupsCollection);
            expect(request.getFilters()).toEqual({i: {query: { $and: [
                { $and: [
                    {'11': {$elemMatch: {b: {$in: [3, null]}, c: 3}}},
                    {'11': {$elemMatch: {b: {$in: [3, null]}, c: 2}}}
                ] }
            ] }}});
        });

        it("Null select or and Null select or", function () {
            var request = app.request("requestModel");
            var filtersGroups = app.request('filtersGroups');
            filtersGroups.deserialize([{"parent":11,"fields":[{"id":241,"parent":241,"or":true,"on":true,"filter":[3,null]},{"id":242,"parent":242,"or":true,"on":true,"filter":[3,2,null]}],"on":true,"include":true}], TestGroupsCollection);
            expect(request.getFilters()).toEqual({i: {query: { $and: [
                { $and: [
                    {$or: [{ '11.b': null }, {'11': {$elemMatch: {b: {$in: [3, null]}}}}]},
                    {$or: [{ '11.c': null }, {'11': {$elemMatch: {c: {$in: [3, 2, null]}}}}]}
                ] }
            ] }}});
        });

        it("Null select and", function () {
            var request = app.request("requestModel");
            var filtersGroups = app.request('filtersGroups');
            filtersGroups.deserialize([{"parent":11,"fields":[{"id":241,"parent":241,"or":false,"on":true,"filter":[3,null]}],"on":true,"include":true}], TestGroupsCollection);
            expect(request.getFilters()).toEqual({i: {query: { $and: [
                { $and: [
                    {'11': {$elemMatch: {b: 3}}},
                    {'11': {$elemMatch: {b: null}}}
                ] }
            ] }}});
        });

        it("Null select and and Null select or", function () {
            var request = app.request("requestModel");
            var filtersGroups = app.request('filtersGroups');
            filtersGroups.deserialize([{"parent":11,"fields":[{"id":241,"parent":241,"or":false,"on":true,"filter":[3,null]},{"id":242,"parent":242,"or":true,"on":true,"filter":[4,null]}],"on":true,"include":true}], TestGroupsCollection);
            expect(request.getFilters()).toEqual({i: {query: { $and: [
                { $and: [
                    {'11': {$elemMatch: {b: 3, c: {$in: [4, null]}}}},
                    {'11': {$elemMatch: {b: null, c: {$in: [4, null]}}}}
                ] }
            ] }}});
        });

    });
});