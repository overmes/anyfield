from functools import wraps
from django.core.cache import cache as _cache


def cache(*args, **kwargs):

    func = None
    if len(args) == 1 and __builtins__.callable(args[0]):
        func = args[0]

    if func:
        seconds = 60  # default values

    if not func:
        seconds = kwargs.get('seconds')

    def callable(func):
        @wraps(func)
        def wrapped(*args, **kwargs):
            cache_key = [func, args, kwargs]
            result = _cache.get(cache_key)
            if result is not None:
                return result
            result = func(*args, **kwargs)
            _cache.set(cache_key, result, timeout=seconds)
            return result
        return wrapped

    return callable(func) if func else callable