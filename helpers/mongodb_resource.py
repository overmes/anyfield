import json
from pymongo import MongoClient

from django.core.exceptions import ImproperlyConfigured
from django.conf import settings

from tastypie.bundle import Bundle
from tastypie.resources import Resource


db = MongoClient(
    host=settings.MONGODB.get('HOST'),
    port=settings.MONGODB.get('PORT')
)[settings.MONGODB['NAME']]


class MongoDBResource(Resource):

    def __init__(self):
        super().__init__()
        self.db = db

    def dehydrate(self, bundle):
        bundle.data.update(bundle.obj)
        return bundle

    """
    A base resource that allows to make CRUD operations for mongodb.
    """
    def get_collection(self):
        """
        Encapsulates collection name.
        """
        try:
            return self.db[self._meta.collection]
        except AttributeError:
            raise ImproperlyConfigured("Define a collection in your resource.")

    def get_group_collection(self):
        try:
            return self.db[self._meta.group_collection]
        except AttributeError:
            raise ImproperlyConfigured("Define a collection in your resource.")

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}

        if isinstance(bundle_or_obj, Bundle):
            kwargs['pk'] = json.dumps(bundle_or_obj.obj.get('_id'))
        else:
            kwargs['pk'] = json.dumps(bundle_or_obj.get('_id'))

        return kwargs

    def get_object_list(self, request):
        kwargs = dict()
        kwargs['spec'] = json.loads(request.GET.get('filters', '{}'))
        kwargs['sort'] = json.loads(request.GET.get('sort', '{}'))
        kwargs['fields'] = json.loads(request.GET.get('fields', '[]'))
        kwargs['skip'] = int(request.GET.get('skip', 0))
        kwargs['limit'] = int(request.GET.get('query_limit', 0))
        cursor = self.get_collection().find(**kwargs)
        self.totalCount = cursor.count()
        return list(cursor)

    def obj_get_list(self, request=None, **kwargs):
        return self.get_object_list(kwargs['bundle'].request)

    def obj_get(self, request=None, **kwargs):
        return self.get_collection().find_one({'_id': kwargs.get("pk")})

    def obj_create(self, bundle, request=None, **kwargs):
        return None

    def obj_update(self, bundle, request=None, **kwargs):
        return None

    def obj_delete_list(self, request=None, **kwargs):
        pass

    def obj_delete(self, request=None, **kwargs):
        pass

    def rollback(self, bundles):
        pass