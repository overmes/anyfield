from helpers.cache import cache


@cache(seconds=30)
def get_text_fids(type_id):
    from fieldtype.models import FieldModel
    return FieldModel.objects.filter(parent=type_id, type='te').values_list('fid', flat=True)

@cache(seconds=30)
def get_li_fids(type_id):
    from fieldtype.models import FieldModel
    return FieldModel.objects.filter(parent=type_id, type='li').values_list('fid', flat=True)

@cache(seconds=30)
def get_fids(type_id):
    from fieldtype.models import FieldModel
    return FieldModel.objects.filter(parent=type_id).values_list('fid', flat=True)

@cache(seconds=30)
def is_editable(type_id):
    from fieldtype.models import FieldTypeModel
    return FieldTypeModel.objects.get(id=type_id).edit != 'no'

@cache(seconds=30)
def is_item_type(type_id):
    from fieldtype.models import FieldTypeModel
    return FieldTypeModel.objects.get(id=type_id).type == 'it'