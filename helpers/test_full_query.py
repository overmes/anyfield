import pprint

from django.test import TestCase, override_settings
from query.driver import QueryDriver

from query.query import QuerySolver
from query.query_lib import AbstractItemQuerySolver
from helpers.mal_migration.default_fields import fields_data
from search.api import ItemsCache


@override_settings(MONGODB={'NAME': 'anyfield_mal'})
class DefaultTestCase(TestCase):
    def test_full_query_1_1(self):
        query_solver = QuerySolver(fields_data, user=1, query={
            'i': {'query': {'$and': [{'$and': [{'2': {'$elemMatch': {'a': 'Code Geass'}}}]}]}, 'fields': ['2', '15']}
        }, sort=('i', (('15.a', -1), )))
        result = query_solver.solve()
        pprint.pprint(result)
        pprint.pprint(query_solver._log.timers)

    def test_full_query_1_2(self):
        query_solver = QuerySolver(fields_data, user=1, query={
            'i': {'query': {'$and': [{'$and': [{'11': {'$elemMatch': {'e': 'ツバサ'}}}]}]}, 'fields': ['11', '12']}
        }, sort=('i', (('15.a', -1), )))
        result = query_solver.solve()
        pprint.pprint(result)
        pprint.pprint(query_solver._log.timers)

    def test_full_query_1_3(self):
        query_solver = QuerySolver(fields_data, user=1, query={
            'i': {'query': {'$and': [{'$and': [{'2': {'$elemMatch': {'a': {'$not': {'$eq': 'Code Geass'}}}}}]}]}, 'fields': ['2', '15']}
        }, sort=('i', (('15.a', -1), )))
        result = query_solver.solve()
        pprint.pprint(result)
        pprint.pprint(query_solver._log.timers)

    def test_full_query_2_0(self):
        QuerySolver.MAX_LIMIT = 1000
        AbstractItemQuerySolver.MAX_LIMIT = 1000
        query_solver = QuerySolver(fields_data, user=2, query={
            'i': {'fields': ['2']},
            '475117': {'query': {'$and': [{'$and': [{'$and': [{'18.a': {'$not': {'$eq': None}}}]}]}]}, 'fields': ['18']}
        }, sort=('475117', (('18.a', 1), )), limit=1000)
        result = query_solver.solve()
        pprint.pprint(result, width=160, compact=True)
        pprint.pprint(len(result))
        pprint.pprint(query_solver._log.timers)

    def test_full_query_2_1(self):
        QuerySolver.MAX_LIMIT = 1000
        AbstractItemQuerySolver.MAX_LIMIT = 1000
        query_solver = QuerySolver(fields_data, user=475117, query={
            'i': {'fields': ['2']},
            '475117': {'query': {'$and': [{'$and': [{'18.a': {'$not': {'$eq': None}}}]}]}, 'fields': ['18']}
        }, sort=('475117', (('18.a', 1), )), limit=50)
        result = query_solver.solve()
        pprint.pprint(result, width=160, compact=True)
        pprint.pprint(len(result))
        pprint.pprint(query_solver._log.timers)

    def test_full_query_2_3(self):
        QuerySolver.MAX_LIMIT = 1000
        AbstractItemQuerySolver.MAX_LIMIT = 1000
        query_solver = QuerySolver(fields_data, user=2, query={
            'i': {'fields': ['2']},
            '475117': {'query': {'$and': [{'$and': [{'$and': [{'18.a': None}]}]}]}, 'fields': ['18']}
        }, sort=('475117', (('18.a', 1), )), limit=100)
        result = query_solver.solve()
        pprint.pprint(result, width=160, compact=True)
        pprint.pprint(len(result))
        pprint.pprint(query_solver._log.timers)

    def test_full_query_2_4(self):
        QuerySolver.MAX_LIMIT = 1000
        AbstractItemQuerySolver.MAX_LIMIT = 1000
        query_solver = QuerySolver(fields_data, user=2, query={
            'i': {'fields': ['2']},
            '475117': {'query': {'$and': [{'$and': [{'$and': [{'18': {'$exists': True}}]}]}]}, 'fields': ['18']}
        }, sort=('475117', (('18.a', 1), )), limit=1000)
        result = query_solver.solve()
        pprint.pprint(result, width=160, compact=True)
        pprint.pprint(len(result))
        pprint.pprint(query_solver._log.timers)

    def test_full_query_2_5(self):
        QuerySolver.MAX_LIMIT = 1000
        AbstractItemQuerySolver.MAX_LIMIT = 1000
        # items_set = set(i['i'] for i in QueryDriver().items_col.find({}, {'i': 1}))
        items_set = None
        query_solver = QuerySolver(fields_data, user=2, query={
            'i': {'fields': ['2']},
            '475117': {'query': {'$and': [{'$and': [{'$and': [{'18.a': None}]}]}]}, 'fields': ['18']}
        }, sort=('i', (('15.a', -1), )), limit=100, items_set=items_set)
        result = query_solver.solve()
        pprint.pprint(result, width=160, compact=True)
        pprint.pprint(len(result))
        pprint.pprint(query_solver._log.timers)

    def test_full_query_3(self):
        query_solver = QuerySolver(fields_data, user=475117, query={
            'i': {'query': {'$and': [
                {'$and': [{'15': {'$elemMatch': {'a': {'$lt': 9.4}}}}]},
                {'$and': [{'11': {'$elemMatch': {'b': 1}}}]}
            ]}, 'fields': ['11', '15']},
            '475117': {'query': {'$and': [{'$and': [{'$or': [{'18.a': None}, {'18': []}]}]}]}, 'fields': ['18']}
        }, sort=('i', (('15.a', -1), )))
        result = query_solver.solve()
        pprint.pprint(result)
        pprint.pprint(query_solver._log.timers)

    def test_full_query_3_1(self):
        query_solver = QuerySolver(fields_data, user=475117, query={
            'i': {'query': {'$and': [
                {'$and': [{'15': {'$elemMatch': {'a': {'$lt': 9.4}}}}]},
                {'$and': [{'11': {'$elemMatch': {'b': 1}}}]}
            ]}, 'fields': ['11', '15']},
            '475117': {'query': {'$and': [{'$and': [{'18.a': {'$not': {'$exists': False}}}]}]}, 'fields': ['18']}
        }, sort=('i', (('15.a', -1), )))
        result = query_solver.solve()
        pprint.pprint(result)
        pprint.pprint(query_solver._log.timers)

    def test_full_query_4(self):
        query_solver = QuerySolver(fields_data, user=475117, query={
            'i': {'query': {'$and': [
                {'$and': [{'14': {'$elemMatch': {'a': 26}}}, {'14': {'$elemMatch': {'a': 4}}}]},
                {'$and': [{'11': {'$elemMatch': {'b': 1}}}]}
            ]}, 'fields': ['11', '14']},
            '475117': {'query': {'$and': [{'$and': [{'18': {'$elemMatch': {'b': 2}}}]}]}, 'fields': ['18']}
        }, sort=('i', (('15.a', -1), )))
        result = query_solver.solve()
        pprint.pprint(result)
        pprint.pprint(query_solver._log.timers)
        print(str(query_solver))

    def test_full_query_5(self):
        query_solver = QuerySolver(fields_data, user=475117, query={
            'i': {'query': {'$and': [
                {'$and': [{'14': {'$elemMatch': {'a': {'$in': [26, 4]}}}}]},
                {'$and': [{'11': {'$elemMatch': {'b': 1}}}]}
            ]}, 'fields': ['11', '14']},
            '475117': {'query': {'$and': [{'$and': [{'18': {'$elemMatch': {'b': 2}}}]}]}, 'fields': ['18']}
        }, sort=('i', (('15.a', -1), )))
        result = query_solver.solve()
        pprint.pprint(result)
        pprint.pprint(query_solver._log.timers)
        print(str(query_solver))

    def test_full_query_6(self):
        query_solver = QuerySolver(fields_data, user=475117, query={
            'i': {'query': {'$and': [
                {'$and': [{'14.a': {'$nin': [4, 26]}}]},
                {'$and': [{'11': {'$elemMatch': {'b': 1}}}]}
            ]}, 'fields': ['11', '14']}
        }, sort=('i', (('15.a', -1), )))
        result = query_solver.solve()
        pprint.pprint(result)
        pprint.pprint(query_solver._log.timers)
        print(str(query_solver))

    def test_full_query_7(self):
        query_solver = QuerySolver(fields_data, user=475117, query={
            'i': {'query': {'$and': [
                {'$and': [{'14': {'$not': {'$elemMatch': {'$or': [{'a': {'$gte': 8}}, {'a': {'$lte': 2}}]}}}}]},
                {'$and': [{'11': {'$elemMatch': {'b': 1}}}]}
            ]}, 'fields': ['11', '14']}
        }, sort=('i', (('15.a', -1), )))
        result = query_solver.solve()
        pprint.pprint(result)
        pprint.pprint(query_solver._log.timers)
        print(str(query_solver))

    def test_full_query_8(self):
        query_solver = QuerySolver(fields_data, user=475117, query={}, sort=('79', (('20.a', -1), )))
        result = query_solver.solve()
        pprint.pprint(result)
        pprint.pprint(query_solver._log.timers)
        print(str(query_solver))