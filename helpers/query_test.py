import sys
import os
from django.db.models import Prefetch


sys.path.append('/home/overmes/PycharmProjects/anyfield')
os.environ['DJANGO_SETTINGS_MODULE'] = 'config.settings'
import django
django.setup()

from fieldtype.models import FieldTypeModel, FieldModel, SelectValueModel

for t in FieldTypeModel.objects.prefetch_related(
        Prefetch('fieldmodel_set__selectvaluemodel_set', queryset=SelectValueModel.objects.select_related('parent')),
        Prefetch('fieldmodel_set__rangevaluemodel_set', queryset=SelectValueModel.objects.select_related('parent'))).all():
    fields = t.fieldmodel_set.all()
    for f in fields:
        print(f.selectvaluemodel_set.all())
        print(f.rangevaluemodel_set.all())