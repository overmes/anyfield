fields_types = {
    'id': 'id',
    'select': 'se',
    'value': 'va',
    'text': 'te',
    'image': 'im',
    'date': 'da',
    'link': 'li'
}
fields_data = [
    {
        'id': 1,
        'name': 'Special',
        'fields': [
            {'fid': 'a', 'type': 'id', 'name': 'Id', 'description': 'Unique Id', 'values': [0, 100000, 1], 'is_temp': False}
        ],
        'description': 'Special site data',
        'type': 'it',
        'read': 'pu',
        'edit': 'no',
        'is_temp': False
    },
    {
        'id': 2,
        'name': 'Title',
        'fields': [
            {'fid': 'a', 'type': 'te', 'name': 'Title', 'description': 'Main title', 'is_temp': False}
        ],
        'description': 'Main title',
        'type': 'it',
        'read': 'pu',
        'edit': 'no',
        'is_temp': False
    },
    {
        'id': 10, 'name': 'Content Type', 'description': 'Item Content Types',
        'fields': [
            {
                'fid': 'a',
                'type': 'se',
                'values': [{'number': 1, 'name': 'Anime', 'is_temp': False},
                           {'number': 2, 'name': 'Manga', 'is_temp': False}],
                'description': 'Content Type',
                'name': 'Type',
                'is_temp': False
            }
        ],
        'type': 'it',
        'read': 'pu',
        'edit': 'no',
        'is_temp': False
    },
    {
        'id': 11, 'name': 'MAL Main Data', 'description': 'MAL Main Item Data',
        'fields': [
            {
                'fid': 'a',
                'type': 'va',
                'values': [0, 100000, 0.01],
                'description': 'Id',
                'name': 'Id',
                'is_temp': False
            },
            {
                'fid': 'b',
                'type': 'se',
                'values': [],
                'description': 'Type',
                'name': 'Type',
                'is_temp': False
            },
            {
                'fid': 'c',
                'type': 'se',
                'values': [],
                'description': 'Status',
                'name': 'Status',
                'is_temp': False
            },
            {
                'fid': 'd',
                'type': 'te',
                'description': 'English Title',
                'name': 'English Title',
                'is_temp': False
            },
            {
                'fid': 'e',
                'type': 'te',
                'description': 'Japanese Title',
                'name': 'Japanese Title',
                'is_temp': False
            },
            {
                'fid': 'f',
                'type': 'im',
                'description': 'Image',
                'name': 'Image',
                'is_temp': False
            }
        ],
        'type': 'it',
        'read': 'pu',
        'edit': 'no',
        'is_temp': False
    },
    {
        'id': 12, 'name': 'MAL Additional Data', 'description': 'MAL Additional Item Data',
        'fields': [
            {
                'fid': 'a',
                'type': 'da',
                'description': 'Aired From',
                'name': 'Aired From',
                'is_temp': False
            },
            {
                'fid': 'b',
                'type': 'da',
                'description': 'Aired To',
                'name': 'Aired To',
                'is_temp': False
            },
            {
                'fid': 'c',
                'type': 'va',
                'values': [0, 10000, 1],
                'description': 'Chapters',
                'name': 'Chapters',
                'is_temp': False
            },
            {
                'fid': 'd',
                'type': 'va',
                'values': [0, 10000, 1],
                'description': 'Volumes',
                'name': 'Volumes',
                'is_temp': False
            },
            {
                'fid': 'e',
                'type': 'te',
                'description': 'Synopsis',
                'name': 'Synopsis',
                'is_temp': False
            }
        ],
        'type': 'it',
        'read': 'pu',
        'edit': 'no',
        'is_temp': False
    },
    {
        'id': 14, 'name': 'MAL Genres', 'description': 'MAL Genres',
        'fields': [
            {
                'fid': 'a',
                'type': 'se',
                'values': [],
                'description': 'Genre',
                'name': 'Genre',
                'is_temp': False
            }
        ],
        'type': 'it',
        'read': 'pu',
        'edit': 'no',
        'is_temp': False
    },
    {
        'id': 15, 'name': 'MAL Common Stats', 'description': 'MAL Common statistic',
        'fields': [
            {
                'fid': 'a',
                'type': 'va',
                'values': [0, 1, 0.01],
                'description': 'Members Average Score',
                'name': 'Score',
                'is_temp': False
            },
            {
                'fid': 'b',
                'type': 'va',
                'values': [0, 100000, 1],
                'description': 'Favorites Count',
                'name': 'Favorites',
                'is_temp': False
            },
            {
                'fid': 'c',
                'type': 'va',
                'values': [0, 100000, 1],
                'description': 'Members Count',
                'name': 'Members',
                'is_temp': False
            },
            {
                'fid': 'd',
                'type': 'va',
                'values': [0, 100000, 1],
                'description': 'Scores Count',
                'name': 'Scores',
                'is_temp': False
            },
        ],
        'type': 'it',
        'read': 'pu',
        'edit': 'no',
        'is_temp': False
    },
    {
        'id': 16, 'name': 'MAL Type Stats ', 'description': 'MAL Users types statistic',
        'fields': [
            {
                'fid': 'a',
                'type': 'va',
                'values': [0, 10, 0.01],
                'description': 'Average',
                'name': 'Average',
                'is_temp': False
            },
            {
                'fid': 'b',
                'type': 'va',
                'values': [0, 10, 0.01],
                'description': 'Weight Average',
                'name': 'Weight',
                'is_temp': False
            },
            {
                'fid': 'c',
                'type': 'va',
                'values': [0, 1, 0.01],
                'description': 'Completed %',
                'name': 'Completed %',
                'is_temp': False
            },
            {
                'fid': 'd',
                'type': 'va',
                'values': [0, 100000, 1],
                'description': 'Completed',
                'name': 'Completed',
                'is_temp': False
            },
            {
                'fid': 'e',
                'type': 'va',
                'values': [0, 1, 0.01],
                'description': 'Drop %',
                'name': 'Drop %',
                'is_temp': False
            },
            {
                'fid': 'f',
                'type': 'va',
                'values': [0, 100000, 1],
                'description': 'Drop',
                'name': 'Drop',
                'is_temp': False
            },
            {
                'fid': 'g',
                'type': 'va',
                'values': [0, 1, 0.01],
                'description': 'Hold %',
                'name': 'Hold %',
                'is_temp': False
            },
            {
                'fid': 'h',
                'type': 'va',
                'values': [0, 100000, 1],
                'description': 'Hold',
                'name': 'Hold',
                'is_temp': False
            },
            {
                'fid': 'i',
                'type': 'va',
                'values': [0, 1, 0.01],
                'description': 'Plan %',
                'name': 'Plan %',
                'is_temp': False
            },
            {
                'fid': 'j',
                'type': 'va',
                'values': [0, 100000, 1],
                'description': 'Plan',
                'name': 'Plan',
                'is_temp': False
            },
            {
                'fid': 'k',
                'type': 'va',
                'values': [0, 1, 0.01],
                'description': 'Watch %',
                'name': 'Watch %',
                'is_temp': False
            },
            {
                'fid': 'l',
                'type': 'va',
                'values': [0, 100000, 1],
                'description': 'Watch',
                'name': 'Watch',
                'is_temp': False
            },
        ],
        'type': 'it',
        'read': 'pu',
        'edit': 'no',
        'is_temp': False
    },
    {
        'id': 17, 'name': 'Related', 'description': 'Related items',
        'fields': [
            {
                'fid': 'a',
                'type': 'se',
                'values': [],
                'description': 'Relation Type',
                'name': 'Type',
                'is_temp': False
            },
            {
                'fid': 'b',
                'type': 'li',
                'description': 'Related Item',
                'name': 'Item',
                'is_temp': False
            }
        ],
        'type': 'it',
        'read': 'pu',
        'edit': 'no',
        'is_temp': False
    },
    {
        'id': 18, 'name': 'Score', 'description': 'User Score',
        'fields': [
            {
                'fid': 'a',
                'type': 'va',
                'values': [0, 10, 0.1],
                'description': 'Score',
                'name': 'Score',
                'is_temp': False
            },
            {
                'fid': 'b',
                'type': 'se',
                'values': [{'number': 1, 'name': 'Watching'}, {'number': 2, 'name': 'Completed'}, {'number': 3, 'name': 'On hold'},
                           {'number': 4, 'name': 'Dropped'}, {'number': 5, 'name': 'Plan'}],
                'description': 'Status',
                'name': 'Status',
                'is_temp': False
            },
            {
                'fid': 'c',
                'type': 'da',
                'description': 'Date',
                'name': 'Date',
                'is_temp': False
            },

        ],
        'type': 'us',
        'read': 'pu',
        'edit': 'us',
        'is_temp': False
    }
]
