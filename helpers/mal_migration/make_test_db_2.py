import sys
import os
import ujson

import pymongo


sys.path.append('/home/user/data/PycharmProjects/anyfield')
os.environ['DJANGO_SETTINGS_MODULE'] = 'config.settings'
import django
django.setup()
from helpers.mal_migration.default_fields import fields_data
from user.models import User
from itemfield.models import ItemModel, ItemFieldModel
from fieldtype.models import FieldModel, SelectValueModel

con = pymongo.MongoClient()
source_db = con.anidb
source_items_col = source_db.anime

dest_db = con.anyfield_mal
dest_items_col = dest_db.items
dest_text_col = dest_db.text
dest_users_col = dest_db.users
dest_fields_col = dest_db.fields
dest_stats_col = dest_db.stats

fields_data_dict = {f['id']: f for f in fields_data}

select_dict = {}
def get_select_index(num, name, value):
    current_field = select_dict.setdefault((num, name), {})
    index = current_field.setdefault(value, len(current_field))
    return index

copy_fields = {
    'title': (2, 'a'),
    'status': (11, 'c'),
    'english': (11, 'd'),
    'japanese': (11, 'e'),
    'image': (11, 'f'),
    'aired_from': (12, 'a'),
    'aired_to': (12, 'b'),
    'chapters': (12, 'c'),
    'volumes': (12, 'd'),
    'synopsis': (12, 'e'),
    'members_score': (15, 'a'),
    'favorites': (15, 'b'),
    'members': (15, 'c'),
    'scores': (15, 'd'),
    'avg': (16, 'a'),
    'weight': (16, 'b'),
    'completed_percent': (16, 'c'),
    'completed_sum': (16, 'd'),
    'drop_percent': (16, 'e'),
    'drop_sum': (16, 'f'),
    'hold_percent': (16, 'g'),
    'hold_sum': (16, 'h'),
    'plan_percent': (16, 'i'),
    'plan_sum': (16, 'j'),
    'watching_percent': (16, 'k'),
    'watching_sum': (16, 'l'),
}

manual_fields = {
    'related',
    '_id',
    'genres',
    'my_id',
    'my_type'
}

id_index_dict = {}

text_fields = []

dest_items = []

user = User.objects.get(username='admin')

def field_model_cache(tid, fid, cache={}):
    cache_value = cache.get((tid, fid))
    if cache_value:
        return cache_value
    else:
        return cache.setdefault((tid, fid), FieldModel.objects.get(parent=tid, fid=fid))

ItemModel.objects.all().delete()
for source_item in source_items_col.find().limit(100):
    # i field
    item = ItemModel.objects.create()
    dest_fields = {'i': item}
    dest_items.append(dest_fields)

    # 10 & 11 field
    field_11_first = dest_fields.setdefault('11', [{}])[0]
    mal_id_type = source_item.get('_id')
    field_11_first['a'] = mal_id_type['i']
    if mal_id_type['t']:
        field_11_first['b'] = get_select_index(11, 'b', mal_id_type['t'])

    field_10_first = dest_fields.setdefault('10', [{}])[0]
    my_type = 1 if mal_id_type['t'] in ["TV", "Movie", "OVA", "Special", "ONA", "Music", ""] else 2
    field_10_first['a'] = my_type

    id_index_dict[(field_11_first['a'], my_type)] = item

    # 14 field
    field_14 = []
    source_genres = source_item.get('genres')
    for genre in source_genres:
        genre_index = get_select_index(14, 'a', genre)
        field_14.append({'a': genre_index})
    if field_14:
        dest_fields['14'] = field_14

    # 17 field
    field_17 = []
    source_related = source_item.get('related')
    for rel_type, rel_list in source_related.items():
        rel_index = get_select_index(17, 'a', rel_type)
        for rel_link in rel_list:
            rel_type_index = 1 if rel_link['t'] == 'anime' else 2
            field_17.append({'a': rel_index, 'b': (rel_link['i'], rel_type_index)})
    if field_17:
        dest_fields['17'] = field_17

    for fname, path in copy_fields.items():
        field_first = dest_fields.get(str(path[0]), [{}])[0]
        field_object = field_model_cache(path[0], path[1])
        value_type = field_object.type
        if value_type == 'se':
            value_index = get_select_index(path[0], path[1], source_item.get(fname))
            field_first[path[1]] = value_index
        else:
            value = source_item.get(fname)
            if value:
                field_first[path[1]] = value

        if field_first:
            dest_fields[str(path[0])] = [field_first]

# replace rel
for insert_item in dest_items:
    rel_field = insert_item.get('17', [])
    for rel in rel_field:
        if rel:
            replace_index = id_index_dict.get(rel['b'])
            if replace_index:
                rel['b'] = replace_index.id
            else:
                del rel['a'], rel['b']
    insert_item['17'] = [r for r in rel_field if r] or []

for path, data in select_dict.items():
    field_type = FieldModel.objects.get(parent=path[0], fid=path[1])
    SelectValueModel.objects.filter(parent=field_type).delete()
    select_values = [SelectValueModel(parent=field_type, number=v, name=n, user=user) for n, v in data.items()]
    SelectValueModel.objects.bulk_create(select_values)

ItemFieldModel.objects.all().delete()
for types_data in dest_items:
    current_item = types_data.pop('i')
    for str_type_id, fields in types_data.items():
        for field in fields:
            ItemFieldModel.objects.create(user=user, type_id=int(str_type_id), item=current_item, raw_field=field,
                                          is_temp=False)

dest_items_col.create_index([('i', pymongo.ASCENDING)])
dest_text_col.create_index([('text', 'text'), ('f', pymongo.ASCENDING), ('s', pymongo.ASCENDING)])
dest_text_col.create_index([('i', pymongo.ASCENDING), ('f', pymongo.ASCENDING), ('s', pymongo.ASCENDING)])
dest_text_col.create_index([('id', pymongo.ASCENDING)])
