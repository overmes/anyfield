import sys
import os


sys.path.append('/home/user/data/PycharmProjects/anyfield')
os.environ['DJANGO_SETTINGS_MODULE'] = 'config.settings'
import django
django.setup()

from user.models import User
from fieldtype.models import FieldTypeModel
from helpers.mal_migration.default_fields import fields_data

user = User.objects.get(username='admin')
for field_type in fields_data:
    FieldTypeModel.load_fields_from_json(field_type, user)
