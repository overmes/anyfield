import os
import pymongo
import sys
import ujson

sys.path.append('/home/user/data/PycharmProjects/anyfield')
os.environ['DJANGO_SETTINGS_MODULE'] = 'config.settings'
import django
django.setup()
from user.models import User
from fieldtype.models import FieldTypeModel
from itemfield.models import ItemFieldModel

con = pymongo.MongoClient()
source_db = con.test
source_users_col = source_db.users

dest_db = con.anyfield_mal
dest_users_col = dest_db.users
dest_items_col = dest_db.items
dest_fields_col = dest_db.fields
dest_stats_col = dest_db.stats

my_id = 475117

items_cache = {}
userModel = User.objects.get(username='admin')
scoreType = FieldTypeModel.objects.get(id=18)

for f in ItemFieldModel.objects.filter(user=userModel, type=scoreType):
    f.delete()

for user in source_users_col.find({'_id': my_id}):
    user_id = user['_id']
    user_scores = user['scores']
    for item_id, score_data in user_scores.items():
        item_id = int(item_id)
        item_type_index = 1 if item_id > 0 else 2
        abs_item_id = abs(item_id)
        query = {'11': {'$elemMatch': {'a': abs_item_id}}, '10': {'$elemMatch': {'a': item_type_index}}}
        item_index = items_cache.setdefault(item_id, (dest_items_col.find_one(query) or {'i': None}))['i']
        if item_index:
            field = {'b': score_data[1], 'pr': user_id}
            field.update({'a': score_data[0]}) if score_data[0] else None
            field.update({'c': score_data[2]}) if score_data[2] else None
            ItemFieldModel.objects.create(user=userModel, type=scoreType, item_id=item_index, raw_field=ujson.dumps(field), is_temp=False)

dest_users_col.create_index([('i', pymongo.ASCENDING)])
dest_users_col.create_index([('u', pymongo.ASCENDING)])