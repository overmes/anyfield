import json
from braces import views
from django.contrib.auth import login, authenticate, logout, get_user_model
from django.db import IntegrityError
from django.views.generic import View


class UserLogoutView(views.JSONResponseMixin, View):
    def __init__(self):
        super().__init__()

    def get(self, request):
        context = {'success': True}
        if request.user.is_authenticated():
            context['id'] = request.user.id
            context['username'] = request.user.username
            logout(self.request)
        return self.render_json_response(context)


class UserView(views.JSONResponseMixin, View):
    require_json = True

    def __init__(self):
        super().__init__()

    def get(self, request):
        context = {}
        if request.user.is_authenticated():
            context['success'] = True
            context['id'] = request.user.id
            context['username'] = request.user.username
            context['karma'] = request.user.get_user_karma()
        else:
            username = request.GET.get('username') or ''
            password = request.GET.get('password') or ''
            user = authenticate(username=username, password=password)
            if user is not None:
                context['id'] = user.id
                context['username'] = user.username
                if user.is_active:
                    login(request, user)
                    context['karma'] = user.get_user_karma()
                    context['success'] = True
                else:
                    context['success'] = False
                    context['error_msg'] = 'User account has been disabled.'
            else:
                context['success'] = False
                context['error_msg'] = 'Invalid username/password.'
        return self.render_json_response(context)

    def post(self, request):
        context = {}
        json_data = self.get_request_json(request)

        username = json_data.get('username')
        password = json_data.get('password')
        password_confirm = json_data.get('password_confirm')

        if password != password_confirm:
            context['success'] = False
            context['error_msg'] = 'Password does not match the confirm password.'
        else:
            try:
                user = get_user_model().objects.create_user(username, email=password, password=password, is_staff=False,
                                                            is_superuser=False)
                user = authenticate(username=username, password=password)
                login(self.request, user)
                context['id'] = user.id
                context['username'] = user.username
                context['success'] = True
            except IntegrityError:
                context['success'] = False
                context['error_msg'] = 'User {} already exists.'.format(username)
        return self.render_json_response(context)

    def get_request_json(self, request):
        try:
            return json.loads(request.body.decode(u'utf-8'))
        except ValueError:
            return {}