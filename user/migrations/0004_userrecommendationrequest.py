# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('user', '0003_userkarma'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserRecommendationRequest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('status', models.CharField(choices=[('er', 'Error'), ('su', 'Success'), ('lo', 'Loading')], max_length=100, default='lo')),
                ('message', models.CharField(max_length=100, blank='')),
                ('date', models.DateTimeField(null=True, auto_now=True)),
                ('private', models.BooleanField(default=False)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
