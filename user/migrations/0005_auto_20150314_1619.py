# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0004_userrecommendationrequest'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userrecommendationrequest',
            name='message',
            field=models.TextField(blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='userscorerequest',
            name='message',
            field=models.TextField(blank=True),
            preserve_default=True,
        ),
    ]
