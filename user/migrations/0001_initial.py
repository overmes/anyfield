# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='UserScoreRequest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('username', models.CharField(max_length=100)),
                ('status', models.CharField(default='lo', max_length=100, choices=[('er', 'Error'), ('su', 'Success'), ('lo', 'Loading')])),
                ('message', models.CharField(max_length=100, blank='')),
                ('scores_count', models.IntegerField(null=True)),
                ('date', models.DateTimeField(auto_now=True, null=True)),
                ('previous_delete', models.NullBooleanField(default=False)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
