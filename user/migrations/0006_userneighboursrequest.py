# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('user', '0005_auto_20150314_1619'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserNeighboursRequest',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('status', models.CharField(choices=[('er', 'Error'), ('su', 'Success'), ('lo', 'Loading')], max_length=100, default='lo')),
                ('message', models.TextField(blank=True)),
                ('date', models.DateTimeField(null=True, auto_now=True)),
                ('cross_count', models.IntegerField(default=20)),
                ('neighbours', models.TextField(default='[]')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
