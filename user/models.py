from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
import ujson
from config.tasks import load_user_scores, load_recommendation, load_neighbours
from fieldtype.models import FieldTypeModel
from query.query import QuerySolver


def add_karma(self, karma, type):
    user_karma, created = UserKarma.objects.get_or_create(user=self)
    user_karma.karma += karma
    user_karma.save()


def get_user_karma(self):
    try:
        return UserKarma.objects.get(user=self).karma
    except UserKarma.DoesNotExist:
        return 0


def get_scores(self):
    all_groups = FieldTypeModel.get_all_groups()
    params = {
        'user': self.id,
        'query': {
            str(self.id): {'fields': [str(FieldTypeModel.score_type_index())], 'query': {'18.a': {'$gt': 0}}},
            'i': {'fields': ['10', '11']}
        }
    }
    params.update({'limit': 1000})
    params.update(all_fields=all_groups)
    QuerySolver.MAX_LIMIT = 1000
    query_solver = QuerySolver(**params)
    results = query_solver.solve()
    QuerySolver.MAX_LIMIT = 100
    return results


def scores_to_mal(self, results):
    mal_scores = {}
    for group_data in results:
        mal_type = group_data['i']['10'][0]['a']
        mal_index = group_data['i']['11'][0]['a']
        mal_index = mal_index if mal_type == 1 else -mal_index
        scores_group = group_data.get(str(self.id)).get(str(FieldTypeModel.score_type_index()))
        last_score_group = scores_group[-1]
        score = round(last_score_group.get('a', 0))
        mal_scores[str(mal_index)] = [score, last_score_group.get('b', 0), last_score_group.get('c', 0)]
    return mal_scores


User.add_to_class('add_karma', add_karma)
User.add_to_class('get_user_karma', get_user_karma)
User.add_to_class('get_scores', get_scores)
User.add_to_class('scores_to_mal', scores_to_mal)


class UserKarma(models.Model):
    user = models.ForeignKey(User)
    karma = models.FloatField(default=0)


class UserApiRequest(models.Model):
    user = models.ForeignKey(User)
    status = models.CharField(max_length=100, choices=(('er', 'Error'), ('su', 'Success'), ('lo', 'Loading')),
                              default='lo')
    message = models.TextField(blank=True, default='')
    date = models.DateTimeField(null=True, auto_now=True)

    class Meta:
        abstract = True

    def process_answer(self, data):
        raise NotImplemented

    def set_error(self, message):
        self.status = 'er'
        self.message = message
        self.save()

    def get_mal_scores(self):
        return self.user.scores_to_mal(self.user.get_scores())

    def __str__(self):
        return '{} {} {} {}'.format(self.user_id, self.date, self.get_status_display(), self.message)


class UserScoreRequest(UserApiRequest):
    username = models.CharField(max_length=100)
    scores_count = models.IntegerField(default=0)
    previous_delete = models.NullBooleanField(default=False)
    private = models.BooleanField(default=False)

    @property
    def score_type_index(self):
        return FieldTypeModel.score_type_index()

    def process_answer(self, user_scores):
        message = user_scores.get('error', '') or ''
        if not user_scores.get('error'):
            if self.previous_delete:
                self.delete_user_scores()
            self.status = 'su'
            self.scores_count = user_scores.get('count')
            self.message = message
            self.save()
            self.import_fields(user_scores.get('scores'))
        else:
            self.set_error(message)

    def delete_user_scores(self):
        from itemfield.models import ItemFieldModel

        for field in ItemFieldModel.objects.filter(type_id=self.score_type_index, user=self.user):
            field.delete()

    def import_fields(self, scores):
        from itemfield.models import ItemFieldModel, ItemModel

        for mal_id, score_data in scores.items():
            mal_id = int(mal_id)
            item_type_index = 1 if mal_id > 0 else 2
            abs_item_id = abs(mal_id)
            item_index = ItemModel.find_id_by_mal_id(abs_item_id, item_type_index)
            private = self.user_id if self.private else 0
            if item_index and not ItemFieldModel.objects.filter(type_id=self.score_type_index, user=self.user,
                                                                item_id=item_index).count():
                field = {'b': score_data[1], 'pr': private}
                field.update({'a': score_data[0]}) if score_data[0] else None
                field.update({'c': score_data[2]}) if score_data[2] else None
                ItemFieldModel.objects.create(user=self.user, type_id=self.score_type_index, item_id=item_index,
                                              raw_field=field, is_temp=False)


class UserRecommendationRequest(UserApiRequest):
    private = models.BooleanField(default=False)

    @property
    def recommendation_type_index(self):
        return FieldTypeModel.recommendation_type_index()

    @property
    def score_type_index(self):
        return FieldTypeModel.score_type_index()

    def process_answer(self, recommendation):
        self.delete_user_recommendations()
        self.status = 'su'
        self.save()
        self.import_fields(recommendation)

    def import_fields(self, recommendation):
        from itemfield.models import ItemFieldModel, ItemModel
        private = self.user_id if self.private else 0

        for (mal_id, score) in recommendation.get('anime') + recommendation.get('manga'):
            item_index = ItemModel.find_id_by_mal_id(abs(mal_id), 1 if mal_id > 0 else 2)
            field = {'a': score, 'pr': private}
            if item_index:
                ItemFieldModel.objects.create(user=self.user, type_id=self.recommendation_type_index,
                                              item_id=item_index, raw_field=field, is_temp=False)

    def delete_user_recommendations(self):
        from itemfield.models import ItemFieldModel
        for field in ItemFieldModel.objects.filter(type_id=self.recommendation_type_index, user=self.user):
            field.delete()


class UserNeighboursRequest(UserApiRequest):
    cross_count = models.IntegerField(default=20)
    neighbours = models.TextField(default='[]')

    def process_answer(self, neighbours_data):
        self.status = 'su'
        self.neighbours = ujson.dumps(neighbours_data.get('result', []))
        self.message = neighbours_data.get('message')
        self.save()


@receiver(post_save, sender=UserScoreRequest)
def score_handler(sender, instance, created, **kwargs):
    if created:
        load_user_scores.delay(instance)


@receiver(post_save, sender=UserRecommendationRequest)
def rec_handler(sender, instance, created, **kwargs):
    if created:
        load_recommendation.delay(instance)


@receiver(post_save, sender=UserNeighboursRequest)
def neighbours_handler(sender, instance, created, **kwargs):
    if created:
        load_neighbours.delay(instance)