from django.contrib import admin
from user.models import UserScoreRequest, UserRecommendationRequest, UserNeighboursRequest

admin.site.register(UserScoreRequest)
admin.site.register(UserRecommendationRequest)
admin.site.register(UserNeighboursRequest)