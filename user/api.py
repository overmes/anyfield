from tastypie import fields
from tastypie.authentication import SessionAuthentication
from tastypie.cache import SimpleCache
from tastypie.resources import ModelResource
from tastypie.constants import ALL
from tastypie.validation import CleanedDataFormValidation
import ujson
from fieldtype.api import DenyAuthorization
from user.forms import UserScoreRequestForm, UserRecommendationRequestForm, UserNeighboursRequestForm
from user.models import User, UserScoreRequest, UserRecommendationRequest, UserNeighboursRequest


class UserResource(ModelResource):
    class Meta:
        fields = ['username', 'id']
        allowed_methods = ['get']
        queryset = User.objects.all()
        resource_name = 'user'
        cache = SimpleCache(timeout=120)
        filtering = {
            "username": ALL
        }


class UserScoreRequestAuthorization(DenyAuthorization):
    def read_list(self, object_list, bundle):
        return object_list

    def read_detail(self, object_list, bundle):
        return True

    def create_detail(self, object_list, bundle):
        return True


class UserApiRequestResource(ModelResource):
    user = fields.OneToOneField('user.api.UserResource', 'user', readonly=True)

    def hydrate(self, bundle):
        bundle.data['user'] = bundle.request.user.id
        return bundle

    def dehydrate_status(self, bundle):
        return bundle.obj.get_status_display()


class UserScoreRequestResource(UserApiRequestResource):
    class Meta:
        always_return_data = True
        queryset = UserScoreRequest.objects.filter()
        resource_name = 'userRequest'
        ordering = 'date'
        authentication = SessionAuthentication()
        authorization = UserScoreRequestAuthorization()
        validation = CleanedDataFormValidation(form_class=UserScoreRequestForm)
        filtering = {
            "user": ('exact',)
        }


class UserNeighboursRequestResource(UserApiRequestResource):
    def dehydrate_neighbours(self, bundle):
        return ujson.loads(bundle.obj.neighbours)

    class Meta:
        always_return_data = True
        queryset = UserNeighboursRequest.objects.filter()
        resource_name = 'userNeighbours'
        ordering = 'date'
        authentication = SessionAuthentication()
        authorization = UserScoreRequestAuthorization()
        validation = CleanedDataFormValidation(form_class=UserNeighboursRequestForm)
        filtering = {
            "user": ('exact',)
        }


class UserRecommendationRequestResource(UserApiRequestResource):
    class Meta:
        always_return_data = True
        queryset = UserRecommendationRequest.objects.filter()
        resource_name = 'userRec'
        ordering = 'date'
        authentication = SessionAuthentication()
        authorization = UserScoreRequestAuthorization()
        validation = CleanedDataFormValidation(form_class=UserRecommendationRequestForm)
        filtering = {
            "user": ('exact',)
        }