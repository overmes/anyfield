from django import forms
from user.models import UserScoreRequest, UserRecommendationRequest, UserNeighboursRequest


class UserScoreRequestForm(forms.ModelForm):
    class Meta:
        model = UserScoreRequest
        fields = ['user', 'username', 'previous_delete', 'private']
        
        
class UserRecommendationRequestForm(forms.ModelForm):
    class Meta:
        model = UserRecommendationRequest
        fields = ['user', 'private']


class UserNeighboursRequestForm(forms.ModelForm):
    class Meta:
        model = UserNeighboursRequest
        fields = ['user', 'cross_count']