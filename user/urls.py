from django.conf.urls import patterns, include, url
from user.api import UserResource, UserScoreRequestResource, UserRecommendationRequestResource, \
    UserNeighboursRequestResource
from user.views import UserView, UserLogoutView


user_resource = UserResource()
score_request_resource = UserScoreRequestResource()
recommendation_request_resource = UserRecommendationRequestResource()
neighbours_request_resource = UserNeighboursRequestResource()

api_patterns = patterns(
    '',
    url(r'^api/', include(user_resource.urls)),
    url(r'^api/', include(score_request_resource.urls)),
    url(r'^api/', include(recommendation_request_resource.urls)),
    url(r'^api/', include(neighbours_request_resource.urls)),
)

urlpatterns = patterns(
    '',
    url(r'', include(api_patterns)),
    url(r'^user/logout/$', UserLogoutView.as_view(), name='logout'),
    url(r'^user/$', UserView.as_view(), name='user'),
)