from adminsortable.models import Sortable
from django.db import models
from django.db.models import Prefetch

from fieldtype.models import FieldTypeModel, FieldModel
from itemfield.models import ItemModel
from user.models import User


def related_query(query):
    return query.select_related('user').prefetch_related(
        Prefetch('fieldtypestatemodel_set__fieldstatemodel_set', queryset=FieldStateModel.objects.filter()),
        Prefetch('fieldtypestatemodel_set__fieldstatemodel_set__parent', queryset=FieldModel.objects.filter()),
        Prefetch('fieldtypestatemodel_set__parent', queryset=FieldTypeModel.objects.filter()),
        Prefetch('fieldtypestatemodel_set__user', queryset=User.objects.filter()),
    )


class StatePrefetchManager(models.Manager):
    def get_queryset(self):
        return related_query(super().get_queryset())


class StateModel(models.Model):
    user = models.ForeignKey(User)
    date = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100, blank=True)
    is_default = models.NullBooleanField(db_index=True)
    skip = models.IntegerField()
    limit = models.IntegerField()
    sort = models.CharField(max_length=100, default='', blank=True)
    selected_items = models.ManyToManyField(ItemModel, null=True)

    objects = models.Manager()
    prefetch = StatePrefetchManager()

    def __str__(self):
        return str(self.id)


class FieldTypeStateModel(Sortable):
    class Meta(Sortable.Meta):
        pass

    parent = models.ForeignKey(FieldTypeModel)
    state = models.ForeignKey(StateModel)
    TYPES = (('f', 'field'), ('l', 'filter'))
    type = models.CharField(max_length=1, choices=TYPES)
    on = models.BooleanField(default=True)
    user = models.ForeignKey(User, null=True)
    include = models.NullBooleanField(default=True)

    def __str__(self):
        return "{0} {1}".format(self.get_type_display(), self.parent)


class FieldStateModel(Sortable):
    class Meta(Sortable.Meta):
        pass

    parent = models.ForeignKey(FieldModel)
    parent_type = models.ForeignKey(FieldTypeStateModel)
    on = models.BooleanField(default=True)
    filter = models.CharField(max_length=200, default='', blank=True)
    or_state = models.NullBooleanField(default=None)

    def __str__(self):
        return "{0} {1}".format(self.parent.parent, self.parent)
