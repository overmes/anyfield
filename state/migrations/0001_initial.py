# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('fieldtype', '__first__'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='FieldStateModel',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('order', models.PositiveIntegerField(default=1, editable=False, db_index=True)),
                ('on', models.BooleanField(default=True)),
                ('filter', models.CharField(default='', blank=True, max_length=200)),
                ('parent', models.ForeignKey(to='fieldtype.FieldModel')),
            ],
            options={
                'ordering': ['order'],
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FieldTypeStateModel',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('order', models.PositiveIntegerField(default=1, editable=False, db_index=True)),
                ('type', models.CharField(choices=[('f', 'field'), ('l', 'filter')], max_length=1)),
                ('on', models.BooleanField(default=True)),
                ('include', models.NullBooleanField(default=True)),
                ('parent', models.ForeignKey(to='fieldtype.FieldTypeModel')),
            ],
            options={
                'ordering': ['order'],
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='StateModel',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('name', models.CharField(blank=True, max_length=100)),
                ('is_default', models.NullBooleanField(db_index=True)),
                ('skip', models.IntegerField()),
                ('limit', models.IntegerField()),
                ('sort', models.CharField(default='', blank=True, max_length=100)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='fieldtypestatemodel',
            name='state',
            field=models.ForeignKey(to='state.StateModel'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='fieldtypestatemodel',
            name='user',
            field=models.ForeignKey(null=True, to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='fieldstatemodel',
            name='parent_type',
            field=models.ForeignKey(to='state.FieldTypeStateModel'),
            preserve_default=True,
        ),
    ]
