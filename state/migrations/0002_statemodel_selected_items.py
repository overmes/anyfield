# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('itemfield', '__first__'),
        ('state', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='statemodel',
            name='selected_items',
            field=models.ManyToManyField(to='itemfield.ItemModel', null=True),
            preserve_default=True,
        ),
    ]
