# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('state', '0002_statemodel_selected_items'),
    ]

    operations = [
        migrations.AddField(
            model_name='fieldstatemodel',
            name='or_state',
            field=models.NullBooleanField(default=None),
            preserve_default=True,
        ),
    ]
