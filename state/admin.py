from adminsortable.admin import SortableAdmin
from django.contrib import admin
from state.models import StateModel, FieldTypeStateModel, FieldStateModel

admin.site.register(StateModel)
admin.site.register(FieldTypeStateModel, SortableAdmin)
admin.site.register(FieldStateModel, SortableAdmin)
