from django.conf.urls import patterns, url, include
from state.api import StateResource


state_resource = StateResource()


api_patterns = patterns(
    '',
    url(r'^api/', include(state_resource.urls)),
)

urlpatterns = patterns(
    '',
    url(r'', include(api_patterns)),
)