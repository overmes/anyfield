from constance import config
from tastypie.authentication import SessionAuthentication, MultiAuthentication
from tastypie.authorization import Authorization
from tastypie.paginator import Paginator
from tastypie.resources import ModelResource, fields
import ujson
from fieldtype.api import AnyGetAuthentication, DenyAuthorization
from query.driver import QueryDriver
from state.models import FieldStateModel, FieldTypeStateModel, StateModel, related_query


class StateAuthorization(DenyAuthorization):
    def read_detail(self, object_list, bundle):
        return True

    def read_list(self, object_list, bundle):
        return object_list

    def create_detail(self, object_list, bundle):
        return True

    def delete_detail(self, object_list, bundle):
        return bundle.request.user == bundle.obj.user

    def update_detail(self, object_list, bundle):
        return bundle.request.user == bundle.obj.user


class StateResource(ModelResource):
    types = fields.ToManyField('state.api.FieldTypeStateResource', 'fieldtypestatemodel_set', full=True, null=True,
                               use_in='detail', readonly=True)
    selected_items = fields.ManyToManyField('itemfield.api.ItemResource', 'selected_items', use_in='detail',
                                            readonly=True)
    user = fields.ToOneField('user.api.UserResource', 'user', readonly=True)

    class Meta:
        queryset = StateModel.objects.all()
        ordering = 'date'
        authentication = MultiAuthentication(SessionAuthentication(), AnyGetAuthentication())
        authorization = StateAuthorization()
        always_return_data = True
        paginator_class = Paginator
        limit = 10
        filtering = {
            "user": ('exact',),
        }

    def dehydrate_selected_items(self, bundle):
        return [o.id for o in bundle.obj.selected_items.all()]

    def dehydrate_sort(self, bundle):
        if bundle.obj.sort:
            if type(bundle.obj.sort) is str:
                return ujson.loads(bundle.obj.sort.replace('\'', '"'))
        else:
            return []

    def obj_get(self, bundle, **kwargs):
        if kwargs['pk'] == 'default':
            if bundle.request.user.is_authenticated():
                default_state = StateModel.prefetch.filter(user=bundle.request.user, is_default=True).order_by('-date')
                if default_state:
                    return default_state[0]
            return self.get_default_state()
        else:
            self._meta.queryset = StateModel.prefetch.all()
            return super().obj_get(bundle, **kwargs)

    def obj_create(self, bundle, **kwargs):
        new_state = self.create_new_state(bundle)
        bundle.obj = new_state
        try:
            self.create_types_field(bundle, new_state)
            return bundle
        except BaseException:
            new_state.delete()
            return bundle

    def create_new_state(self, bundle, id=None):
        sort = ujson.dumps(bundle.data.get('sort')) if bundle.data.get('sort') else ''
        return StateModel.objects.create(user=bundle.request.user, sort=sort, name=bundle.data.get('name', ''),
                                         limit=bundle.data.get('limit'), skip=bundle.data.get('skip'),
                                         is_default=bundle.data.get('is_default'), id=id)

    def create_types_field(self, bundle, state):
        state.selected_items.add(*bundle.data['selected_items'])
        for type in bundle.data['types']:
            new_type_state = FieldTypeStateModel.objects.create(parent_id=type['parent'], state=state, on=type['on'],
                                                                type=type['type'], user_id=type.get('user'),
                                                                include=type.get('include'))
            for field in type['fields']:
                # special case for selected items type
                filter = ujson.dumps(field.get('filter')) if field.get('filter') and type['parent'] != 3 else ''
                FieldStateModel.objects.create(parent_id=field['parent'], parent_type=new_type_state,
                                               or_state=field.get('or'), on=field['on'], filter=filter)

    def obj_update(self, bundle, **kwargs):
        if bundle.data.get('full') != True:
            return super().obj_update(bundle, **kwargs)
        else:
            pk = kwargs.get('pk')
            old_object = StateModel.objects.get(pk=pk)
            old_object.delete()
            new_state = self.create_new_state(bundle, old_object.id)
            try:
                self.create_types_field(bundle, new_state)
                bundle.obj = new_state
                return bundle
            except BaseException:
                new_state.delete()
                old_object.save()
                return bundle

    def get_default_state(self):
        default_state_id = config.DEFAULT_STATE
        return StateModel.prefetch.get(id=default_state_id)


class FieldTypeStateResource(ModelResource):
    parent = fields.ToOneField('fieldtype.api.FieldTypeResource', 'parent')
    user = fields.ToOneField('user.api.UserResource', 'user', null=True)
    username = fields.CharField(readonly=True)
    fields = fields.ToManyField('state.api.FieldStateResource', 'fieldstatemodel_set', full=True, null=True)

    def dehydrate_parent(self, bundle):
        return bundle.obj.parent_id

    def dehydrate_type(self, bundle):
        return bundle.obj.get_type_display()

    def dehydrate_user(self, bundle):
        return bundle.obj.user_id if bundle.obj.user_id else None

    def dehydrate_username(self, bundle):
        return bundle.obj.user.username if bundle.obj.user_id else None

    class Meta:
        queryset = FieldTypeStateModel.objects.all()
        include_resource_uri = False
        fields = ['parent', 'type', 'on', 'include', 'user']


class FieldStateResource(ModelResource):
    parent = fields.ToOneField('fieldtype.api.FieldResource', 'parent')
    title = fields.CharField(readonly=True)

    def dehydrate_parent(self, bundle):
        return bundle.obj.parent_id

    def dehydrate_filter(self, bundle):
        return ujson.loads(bundle.obj.filter) if bundle.obj.filter else None

    def dehydrate_title(self, bundle):
        if bundle.obj.parent.type == 'li' and bundle.obj.filter and bundle.obj.filter.isdigit():
            return QueryDriver().get_title(int(bundle.obj.filter))

    def dehydrate(self, bundle):
        if bundle.obj.or_state is not None:
            bundle.data['or'] = bundle.obj.or_state
        return bundle

    class Meta:
        queryset = FieldStateModel.objects.all()
        include_resource_uri = False
        fields = ['parent', 'on', 'filter']
